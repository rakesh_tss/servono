describe('project1 App', function () {
    var page;
    beforeEach(function () {
        page = new project1Page();
    });
    it('should display message saying app works', function () {
        page.navigateTo();
        expect(page.getParagraphText()).toEqual('app works!');
    });
});
//# sourceMappingURL=app.e2e-spec.js.map