var protractor_1 = require('protractor');
var Project1Page = (function () {
    function Project1Page() {
    }
    Project1Page.prototype.navigateTo = function () {
        return protractor_1.browser.get('/');
    };
    Project1Page.prototype.getParagraphText = function () {
        return protractor_1.element(protractor_1.by.css('app-root h1')).getText();
    };
    return Project1Page;
})();
exports.Project1Page = Project1Page;
//# sourceMappingURL=app.po.js.map