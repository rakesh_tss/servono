import { Project1Page } from './app.po';

describe('servonovo App', () => {
  let page: servonovoPage;

  beforeEach(() => {
    page = new servonovoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
