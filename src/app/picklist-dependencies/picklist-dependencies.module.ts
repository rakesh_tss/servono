import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PicklistDependenciesRoutingModule } from './picklist-dependencies-routing.module';
import { PicklistDependenciesComponent } from './picklist-dependencies.component';
import {CommonTssModule} from "../shared/commonTssModules";
import { ManagePicklistDependencyComponent } from './manage-picklist-dependency/manage-picklist-dependency.component';

@NgModule({
  imports: [
    CommonModule,
    PicklistDependenciesRoutingModule,
    CommonTssModule
  ],
  declarations: [PicklistDependenciesComponent, ManagePicklistDependencyComponent]
})
export class PicklistDependenciesModule { }
