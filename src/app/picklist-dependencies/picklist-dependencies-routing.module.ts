import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PicklistDependenciesComponent} from "./picklist-dependencies.component";
import {ManagePicklistDependencyComponent} from "./manage-picklist-dependency/manage-picklist-dependency.component";
const routes: Routes = [
  {
    path : '',
    component : PicklistDependenciesComponent
  },
  {
    path: ':Id',
    component: ManagePicklistDependencyComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PicklistDependenciesRoutingModule { }
