import { Component, OnInit } from '@angular/core';
import {LazyLoadEvent,SelectItem,ConfirmationService} from "primeng/primeng";
import { FormGroup, FormControl, FormBuilder, Validators,NgForm,NgModel} from '@angular/forms';
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {BreadcrumbService} from "../_services/breadcrumb.service";
import {HttpService} from "../_services/http.service";
import {CommonService} from "../_services/common.service";

@Component({
  selector: 'app-picklist-dependencies',
  templateUrl: './picklist-dependencies.component.html',
  styleUrls: ['./picklist-dependencies.component.css']
})
export class PicklistDependenciesComponent implements OnInit {

  constructor(private breadcrumbService:BreadcrumbService, private httpService:HttpService, private modalService:NgbModal,private cs:CommonService, private confirmationService:ConfirmationService ) { }

  ngOnInit() {
    this.breadcrumbService.setBreadcrumb([{label: 'Pick List Dependencies Management'}]);
  }

  dataRows:any = [];
  totalRecords:Number = 0;
  refreshTable:boolean = true;

  loadDataLazy(event:LazyLoadEvent) {
    //this.dataRows = [{'pk_id':1,'source':'source1','target':'target1','source_id':1,'target_id':2},{'pk_id':2,'source':'source2','target':'target2','source_id':1,'target_id':2}];
    //this.totalRecords = 2;
    this.httpService.postTable('GetAllPickListDependenciesBySearch', event).subscribe(
        res=> {
          if (res) {
            this.dataRows = res.jqGridData.rows;
            this.totalRecords = res.jqGridData.records;
          }
        }
    );
  }

  tableReload() {
    this.refreshTable = false;
    setTimeout(() => this.refreshTable = true, 0);
  }

  plSubmittedForm:boolean = false;
  plMF:FormGroup;
  plModal:NgbModalRef;

  private openPlModal(content,data) {
    this.plSubmittedForm = false;
    this.getPickList();
    this.plMF = new FormGroup({
      'pk_id' : new FormControl(null),
      'source_pick_list_id':new FormControl(null,Validators.required),
      'target_pick_list_id':new FormControl(null,Validators.required)
    });
    if(data)
    {
      this.plMF.patchValue(data);
    }
    this.plModal = this.modalService.open(content);
  }

  pickList:any = [];

  getPickList() {
    //this.pickList = [{'pk_id' : 1,'name' : 'Source 1'},{'pk_id' : 2,'name' : 'Source 2'}];
    //this.parentPickList = [{label:'Select', value:null}];
    this.httpService.get('GetAllPicklists').subscribe(
        res => {
          if (res) {
            this.pickList = this.cs.getDropdownOptions(res.data,'name','pk_id');
          }
        })
  }

  plModalFormSubmit(formValue, valid) {
    this.plSubmittedForm = true;
    if (valid) {
      this.httpService.post('saveOrUpdatePicklistDependency', formValue).subscribe(
          res => {
            if (res) {
              this.plModal.dismiss();
              this.plSubmittedForm = false;
              this.tableReload();
            }
          })
    }
  }

 removeSelectedItem(row) {
   console.log(row.pk_id);
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      header: 'Delete Confirmation',
      icon: 'fa fa-trash',
      accept: () => {
        //console.log(row.pk_id);
        this.httpService.post('DeletePicklistDependency',{'ids':row.pk_id}).subscribe(res=> {
          if (res) {
            this.tableReload();
          }
        })
      }
    });
  }

}
