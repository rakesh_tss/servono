import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {BreadcrumbService} from "../../_services/breadcrumb.service";
import {HttpService} from "../../_services/http.service";

@Component({
  selector: 'app-manage-picklist-dependency',
  templateUrl: './manage-picklist-dependency.component.html',
  styleUrls: ['./manage-picklist-dependency.component.scss']
})
export class ManagePicklistDependencyComponent implements OnInit {
  constructor(private breadcrumbService:BreadcrumbService,private httpService:HttpService,private activatedRoute:ActivatedRoute) { }
  dependencyPicklistId:number;
  sourcePicklist : any = [];
  targetPicklist : any = [];
  relationPicklist : any = [];
  ngOnInit() {
    this.activatedRoute.params.subscribe((params:Params) => {
      this.dependencyPicklistId = params['Id'];
      this.breadcrumbService.setBreadcrumb([{label: 'Pick List Dependency Management',routerLink:['/Custom/Picklist Dependencies']},{label:'Set up'}]);

    });
    this.getPicklist();
  }
  getPicklist()
  {
    this.httpService.get('GetAllPicklistDependencyMappingData/'+this.dependencyPicklistId,'').subscribe(
        res => {
          if (res) {
            this.sourcePicklist = res.data.SourcePicklistValues;
            this.targetPicklist = res.data.TargetPicklistValues;
            this.relationPicklist = res.data.PicklistDependencyMappingKeys;
            this.sourcePicklist.forEach((data)=>{
              data.targetPicklist = [];
              this.targetPicklist.forEach((item)=>{
                item.is_checked = false;
                this.relationPicklist.forEach((rel)=>{
                  if(data.source_pick_list_value_id == rel.source_pick_list_value_id && item.target_pick_list_value_id == rel.target_pick_list_value_id)
                  {
                    item.is_checked = true;
                  }
                });
                data.targetPicklist.push(Object.assign({}, item));
              });
            });
          }
        });
  }
  savePicklistDependency(){
    let picklist : any = {};
    picklist.pk_id = this.dependencyPicklistId;
    picklist.sourcePicklist = this.sourcePicklist;
    this.httpService.post('UpdatePicklistDependencyMapping',picklist).subscribe(res=>{

    });
  }
}
