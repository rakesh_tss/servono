import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagePicklistDependencyComponent } from './manage-picklist-dependency.component';

describe('ManagePicklistDependencyComponent', () => {
  let component: ManagePicklistDependencyComponent;
  let fixture: ComponentFixture<ManagePicklistDependencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagePicklistDependencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePicklistDependencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
