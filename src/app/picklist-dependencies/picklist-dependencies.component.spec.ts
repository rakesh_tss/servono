import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PicklistDependenciesComponent } from './picklist-dependencies.component';

describe('PicklistDependenciesComponent', () => {
  let component: PicklistDependenciesComponent;
  let fixture: ComponentFixture<PicklistDependenciesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PicklistDependenciesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PicklistDependenciesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
