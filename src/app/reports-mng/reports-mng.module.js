var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var reports_mng_routing_module_1 = require('./reports-mng-routing.module');
var reports_mng_component_1 = require('./reports-mng.component');
var report_create_component_1 = require('./report-create/report-create.component');
var primeng_1 = require("primeng/primeng");
var commonTssModules_1 = require("../shared/commonTssModules");
var primeng_2 = require("primeng/primeng");
var report_details_component_1 = require('./report-details/report-details.component');
var select_columns_component_1 = require('./select-columns/select-columns.component');
var select_charts_component_1 = require('./select-charts/select-charts.component');
var filters_component_1 = require('./filters/filters.component');
var ReportsMngModule = (function () {
    function ReportsMngModule() {
    }
    ReportsMngModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                reports_mng_routing_module_1.ReportsMngRoutingModule,
                primeng_1.PanelModule,
                commonTssModules_1.CommonTssModule,
                primeng_2.StepsModule
            ],
            declarations: [reports_mng_component_1.ReportsMngComponent, report_create_component_1.ReportCreateComponent, report_details_component_1.ReportDetailsComponent, select_columns_component_1.SelectColumnsComponent, select_charts_component_1.SelectChartsComponent, filters_component_1.FiltersComponent]
        })
    ], ReportsMngModule);
    return ReportsMngModule;
})();
exports.ReportsMngModule = ReportsMngModule;
//# sourceMappingURL=reports-mng.module.js.map