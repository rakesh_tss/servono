import { Component, OnInit } from '@angular/core';
import {CommonService} from "../../_services/common.service";
import {HttpService} from "../../_services/http.service";
import {SelectItem} from "primeng/primeng";
import { FormGroup, FormControl, FormBuilder, Validators,NgForm,NgModel} from '@angular/forms';

@Component({
  selector: 'app-report-details',
  templateUrl: './report-details.component.html',
  styleUrls: ['./report-details.component.css']
})
export class ReportDetailsComponent implements OnInit {

  constructor(private commonService:CommonService,private httpService:HttpService,private _fb:FormBuilder) { }

  moduleList:SelectItem[] = [];
  reportForm:FormGroup;

  ngOnInit() {
      this.reportForm = this._fb.group({
          'name': new FormControl('', [Validators.required])
      });
      console.log(this.reportForm);

      this.getModuleList();
  }

  private getModuleList() {
    this.httpService.get('GetModules').subscribe(
        res => {
          if (res) {
            this.moduleList = this.commonService.getDropdownOptions(res.data,'module_label','pk_id',true);
          }
        });
  }
    submittedForm:boolean = false;
    reportFormSave(formValue,valid){
        this.submittedForm = true;
        if(valid){
            this.httpService.post('report/create',formValue).subscribe(
                res => {
                    this.submittedForm = false;
                    if (res) {

                    }
                });
        }

  }

}
