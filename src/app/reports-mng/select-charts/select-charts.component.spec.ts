import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectChartsComponent } from './select-charts.component';

describe('SelectChartsComponent', () => {
  let component: SelectChartsComponent;
  let fixture: ComponentFixture<SelectChartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectChartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
