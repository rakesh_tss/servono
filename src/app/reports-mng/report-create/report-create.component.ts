import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {BreadcrumbService} from "../../_services/breadcrumb.service";
@Component({
  selector: 'app-report-create',
  templateUrl: './report-create.component.html',
  styleUrls: ['./report-create.component.css']
})
export class ReportCreateComponent implements OnInit {

  reportId:string = null;
  stepId:number = 0;
  stepItems:any;
  activeInd:number = 0;
  stepReadonly:boolean = false;
  constructor(private breadcrumbService:BreadcrumbService,private router:Router,private activatedRoute:ActivatedRoute) { }
  ngOnInit() {
    this.activatedRoute.params.subscribe((params:Params) => {
      if (params['reportId'] && params['reportId'] != 0) {
        this.reportId = params['reportId'];

      }
      this.stepId = params['stepId'];
      //setBreadcrumb
      if (!this.reportId) {
        this.breadcrumbService.setBreadcrumb([{
          label: 'Report Management',
          routerLink: ['/Custom/Reports']
        }, {label: 'Create'}]);
      } else {
        this.breadcrumbService.setBreadcrumb([{
          label: 'Report Management',
          routerLink: ['/Custom/Reports']
        }, {label: this.reportId + ' Update'}]);
      }

      if (this.reportId) {
        this.stepReadonly = false;
        this.activeInd = Number(this.stepId);
      }
      let currentUrl = 'Custom/Reports/Create/' + this.reportId;
      this.stepItems = [
        {
          label: 'Report Details',
          command: (event:any) => {
            //this.activeInd = 0;
            if (this.stepId != 0)
              this.router.navigate([currentUrl + '/0']);
          }

        },
        {
          label: 'Select Columns',
          command: (event:any) => {
            //this.activeInd = 1;
            if (this.stepId != 1)
              this.router.navigate([currentUrl + '/1']);
          }

        },
        {
          label: 'Select Charts',
          command: (event:any) => {
            //this.activeInd = 2;
            if (this.stepId != 2)
              this.router.navigate([currentUrl + '/2']);
          }

        },
        {
          label: 'Filters',
          command: (event:any) => {
            //this.activeInd = 3;
            if (this.stepId != 3)
              this.router.navigate([currentUrl + '/3']);
          }
        }

      ];
      if(this.stepId !=0 && this.reportId==null){
        //this.router.navigate(['Custom/Reports/Create/0/0']);
      }
    });
  }
}
