var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var ReportCreateComponent = (function () {
    function ReportCreateComponent(breadcrumbService, router, activatedRoute) {
        this.breadcrumbService = breadcrumbService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.reportId = null;
        this.stepId = 0;
        this.activeInd = 0;
        this.stepReadonly = false;
    }
    ReportCreateComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            if (params['reportId'] && params['reportId'] != 0) {
                _this.reportId = params['reportId'];
            }
            _this.stepId = params['stepId'];
            //setBreadcrumb
            if (!_this.reportId) {
                _this.breadcrumbService.setBreadcrumb([{
                        label: 'Report Management',
                        routerLink: ['/Custom/Reports']
                    }, { label: 'Create' }]);
            }
            else {
                _this.breadcrumbService.setBreadcrumb([{
                        label: 'Report Management',
                        routerLink: ['/Custom/Reports']
                    }, { label: _this.reportId + ' Update' }]);
            }
            if (_this.reportId) {
                _this.stepReadonly = false;
                _this.activeInd = Number(_this.stepId);
            }
            var currentUrl = 'Custom/Reports/Create/' + _this.reportId;
            _this.stepItems = [
                {
                    label: 'Report Details',
                    command: function (event) {
                        //this.activeInd = 0;
                        if (_this.stepId != 0)
                            _this.router.navigate([currentUrl + '/0']);
                    }
                },
                {
                    label: 'Select Columns',
                    command: function (event) {
                        //this.activeInd = 1;
                        if (_this.stepId != 1)
                            _this.router.navigate([currentUrl + '/1']);
                    }
                },
                {
                    label: 'Select Charts',
                    command: function (event) {
                        //this.activeInd = 2;
                        if (_this.stepId != 2)
                            _this.router.navigate([currentUrl + '/2']);
                    }
                },
                {
                    label: 'Filters',
                    command: function (event) {
                        //this.activeInd = 3;
                        if (_this.stepId != 3)
                            _this.router.navigate([currentUrl + '/3']);
                    }
                }
            ];
            if (_this.stepId != 0 && _this.reportId == null) {
            }
        });
    };
    ReportCreateComponent = __decorate([
        core_1.Component({
            selector: 'app-report-create',
            templateUrl: './report-create.component.html',
            styleUrls: ['./report-create.component.css']
        })
    ], ReportCreateComponent);
    return ReportCreateComponent;
})();
exports.ReportCreateComponent = ReportCreateComponent;
//# sourceMappingURL=report-create.component.js.map