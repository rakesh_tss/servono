var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var reports_mng_component_1 = require("./reports-mng.component");
var report_create_component_1 = require("./report-create/report-create.component");
var report_details_component_1 = require("./report-details/report-details.component");
var select_columns_component_1 = require("./select-columns/select-columns.component");
var select_charts_component_1 = require("./select-charts/select-charts.component");
var filters_component_1 = require("./filters/filters.component");
var routes = [
    {
        path: '',
        component: reports_mng_component_1.ReportsMngComponent,
        data: {
            title: 'Report Management'
        }
    },
    {
        path: ':Create/:reportId',
        component: report_create_component_1.ReportCreateComponent,
        children: [
            {
                path: '0',
                component: report_details_component_1.ReportDetailsComponent
            },
            {
                path: '1',
                component: select_columns_component_1.SelectColumnsComponent
            },
            {
                path: '2',
                component: select_charts_component_1.SelectChartsComponent
            },
            {
                path: '3',
                component: filters_component_1.FiltersComponent
            }
        ]
    }
];
var ReportsMngRoutingModule = (function () {
    function ReportsMngRoutingModule() {
    }
    ReportsMngRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], ReportsMngRoutingModule);
    return ReportsMngRoutingModule;
})();
exports.ReportsMngRoutingModule = ReportsMngRoutingModule;
//# sourceMappingURL=reports-mng-routing.module.js.map