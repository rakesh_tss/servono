import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, FormBuilder, Validators,NgForm,NgModel} from '@angular/forms';

import {HttpService} from "../../_services/http.service";
@Component({
    selector: 'app-select-columns',
    templateUrl: './select-columns.component.html',
    styleUrls: ['./select-columns.component.scss']
})
export class SelectColumnsComponent implements OnInit {

    data: any;
    data1: any;
    data2: any;

    constructor(private modalService:NgbModal,
                private httpService:HttpService) {
        this.data = {
            datasets: [
                {
                    data: [80, 320],
                    backgroundColor: [
                        "#8C8C8C",
                        "#DDDDDD"

                    ],
                    hoverBackgroundColor: [
                        "#8C8C8C",
                        "#DDDDDD"
                    ]
                }]
        };

        this.data1 = {
            labels: ['NEW ENGLAND', 'MID EAST', 'GREAT LAKES', 'PLAINS', 'SOUTHEAST', 'SOUTHWEST', 'ROCKYMOUNTAIN'],
            datasets: [
                {
                    label: 'GSP in 2000',
                    backgroundColor: '#3b6b9c',
                    borderColor: '#1E88E5',
                    data: [10, 59, 80, 81, 56, 55, 40]
                },
                {
                    label: 'GSP in 2001',
                    backgroundColor: '#984340',
                    borderColor: '#7CB342',
                    data: [28, 48, 40, 19, 86, 27, 90]
                },
                {
                    label: 'GSP in 2002',
                    backgroundColor: '#688e3b',
                    borderColor: '#7CB342',
                    data: [30, 29, 76, 65, 86, 67, 45]
                },
                {
                    label: 'GSP in 2003',
                    backgroundColor: '#66558b',
                    borderColor: '#7CB342',
                    data: [19, 45, 14, 23, 45, 25, 65]
                },
                {
                    label: 'GSP in 2004',
                    backgroundColor: '#368ea2',
                    borderColor: '#7CB342',
                    data: [78, 56, 38, 78, 15, 49, 25]
                }
            ]
        },
        this.data2 = {
            labels: ['NEW ENGLAND', 'MID EAST', 'GREAT LAKES', 'PLAINS', 'SOUTHEAST', 'SOUTHWEST', 'ROCKYMOUNTAIN'],
            datasets: [
                {
                    label: 'GSP in 2000',
                    backgroundColor: '#3b6b9c',
                    borderColor: '#1E88E5',
                    data: [10, 59, 80, 81, 56, 55, 40]
                },
                {
                    label: 'GSP in 2001',
                    backgroundColor: '#984340',
                    borderColor: '#7CB342',
                    data: [28, 48, 40, 19, 86, 27, 90]
                },
                {
                    label: 'GSP in 2002',
                    backgroundColor: '#688e3b',
                    borderColor: '#7CB342',
                    data: [30, 29, 76, 65, 86, 67, 45]
                },
                {
                    label: 'GSP in 2003',
                    backgroundColor: '#66558b',
                    borderColor: '#7CB342',
                    data: [19, 45, 14, 23, 45, 25, 65]
                },
                {
                    label: 'GSP in 2004',
                    backgroundColor: '#368ea2',
                    borderColor: '#7CB342',
                    data: [78, 56, 38, 78, 15, 49, 25]
                }
            ]
        }

    }

    colFields:any = [{display_label:'Column1'},
        {display_label:'Column2'},
        {display_label:'Column3'},
        {display_label:'Column4'},
        {display_label:'Column5'},
        {display_label:'Column6'},
        {display_label:'Column7'},
        {display_label:'Column8'},
        {display_label:'Column9'}];
    selectedFields:any=[];
    ngOnInit() {
    }
    itemFG:FormGroup;
    itemModal:NgbModalRef;
    myModal:NgbModalRef;
    itemSubmittedForm:boolean = false;
    openItemModal(content)
    {
        this.itemSubmittedForm = false;
        /*this.itemFG = new FormGroup({
            'app_module_id': new FormControl(this.moduleData.pk_id),
            'prefix': new FormControl(null,Validators.required),
            'seq': new FormControl(null,Validators.required),
        });*/
        this.itemModal = this.modalService.open(content,{size: 'lg', windowClass: 'full-width'});
    }
    itemModalFormSubmit(formValue, valid) {
        this.itemSubmittedForm = true;
        if (valid) {
            this.httpService.post('SaveModuleNumbering', formValue).subscribe(
                res => {
                    if (res) {
                        this.itemModal.dismiss();
                        this.itemSubmittedForm = false;
                        //this.tableReload();
                    }
                })
        }
    }

    /*add widget*/
    openWidgetModal(content)
    {
        this.itemSubmittedForm = false;
        this.itemFG = new FormGroup({
            'pk_id': new FormControl(null),
            'name': new FormControl(null, Validators.required),
            'widget_type': new FormControl('Top'),
            'html_template': new FormControl(null),
            'js_function_name': new FormControl(null),
            'js_code': new FormControl(null),
            'query_code': new FormControl(null),
            'call_required': new FormControl(false),
            'seq_order': new FormControl(null, Validators.required)
        });
        /*this.itemFG = new FormGroup({
         'app_module_id': new FormControl(this.moduleData.pk_id),
         'prefix': new FormControl(null,Validators.required),
         'seq': new FormControl(null,Validators.required),
         });*/
        this.myModal = this.modalService.open(content,{size: 'lg'});
    }
    myModalFormSubmit(formValue, valid) {
        this.itemSubmittedForm = true;
        if (valid) {
            this.httpService.post('SaveModuleNumbering', formValue).subscribe(
                res => {
                    if (res) {
                        this.myModal.dismiss();
                        this.itemSubmittedForm = false;
                        //this.tableReload();
                    }
                })
        }
    }

}
