import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsMngRoutingModule } from './reports-mng-routing.module';
import { ReportsMngComponent } from './reports-mng.component';
import { ReportCreateComponent } from './report-create/report-create.component';
import {PanelModule} from "primeng/primeng";
import {CommonTssModule} from "../shared/commonTssModules";
import {StepsModule} from "primeng/primeng";
import { ReportDetailsComponent } from './report-details/report-details.component';
import { SelectColumnsComponent } from './select-columns/select-columns.component';
import { SelectChartsComponent } from './select-charts/select-charts.component';
import { FiltersComponent } from './filters/filters.component';
import {DndModule} from "ng2-dnd/index";
import {ChartModule} from 'primeng/primeng';
import {NgResizableModule} from "ngresizable/ngresizable.module";

@NgModule({
  imports: [
    CommonModule,
    ReportsMngRoutingModule,
    PanelModule,
    CommonTssModule,
    StepsModule,
    DndModule.forRoot(),
    ChartModule,
    NgResizableModule
  ],
  declarations: [ReportsMngComponent,
    ReportCreateComponent,
    ReportDetailsComponent,
    SelectColumnsComponent,
    SelectChartsComponent,
    FiltersComponent]
})
export class ReportsMngModule { }
