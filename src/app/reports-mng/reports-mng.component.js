var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var ReportsMngComponent = (function () {
    function ReportsMngComponent(breadcrumbService, router) {
        this.breadcrumbService = breadcrumbService;
        this.router = router;
    }
    ReportsMngComponent.prototype.ngOnInit = function () {
        this.breadcrumbService.setBreadcrumb([{ label: 'Report Management' }]);
    };
    ReportsMngComponent.prototype.redirect = function (pagename) {
        this.router.navigate([pagename]);
    };
    ReportsMngComponent = __decorate([
        core_1.Component({
            selector: 'app-reports-mng',
            templateUrl: './reports-mng.component.html',
            styleUrls: ['./reports-mng.component.css']
        })
    ], ReportsMngComponent);
    return ReportsMngComponent;
})();
exports.ReportsMngComponent = ReportsMngComponent;
//# sourceMappingURL=reports-mng.component.js.map