import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReportsMngComponent} from "./reports-mng.component";
import {ReportCreateComponent} from "./report-create/report-create.component";
import {ReportDetailsComponent} from "./report-details/report-details.component";
import {SelectColumnsComponent} from "./select-columns/select-columns.component";
import {SelectChartsComponent} from "./select-charts/select-charts.component";
import {FiltersComponent} from "./filters/filters.component";

const routes: Routes = [
  {
    path: '',
    component:ReportsMngComponent,
    data: {
      title: 'Report Management'
    }
  },
  {
    path: ':Create/:reportId',
    component: ReportCreateComponent,
    children:[
      {
        path: '0',
        component: ReportDetailsComponent
      },
      {
        path: '1',
        component: SelectColumnsComponent
      },
      {
        path: '2',
        component: SelectChartsComponent
      },
      {
        path: '3',
        component: FiltersComponent
      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsMngRoutingModule { }
