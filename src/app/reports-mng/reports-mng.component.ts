import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {BreadcrumbService} from "../_services/breadcrumb.service";
import {LazyLoadEvent} from "primeng/primeng";
import {HttpService} from "../_services/http.service";
@Component({
    selector: 'app-reports-mng',
    templateUrl: './reports-mng.component.html',
    styleUrls: ['./reports-mng.component.css']
})
export class ReportsMngComponent implements OnInit {

    constructor(private breadcrumbService:BreadcrumbService, private router:Router,private httpService:HttpService) {
    }

    ngOnInit() {
        this.breadcrumbService.setBreadcrumb([{label: 'Report Management'}]);
    }

    redirect(pagename:string) {
        this.router.navigate([pagename]);
    }

    refreshTable:boolean = true;

    tableReload() {
        this.refreshTable = false;
        setTimeout(() => this.refreshTable = true, 0);
    }

    dataRows:any = [];
    totalRecords:Number = 0;

    loadDataLazy(event:LazyLoadEvent) {
        this.httpService.postTable('reports/list', event).subscribe(
            res=> {
                if (res) {
                    this.dataRows = res.jqGridData.rows;
                    this.totalRecords = res.jqGridData.records;
                }
            }
        );
    }
}
