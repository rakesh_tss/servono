import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsMngComponent } from './reports-mng.component';

describe('ReportsMngComponent', () => {
  let component: ReportsMngComponent;
  let fixture: ComponentFixture<ReportsMngComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsMngComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsMngComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
