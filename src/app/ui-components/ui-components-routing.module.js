var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var ui_components_component_1 = require("./ui-components.component");
var curd_component_1 = require("./curd/curd.component");
var forms_component_1 = require("./forms/forms.component");
var primeng_form_component_1 = require("./primeng-form/primeng-form.component");
var input_component_1 = require("./input/input.component");
var routes = [{
        path: '',
        data: {
            title: 'UI Components'
        },
        children: [
            {
                path: '',
                component: ui_components_component_1.UiComponentsComponent,
                data: {
                    title: 'List'
                }
            },
            {
                path: 'curd',
                component: curd_component_1.CurdComponent,
                data: {
                    title: 'CURD'
                }
            },
            {
                path: 'forms',
                component: forms_component_1.FormsComponent,
                data: {
                    title: 'Forms'
                }
            },
            {
                path: 'primeng-form',
                component: primeng_form_component_1.PrimengFormComponent,
                data: {
                    title: 'Primeng Form'
                }
            },
            {
                path: 'input',
                component: input_component_1.InputComponent,
                data: {
                    title: 'Input'
                }
            }
        ]
    }];
var UiComponentsRoutingModule = (function () {
    function UiComponentsRoutingModule() {
    }
    UiComponentsRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule],
            providers: []
        })
    ], UiComponentsRoutingModule);
    return UiComponentsRoutingModule;
})();
exports.UiComponentsRoutingModule = UiComponentsRoutingModule;
//# sourceMappingURL=ui-components-routing.module.js.map