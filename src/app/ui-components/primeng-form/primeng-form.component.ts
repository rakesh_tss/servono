import { Component, OnInit } from '@angular/core';
import {HttpService} from "../../_services/http.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-primeng-form',
  templateUrl: './primeng-form.component.html',
  styleUrls: ['./primeng-form.component.scss']
})
export class PrimengFormComponent implements OnInit {

  constructor(private httpService:HttpService,private router:Router) { }

  ngOnInit() {
  }
  fObj:any = {};//form object
  loading = false;
  onSubmit() {
    this.loading = true;
    this.httpService.post('myurl',this.fObj).subscribe(result=>{
      var result =result;
      if (result) {
        this.router.navigate(['/']);
      }

    },err=>{
    });
  }


}
