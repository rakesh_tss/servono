var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var PrimengFormComponent = (function () {
    function PrimengFormComponent(httpService, router) {
        this.httpService = httpService;
        this.router = router;
        this.fObj = {}; //form object
        this.loading = false;
    }
    PrimengFormComponent.prototype.ngOnInit = function () {
    };
    PrimengFormComponent.prototype.onSubmit = function () {
        var _this = this;
        this.loading = true;
        this.httpService.post('myurl', this.fObj).subscribe(function (result) {
            var result = result;
            if (result) {
                _this.router.navigate(['/']);
            }
        }, function (err) {
        });
    };
    PrimengFormComponent = __decorate([
        core_1.Component({
            selector: 'app-primeng-form',
            templateUrl: './primeng-form.component.html',
            styleUrls: ['./primeng-form.component.scss']
        })
    ], PrimengFormComponent);
    return PrimengFormComponent;
})();
exports.PrimengFormComponent = PrimengFormComponent;
//# sourceMappingURL=primeng-form.component.js.map