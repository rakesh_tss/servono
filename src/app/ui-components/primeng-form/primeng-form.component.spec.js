/* tslint:disable:no-unused-variable */
var testing_1 = require('@angular/core/testing');
var primeng_form_component_1 = require('./primeng-form.component');
describe('PrimengFormComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [primeng_form_component_1.PrimengFormComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(primeng_form_component_1.PrimengFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=primeng-form.component.spec.js.map