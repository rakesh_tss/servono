import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }  from '@angular/forms';
import { HttpModule } from '@angular/http';
import { UiComponentsRoutingModule } from './ui-components-routing.module';
import {UiComponentsComponent} from "./ui-components.component";
import {CurdComponent} from "./curd/curd.component";
import {DataListModule,
    DataTableModule,
    MultiSelectModule,
    DialogModule,
    ButtonModule,
    CalendarModule,
    DropdownModule,
    GrowlModule,
    TabViewModule,
    OrderListModule,
    PanelModule,
    ConfirmDialogModule} from 'primeng/primeng';
import {FormsComponent} from "./forms/forms.component";
import {PrimengFormComponent} from "./primeng-form/primeng-form.component";
import {InputComponent} from "./input/input.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    UiComponentsRoutingModule,
      DataTableModule,
      MultiSelectModule,
      DialogModule,
      ButtonModule,
      CalendarModule,
      DropdownModule,
      GrowlModule,
      TabViewModule,
      OrderListModule,
      PanelModule,
    ConfirmDialogModule

  ],
  declarations: [
      UiComponentsComponent,
      CurdComponent,
      FormsComponent,
      PrimengFormComponent,
      InputComponent
  ]
})
export class UiComponentsModule {
  
}
