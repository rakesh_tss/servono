import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UiComponentsComponent} from "./ui-components.component";
import {CurdComponent} from "./curd/curd.component";
import {FormsComponent} from "./forms/forms.component";
import {PrimengFormComponent} from "./primeng-form/primeng-form.component";
import {InputComponent} from "./input/input.component";

const routes: Routes = [{
  path: '',
  data: {
    title: 'UI Components'
  },
  children: [
    {
      path: '',
      component:UiComponentsComponent,
      data: {
        title: 'List'
      }
    },
    {
      path: 'curd',
      component: CurdComponent,
      data: {
        title: 'CURD'
      }
    },
    {
      path: 'forms',
      component: FormsComponent,
      data: {
        title: 'Forms'
      }
    },
    {
      path: 'primeng-form',
      component: PrimengFormComponent,
      data: {
        title: 'Primeng Form'
      }
    },
    {
      path: 'input',
      component: InputComponent,
      data: {
        title: 'Input'
      }
    }

  ]

}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class UiComponentsRoutingModule { }
