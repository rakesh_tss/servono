var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var ui_components_routing_module_1 = require('./ui-components-routing.module');
var ui_components_component_1 = require("./ui-components.component");
var curd_component_1 = require("./curd/curd.component");
var primeng_1 = require('primeng/primeng');
var forms_component_1 = require("./forms/forms.component");
var primeng_form_component_1 = require("./primeng-form/primeng-form.component");
var input_component_1 = require("./input/input.component");
var UiComponentsModule = (function () {
    function UiComponentsModule() {
    }
    UiComponentsModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                http_1.HttpModule,
                ui_components_routing_module_1.UiComponentsRoutingModule,
                primeng_1.DataTableModule,
                primeng_1.MultiSelectModule,
                primeng_1.DialogModule,
                primeng_1.ButtonModule,
                primeng_1.CalendarModule,
                primeng_1.DropdownModule,
                primeng_1.GrowlModule,
                primeng_1.TabViewModule,
                primeng_1.OrderListModule,
                primeng_1.PanelModule,
                primeng_1.ConfirmDialogModule
            ],
            declarations: [
                ui_components_component_1.UiComponentsComponent,
                curd_component_1.CurdComponent,
                forms_component_1.FormsComponent,
                primeng_form_component_1.PrimengFormComponent,
                input_component_1.InputComponent
            ]
        })
    ], UiComponentsModule);
    return UiComponentsModule;
})();
exports.UiComponentsModule = UiComponentsModule;
//# sourceMappingURL=ui-components.module.js.map