import { Component, OnInit } from '@angular/core';
import {CurdService} from "../../_services/curd.service";
import {ConfirmationService,LazyLoadEvent} from "primeng/primeng";

@Component({
  selector: 'app-curd',
  templateUrl: './curd.component.html',
  styleUrls: ['./curd.component.scss'],
  providers:[CurdService,ConfirmationService]
})
export class CurdComponent implements OnInit {

  constructor(private curdService:CurdService,private confirmationService: ConfirmationService) { }

  ngOnInit() {
  }
  dataRows:any[];
  totalRecords: number;
  loadData(event: LazyLoadEvent) {
    this.curdService.get(event).subscribe(
        res=>{
          this.dataRows = res.rows;
          this.totalRecords = res.records;
        }
    );
  }
  onCreate():void {

  }
  onUpdate(row):void {

  }

  onView(row):void{

  }

  onDelete(row):void {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        //Actual logic to perform a confirmation
      }
    });
  }

}
