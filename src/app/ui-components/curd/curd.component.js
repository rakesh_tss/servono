var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var curd_service_1 = require("../../_services/curd.service");
var primeng_1 = require("primeng/primeng");
var CurdComponent = (function () {
    function CurdComponent(curdService, confirmationService) {
        this.curdService = curdService;
        this.confirmationService = confirmationService;
    }
    CurdComponent.prototype.ngOnInit = function () {
    };
    CurdComponent.prototype.loadData = function (event) {
        var _this = this;
        this.curdService.get(event).subscribe(function (res) {
            _this.dataRows = res.rows;
            _this.totalRecords = res.records;
        });
    };
    CurdComponent.prototype.onCreate = function () {
    };
    CurdComponent.prototype.onUpdate = function (row) {
    };
    CurdComponent.prototype.onView = function (row) {
    };
    CurdComponent.prototype.onDelete = function (row) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: function () {
                //Actual logic to perform a confirmation
            }
        });
    };
    CurdComponent = __decorate([
        core_1.Component({
            selector: 'app-curd',
            templateUrl: './curd.component.html',
            styleUrls: ['./curd.component.scss'],
            providers: [curd_service_1.CurdService, primeng_1.ConfirmationService]
        })
    ], CurdComponent);
    return CurdComponent;
})();
exports.CurdComponent = CurdComponent;
//# sourceMappingURL=curd.component.js.map