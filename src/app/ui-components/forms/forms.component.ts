import { Component, OnInit } from '@angular/core';
import {SelectItem} from "primeng/primeng";
import {Message} from "primeng/primeng";
import {FormBuilder,Validators} from '@angular/forms'
import { Router } from '@angular/router';
import {HttpService} from "../../_services/http.service";

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {

  constructor(private httpService:HttpService,private router:Router) {}
  ngOnInit() {
  }
  fObj:any = {};//form object
  loading = false;
  onSubmit() {
    this.loading = true;
    this.httpService.post('myurl',this.fObj).subscribe(result=>{
      var result =result;
      if (result) {
        this.router.navigate(['/']);
      }

    },err=>{
    });


  }
}
