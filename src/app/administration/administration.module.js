var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var administration_routing_module_1 = require('./administration-routing.module');
var administration_component_1 = require("./administration.component");
var users_component_1 = require("./users/users.component");
var login_history_component_1 = require("./login-history/login-history.component");
var primeng_1 = require('primeng/primeng');
var AdministrationModule = (function () {
    function AdministrationModule() {
    }
    AdministrationModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                administration_routing_module_1.AdministrationRoutingModule,
                primeng_1.DataTableModule
            ],
            declarations: [
                administration_component_1.AdministrationComponent,
                users_component_1.UsersComponent,
                login_history_component_1.LoginHistoryComponent
            ]
        })
    ], AdministrationModule);
    return AdministrationModule;
})();
exports.AdministrationModule = AdministrationModule;
//# sourceMappingURL=administration.module.js.map