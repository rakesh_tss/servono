import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdministrationComponent} from "./administration.component";
import {UsersComponent} from "./users/users.component";
import {LoginHistoryComponent} from "./login-history/login-history.component";

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Administration'
    },
    children: [
      {
        path: '',
        redirectTo: 'Users',
      },
      {
        path: 'Users',
        component:UsersComponent,
        data: {
          title: 'Users'
        }
      },
      {
        path: 'Login Histories',
        component:LoginHistoryComponent,
        data: {
          title: 'Login Histories'
        }
      }
    ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class AdministrationRoutingModule { }
