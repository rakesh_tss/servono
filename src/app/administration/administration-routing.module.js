var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var users_component_1 = require("./users/users.component");
var login_history_component_1 = require("./login-history/login-history.component");
var routes = [
    {
        path: '',
        data: {
            title: 'Administration'
        },
        children: [
            {
                path: '',
                redirectTo: 'Users'
            },
            {
                path: 'Users',
                component: users_component_1.UsersComponent,
                data: {
                    title: 'Users'
                }
            },
            {
                path: 'Login Histories',
                component: login_history_component_1.LoginHistoryComponent,
                data: {
                    title: 'Login Histories'
                }
            }
        ]
    }];
var AdministrationRoutingModule = (function () {
    function AdministrationRoutingModule() {
    }
    AdministrationRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule],
            providers: []
        })
    ], AdministrationRoutingModule);
    return AdministrationRoutingModule;
})();
exports.AdministrationRoutingModule = AdministrationRoutingModule;
//# sourceMappingURL=administration-routing.module.js.map