import { Component, OnInit } from '@angular/core';
import {LazyLoadEvent} from "primeng/primeng";
import {HttpService} from "../../_services/http.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  constructor(private httpService:HttpService) { }
  cars:any []
  ngOnInit() {
  }
  dataRows:any[]=[];
  headerShow:boolean;
  totalRecords: number;
  loadData(event: LazyLoadEvent) {
    this.httpService.getTable('getDataListView/Default/Settings/Menu',event).subscribe(
        res=>{
          this.dataRows = res.jqGridData.rows;
          this.totalRecords = res.jqGridData.records;
        }
    );
  }

}
