var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var UsersComponent = (function () {
    function UsersComponent(httpService) {
        this.httpService = httpService;
        this.dataRows = [];
    }
    UsersComponent.prototype.ngOnInit = function () {
    };
    UsersComponent.prototype.loadData = function (event) {
        var _this = this;
        this.httpService.getTable('getDataListView/Default/Settings/Menu', event).subscribe(function (res) {
            _this.dataRows = res.jqGridData.rows;
            _this.totalRecords = res.jqGridData.records;
        });
    };
    UsersComponent = __decorate([
        core_1.Component({
            selector: 'app-users',
            templateUrl: './users.component.html',
            styleUrls: ['./users.component.scss']
        })
    ], UsersComponent);
    return UsersComponent;
})();
exports.UsersComponent = UsersComponent;
//# sourceMappingURL=users.component.js.map