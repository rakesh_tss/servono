import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministrationRoutingModule } from './administration-routing.module';
import {AdministrationComponent} from "./administration.component";
import {UsersComponent} from "./users/users.component";
import {LoginHistoryComponent} from "./login-history/login-history.component";
import {DataTableModule,SharedModule} from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    AdministrationRoutingModule,
    DataTableModule
  ],
  declarations: [
      AdministrationComponent,
      UsersComponent,
      LoginHistoryComponent
  ]
})
export class AdministrationModule { }
