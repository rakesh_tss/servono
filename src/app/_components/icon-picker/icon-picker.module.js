var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var icon_picker_component_1 = require('./icon-picker.component');
var commonTssModules_1 = require("../../shared/commonTssModules");
var IconPickerModule = (function () {
    function IconPickerModule() {
    }
    IconPickerModule = __decorate([
        core_1.NgModule({
            imports: [
                commonTssModules_1.CommonTssModule
            ],
            declarations: [icon_picker_component_1.IconPickerComponent],
            exports: [
                icon_picker_component_1.IconPickerComponent
            ]
        })
    ], IconPickerModule);
    return IconPickerModule;
})();
exports.IconPickerModule = IconPickerModule;
//# sourceMappingURL=icon-picker.module.js.map