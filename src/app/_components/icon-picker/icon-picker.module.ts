import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconPickerComponent } from './icon-picker.component';
import {CommonTssModule} from "../../shared/commonTssModules";

@NgModule({
  imports: [
    CommonTssModule
  ],
  declarations: [IconPickerComponent],
  exports:[
    IconPickerComponent
  ]
})
export class IconPickerModule { }
