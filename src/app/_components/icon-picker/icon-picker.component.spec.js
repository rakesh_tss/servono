var testing_1 = require('@angular/core/testing');
var icon_picker_component_1 = require('./icon-picker.component');
describe('IconPickerComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [icon_picker_component_1.IconPickerComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(icon_picker_component_1.IconPickerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=icon-picker.component.spec.js.map