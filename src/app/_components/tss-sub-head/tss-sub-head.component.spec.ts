import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TssSubHeadComponent } from './tss-sub-head.component';

describe('TssSubHeadComponent', () => {
  let component: TssSubHeadComponent;
  let fixture: ComponentFixture<TssSubHeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TssSubHeadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TssSubHeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
