import { Component, OnInit,Renderer } from '@angular/core';

@Component({
  selector: 'tss-sub-head',
  templateUrl: './tss-sub-head.component.html',
  styleUrls: ['./tss-sub-head.component.css']
})
export class TssSubHeadComponent implements OnInit {

  constructor(private _renderer:Renderer) { }

  ngOnInit() {
  }
  showMenuContent:boolean = true;
  showMenu(content){
    if(content.classList.contains('m-sub-show')){
      this._renderer.setElementClass(content,'m-sub-show',false);
    }else{
      this._renderer.setElementClass(content,'m-sub-show',true);
    }


  }
}
