import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TssSubHeadComponent } from './tss-sub-head.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TssSubHeadComponent],
  exports:[
    TssSubHeadComponent
  ]
})
export class TssSubHeadModule { }
