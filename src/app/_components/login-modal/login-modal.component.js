var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var LoginModalComponent = (function () {
    function LoginModalComponent(activeModal, router) {
        this.activeModal = activeModal;
        this.router = router;
        this.reLoginObj = {};
    }
    LoginModalComponent.prototype.ngOnInit = function () {
    };
    LoginModalComponent.prototype.submitLogin = function () {
        var _this = this;
        this.httpService.post('processLogin', this.reLoginObj).subscribe(function (res) {
            if (res) {
                _this.httpService.reLoginModal.close();
                var result = {};
                result = res;
                //result.Data.MenuObject = this.menuObject;
                localStorage.setItem('currentUser', JSON.stringify(result.Data));
            }
        });
    };
    LoginModalComponent.prototype.logOut = function () {
        this.httpService.reLoginModal.close();
        this.router.navigate(['/login']);
    };
    __decorate([
        core_1.Input()
    ], LoginModalComponent.prototype, "reLoginObj");
    __decorate([
        core_1.Input()
    ], LoginModalComponent.prototype, "httpService");
    LoginModalComponent = __decorate([
        core_1.Component({
            selector: 'app-login-modal',
            templateUrl: './login-modal.component.html',
            styleUrls: ['./login-modal.component.css']
        })
    ], LoginModalComponent);
    return LoginModalComponent;
})();
exports.LoginModalComponent = LoginModalComponent;
//# sourceMappingURL=login-modal.component.js.map