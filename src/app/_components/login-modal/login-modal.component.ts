import { Component, OnInit,Input } from '@angular/core';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {LocalStorageService} from "../../_services/local-storage.service";
@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.css']
})
export class LoginModalComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal,
              private router:Router, private localStorage:LocalStorageService) { }
  @Input() reLoginObj:any={};
  @Input() httpService;
  ngOnInit() {
  }
  submitLogin(){
    this.httpService.post('processLogin',this.reLoginObj).subscribe(res=>{
      if(res){
        this.httpService.reLoginModal.close();
        let result:any = {};
        result= res;
        //result.Data.MenuObject = this.menuObject;
        this.localStorage.setItem('currentUser', JSON.stringify(result.Data));
      }
    })
  }
  logOut(){
    this.httpService.reLoginModal.close();
    this.router.navigate(['/login']);
  }

}
