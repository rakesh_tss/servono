import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DataTableModule} from "primeng/primeng";
import {TssDatatableComponent} from "./tss-datatable.component";

@NgModule({
  imports: [
    CommonModule,
    DataTableModule
  ],
  declarations: [
      TssDatatableComponent
  ],
  exports:[
      TssDatatableComponent
  ]
})
export class TssDatatableModule { }
