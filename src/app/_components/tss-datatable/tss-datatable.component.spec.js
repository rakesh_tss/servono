var testing_1 = require('@angular/core/testing');
var tss_datatable_component_1 = require('./tss-datatable.component');
describe('TssDatatableComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [tss_datatable_component_1.TssDatatableComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(tss_datatable_component_1.TssDatatableComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=tss-datatable.component.spec.js.map