import { Component, OnInit } from '@angular/core';
import {HttpService} from "../../_services/http.service";
import {CommonService} from "../../_services/common.service";
import { Router } from '@angular/router';
import {LazyLoadEvent} from "primeng/primeng";

@Component({
    selector: 'tss-dataTable',
    templateUrl: './tss-datatable.component.html',
    styleUrls: ['./tss-datatable.component.css']
})
export class TssDatatableComponent implements OnInit {

    constructor(private httpService:HttpService,
                private commonService:CommonService, private router:Router) {

    }


    ngOnInit():void {


    }

    displayDialog:boolean;
    dataRows:any[];
    dataRow:any = {};
    totalRecords:number;
    menuFormObj:any = {};

    loadDataLazy(event:LazyLoadEvent) {
        this.httpService.postTable('GetGridList/profileaccess', event).subscribe(
            res=> {
                this.dataRows = res.jqGridData.rows;
                this.totalRecords = res.jqGridData.records;
            }
        );
    }

}
