var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var TssDatatableComponent = (function () {
    function TssDatatableComponent(httpService, commonService, router) {
        this.httpService = httpService;
        this.commonService = commonService;
        this.router = router;
        this.dataRow = {};
        this.menuFormObj = {};
    }
    TssDatatableComponent.prototype.ngOnInit = function () {
    };
    TssDatatableComponent.prototype.loadDataLazy = function (event) {
        var _this = this;
        this.httpService.postTable('GetGridList/profileaccess', event).subscribe(function (res) {
            _this.dataRows = res.jqGridData.rows;
            _this.totalRecords = res.jqGridData.records;
        });
    };
    TssDatatableComponent = __decorate([
        core_1.Component({
            selector: 'tss-dataTable',
            templateUrl: './tss-datatable.component.html',
            styleUrls: ['./tss-datatable.component.css']
        })
    ], TssDatatableComponent);
    return TssDatatableComponent;
})();
exports.TssDatatableComponent = TssDatatableComponent;
//# sourceMappingURL=tss-datatable.component.js.map