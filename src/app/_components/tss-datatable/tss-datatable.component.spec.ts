import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TssDatatableComponent } from './tss-datatable.component';

describe('TssDatatableComponent', () => {
  let component: TssDatatableComponent;
  let fixture: ComponentFixture<TssDatatableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TssDatatableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TssDatatableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
