import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TssGeoComponent } from './tss-geo.component';

describe('TssGeoComponent', () => {
  let component: TssGeoComponent;
  let fixture: ComponentFixture<TssGeoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TssGeoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TssGeoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
