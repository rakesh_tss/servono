import { Component, OnInit,NgZone,ViewChild,ElementRef,forwardRef} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor,FormControl } from "@angular/forms";
import {MapsAPILoader } from '@agm/core';
import {} from '@types/googlemaps';

declare var google;
export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => TssGeoComponent),
  multi: true
};

@Component({
  selector: 'tss-geo',
  templateUrl: './tss-geo.component.html',
  styleUrls: ['./tss-geo.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class TssGeoComponent implements OnInit {
  constructor(private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone) {

  }
  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;

  @ViewChild("search")
  public searchElementRef: ElementRef;

  private _value: any;
  get value(): any { return this._value; };
  set value(v: any) {
    if (v !== this._value) {
      this._value = v;
      this.onChange(v);
    }
  }
  writeValue(value: any) {
    this._value = value;
    this.onChange(value);
  }
  onChange = (_) => {};
  onTouched = () => {};
  registerOnChange(fn: (_: any) => void): void { this.onChange = fn; }
  registerOnTouched(fn: () => void): void { this.onTouched = fn; }

  ngOnInit() {
    this.zoom = 4;
    this.latitude = 39.8282;
    this.longitude = -98.5795;
    this.setCurrentPosition();
    /*this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });*/
  }
  setCurrentPosition() {
    console.log('navigator',navigator);
    if ("geolocation" in navigator) {

      navigator.geolocation.watchPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
        this.value = {lat:this.latitude,lng:this.longitude};
      },(errorObj)=>{
        alert(errorObj.code + ": " + errorObj.message);
      },{enableHighAccuracy: true, maximumAge: 10000});
    }
  }
  markerDragEnd(m, event){
    this.latitude = event.coords.lat;
    this.longitude = event.coords.lng;
    this.value=event.coords;
  }



}
