import { NgModule } from '@angular/core';
import {  FormsModule } from "@angular/forms";
import { CommonModule } from '@angular/common';
import { TssGeoComponent } from './tss-geo.component';
import { AgmCoreModule } from '@agm/core';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBenAHn-MHHsaw78324M_i6PkMcGParewQ'
    })
  ],

  declarations: [TssGeoComponent],
  exports:[
    TssGeoComponent
  ]
})
export class TssGeoModule {

}
