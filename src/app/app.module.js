var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var common_1 = require('@angular/common');
var platform_browser_1 = require('@angular/platform-browser');
var common_2 = require('@angular/common');
var http_1 = require('@angular/http');
var angular2_toaster_1 = require('angular2-toaster');
var animations_1 = require('@angular/platform-browser/animations');
var app_component_1 = require('./app.component');
var nav_dropdown_directive_1 = require('./shared/nav-dropdown.directive');
var sidebar_directive_1 = require('./shared/sidebar.directive');
var aside_directive_1 = require('./shared/aside.directive');
var breadcrumb_component_1 = require('./shared/breadcrumb.component');
var smart_resize_directive_1 = require('./shared/smart-resize.directive');
// Routing Module
var app_routing_1 = require('./app.routing');
//Layouts
var full_layout_component_1 = require('./layouts/full-layout.component');
var simple_layout_component_1 = require("./layouts/simple-layout.component");
var auth_guard_1 = require("./_services/auth.guard");
var authentication_service_1 = require("./_services/authentication.service");
var http_service_1 = require("./_services/http.service");
var layout_service_1 = require("./_services/layout.service");
var primeng_1 = require("primeng/primeng");
var breadcrumb_service_1 = require("./_services/breadcrumb.service");
var _404_component_1 = require("./pages/404.component");
var permission_service_1 = require("./_services/permission.service");
var app_config_1 = require("./app.config");
var common_3 = require('@angular/common');
var error_handler_service_1 = require("./_services/error-handler.service");
var ng_bootstrap_1 = require('@ng-bootstrap/ng-bootstrap');
var login_modal_component_1 = require('./_components/login-modal/login-modal.component');
function initialConfigLoad(config) {
    return function () { return config.load(); };
}
exports.initialConfigLoad = initialConfigLoad;
;
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                common_1.CommonModule,
                app_routing_1.AppRoutingModule,
                http_1.HttpModule,
                angular2_toaster_1.ToasterModule,
                primeng_1.MenuModule,
                primeng_1.OverlayPanelModule,
                primeng_1.BreadcrumbModule,
                animations_1.BrowserAnimationsModule,
                primeng_1.DialogModule,
                primeng_1.PasswordModule,
                primeng_1.InputTextModule,
                primeng_1.ButtonModule,
                ng_bootstrap_1.NgbModule.forRoot(),
            ],
            declarations: [
                app_component_1.AppComponent,
                full_layout_component_1.FullLayoutComponent,
                simple_layout_component_1.SimpleLayoutComponent,
                nav_dropdown_directive_1.NAV_DROPDOWN_DIRECTIVES,
                breadcrumb_component_1.BreadcrumbsComponent,
                sidebar_directive_1.SIDEBAR_TOGGLE_DIRECTIVES,
                aside_directive_1.AsideToggleDirective,
                smart_resize_directive_1.SmartResizeDirective,
                _404_component_1.p404Component,
                login_modal_component_1.LoginModalComponent
            ],
            entryComponents: [
                login_modal_component_1.LoginModalComponent
            ],
            providers: [
                {
                    provide: core_1.ErrorHandler,
                    useClass: error_handler_service_1.GlobalErrorHandler
                },
                {
                    provide: common_3.APP_BASE_HREF, useValue: '/'
                },
                {
                    provide: common_2.LocationStrategy,
                    useClass: common_2.HashLocationStrategy
                },
                app_config_1.AppConfig,
                { provide: core_1.APP_INITIALIZER, useFactory: initialConfigLoad, deps: [app_config_1.AppConfig], multi: true },
                auth_guard_1.AuthGuard, authentication_service_1.AuthenticationService, http_service_1.HttpService, layout_service_1.LayoutService, breadcrumb_service_1.BreadcrumbService, permission_service_1.PermissionService],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
})();
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map