import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from "./_services/auth.guard";
//Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import {SimpleLayoutComponent} from "./layouts/simple-layout.component";
import {p404Component} from "./pages/404.component";
import {DocumentsComponent} from "./documents/documents.component";

export const routes:Routes = [

    {
        path: '',
        component: FullLayoutComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Home'
        },
        children: [
            {
              path:'documents',
                loadChildren: 'app/documents/documents.module#DocumentsModule'
            },
            {
                path:'404',
                component:p404Component
            },
            {
                path:'403',
                component:p404Component
            },
            {
                path: 'Custom/Dashboards',
                loadChildren: 'app/dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'Modules',
                loadChildren: 'app/menu-module/menu-module.module#MenuModuleModule'
            },
            {
                path: 'uicomponents',
                loadChildren: 'app/ui-components/ui-components.module#UiComponentsModule'
            },
            {
                path: 'Settings',
                loadChildren: 'app/settings/settings.module#SettingsModule'
            },
            {
                path: 'Administration',
                loadChildren: 'app/administration/administration.module#AdministrationModule'
            },
            {
                path: 'tss',
                loadChildren: 'app/tss/tss.module#TssModule'
            },
            {
                path: 'Custom/Picklists',
                loadChildren: 'app/plicklists-mng/plicklists-mng.module#PlicklistsMngModule'
            },
            {
                path: 'Custom/Modules',
                loadChildren: 'app/module-mng/module-mng.module#ModuleMngModule'
            },
            {
                path: 'Custom/Menu Groups',
                loadChildren: 'app/menu-group/menu-group.module#MenuGroupModule'
            },
            {
                path: 'Custom/Menus',
                loadChildren: 'app/menu-mng/menu-mng.module#MenuMngModule'
            },
            {
                path: 'Custom/Profiles',
                loadChildren: 'app/profile-mng/profile-mng.module#ProfileMngModule'
            },
            {
                path: 'Custom/Roles',
                loadChildren: 'app/roles-mng/roles-mng.module#RolesMngModule'
            },
            {
                path: 'Custom/Sharing Rules',
                loadChildren: 'app/sharing-rules/sharing-rules.module#SharingRulesModule'
            },
            {
                path: 'Custom/User Groups',
                loadChildren: 'app/user-groups/user-groups.module#UserGroupsModule'
            },
            {
                path: 'Custom/Login Histories',
                loadChildren: 'app/login-history-mng/login-history-mng.module#LoginHistoryMngModule'
            },
            {
                path: 'Custom/Access Logs',
                loadChildren: 'app/access-log/access-log.module#AccessLogModule'
            },
            {
                path: 'Custom/Calendars',
                loadChildren: 'app/calender/calender.module#CalenderModule'
            },
            {
                path: 'Custom/Picklist Dependencies',
                loadChildren: 'app/picklist-dependencies/picklist-dependencies.module#PicklistDependenciesModule'
            },
            {
                path:'Custom/Reports',
                loadChildren:'app/reports-mng/reports-mng.module#ReportsMngModule'
            },
            {
              path:'Custom/Reports/Create',
              loadChildren:'app/reports-mng/reports-mng.module#ReportsMngModule'
            }

        ]
    },
    {
        path: '',
        component: SimpleLayoutComponent,
        data: {
            title: 'Pages'
        },
        children: [
            {
                path: '',
                loadChildren: 'app/pages/pages.module#PagesModule',
            }
        ]
    },
    {path: '**',redirectTo: '404'}

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}
