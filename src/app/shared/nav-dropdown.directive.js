var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var NavDropdownDirective = (function () {
    function NavDropdownDirective() {
        this._open = false;
    }
    /**
    * Checks if the dropdown menu is open or not.
    */
    NavDropdownDirective.prototype.isOpen = function () { return this._open; };
    /**
    * Opens the dropdown menu.
    */
    NavDropdownDirective.prototype.open = function () {
        this._open = true;
    };
    /**
    * Closes the dropdown menu .
    */
    NavDropdownDirective.prototype.close = function () {
        this._open = false;
    };
    /**
    * Toggles the dropdown menu.
    */
    NavDropdownDirective.prototype.toggle = function () {
        if (this.isOpen()) {
            this.close();
        }
        else {
            this.open();
        }
    };
    NavDropdownDirective = __decorate([
        core_1.Directive({
            selector: '.nav-dropdown',
            host: {
                '[class.open]': '_open'
            }
        })
    ], NavDropdownDirective);
    return NavDropdownDirective;
})();
exports.NavDropdownDirective = NavDropdownDirective;
/**
* Allows the dropdown to be toggled via click.
*/
var NavDropdownToggleDirective = (function () {
    function NavDropdownToggleDirective(dropdown, el) {
        this.dropdown = dropdown;
        this.el = el;
    }
    NavDropdownToggleDirective.prototype.toggleOpen = function ($event) {
        var data = document.querySelector('body ul li.nav-dropdown.open');
        if (data) {
        }
        // console.log('data',data);
        $event.preventDefault();
        //this.el.nativeElement.parentElement.classList.remove('open');
        if (this.el.nativeElement.parentElement.classList.contains('open')) {
            this.el.nativeElement.parentElement.classList.remove('open');
        }
        else {
            if (data) {
                data.classList.remove('open');
            }
            this.el.nativeElement.parentElement.classList.add('open');
        }
        //this.dropdown.toggle();
    };
    __decorate([
        core_1.HostListener('click', ['$event'])
    ], NavDropdownToggleDirective.prototype, "toggleOpen");
    NavDropdownToggleDirective = __decorate([
        core_1.Directive({
            selector: '.nav-dropdown-toggle'
        })
    ], NavDropdownToggleDirective);
    return NavDropdownToggleDirective;
})();
exports.NavDropdownToggleDirective = NavDropdownToggleDirective;
exports.NAV_DROPDOWN_DIRECTIVES = [NavDropdownDirective, NavDropdownToggleDirective];
// export const NGB_DROPDOWN_DIRECTIVES = [NgbDropdownToggle, NgbDropdown];
//# sourceMappingURL=nav-dropdown.directive.js.map