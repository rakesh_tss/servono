var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
/**
* Smart body resize
*/
var SmartResizeDirective = (function () {
    function SmartResizeDirective() {
        var _this = this;
        this.body = document.body;
        this.html = document.documentElement;
        if (this.hasClass(document.querySelector('body'), 'fixed-nav')) {
        }
        else {
            setTimeout(function () {
                _this.height = Math.max(_this.body.scrollHeight, _this.body.offsetHeight, _this.html.clientHeight, _this.html.scrollHeight, _this.html.offsetHeight);
                document.getElementsByTagName('body')[0].style.minHeight = _this.height + "px";
            }, 100);
        }
    }
    //Check if element has class
    SmartResizeDirective.prototype.hasClass = function (target, elementClassName) {
        return new RegExp('(\\s|^)' + elementClassName + '(\\s|$)').test(target.className);
    };
    SmartResizeDirective.prototype.onResize = function (event) {
        if (this.hasClass(document.querySelector('body'), 'fixed-nav')) {
        }
        else {
            this.height = Math.max(this.body.scrollHeight, this.body.offsetHeight, this.html.clientHeight, this.html.scrollHeight, this.html.offsetHeight);
            document.getElementsByTagName('body')[0].style.minHeight = this.height + "px";
        }
    };
    __decorate([
        core_1.HostListener('window:resize', ['$event'])
    ], SmartResizeDirective.prototype, "onResize");
    SmartResizeDirective = __decorate([
        core_1.Directive({
            selector: 'main'
        })
    ], SmartResizeDirective);
    return SmartResizeDirective;
})();
exports.SmartResizeDirective = SmartResizeDirective;
//# sourceMappingURL=smart-resize.directive.js.map