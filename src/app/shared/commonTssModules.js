var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var common_1 = require('@angular/common');
var primeng_1 = require('primeng/primeng');
var primeng_2 = require("primeng/primeng");
var tss_datatable_module_1 = require("../_components/tss-datatable/tss-datatable.module");
var common_service_1 = require("../_services/common.service");
var primeng_3 = require("primeng/primeng");
var datex_pipe_1 = require("../_pipe/datex.pipe");
var index_1 = require("ng2-filter-pipe/dist/index");
var ng_bootstrap_1 = require('@ng-bootstrap/ng-bootstrap');
var COMMON_TSS_MODULES = [
    forms_1.FormsModule,
    forms_1.ReactiveFormsModule,
    common_1.CommonModule,
    primeng_1.DataTableModule,
    primeng_1.FileUploadModule,
    primeng_1.MultiSelectModule,
    primeng_1.ButtonModule,
    primeng_1.DialogModule,
    primeng_1.PanelModule,
    primeng_1.InplaceModule,
    primeng_1.DataListModule,
    primeng_1.ConfirmDialogModule,
    primeng_1.InputMaskModule,
    primeng_1.MenuModule,
    primeng_1.SpinnerModule,
    primeng_1.CheckboxModule,
    primeng_1.CalendarModule,
    primeng_1.EditorModule,
    primeng_1.SharedModule,
    primeng_1.TabViewModule,
    primeng_1.OverlayPanelModule,
    primeng_1.TooltipModule,
    primeng_1.InputSwitchModule,
    primeng_1.RadioButtonModule,
    primeng_1.DropdownModule,
    primeng_1.InputTextareaModule,
    primeng_3.InputTextModule,
    tss_datatable_module_1.TssDatatableModule,
    index_1.Ng2FilterPipeModule,
    ng_bootstrap_1.NgbModule
];
var CommonTssModule = (function () {
    function CommonTssModule() {
    }
    CommonTssModule = __decorate([
        core_1.NgModule({
            imports: COMMON_TSS_MODULES,
            exports: [COMMON_TSS_MODULES, datex_pipe_1.DatexPipe],
            declarations: [
                datex_pipe_1.DatexPipe
            ],
            providers: [
                primeng_2.ConfirmationService, common_service_1.CommonService
            ]
        })
    ], CommonTssModule);
    return CommonTssModule;
})();
exports.CommonTssModule = CommonTssModule;
//# sourceMappingURL=commonTssModules.js.map