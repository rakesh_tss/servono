import { Directive, HostListener,ElementRef } from '@angular/core';

@Directive({
  selector: '.nav-dropdown',
  host: {
    '[class.open]': '_open',
  }
})
export class NavDropdownDirective {

  private _open = false;

  /**
  * Checks if the dropdown menu is open or not.
  */
  isOpen() { return this._open; }

  /**
  * Opens the dropdown menu.
  */
  open() {
    this._open = true;
  }

  /**
  * Closes the dropdown menu .
  */
  close() {
    this._open = false;
  }

  /**
  * Toggles the dropdown menu.
  */
  toggle() {
    if (this.isOpen()) {
      this.close();
    } else {
      this.open();
    }
  }
}

/**
* Allows the dropdown to be toggled via click.
*/
@Directive({
  selector: '.nav-dropdown-toggle',
})
export class NavDropdownToggleDirective{
  constructor(private dropdown: NavDropdownDirective,private el:ElementRef) {
  }

  @HostListener('click', ['$event'])
  toggleOpen($event:any) {

    let data = document.querySelector('body ul li.nav-dropdown.open');
    if(data){
     // data.classList.remove('open');
      //console.log(' data._open', data);
      //data._open;
    }
   // console.log('data',data);
    $event.preventDefault();
    //this.el.nativeElement.parentElement.classList.remove('open');
    if(this.el.nativeElement.parentElement.classList.contains('open')){
      this.el.nativeElement.parentElement.classList.remove('open');
    }else{
      if(data){
        data.classList.remove('open');
        //console.log(' data._open', data);
        //data._open;
      }
      this.el.nativeElement.parentElement.classList.add('open');
    }

    //this.dropdown.toggle();
  }
}

export const NAV_DROPDOWN_DIRECTIVES = [NavDropdownDirective, NavDropdownToggleDirective];
// export const NGB_DROPDOWN_DIRECTIVES = [NgbDropdownToggle, NgbDropdown];
