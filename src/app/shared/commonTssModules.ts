import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule}  from '@angular/forms';
import { CommonModule } from '@angular/common';
import {
    DataTableModule,
    FileUploadModule,
    MultiSelectModule,
    ButtonModule,
    DialogModule,
    PanelModule,
    InplaceModule,
    DataListModule,
    ConfirmDialogModule,
    InputMaskModule,
    MenuModule,
    SpinnerModule,
    CheckboxModule,
    CalendarModule,
    EditorModule,
    SharedModule,
    TabViewModule,
    OverlayPanelModule,
    TooltipModule,
    InputSwitchModule,
    RadioButtonModule,
    DropdownModule,
    InputTextareaModule
} from 'primeng/primeng'
import {ConfirmationService} from "primeng/primeng";
import {TssDatatableModule} from "../_components/tss-datatable/tss-datatable.module";
import {CommonService} from "../_services/common.service";
import {InputTextModule} from "primeng/primeng";
import {DatexPipe} from "../_pipe/datex.pipe";
import {Ng2FilterPipeModule} from "ng2-filter-pipe/dist/index";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {TssSubHeadModule} from "../_components/tss-sub-head/tss-sub-head.module";
import {LocalStorageService} from "../_services/local-storage.service";
const COMMON_TSS_MODULES = [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    DataTableModule,
    FileUploadModule,
    MultiSelectModule,
    ButtonModule,
    DialogModule,
    PanelModule,
    InplaceModule,
    DataListModule,
    ConfirmDialogModule,
    InputMaskModule,
    MenuModule,
    SpinnerModule,
    CheckboxModule,
    CalendarModule,
    EditorModule,
    SharedModule,
    TabViewModule,
    OverlayPanelModule,
    TooltipModule,
    InputSwitchModule,
    RadioButtonModule,
    DropdownModule,
    InputTextareaModule,
    InputTextModule,
    TssDatatableModule,
    Ng2FilterPipeModule,
    NgbModule,
    TssSubHeadModule
];

@NgModule({
    imports: COMMON_TSS_MODULES,
    exports: [COMMON_TSS_MODULES,DatexPipe],
    declarations:[
        DatexPipe
    ],
    providers:[
        ConfirmationService,CommonService,LocalStorageService
    ]
})
export class CommonTssModule { }