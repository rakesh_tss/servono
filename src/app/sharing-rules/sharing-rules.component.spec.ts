import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharingRulesComponent } from './sharing-rules.component';

describe('SharingRulesComponent', () => {
  let component: SharingRulesComponent;
  let fixture: ComponentFixture<SharingRulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharingRulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharingRulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
