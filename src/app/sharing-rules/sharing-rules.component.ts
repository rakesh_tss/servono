import { Component, OnInit } from '@angular/core';
import {HttpService} from "../_services/http.service";
import {BreadcrumbService} from "../_services/breadcrumb.service";
import {CommonService} from "../_services/common.service";
import {ConfirmationService} from "primeng/primeng";

@Component({
    selector: 'app-sharing-rules',
    templateUrl: './sharing-rules.component.html',
    styleUrls: ['./sharing-rules.component.css']
})
export class SharingRulesComponent implements OnInit {

    constructor(private httpService:HttpService,
                private confirmationService:ConfirmationService,
                private breadcrumbService:BreadcrumbService, private commonService:CommonService) {
    }

    ngOnInit() {
        this.breadcrumbService.setBreadcrumb([{label: 'Sharing Rules'}]);
        this.getModuleList();
    }

    public moduleList:any = [];

    private getModuleList() {
        this.httpService.get('GetModules').subscribe(
            res => {
                if (res) {
                    this.moduleList = res.data;
                }
            });
    }

    saveRules() {
        this.httpService.post('SaveUpdateModuleSharing', {data: this.moduleList}).subscribe(res=> {
            if (res) {

            }
        })
    }

    addCustomRoleModel:boolean = false;
    userTypeList:any = [];
    usergroupsList:any = [];
    customRole:any = {};
    currentActiveRow:any = {};

    addCustomRole(row, data) {
        this.currentActiveRow = row;
        this.addCustomRoleModel = true;
        this.customRole = {};
        this.customRole.Privileges = 0;
        if (data) {
            this.customRole = data;
            this.getRecordOf(this.customRole.record_utid, this.customRole.records_of_owner_type, null);
            this.getAccessRecordOf(this.customRole.access_utid, this.customRole.access_by_type, null);
        }
        this.customRole.app_module = row.pk_id;
        this.getDropdownData('usertypes', '0').then(res=> {
            this.userTypeList = this.commonService.getDropdownOptions(res, 'name', 'pk_id');
        });
        this.getDropdownData('sharing accessible types', '0').then(res=> {
            this.usergroupsList = this.commonService.getDropdownOptions(res, 'name', 'pk_id');
        });
    }

    getCustomModuleSharingRules(row) {
        this.httpService.get('GetCustomModuleSharingRules/' + row.pk_id).subscribe(res=> {
            if (res) {
                row.data = res.data;
            }
        })
    }

    addCustomRule() {
        this.httpService.post('SaveUpdateCustomModuleSharing', this.customRole).subscribe(res=> {
            if (res) {
                this.addCustomRoleModel = false;
                this.getCustomModuleSharingRules(this.currentActiveRow);
            }
        })

    }

    recordsList:any = [];

    getRecordOf(usertypes, groupType, $event) {
        let gT:string = 'roles';
        if (usertypes && groupType) {
            if (groupType == "Group") {
                gT = 'usergroups';
                this.getDropdownData(gT, usertypes).then(res=> {
                    this.recordsList = this.commonService.getDropdownOptions(res, 'name', 'pk_id');
                });
            } else {
                this.getDropdownData(gT, usertypes).then(res=> {
                    this.recordsList = this.commonService.getDropdownOptions(res, 'app_role', 'pk_id');
                });
            }
        }
    }

    recordsAccessList:any = [];

    getAccessRecordOf(usertypes, groupType, $event) {
        let gT:string = 'roles';
        if (usertypes && groupType) {
            if (groupType == "Group") {
                gT = 'usergroups';
                this.getDropdownData(gT, usertypes).then(res=> {
                    this.recordsAccessList = this.commonService.getDropdownOptions(res, 'name', 'pk_id');
                });
            } else {
                this.getDropdownData(gT, usertypes).then(res=> {
                    this.recordsAccessList = this.commonService.getDropdownOptions(res, 'app_role', 'pk_id');
                });
            }
        }
    }

    getDropdownData(type:string, userType:string) {
        return this.httpService.get('DropListData/' + type + '/' + userType)
            .toPromise()
            .then(res => res.data);
    }

    removeCustomModuleSharingRule(row,sharingRuleRow) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
                this.httpService.post('RemoveCustomModuleSharingRule/'+sharingRuleRow.pk_id,{}).subscribe(res=>{
                    if(res){
                        this.getCustomModuleSharingRules(row);
                        //this.tableReload();
                    }
                })

            }
        });

    }

}
