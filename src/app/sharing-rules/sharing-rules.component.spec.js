var testing_1 = require('@angular/core/testing');
var sharing_rules_component_1 = require('./sharing-rules.component');
describe('SharingRulesComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [sharing_rules_component_1.SharingRulesComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(sharing_rules_component_1.SharingRulesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=sharing-rules.component.spec.js.map