import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharingRulesRoutingModule } from './sharing-rules-routing.module';
import {SharingRulesComponent} from "./sharing-rules.component";
import {CommonTssModule} from "../shared/commonTssModules";

@NgModule({
  imports: [
    CommonTssModule,
    SharingRulesRoutingModule
  ],
  declarations: [
    SharingRulesComponent
  ]
})
export class SharingRulesModule { }
