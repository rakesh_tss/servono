var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var sharing_rules_routing_module_1 = require('./sharing-rules-routing.module');
var sharing_rules_component_1 = require("./sharing-rules.component");
var commonTssModules_1 = require("../shared/commonTssModules");
var SharingRulesModule = (function () {
    function SharingRulesModule() {
    }
    SharingRulesModule = __decorate([
        core_1.NgModule({
            imports: [
                commonTssModules_1.CommonTssModule,
                sharing_rules_routing_module_1.SharingRulesRoutingModule
            ],
            declarations: [
                sharing_rules_component_1.SharingRulesComponent
            ]
        })
    ], SharingRulesModule);
    return SharingRulesModule;
})();
exports.SharingRulesModule = SharingRulesModule;
//# sourceMappingURL=sharing-rules.module.js.map