var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var SharingRulesComponent = (function () {
    function SharingRulesComponent(httpService, confirmationService, breadcrumbService, commonService) {
        this.httpService = httpService;
        this.confirmationService = confirmationService;
        this.breadcrumbService = breadcrumbService;
        this.commonService = commonService;
        this.moduleList = [];
        this.addCustomRoleModel = false;
        this.userTypeList = [];
        this.usergroupsList = [];
        this.customRole = {};
        this.currentActiveRow = {};
        this.recordsList = [];
        this.recordsAccessList = [];
    }
    SharingRulesComponent.prototype.ngOnInit = function () {
        this.breadcrumbService.setBreadcrumb([{ label: 'Sharing Rules' }]);
        this.getModuleList();
    };
    SharingRulesComponent.prototype.getModuleList = function () {
        var _this = this;
        this.httpService.get('GetModules').subscribe(function (res) {
            if (res) {
                _this.moduleList = res.data;
            }
        });
    };
    SharingRulesComponent.prototype.saveRules = function () {
        this.httpService.post('SaveUpdateModuleSharing', { data: this.moduleList }).subscribe(function (res) {
            if (res) {
            }
        });
    };
    SharingRulesComponent.prototype.addCustomRole = function (row, data) {
        var _this = this;
        this.currentActiveRow = row;
        this.addCustomRoleModel = true;
        this.customRole = {};
        this.customRole.Privileges = 0;
        if (data) {
            this.customRole = data;
            this.getRecordOf(this.customRole.record_utid, this.customRole.records_of_owner_type, null);
            this.getAccessRecordOf(this.customRole.access_utid, this.customRole.access_by_type, null);
        }
        this.customRole.app_module = row.pk_id;
        this.getDropdownData('usertypes', '0').then(function (res) {
            _this.userTypeList = _this.commonService.getDropdownOptions(res, 'name', 'pk_id');
        });
        this.getDropdownData('sharing accessible types', '0').then(function (res) {
            _this.usergroupsList = _this.commonService.getDropdownOptions(res, 'name', 'pk_id');
        });
    };
    SharingRulesComponent.prototype.getCustomModuleSharingRules = function (row) {
        this.httpService.get('GetCustomModuleSharingRules/' + row.pk_id).subscribe(function (res) {
            if (res) {
                row.data = res.data;
            }
        });
    };
    SharingRulesComponent.prototype.addCustomRule = function () {
        var _this = this;
        this.httpService.post('SaveUpdateCustomModuleSharing', this.customRole).subscribe(function (res) {
            if (res) {
                _this.addCustomRoleModel = false;
                _this.getCustomModuleSharingRules(_this.currentActiveRow);
            }
        });
    };
    SharingRulesComponent.prototype.getRecordOf = function (usertypes, groupType, $event) {
        var _this = this;
        var gT = 'roles';
        if (usertypes && groupType) {
            if (groupType == "Group") {
                gT = 'usergroups';
                this.getDropdownData(gT, usertypes).then(function (res) {
                    _this.recordsList = _this.commonService.getDropdownOptions(res, 'name', 'pk_id');
                });
            }
            else {
                this.getDropdownData(gT, usertypes).then(function (res) {
                    _this.recordsList = _this.commonService.getDropdownOptions(res, 'app_role', 'pk_id');
                });
            }
        }
    };
    SharingRulesComponent.prototype.getAccessRecordOf = function (usertypes, groupType, $event) {
        var _this = this;
        var gT = 'roles';
        if (usertypes && groupType) {
            if (groupType == "Group") {
                gT = 'usergroups';
                this.getDropdownData(gT, usertypes).then(function (res) {
                    _this.recordsAccessList = _this.commonService.getDropdownOptions(res, 'name', 'pk_id');
                });
            }
            else {
                this.getDropdownData(gT, usertypes).then(function (res) {
                    _this.recordsAccessList = _this.commonService.getDropdownOptions(res, 'app_role', 'pk_id');
                });
            }
        }
    };
    SharingRulesComponent.prototype.getDropdownData = function (type, userType) {
        return this.httpService.get('DropListData/' + type + '/' + userType)
            .toPromise()
            .then(function (res) { return res.data; });
    };
    SharingRulesComponent.prototype.removeCustomModuleSharingRule = function (row, sharingRuleRow) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.httpService.post('RemoveCustomModuleSharingRule/' + sharingRuleRow.pk_id, {}).subscribe(function (res) {
                    if (res) {
                        _this.getCustomModuleSharingRules(row);
                    }
                });
            }
        });
    };
    SharingRulesComponent = __decorate([
        core_1.Component({
            selector: 'app-sharing-rules',
            templateUrl: './sharing-rules.component.html',
            styleUrls: ['./sharing-rules.component.css']
        })
    ], SharingRulesComponent);
    return SharingRulesComponent;
})();
exports.SharingRulesComponent = SharingRulesComponent;
//# sourceMappingURL=sharing-rules.component.js.map