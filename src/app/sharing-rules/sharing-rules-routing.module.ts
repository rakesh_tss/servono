import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SharingRulesComponent} from "./sharing-rules.component";

const routes: Routes = [
  {
    path:'',
    component:SharingRulesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class SharingRulesRoutingModule { }
