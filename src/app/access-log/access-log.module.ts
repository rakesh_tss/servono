import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccessLogRoutingModule } from './access-log-routing.module';
import { AccessLogComponent } from './access-log.component';
import {CommonTssModule} from "../shared/commonTssModules";

@NgModule({
  imports: [
    CommonTssModule,
    AccessLogRoutingModule
  ],
  declarations: [AccessLogComponent]
})
export class AccessLogModule { }
