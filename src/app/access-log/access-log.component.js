var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var AccessLogComponent = (function () {
    function AccessLogComponent(breadcrumbService, httpService) {
        this.breadcrumbService = breadcrumbService;
        this.httpService = httpService;
        this.logDateList = [];
        this.dataRow = {};
        this.refreshTable = false;
        this.menuFormObj = {};
        this.breadcrumbService.setBreadcrumb([{ label: 'Access Log' }]);
    }
    AccessLogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.httpService.get('GetLogDatesForFilter/1').subscribe(function (res) {
            if (res) {
                res.data.forEach(function (data) {
                    var obj = {};
                    obj.value = data.log_tbl_date;
                    obj.label = data.start_date_name;
                    _this.logDateList.push(obj);
                });
                if (_this.logDateList.length > 0) {
                    _this.selectedDate = _this.logDateList[0].value;
                    _this.tableReload();
                }
            }
        });
    };
    AccessLogComponent.prototype.loadDataLazy = function (event) {
        var _this = this;
        var param = event;
        param.type = 1;
        param.date = this.selectedDate;
        this.httpService.postTable('GetAllLogsBySearch', param).subscribe(function (res) {
            if (res) {
                _this.dataRows = res.jqGridData.rows;
                _this.totalRecords = res.jqGridData.records;
            }
        });
    };
    AccessLogComponent.prototype.tableReload = function () {
        var _this = this;
        this.refreshTable = false;
        setTimeout(function () { return _this.refreshTable = true; }, 100);
    };
    AccessLogComponent = __decorate([
        core_1.Component({
            selector: 'app-access-log',
            templateUrl: './access-log.component.html',
            styleUrls: ['./access-log.component.css']
        })
    ], AccessLogComponent);
    return AccessLogComponent;
})();
exports.AccessLogComponent = AccessLogComponent;
//# sourceMappingURL=access-log.component.js.map