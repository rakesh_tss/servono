var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var access_log_routing_module_1 = require('./access-log-routing.module');
var access_log_component_1 = require('./access-log.component');
var commonTssModules_1 = require("../shared/commonTssModules");
var AccessLogModule = (function () {
    function AccessLogModule() {
    }
    AccessLogModule = __decorate([
        core_1.NgModule({
            imports: [
                commonTssModules_1.CommonTssModule,
                access_log_routing_module_1.AccessLogRoutingModule
            ],
            declarations: [access_log_component_1.AccessLogComponent]
        })
    ], AccessLogModule);
    return AccessLogModule;
})();
exports.AccessLogModule = AccessLogModule;
//# sourceMappingURL=access-log.module.js.map