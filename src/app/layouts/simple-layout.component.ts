import { Component, OnInit } from '@angular/core';
import {HttpService} from "../_services/http.service";
import {
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
    NavigationCancel,
    NavigationError,
    Router,
    ActivatedRoute
} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './simple-layout.component.html'
})
export class SimpleLayoutComponent implements OnInit {

  constructor(public httpService:HttpService,private router:Router) {

  }
  public loading:boolean = false;
  ngOnInit(): void { }
}
