var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var FullLayoutComponent = (function () {
    function FullLayoutComponent(breadcrumbService, _renderer, appConfig, activatedRoute, modalService, authenticationService, httpService, router) {
        var _this = this;
        this.breadcrumbService = breadcrumbService;
        this._renderer = _renderer;
        this.appConfig = appConfig;
        this.activatedRoute = activatedRoute;
        this.modalService = modalService;
        this.authenticationService = authenticationService;
        this.httpService = httpService;
        this.router = router;
        this.menuItems = [];
        this.showBreadCrumbModuleLink = false;
        this.showBreadCrumbs = true;
        this.moduleDropDownShow = false;
        this.userDropDownItem = [];
        this.moduleDropDownItem = [];
        this.userName = '';
        this.disabled = false;
        this.status = { isopen: false };
        this.loading = true;
        this.menuModuleList = [];
        this.passwordDialog = false;
        this.cPswd = {};
        this.objBreadcrumbs = [];
        router.events.subscribe(function (event) {
            _this.navigationInterceptor(event);
        });
    }
    FullLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.breadcrumbService.breadcrumbItem.subscribe(function (val) {
            if (val) {
                _this.objBreadcrumbs = val;
                _this.backLink = null;
                document.body.classList.remove('prev-btn');
                _this.objBreadcrumbs.forEach(function (data) {
                    if (data.routerLink) {
                        _this.backLink = data.routerLink;
                        document.body.classList.add('prev-btn');
                    }
                });
            }
        });
        this.userDropDownItem = [
            {
                label: 'Change Password', icon: 'fa-key', command: function () {
                    _this.openChangeModal();
                }
            },
            {
                label: 'Logout', icon: 'fa-lock', command: function () {
                    _this.logout();
                }
            }
        ];
        this.showBreadCrumbs = true;
        if (localStorage.getItem('currentUser')) {
            this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
            if (this.currentUser.UserName) {
                this.userName = this.currentUser.UserName;
            }
            this.menuReload();
        }
    };
    //if mobile view nav open
    FullLayoutComponent.prototype.hasClass = function (target, elementClassName) {
        return new RegExp('(\\s|^)' + elementClassName + '(\\s|$)').test(target.className);
    };
    FullLayoutComponent.prototype.closeMenu = function () {
        if (this.hasClass(document.querySelector('body'), 'mobile-open')) {
            document.querySelector('body').classList.toggle('mobile-open');
        }
    };
    FullLayoutComponent.prototype.onChangeMenuModule = function (data) {
        this.setMenu(data);
    };
    FullLayoutComponent.prototype.menuReload = function () {
        var _this = this;
        this.showBreadCrumbModuleLink = false;
        this.httpService.get('GetRoleMenu').subscribe(function (res) {
            if (res) {
                _this.menuObject = _this.setMenuFormat(res.data);
                if (localStorage.getItem('currentMenu')) {
                    var currentMenu = JSON.parse(localStorage.getItem('currentMenu'));
                    if (_this.menuObject.length > 1) {
                        _this.getMenuModuleList();
                        _this.showBreadCrumbModuleLink = true;
                        _this.moduleDropDownShow = true;
                    }
                    _this.menuItems = currentMenu.menu;
                    _this.menuGroup = currentMenu.group_name;
                    _this.menuObject.forEach(function (item) {
                        if (item.group_name == currentMenu.group_name) {
                            _this.menuItems = item.menu;
                            console.log('this.menuItems', _this.menuItems);
                            localStorage.setItem('currentMenu', JSON.stringify(item));
                        }
                    });
                }
                else {
                    if (_this.menuObject.length > 1) {
                        document.querySelector("body").classList.remove('sidebar-nav');
                        _this.router.navigate(['Modules']);
                    }
                    else {
                        document.querySelector("body").classList.add('sidebar-nav');
                        _this.setMenu(_this.menuObject[0]);
                    }
                }
            }
        });
    };
    FullLayoutComponent.prototype.setMenuFormat = function (menuModule) {
        var _this = this;
        var myMenu = [];
        menuModule.forEach(function (data) {
            var module = data.data;
            module.menu = [];
            data.menu.forEach(function (menuItem) {
                var menuObj = {};
                if (menuItem.list.length > 1) {
                    menuObj = menuItem.data;
                    menuItem.list.forEach(function (subItem) {
                        var obj = {};
                        obj.url = _this.setUrl(subItem, menuItem, module);
                        Object.assign(subItem, obj);
                    });
                    menuObj.sub = menuItem.list;
                }
                else {
                    var singleItem = menuItem.list[0];
                    Object.assign(singleItem, menuItem.data);
                    singleItem.url = _this.setUrl(singleItem, menuItem, module);
                    menuObj = singleItem;
                }
                module.menu.push(menuObj);
            });
            myMenu.push(module);
        });
        return myMenu;
    };
    FullLayoutComponent.prototype.setUrl = function (menuItem, parentMenu, module) {
        var link;
        if (menuItem) {
            if (menuItem.type == 'AUTOMATIC') {
                link = 'tss/' + module.group_name + '/' + parentMenu.data.name + '/' + menuItem.module;
            }
            else {
                link = 'Custom/' + menuItem.module;
            }
        }
        return link;
    };
    FullLayoutComponent.prototype.setMenu = function (menuItem) {
        localStorage.setItem('currentMenu', JSON.stringify(menuItem));
        this.menuItems = menuItem.menu;
        this.menuGroup = menuItem.group_name;
        var link;
        if (this.menuItems[0].sub) {
            link = this.menuItems[0].sub[0].url;
        }
        else {
            link = this.menuItems[0].url;
        }
        this.router.navigate([link]);
    };
    FullLayoutComponent.prototype.toggled = function (open) {
    };
    FullLayoutComponent.prototype.toggleDropdown = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        this.status.isopen = !this.status.isopen;
    };
    FullLayoutComponent.prototype.logout = function () {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    };
    // Shows and hides the loading spinner during RouterEvent changes
    FullLayoutComponent.prototype.navigationInterceptor = function (event) {
        if (event instanceof router_1.NavigationStart) {
            this.loading = true;
        }
        if (event instanceof router_1.NavigationEnd) {
            this.loading = false;
        }
        // Set loading state to false in both of the below events to hide the spinner in case a request fails
        if (event instanceof router_1.NavigationCancel) {
            this.loading = false;
        }
        if (event instanceof router_1.NavigationError) {
            this.loading = false;
        }
    };
    FullLayoutComponent.prototype.getMenuModuleList = function () {
        var _this = this;
        var actionItem = [];
        this.menuModuleList = [];
        this.menuObject.forEach(function (item) {
            _this.menuModuleList.push({
                label: item.group_name, icon: item.icon_class, command: function (event) {
                    _this.onChangeMenuModule(item);
                }
            });
        });
    };
    //changePasswordContent:TemplateRef<any>
    FullLayoutComponent.prototype.openChangeModal = function () {
        this.cPswd = {};
        //this.passwordDialog = true;
        this.pswModal = this.modalService.open(this.changePasswordContent);
    };
    FullLayoutComponent.prototype.ChangePassword = function (form) {
        var _this = this;
        this.httpService.post('ChangePassword', this.cPswd).subscribe(function (res) {
            if (res) {
                //this.passwordDialog = false;
                _this.pswModal.close();
                form.reset();
            }
        });
    };
    __decorate([
        core_1.ViewChild('changePasswordContent')
    ], FullLayoutComponent.prototype, "changePasswordContent");
    FullLayoutComponent = __decorate([
        core_1.Component({
            selector: 'app-dashboard',
            templateUrl: './full-layout.component.html'
        })
    ], FullLayoutComponent);
    return FullLayoutComponent;
})();
exports.FullLayoutComponent = FullLayoutComponent;
//# sourceMappingURL=full-layout.component.js.map