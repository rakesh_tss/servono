import { Component, OnInit,Renderer,TemplateRef,ViewChild, Inject, Injectable} from '@angular/core';
import { AuthenticationService } from '../_services/index';
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { DOCUMENT } from '@angular/platform-browser';
import {
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
    NavigationCancel,
    NavigationError,
    Router,
    ActivatedRoute
} from '@angular/router';
import { MenuItem } from 'primeng/primeng';
import {HttpService} from "../_services/http.service";
import {BreadcrumbService} from "../_services/breadcrumb.service";
import {AppConfig} from "../app.config";
import {LocalStorageService} from "../_services/local-storage.service";

@Component({
    selector: 'app-dashboard',
    templateUrl: './full-layout.component.html'
})
export class FullLayoutComponent implements OnInit {

    objBreadcrumbs:MenuItem[];
    constructor(private breadcrumbService:BreadcrumbService,
                private _renderer:Renderer,
                private appConfig:AppConfig,private activatedRoute:ActivatedRoute,
                private modalService:NgbModal,
                @Inject(DOCUMENT) private document: any,
                public authenticationService:AuthenticationService, public httpService:HttpService, private router:Router, private localStorage:LocalStorageService) {
        this.objBreadcrumbs = [];

        router.events.subscribe((event:RouterEvent) => {
            this.navigationInterceptor(event);
        });
        /*themes*/
        this.themes = [
            {id:0,name:'Default',class:'default'},
            {id:1,name:'Green',class:'green'},
            {id:2,name:'Blue',class:'blue'},
            {id:3,name:'Gray',class:'gray'},
            {id:4,name:'Gray',class:'thickBlue'}
        ]
    }

    /*theme ts*/
    themes:any;
    selectedThemeId:any;
    changeTheme(value){
        this.selectedThemeId = value;
        if(value == 1){
            this.document.getElementById('theme').setAttribute('href', 'assets/themes/lightGreenTheme.css');
        }
        else if(value == 2){
            this.document.getElementById('theme').setAttribute('href', 'assets/themes/blueTheme.css');
        }
        else if(value == 3){
            this.document.getElementById('theme').setAttribute('href', 'assets/themes/grayTheme.css');
        }
        else if(value == 4){
            this.document.getElementById('theme').setAttribute('href', 'assets/themes/thickBlueTheme.css');
        }
        else if(value == 0){
            this.document.getElementById('theme').setAttribute('href', '');
        }
    }
    /*theme ts ends*/

    public menuList;
    public menuItems:any[] = [];
    public menuGroup:string;
    public currentUser:any;
    public menuObject:any;
    showBreadCrumbModuleLink:boolean = false;
    public showBreadCrumbs:boolean = true;
    public selectedMenuModule:any;
    public moduleDropDownShow:boolean = false;
    userDropDownItem:any = [];
    moduleDropDownItem:any = [];
    public userName:string = '';
    public backLink:string;

    ngOnInit():void {
        this.breadcrumbService.breadcrumbItem.subscribe((val:MenuItem[]) => {
            if (val){
                this.objBreadcrumbs = val;
                this.backLink = null;
                document.body.classList.remove('prev-btn');
                this.objBreadcrumbs.forEach((data)=> {
                    if (data.routerLink) {
                        this.backLink = data.routerLink;
                        document.body.classList.add('prev-btn');
                    }
                })
            }

        });

        this.userDropDownItem = [
            {
                label: 'Change Password', icon: 'fa-key', command: ()=> {
                this.openChangeModal()
            }
            },
            {
                label: 'Logout', icon: 'fa-lock', command: ()=> {
                this.logout();
            }
            }

        ];

        this.showBreadCrumbs = true;
        if (this.localStorage.getItem('currentUser')) {
            this.currentUser = JSON.parse(this.localStorage.getItem('currentUser'));
            if (this.currentUser.UserName) {
                this.userName = this.currentUser.UserName;
            }
            this.menuReload();
        }
    }
    //if mobile view nav open
    private hasClass(target:any, elementClassName:string) {
        return new RegExp('(\\s|^)' + elementClassName + '(\\s|$)').test(target.className);
    }
    public closeMenu(){
       if (this.hasClass(document.querySelector('body'), 'mobile-open')) {
            document.querySelector('body').classList.toggle('mobile-open');
        }
    }
    public onChangeMenuModule(data:any) {
        this.setMenu(data);
    }

    public menuReload(){
        this.showBreadCrumbModuleLink = false;
        this.httpService.get('GetRoleMenu').subscribe(res=>{
            if(res){
                this.menuObject = this.setMenuFormat(res.data);
                if(this.localStorage.getItem('currentMenu')){
                    let currentMenu = JSON.parse(this.localStorage.getItem('currentMenu'));
                    if (this.menuObject.length > 1) {
                        this.getMenuModuleList();
                        this.showBreadCrumbModuleLink = true;
                        this.moduleDropDownShow = true;
                    }
                    this.menuItems = currentMenu.menu;
                    this.menuGroup = currentMenu.group_name;
                    this.menuObject.forEach(item=>{
                        if(item.group_name==currentMenu.group_name){
                            this.menuItems = item.menu;
                            console.log('this.menuItems',this.menuItems);
                            this.localStorage.setItem('currentMenu', JSON.stringify(item));
                        }
                    })
                }else{
                    if( this.menuObject.length>1){
                        document.querySelector("body").classList.remove('sidebar-nav');
                        this.router.navigate(['Modules']);
                    }else{
                        document.querySelector("body").classList.add('sidebar-nav');
                        this.setMenu( this.menuObject[0]);
                    }
                }

                /*this.menuObject = this.currentUser.MenuObject;

                let currentMenu = JSON.parse(localStorage.getItem('currentMenu'));
                this.showBreadCrumbModuleLink = false;
                if (currentMenu) {
                    this.menuItems = currentMenu.menu;
                    this.menuGroup = currentMenu.group_name;
                }
                if (this.menuObject.length > 1 && currentMenu) {

                    this.showBreadCrumbModuleLink = true;
                    this.moduleDropDownShow = true;
                }
*/
                //this.getMenuModuleList();

            }
        })
    }

    public setMenuFormat(menuModule) {
        let myMenu = [];

        menuModule.forEach((data)=> {
            let module:any = data.data;
            module.menu = [];
            data.menu.forEach((menuItem)=> {
                let menuObj:any = {};
                if (menuItem.list.length > 1) {
                    menuObj = menuItem.data;
                    menuItem.list.forEach((subItem)=> {
                        let obj:any = {};
                        obj.url = this.setUrl(subItem, menuItem, module);
                        Object.assign(subItem, obj);
                    });
                    menuObj.sub = menuItem.list;
                } else {
                    let singleItem:any = menuItem.list[0];
                    Object.assign(singleItem, menuItem.data);
                    singleItem.url = this.setUrl(singleItem, menuItem, module);
                    menuObj = singleItem;
                }
                module.menu.push(menuObj);
            });
            myMenu.push(module);
        });
        return myMenu;
    }

    setUrl(menuItem, parentMenu, module) {
        let link:string;
        if (menuItem) {
            if (menuItem.type == 'AUTOMATIC') {
                link = 'tss/' + module.group_name + '/' + parentMenu.data.name + '/' + menuItem.module;
            } else {
                link = 'Custom/' + menuItem.module;
            }
        }
        return link;
    }

    public setMenu(menuItem:any) {
        this.localStorage.setItem('currentMenu', JSON.stringify(menuItem));
        this.menuItems = menuItem.menu;
        this.menuGroup = menuItem.group_name;
        let link:string;

        if (this.menuItems[0].sub) {
            link = this.menuItems[0].sub[0].url;
        } else {
            link = this.menuItems[0].url;
        }
        this.router.navigate([link]);
    }

    public disabled:boolean = false;
    public status:{isopen:boolean} = {isopen: false};

    public toggled(open:boolean):void {
    }

    public toggleDropdown($event:MouseEvent):void {
        $event.preventDefault();
        $event.stopPropagation();
        this.status.isopen = !this.status.isopen;
    }

    public logout() {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    }

    loading:boolean = true;
    // Shows and hides the loading spinner during RouterEvent changes
    navigationInterceptor(event:RouterEvent):void {
        if (event instanceof NavigationStart) {
            this.loading = true;
        }
        if (event instanceof NavigationEnd) {
            this.loading = false;
        }
        // Set loading state to false in both of the below events to hide the spinner in case a request fails
        if (event instanceof NavigationCancel) {
            this.loading = false;
        }
        if (event instanceof NavigationError) {
            this.loading = false;
        }
    }

    menuModuleList:any = [];

    getMenuModuleList() {
        let actionItem = [];
        this.menuModuleList = [];
        this.menuObject.forEach((item)=> {
            this.menuModuleList.push({
                label: item.group_name, icon: item.icon_class, command: (event) => {
                    this.onChangeMenuModule(item)
                }
            });
        });
    }

    passwordDialog:boolean = false;
    cPswd:any = {};

    @ViewChild('changePasswordContent')
    changePasswordContent: TemplateRef<any>;
    pswModal:NgbModalRef;
    //changePasswordContent:TemplateRef<any>
    openChangeModal() {
        this.cPswd = {};
        //this.passwordDialog = true;
        this.pswModal = this.modalService.open(this.changePasswordContent);
    }

    ChangePassword(form) {
        this.httpService.post('ChangePassword', this.cPswd).subscribe(res=> {
            if (res) {
                //this.passwordDialog = false;
                this.pswModal.close();
                form.reset();
            }
        })
    }
   /* reLoginModal:NgbModalRef;
    @ViewChild('reLoginContent')
    reLoginContent: TemplateRef<any>;
    reLoginObj:any;
    popupLoin(){
        this.reLoginObj={
            UserName:this.userName
        };
        this.reLoginModal = this.modalService.open(this.reLoginContent);
    }
    submitLogin(){
        this.httpService.post('processLogin',this.reLoginObj).subscribe(res=>{
            if(res){
                this.reLoginModal.close();
            }
        })
    }*/

}
