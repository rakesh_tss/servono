var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var PermissionService = (function () {
    function PermissionService() {
        this.permission = {
            View: false, Create: false, Edit: false, Delete: false
        };
    }
    PermissionService.prototype.get = function () {
        return this.permission;
    };
    PermissionService.prototype.set = function (data) {
        this.permission = data;
    };
    PermissionService = __decorate([
        core_1.Injectable()
    ], PermissionService);
    return PermissionService;
})();
exports.PermissionService = PermissionService;
//# sourceMappingURL=permission.service.js.map