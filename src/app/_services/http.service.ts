import { Injectable,Inject,Output, EventEmitter,ViewChild,TemplateRef} from '@angular/core';
import { Http, Headers, RequestOptions, Response,URLSearchParams} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Router} from '@angular/router';
import {ToasterService,ToasterConfig} from "angular2-toaster/angular2-toaster";
import {environment} from "../../environments/environment";
import * as CryptoJS from "crypto-js";
import {AppConfig} from "../app.config";
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {LoginModalComponent} from "../_components/login-modal/login-modal.component";
import { DOCUMENT } from "@angular/platform-browser";
import {LocalStorageService} from "./local-storage.service";

//import CryptoJS = require("crypto-js");
//import CryptoJS = require('crypto-js');
//import CryptoJS = require('crypto-js');


@Injectable()
export class HttpService {

    constructor(private http:Http, private router:Router,
                private appConfig:AppConfig,
                private toasterService:ToasterService,
                @Inject(DOCUMENT) private doc:Document,
                private modalService:NgbModal,
                private localStorage:LocalStorageService) {

    }

    public apiUrl = this.appConfig.getConfig('apiUrl');//environment.apiUrl;
    public encrypt = this.appConfig.getConfig('encrypt');//environment.apiUrl;
    public baseUrl = this.doc.location.origin + this.doc.location.pathname + '#/'
    public pendingRequestsNumber:number = 0;
    //public CryptoJS = require("crypto-js");
    key:string = '#base64Key#12345';
    keyEnyDec:any = CryptoJS.enc.Base64.parse(btoa(this.key));
    enycOptions:any = {mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7};

    post(url:string, data?:any) {

        this.pendingRequestsNumber++;
        let param:any = {};
        if (this.encrypt) {
            param.requestParams = CryptoJS.AES.encrypt(JSON.stringify(data), this.keyEnyDec, this.enycOptions).toString();
        } else {
            param = data;
        }
        return this.http.post(this.apiUrl + url, param, this.jwt()).map(res => {
            this.pendingRequestsNumber--;
            return this.resStatus(res);
        }, err=> {
            this.pendingRequestsNumber--;
        }).catch(e => {

            if (e.status === 401) {
                this.errorUnauthorized(e.json());
                return Observable.throw('Unauthorized');
            }

            this.pendingRequestsNumber--;
            //return Observable.throw('error');
            this.resStatus(e);
            return Observable.throw('Unauthorized');
            // do any other checking for statuses here
        });
    }

    get(url:string, data?:any) {
        this.pendingRequestsNumber++;
        let params = new URLSearchParams();
        for (let key in data) {
            params.set(key, data[key]);
        }
        let param:any = {};
        param = data;

        let options = new RequestOptions({
            withCredentials: true,
            // Have to make a URLSearchParams with a query string
            search: param
        });


        return this.http.get(this.apiUrl + url, options).map(res => {
            this.pendingRequestsNumber--;
            return this.resStatus(res);
        }, err=> {
            this.pendingRequestsNumber--;
        }).catch(e => {
            this.pendingRequestsNumber--;
            if (e.status === 401) {
                this.errorUnauthorized(e.json());
                return Observable.throw('Unauthorized');
            }
            if (e.status === 0) {
                return Observable.throw('Unauthorized');
            }
            return Observable.throw('connection');
            // do any other checking for statuses here
        });
    }

    getTable(url:string, data?:any) {
        this.pendingRequestsNumber++;
        let params = new URLSearchParams();
        if (data.first) {
            let count = (data.first / data.rows) + 1;
            params.set('page', count.toString());
            params.set('rows', data.rows);
        }
        if (data.sortField) {
            params.set('sidx', data.sortField);
            params.set('sord', (data.sortOrder == 1) ? 'asc' : 'desc');
        }
        for (let key in data) {
            params.set(key, data[key]);
        }

        let param:any = {};
        if (this.encrypt) {
            param.requestParams = CryptoJS.AES.encrypt(JSON.stringify(params), this.keyEnyDec, this.enycOptions).toString();
        } else {
            param = params;
        }
        let options = new RequestOptions({
            withCredentials: true,
            // Have to make a URLSearchParams with a query string
            search: param
        });
        return this.http.get(this.apiUrl + url, options).map(res => {
            this.pendingRequestsNumber--;
            return this.resStatus(res);
        }, err=> {
            this.pendingRequestsNumber--;
        }).catch(e => {
            this.pendingRequestsNumber--;
            if (e.status === 401) {
                this.errorUnauthorized(e.json());
                return Observable.throw('Unauthorized');
            }
            return Observable.throw('connection');
            // do any other checking for statuses here
        });
    }

    getMap(url:string, data?:any) {
        let params = new URLSearchParams();
        for (let key in data) {
            params.set(key, data[key]);
        }
        let options = new RequestOptions({
            withCredentials: true,
            // Have to make a URLSearchParams with a query string
            search: params
        });
        return this.http.get(this.apiUrl + url, options);
    }

    postTable(url:string, data?:any) {

        this.pendingRequestsNumber++;
        if (data.first) {
            let count = (data.first / data.rows) + 1;
            data.page = count.toString();
            data.rows = data.rows;

        }
        if (data.sortField) {
            data.sidx = data.sortField;
            data.sord = (data.sortOrder == 1) ? 'asc' : 'desc';
        }

        if (this.encrypt) {
            let tempData:any = {};
            tempData.requestParams = CryptoJS.AES.encrypt(JSON.stringify(data), this.keyEnyDec, this.enycOptions).toString();
            data = tempData;
        }

        return this.http.post(this.apiUrl + url, data, this.jwt()).map(res => {
            this.pendingRequestsNumber--;
            return this.resStatus(res);
        }, err=> {
            this.pendingRequestsNumber--;
        }).catch(e => {
            this.pendingRequestsNumber--;
            if (e.status === 401) {
                this.errorUnauthorized(e.json());
                return Observable.throw('Unauthorized');
            }

            return Observable.throw('connection');
            // do any other checking for statuses here
        });
    }

    delete(url:string, data:any) {

    }

    put(url:string, data:any) {

    }

    errorUnauthorized(err:any) {
        console.log('err', err);
        this.toasterService.pop('error', 'Error', err.message);
        this.popupLoin();
        //this.router.navigate(['login']);
    }

    resStatus(res:any) {
        let resData = null;
        if (this.encrypt) {
            resData = res.json();
            resData = JSON.parse(CryptoJS.AES.decrypt(resData.resData, this.keyEnyDec, this.enycOptions).toString(CryptoJS.enc.Utf8))
        } else {
            resData = res.json();
        }
        if (resData.success && resData.message) {
            this.toasterService.pop('success', 'success', resData.message);
        }
        if (res.status < 200 || res.status >= 300) {
            if (res.status == 401) {
                this.popupLoin();
                //this.router.navigate(['login']);
            }
            this.toasterService.pop('error', '', resData.message);
            return false;

        }
        if (res.status == 200) {
            if (resData.success) {
                return resData;
            } else {
                this.toasterService.pop('error', 'Error', resData.message);
                if (resData.data && resData.data.errors) {
                    for (let key in resData.data.errors) {
                        //console.log('resData.data.errors[key].ErrorMessage',key+':'+resData.data.errors[key].ErrorMessage);
                        if (resData.data.errors[key].ErrorMessage) {
                            this.toasterService.pop('info', key, key + ' : ' + resData.data.errors[key].ErrorMessage);
                        }
                    }
                }
                if (resData.data && resData.data.UniqueValidationFails) {
                    this.toasterService.pop('info', '', resData.data['UniqueValidationFails']);
                }
                return false;
            }
        }

    }

    private jwt() {
        return new RequestOptions({withCredentials: true});
    }

    reLoginModal:NgbModalRef;
    @ViewChild('reLoginContent')
    reLoginContent:TemplateRef<any>;
    reLoginObj:any;
    isLoginModalOpen = false;

    popupLoin() {
        let currentUser = JSON.parse(this.localStorage.getItem('currentUser'));
        if (!this.isLoginModalOpen && currentUser) {
            this.isLoginModalOpen = true;
            this.reLoginModal = this.modalService.open(LoginModalComponent, {
                windowClass: 'login-popup',
                keyboard: false,
                backdrop: 'static'
            });
            this.reLoginModal.componentInstance.httpService = this;
            this.reLoginModal.componentInstance.reLoginObj = {
                UserName: currentUser.UserName
            };
            this.reLoginModal.result.then((result) => {
                this.isLoginModalOpen = false;
                //console.log('close');
            }, (reason) => {
                this.isLoginModalOpen = false;
                //console.log('reason close');
            });
        } else {
            if (this.reLoginModal) {
                this.reLoginModal.close();
            }
            this.router.navigate(['login']);
        }
    }

    public getJSON(filePath) {
        return this.http.get(filePath).map(res => {
            return res.json();
        }, err=> {
        })
    }


}
