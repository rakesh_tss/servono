import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { User } from '../_models/index';
import 'rxjs/Rx';
import {HttpService} from "./http.service";

@Injectable()
export class UserService{
    constructor(private http: Http,private httpService:HttpService) {
    }
    testSession (){
        return this.httpService.get('testSession',{'id':10,'userId':11});
        //return this.http.get('http://192.168.0.67:8080/DynamicServices/testSession',this.jwt()).map((response: Response) => response.json());
    }

    getAll() {
        return this.http.get('/api/users', this.jwt()).map((response: Response) => response.json());
    }

    getById(id: number) {
        return this.http.get('/api/users/' + id, this.jwt()).map((response: Response) => response.json());
    }

    create(user: User) {
        return this.http.post('http://192.168.0.67:8080/DynamicServices/processLogin', user, this.jwt()).map((response: Response) => response.json());
    }

    update(user: User) {
        return this.http.put('https://reqres.in/api/register' + user.id, user, this.jwt()).map((response: Response) => response.json());
    }

    delete(id: number) {
        return this.http.delete('/api/users/' + id, this.jwt()).map((response: Response) => response.json());
    }

    // private helper methods

    private jwt() {
        // create authorization header with jwt token
       /* let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser) {
            let headers = new Headers();
            //headers: headers,{ 'Authorization': 'Bearer ' + currentUser.token }

        }*/
        return new RequestOptions({ withCredentials: true });
    }
}