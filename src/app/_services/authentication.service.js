var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
require('rxjs/add/operator/map');
var AuthenticationService = (function () {
    function AuthenticationService(http, httpService) {
        this.http = http;
        this.httpService = httpService;
    }
    AuthenticationService.prototype.login = function (username, password) {
        return this.httpService.post('processLogin', { UserName: username, Password: password });
    };
    AuthenticationService.prototype.logout = function () {
        // remove user from local storage to log user out
        return this.httpService.get('logout').subscribe(function (data) {
            localStorage.removeItem('currentUser');
            localStorage.removeItem('currentMenu');
        }, function (error) {
            localStorage.removeItem('currentUser');
            localStorage.removeItem('currentMenu');
        });
    };
    AuthenticationService = __decorate([
        core_1.Injectable()
    ], AuthenticationService);
    return AuthenticationService;
})();
exports.AuthenticationService = AuthenticationService;
//# sourceMappingURL=authentication.service.js.map