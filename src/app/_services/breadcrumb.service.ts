import { EventEmitter,Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { MenuItem } from 'primeng/primeng';

@Injectable()
export class BreadcrumbService {
    public breadcrumbItem:  EventEmitter<MenuItem[]> = new EventEmitter<MenuItem[]>();
    private itemBreadcrums: MenuItem[];
    items: MenuItem[] = [];

    constructor() {
    }

    getBreadcrumb(): MenuItem[] {
        return this.items;
    }

    setBreadcrumb(breadcrumb: MenuItem[]): void {
        this.items = breadcrumb;
        this.breadcrumbItem.next(this.items);
    }
    private getBreadcrumsLink(page: string): MenuItem[] {
        this.itemBreadcrums = [];
        //let item: BreadcrumItem = this.getsearchMoviePage();

        switch (page) {
            case 'dashboard':
                this.itemBreadcrums.push({ label: '' });
                break;
            case 'searchMovie':
                this.itemBreadcrums.push({ label: 'Dashboard', routerLink: ['dashboard/home'] });
                this.itemBreadcrums.push({ label: 'Search Movie' });
                break;
            case 'speechSearchMovie':
                this.itemBreadcrums.push({ label: 'Dashboard', routerLink: ['dashboard/home'] });
                this.itemBreadcrums.push({ label: 'Speech Search Movie' });
                break;
            default:
                this.itemBreadcrums = [];
        }
        return this.itemBreadcrums;
    }
}