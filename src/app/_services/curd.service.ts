import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {HttpService} from "./http.service";

@Injectable()
export class CurdService {

  constructor(private http:Http,private httpService:HttpService) { }
  get (data?:any){
    return this.http.get('app/_data/tableData.json',data).map(res=>{
      return res.json();
    });
  }

}
