var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var CommonService = (function () {
    function CommonService(httpService) {
        this.httpService = httpService;
    }
    /**
     *
     * @param type user
     * @returns {*}
     */
    CommonService.prototype.getDropdown = function (type) {
        return this.httpService.get('GetDropdownData/' + type);
        //return this.http.get('http://192.168.0.67:8080/DynamicServices/testSession',this.jwt()).map((response: Response) => response.json());
    };
    CommonService.prototype.getDropdownOptions = function (data, label, value, defaultValue) {
        var result = [];
        if (!defaultValue) {
            result.push({ label: 'Select', value: null });
        }
        data.forEach(function (item) {
            var obj = {};
            if (label) {
                obj.label = item[label];
            }
            else {
                obj.label = item['name'];
            }
            if (value) {
                obj.value = item[value];
            }
            else {
                obj.value = item['pk_id'];
            }
            result.push(obj);
        });
        return result;
    };
    CommonService = __decorate([
        core_1.Injectable()
    ], CommonService);
    return CommonService;
})();
exports.CommonService = CommonService;
//# sourceMappingURL=common.service.js.map