var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var CurdService = (function () {
    function CurdService(http, httpService) {
        this.http = http;
        this.httpService = httpService;
    }
    CurdService.prototype.get = function (data) {
        return this.http.get('app/_data/tableData.json', data).map(function (res) {
            return res.json();
        });
    };
    CurdService = __decorate([
        core_1.Injectable()
    ], CurdService);
    return CurdService;
})();
exports.CurdService = CurdService;
//# sourceMappingURL=curd.service.js.map