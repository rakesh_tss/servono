import { Injectable } from '@angular/core';
import {HttpService} from "./http.service";

@Injectable()
export class MenuService{

  constructor(private httpService:HttpService) { }
  getDropdownUserData (type:string){
    return this.httpService.get('GetDropdownData/'+type);
    //return this.http.get('http://192.168.0.67:8080/DynamicServices/testSession',this.jwt()).map((response: Response) => response.json());
  }
}
