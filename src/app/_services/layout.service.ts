import { Injectable } from '@angular/core';
import {HttpService} from "./http.service";
import {
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
    NavigationCancel,
    NavigationError,
    Router,
    ActivatedRoute
} from '@angular/router';
import {LocalStorageService} from "./local-storage.service";

@Injectable()
export class LayoutService {

  constructor(private httpService:HttpService,private router:Router, private localStorage:LocalStorageService) { }
  n:number = 0;
  inc(){
    this.n++;
  }
  public menuObject:any;
  public menuItems:any;
  public menuGroup:any;
  public menuReset(){
    //this.showBreadCrumbModuleLink = false;
    this.httpService.get('GetRoleMenu').subscribe(res=>{
      if(res){
        this.menuObject = this.setMenuFormat(res.data);
        if(this.localStorage.getItem('currentMenu')){
          let currentMenu = JSON.parse(this.localStorage.getItem('currentMenu'));
          if (this.menuObject.length > 1) {
            //this.getMenuModuleList();
            //this.showBreadCrumbModuleLink = true;
            //this.moduleDropDownShow = true;
          }
          this.menuItems = currentMenu.menu;
          this.menuGroup = currentMenu.group_name;
          this.menuObject.forEach(item=>{
            if(item.group_name==currentMenu.group_name){
              this.menuItems = item.menu;
              console.log('this.menuItems',this.menuItems);
              this.localStorage.setItem('currentMenu', JSON.stringify(item));
            }
          })
        }else{
          if( this.menuObject.length>1){
            document.querySelector("body").classList.remove('sidebar-nav');
            this.router.navigate(['Modules']);
          }else{
            document.querySelector("body").classList.add('sidebar-nav');
            this.setMenu( this.menuObject[0]);
          }
        }

        /*this.menuObject = this.currentUser.MenuObject;

         let currentMenu = JSON.parse(localStorage.getItem('currentMenu'));
         this.showBreadCrumbModuleLink = false;
         if (currentMenu) {
         this.menuItems = currentMenu.menu;
         this.menuGroup = currentMenu.group_name;
         }
         if (this.menuObject.length > 1 && currentMenu) {

         this.showBreadCrumbModuleLink = true;
         this.moduleDropDownShow = true;
         }
         */
        //this.getMenuModuleList();

      }
    })
  }
  public setMenuFormat(menuModule) {
    let myMenu = [];

    menuModule.forEach((data)=> {
      let module:any = data.data;
      module.menu = [];
      data.menu.forEach((menuItem)=> {
        let menuObj:any = {};
        if (menuItem.list.length > 1) {
          menuObj = menuItem.data;
          menuItem.list.forEach((subItem)=> {
            let obj:any = {};
            obj.url = this.setUrl(subItem, menuItem, module);
            Object.assign(subItem, obj);
          });
          menuObj.sub = menuItem.list;
        } else {
          let singleItem:any = menuItem.list[0];
          Object.assign(singleItem, menuItem.data);
          singleItem.url = this.setUrl(singleItem, menuItem, module);
          menuObj = singleItem;
        }
        module.menu.push(menuObj);
      });
      myMenu.push(module);
    });
    return myMenu;
  }
  setUrl(menuItem, parentMenu, module) {
    let link:string;
    if (menuItem) {
      if (menuItem.type == 'AUTOMATIC') {
        link = 'tss/' + module.group_name + '/' + parentMenu.data.name + '/' + menuItem.module;
      } else {
        link = 'Custom/' + menuItem.module;
      }
    }
    return link;
  }

  public setMenu(menuItem:any) {
    this.localStorage.setItem('currentMenu', JSON.stringify(menuItem));
    this.menuItems = menuItem.menu;
    this.menuGroup = menuItem.group_name;
    let link:string;

    if (this.menuItems[0].sub) {
      link = this.menuItems[0].sub[0].url;
    } else {
      link = this.menuItems[0].url;
    }
    this.router.navigate([link]);
  }
}
