import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import {HttpService} from "./http.service";
import {LocalStorageService} from "./local-storage.service";

@Injectable()
export class AuthenticationService{
    constructor(private http: Http,private httpService:HttpService,private localStorage:LocalStorageService) {
    }
    login(username: string, password: string) {
         return this.httpService.post('processLogin',{ UserName : username, Password : password });
    }

    logout() {
        // remove user from local storage to log user out
       return this.httpService.get('logout').subscribe(
            data => {
                this.localStorage.removeItem('currentUser');
                this.localStorage.removeItem('currentMenu');
            },
            error => {
                this.localStorage.removeItem('currentUser');
                this.localStorage.removeItem('currentMenu');
            });
    }
}