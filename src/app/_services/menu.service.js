var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var MenuService = (function () {
    function MenuService(httpService) {
        this.httpService = httpService;
    }
    MenuService.prototype.getDropdownUserData = function (type) {
        return this.httpService.get('GetDropdownData/' + type);
        //return this.http.get('http://192.168.0.67:8080/DynamicServices/testSession',this.jwt()).map((response: Response) => response.json());
    };
    MenuService = __decorate([
        core_1.Injectable()
    ], MenuService);
    return MenuService;
})();
exports.MenuService = MenuService;
//# sourceMappingURL=menu.service.js.map