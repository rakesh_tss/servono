import { Injectable } from '@angular/core';

@Injectable()
export class PermissionService {

  constructor() { }
  permission:any = {
    View:false,Create:false,Edit:false,Delete:false
  };
  get(){
    return this.permission;
  }
  set(data){
    this.permission = data;
  }
}
