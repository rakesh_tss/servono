import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import {HttpService} from "./http.service";
import {LocalStorageService} from "./local-storage.service";

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router,private httpService:HttpService, private localStorage:LocalStorageService ) {}

    canActivate() {

        if (this.localStorage.getItem('currentUser')) {
            /*this.httpService.get('CheckSession').subscribe(res=>{
                if(res){
                }
            });*/
            return true;
        }else{
            this.router.navigate(['/login']);
            return false;
        }
    }
}

