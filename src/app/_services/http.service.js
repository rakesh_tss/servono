var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/operator/map');
require('rxjs/add/operator/catch');
var CryptoJS = require("crypto-js");
var login_modal_component_1 = require("../_components/login-modal/login-modal.component");
//import CryptoJS = require("crypto-js");
//import CryptoJS = require('crypto-js');
//import CryptoJS = require('crypto-js');
var HttpService = (function () {
    function HttpService(http, router, appConfig, toasterService, modalService) {
        this.http = http;
        this.router = router;
        this.appConfig = appConfig;
        this.toasterService = toasterService;
        this.modalService = modalService;
        this.apiUrl = this.appConfig.getConfig('apiUrl'); //environment.apiUrl;
        this.encrypt = this.appConfig.getConfig('encrypt'); //environment.apiUrl;
        this.pendingRequestsNumber = 0;
        //public CryptoJS = require("crypto-js");
        this.key = '#base64Key#12345';
        this.keyEnyDec = CryptoJS.enc.Base64.parse(btoa(this.key));
        this.enycOptions = { mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7 };
        this.isLoginModalOpen = false;
    }
    HttpService.prototype.post = function (url, data) {
        var _this = this;
        this.pendingRequestsNumber++;
        var param = {};
        if (this.encrypt) {
            param.requestParams = CryptoJS.AES.encrypt(JSON.stringify(data), this.keyEnyDec, this.enycOptions).toString();
        }
        else {
            param = data;
        }
        return this.http.post(this.apiUrl + url, param, this.jwt()).map(function (res) {
            _this.pendingRequestsNumber--;
            return _this.resStatus(res);
        }, function (err) {
            _this.pendingRequestsNumber--;
        }).catch(function (e) {
            if (e.status === 401) {
                _this.errorUnauthorized(e.json());
                return Observable_1.Observable.throw('Unauthorized');
            }
            _this.pendingRequestsNumber--;
            //return Observable.throw('error');
            _this.resStatus(e);
            return Observable_1.Observable.throw('Unauthorized');
            // do any other checking for statuses here
        });
    };
    HttpService.prototype.get = function (url, data) {
        var _this = this;
        this.pendingRequestsNumber++;
        var params = new http_1.URLSearchParams();
        for (var key in data) {
            params.set(key, data[key]);
        }
        var param = {};
        param = data;
        var options = new http_1.RequestOptions({
            withCredentials: true,
            // Have to make a URLSearchParams with a query string
            search: param
        });
        return this.http.get(this.apiUrl + url, options).map(function (res) {
            _this.pendingRequestsNumber--;
            return _this.resStatus(res);
        }, function (err) {
            _this.pendingRequestsNumber--;
        }).catch(function (e) {
            _this.pendingRequestsNumber--;
            if (e.status === 401) {
                _this.errorUnauthorized(e.json());
                return Observable_1.Observable.throw('Unauthorized');
            }
            if (e.status === 0) {
                return Observable_1.Observable.throw('Unauthorized');
            }
            return Observable_1.Observable.throw('connection');
            // do any other checking for statuses here
        });
    };
    HttpService.prototype.getTable = function (url, data) {
        var _this = this;
        this.pendingRequestsNumber++;
        var params = new http_1.URLSearchParams();
        if (data.first) {
            var count = (data.first / data.rows) + 1;
            params.set('page', count.toString());
            params.set('rows', data.rows);
        }
        if (data.sortField) {
            params.set('sidx', data.sortField);
            params.set('sord', (data.sortOrder == 1) ? 'asc' : 'desc');
        }
        for (var key in data) {
            params.set(key, data[key]);
        }
        var param = {};
        if (this.encrypt) {
            param.requestParams = CryptoJS.AES.encrypt(JSON.stringify(params), this.keyEnyDec, this.enycOptions).toString();
        }
        else {
            param = params;
        }
        var options = new http_1.RequestOptions({
            withCredentials: true,
            // Have to make a URLSearchParams with a query string
            search: param
        });
        return this.http.get(this.apiUrl + url, options).map(function (res) {
            _this.pendingRequestsNumber--;
            return _this.resStatus(res);
        }, function (err) {
            _this.pendingRequestsNumber--;
        }).catch(function (e) {
            _this.pendingRequestsNumber--;
            if (e.status === 401) {
                _this.errorUnauthorized(e.json());
                return Observable_1.Observable.throw('Unauthorized');
            }
            return Observable_1.Observable.throw('connection');
            // do any other checking for statuses here
        });
    };
    HttpService.prototype.getMap = function (url, data) {
        var params = new http_1.URLSearchParams();
        for (var key in data) {
            params.set(key, data[key]);
        }
        var options = new http_1.RequestOptions({
            withCredentials: true,
            // Have to make a URLSearchParams with a query string
            search: params
        });
        return this.http.get(this.apiUrl + url, options);
    };
    HttpService.prototype.postTable = function (url, data) {
        var _this = this;
        this.pendingRequestsNumber++;
        if (data.first) {
            var count = (data.first / data.rows) + 1;
            data.page = count.toString();
            data.rows = data.rows;
        }
        if (data.sortField) {
            data.sidx = data.sortField;
            data.sord = (data.sortOrder == 1) ? 'asc' : 'desc';
        }
        if (this.encrypt) {
            var tempData = {};
            tempData.requestParams = CryptoJS.AES.encrypt(JSON.stringify(data), this.keyEnyDec, this.enycOptions).toString();
            data = tempData;
        }
        return this.http.post(this.apiUrl + url, data, this.jwt()).map(function (res) {
            _this.pendingRequestsNumber--;
            return _this.resStatus(res);
        }, function (err) {
            _this.pendingRequestsNumber--;
        }).catch(function (e) {
            _this.pendingRequestsNumber--;
            if (e.status === 401) {
                _this.errorUnauthorized(e.json());
                return Observable_1.Observable.throw('Unauthorized');
            }
            return Observable_1.Observable.throw('connection');
            // do any other checking for statuses here
        });
    };
    HttpService.prototype.delete = function (url, data) {
    };
    HttpService.prototype.put = function (url, data) {
    };
    HttpService.prototype.errorUnauthorized = function (err) {
        console.log('err', err);
        this.toasterService.pop('error', 'Error', err.message);
        this.popupLoin();
        //this.router.navigate(['login']);
    };
    HttpService.prototype.resStatus = function (res) {
        var resData = null;
        if (this.encrypt) {
            resData = res.json();
            resData = JSON.parse(CryptoJS.AES.decrypt(resData.resData, this.keyEnyDec, this.enycOptions).toString(CryptoJS.enc.Utf8));
        }
        else {
            resData = res.json();
        }
        if (resData.success && resData.message) {
            this.toasterService.pop('success', 'success', resData.message);
        }
        if (res.status < 200 || res.status >= 300) {
            if (res.status == 401) {
                this.popupLoin();
            }
            this.toasterService.pop('error', '', resData.message);
            return false;
        }
        if (res.status == 200) {
            if (resData.success) {
                return resData;
            }
            else {
                this.toasterService.pop('error', 'Error', resData.message);
                if (resData.data && resData.data.errors) {
                    for (var key in resData.data.errors) {
                        //console.log('resData.data.errors[key].ErrorMessage',key+':'+resData.data.errors[key].ErrorMessage);
                        if (resData.data.errors[key].ErrorMessage) {
                            this.toasterService.pop('info', key, key + ' : ' + resData.data.errors[key].ErrorMessage);
                        }
                    }
                }
                if (resData.data && resData.data.UniqueValidationFails) {
                    this.toasterService.pop('info', '', resData.data['UniqueValidationFails']);
                }
                return false;
            }
        }
    };
    HttpService.prototype.jwt = function () {
        return new http_1.RequestOptions({ withCredentials: true });
    };
    HttpService.prototype.popupLoin = function () {
        var _this = this;
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (!this.isLoginModalOpen && currentUser) {
            this.isLoginModalOpen = true;
            this.reLoginModal = this.modalService.open(login_modal_component_1.LoginModalComponent, {
                windowClass: 'login-popup',
                keyboard: false,
                backdrop: 'static'
            });
            this.reLoginModal.componentInstance.httpService = this;
            this.reLoginModal.componentInstance.reLoginObj = {
                UserName: currentUser.UserName
            };
            this.reLoginModal.result.then(function (result) {
                _this.isLoginModalOpen = false;
                //console.log('close');
            }, function (reason) {
                _this.isLoginModalOpen = false;
                //console.log('reason close');
            });
        }
        else {
            if (this.reLoginModal) {
                this.reLoginModal.close();
            }
            this.router.navigate(['login']);
        }
    };
    __decorate([
        core_1.ViewChild('reLoginContent')
    ], HttpService.prototype, "reLoginContent");
    HttpService = __decorate([
        core_1.Injectable()
    ], HttpService);
    return HttpService;
})();
exports.HttpService = HttpService;
//# sourceMappingURL=http.service.js.map