import { Injectable } from '@angular/core';
import {AppConfig} from "../app.config";

@Injectable()
export class LocalStorageService {
  constructor(private appConfig:AppConfig) { }
    loginKey:any = this.appConfig.getConfig('loginUser');
   getItem(key){
    return localStorage.getItem(this.loginKey+'_'+key);
  }
   setItem(key,data) {
       localStorage.setItem(this.loginKey+'_'+key,data);
  }
    removeItem(key) {
        localStorage.removeItem(this.loginKey+'_'+key);
    }
}
