var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var BreadcrumbService = (function () {
    function BreadcrumbService() {
        this.breadcrumbItem = new core_1.EventEmitter();
        this.items = [];
    }
    BreadcrumbService.prototype.getBreadcrumb = function () {
        return this.items;
    };
    BreadcrumbService.prototype.setBreadcrumb = function (breadcrumb) {
        this.items = breadcrumb;
        this.breadcrumbItem.next(this.items);
    };
    BreadcrumbService.prototype.getBreadcrumsLink = function (page) {
        this.itemBreadcrums = [];
        //let item: BreadcrumItem = this.getsearchMoviePage();
        switch (page) {
            case 'dashboard':
                this.itemBreadcrums.push({ label: '' });
                break;
            case 'searchMovie':
                this.itemBreadcrums.push({ label: 'Dashboard', routerLink: ['dashboard/home'] });
                this.itemBreadcrums.push({ label: 'Search Movie' });
                break;
            case 'speechSearchMovie':
                this.itemBreadcrums.push({ label: 'Dashboard', routerLink: ['dashboard/home'] });
                this.itemBreadcrums.push({ label: 'Speech Search Movie' });
                break;
            default:
                this.itemBreadcrums = [];
        }
        return this.itemBreadcrums;
    };
    BreadcrumbService = __decorate([
        core_1.Injectable()
    ], BreadcrumbService);
    return BreadcrumbService;
})();
exports.BreadcrumbService = BreadcrumbService;
//# sourceMappingURL=breadcrumb.service.js.map