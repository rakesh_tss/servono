import { Injectable } from '@angular/core';
import {HttpService} from "./http.service";
import {LocalStorageService} from "./local-storage.service";

@Injectable()
export class CommonService {

    constructor(private httpService:HttpService,private localStorage:LocalStorageService) {
    }

    /**
     *
     * @param type user
     * @returns {*}
     */
    getDropdown(type:string) {
        return this.httpService.get('GetDropdownData/' + type);
        //return this.http.get('http://192.168.0.67:8080/DynamicServices/testSession',this.jwt()).map((response: Response) => response.json());
    }

    getDropdownOptions(data:any,label?:string,value?:string,defaultValue?:boolean) {
        let result = [];
        if(!defaultValue){
            result.push({label:'Select',value:null})
        }

        data.forEach((item)=> {
            let obj:any = {};
            if(label){
                obj.label = item[label];
            }else{
                obj.label = item['name'];
            }

            if(value){
                obj.value = item[value];
            }else{
                obj.value = item['pk_id'];
            }

            result.push(obj);
        });
        return result;

    }
    getUser(){
        let currentUser = JSON.parse(this.localStorage.getItem('currentUser'));

        return  currentUser;
    }

}
