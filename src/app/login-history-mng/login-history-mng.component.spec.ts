import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginHistoryMngComponent } from './login-history-mng.component';

describe('LoginHistoryMngComponent', () => {
  let component: LoginHistoryMngComponent;
  let fixture: ComponentFixture<LoginHistoryMngComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginHistoryMngComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginHistoryMngComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
