var testing_1 = require('@angular/core/testing');
var login_history_mng_component_1 = require('./login-history-mng.component');
describe('LoginHistoryMngComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [login_history_mng_component_1.LoginHistoryMngComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(login_history_mng_component_1.LoginHistoryMngComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=login-history-mng.component.spec.js.map