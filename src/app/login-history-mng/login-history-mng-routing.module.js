var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var login_history_mng_component_1 = require("./login-history-mng.component");
var routes = [
    {
        path: '',
        component: login_history_mng_component_1.LoginHistoryMngComponent
    }
];
var LoginHistoryMngRoutingModule = (function () {
    function LoginHistoryMngRoutingModule() {
    }
    LoginHistoryMngRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule],
            providers: []
        })
    ], LoginHistoryMngRoutingModule);
    return LoginHistoryMngRoutingModule;
})();
exports.LoginHistoryMngRoutingModule = LoginHistoryMngRoutingModule;
//# sourceMappingURL=login-history-mng-routing.module.js.map