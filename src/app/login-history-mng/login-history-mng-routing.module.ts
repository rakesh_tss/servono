import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginHistoryMngComponent} from "./login-history-mng.component";

const routes: Routes = [
  {
    path:'',
    component:LoginHistoryMngComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class LoginHistoryMngRoutingModule { }
