import { Component, OnInit } from '@angular/core';
import {BreadcrumbService} from "../_services/breadcrumb.service";
import {LazyLoadEvent} from "primeng/primeng";
import {HttpService} from "../_services/http.service";

@Component({
    selector: 'app-login-history-mng',
    templateUrl: './login-history-mng.component.html',
    styleUrls: ['./login-history-mng.component.css']
})
export class LoginHistoryMngComponent implements OnInit {

    constructor(private breadcrumbService:BreadcrumbService,private httpService:HttpService) {

    }

    logDateList:any[] = [];
    dataRows:any[];
    dataRow:any = {};
    totalRecords:number;
    refreshTable:boolean = true;
    menuFormObj:any = {};
    selectedDate:String;
    ngOnInit():void {
        this.breadcrumbService.setBreadcrumb([{label: 'Login Histories'}]);

     /*   this.httpService.get('GetLogDatesForFilter/1').subscribe(
            res=> {
                if (res) {
                    res.data.forEach((data)=>{
                        let obj:any = {};
                        obj.value = data.log_tbl_date;
                        obj.label = data.start_date_name;
                        this.logDateList.push(obj);
                    })
                    if(this.logDateList.length>0){
                        this.selectedDate = this.logDateList[0].value;
                        this.tableReload();
                    }
                }
            }
        );*/
    }
    loadDataLazy(event:LazyLoadEvent) {
        let param:any = event;
       // param.type = 1;
       // param.date = this.selectedDate;
        this.httpService.postTable('GetAllLoginHistoryBySearch', event).subscribe(
            res=> {
                if(res){
                    this.dataRows = res.jqGridData.rows;
                    this.totalRecords = res.jqGridData.records;
                }
            }
        );
    }
    tableReload(){
        this.refreshTable = false;
        setTimeout(() => this.refreshTable = true, 100);
    }
}
