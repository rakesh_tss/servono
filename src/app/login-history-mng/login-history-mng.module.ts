import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginHistoryMngRoutingModule } from './login-history-mng-routing.module';
import {CommonTssModule} from "../shared/commonTssModules";
import {LoginHistoryMngComponent} from "./login-history-mng.component";

@NgModule({
  imports: [
    CommonTssModule,
    LoginHistoryMngRoutingModule
  ],
  declarations: [
    LoginHistoryMngComponent
  ]
})
export class LoginHistoryMngModule { }
