var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var LoginHistoryMngComponent = (function () {
    function LoginHistoryMngComponent(breadcrumbService, httpService) {
        this.breadcrumbService = breadcrumbService;
        this.httpService = httpService;
        this.logDateList = [];
        this.dataRow = {};
        this.refreshTable = false;
        this.menuFormObj = {};
    }
    LoginHistoryMngComponent.prototype.ngOnInit = function () {
        this.breadcrumbService.setBreadcrumb([{ label: 'Login Histories' }]);
        /*   this.httpService.get('GetLogDatesForFilter/1').subscribe(
               res=> {
                   if (res) {
                       res.data.forEach((data)=>{
                           let obj:any = {};
                           obj.value = data.log_tbl_date;
                           obj.label = data.start_date_name;
                           this.logDateList.push(obj);
                       })
                       if(this.logDateList.length>0){
                           this.selectedDate = this.logDateList[0].value;
                           this.tableReload();
                       }
                   }
               }
           );*/
    };
    LoginHistoryMngComponent.prototype.loadDataLazy = function (event) {
        var _this = this;
        var param = event;
        // param.type = 1;
        // param.date = this.selectedDate;
        this.httpService.postTable('GetAllLoginHistoryBySearch', event).subscribe(function (res) {
            if (res) {
                _this.dataRows = res.jqGridData.rows;
                _this.totalRecords = res.jqGridData.records;
            }
        });
    };
    LoginHistoryMngComponent.prototype.tableReload = function () {
        var _this = this;
        this.refreshTable = false;
        setTimeout(function () { return _this.refreshTable = true; }, 100);
    };
    LoginHistoryMngComponent = __decorate([
        core_1.Component({
            selector: 'app-login-history-mng',
            templateUrl: './login-history-mng.component.html',
            styleUrls: ['./login-history-mng.component.css']
        })
    ], LoginHistoryMngComponent);
    return LoginHistoryMngComponent;
})();
exports.LoginHistoryMngComponent = LoginHistoryMngComponent;
//# sourceMappingURL=login-history-mng.component.js.map