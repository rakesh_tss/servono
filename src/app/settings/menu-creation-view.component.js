var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var MenuCreationViewComponent = (function () {
    function MenuCreationViewComponent() {
        this.moduleList = [];
        this.module_name = '';
        this.menu_name = '';
    }
    MenuCreationViewComponent.prototype.ngOnInit = function () {
    };
    MenuCreationViewComponent.prototype.createNewModule = function (moduleName) {
        var obj = {};
        obj.module_name = moduleName;
        obj.module_menu = [];
        this.moduleList.push(obj);
        this.module_name = '';
    };
    MenuCreationViewComponent.prototype.createModuleMenu = function (module, menuName) {
        var obj = {};
        obj.menu_name = menuName;
        obj.sub_menu = [];
        this.menu_name = '';
        module.module_menu.push(obj);
    };
    MenuCreationViewComponent = __decorate([
        core_1.Component({
            selector: 'app-menu-creation-view',
            templateUrl: './menu-creation-view.component.html',
            styleUrls: ['./menu-creation-view.component.scss']
        })
    ], MenuCreationViewComponent);
    return MenuCreationViewComponent;
})();
exports.MenuCreationViewComponent = MenuCreationViewComponent;
//# sourceMappingURL=menu-creation-view.component.js.map