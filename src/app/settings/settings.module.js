var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// Module
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var settings_routing_module_1 = require('./settings-routing.module');
var primeng_1 = require("primeng/primeng");
//components
var menu_management_component_1 = require("./menu-management.component");
var settings_component_1 = require("./settings.component");
var menu_creation_view_component_1 = require("./menu-creation-view.component");
var SettingsModule = (function () {
    function SettingsModule() {
    }
    SettingsModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                http_1.HttpModule,
                settings_routing_module_1.SettingsRoutingModule,
                primeng_1.DataTableModule,
                primeng_1.MultiSelectModule,
                primeng_1.DialogModule,
                primeng_1.ButtonModule,
                primeng_1.CalendarModule,
                primeng_1.DropdownModule,
                primeng_1.GrowlModule,
                primeng_1.TabViewModule,
                primeng_1.OrderListModule,
                primeng_1.PanelModule
            ],
            declarations: [
                menu_management_component_1.MenuManagementComponent,
                menu_creation_view_component_1.MenuCreationViewComponent,
                settings_component_1.SettingsComponent
            ]
        })
    ], SettingsModule);
    return SettingsModule;
})();
exports.SettingsModule = SettingsModule;
//# sourceMappingURL=settings.module.js.map