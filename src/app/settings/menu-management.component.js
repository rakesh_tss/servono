var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var common_service_1 = require("../_services/common.service");
var MenuManagementComponent = (function () {
    function MenuManagementComponent(httpService, commonService, router) {
        this.httpService = httpService;
        this.commonService = commonService;
        this.router = router;
        this.dataRow = {};
        this.menuFormObj = {};
        this.msgs = [];
        this.userTypeList = [];
    }
    MenuManagementComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.commonService.getDropdown('user types').subscribe(function (res) {
            for (var key in res.data) {
                var obj = {};
                obj.value = res.data[key].app_user_type_id;
                obj.label = res.data[key].user_type;
                _this.userTypeList.push(obj);
            }
        });
    };
    MenuManagementComponent.prototype.loadDataLazy = function (event) {
        var _this = this;
        this.httpService.getTable('getDataListView/Default/Settings/Menu', event).subscribe(function (res) {
            _this.dataRows = res.jqGridData.rows;
            _this.totalRecords = res.jqGridData.records;
        });
    };
    MenuManagementComponent.prototype.onCreate = function () {
        this.dialogTitle = 'Create Menu';
        this.dataRow = {};
        this.displayDialog = true;
    };
    MenuManagementComponent.prototype.onUpdate = function (row) {
        this.dialogTitle = 'Update Menu';
        this.dataRow = row;
        this.displayDialog = true;
    };
    MenuManagementComponent.prototype.onView = function (row) {
        //this.msgs = [];
        // this.msgs.push({severity:'info', summary:'Success', detail:'Data Updated'});
        this.router.navigate(['/Settings/Menus/Edit/' + row.app_menu_id]);
    };
    MenuManagementComponent.prototype.onDelete = function (row) {
    };
    MenuManagementComponent.prototype.dialogClose = function () {
        this.displayDialog = false;
    };
    MenuManagementComponent = __decorate([
        core_1.Component({
            selector: 'app--menu-management',
            templateUrl: './menu-management.component.html',
            styleUrls: ['./menu-management.component.scss'],
            providers: [common_service_1.CommonService]
        })
    ], MenuManagementComponent);
    return MenuManagementComponent;
})();
exports.MenuManagementComponent = MenuManagementComponent;
//# sourceMappingURL=menu-management.component.js.map