import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu-creation-view',
  templateUrl: './menu-creation-view.component.html',
  styleUrls: ['./menu-creation-view.component.scss']
})
export class MenuCreationViewComponent implements OnInit {

  constructor() { }
  moduleList:any[] = [];
  module_name:string = '';
  menu_name:string = '';
  ngOnInit() {
  }

  createNewModule(moduleName){

    let obj:any = {} ;
    obj.module_name = moduleName;
    obj.module_menu = [];
    this.moduleList.push(obj);
    this.module_name = '';
  }

  createModuleMenu(module,menuName){
    let obj:any = {} ;
    obj.menu_name = menuName;
    obj.sub_menu = [];
    this.menu_name = '';
    module.module_menu.push(obj);
  }

}
