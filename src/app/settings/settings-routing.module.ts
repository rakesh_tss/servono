import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MenuManagementComponent} from "./menu-management.component";
import {SettingsComponent} from "./settings.component";
import {MenuCreationViewComponent} from "./menu-creation-view.component";

const routes:Routes = [{
    path: '',
    data: {
        title: 'Settings'
    },
    children: [
        {
            path: 'Settings',
            component: SettingsComponent,
            data: {
                title: 'Settings'
            }
        },
        {
            path: 'Menus',
            children: [
                {
                    path: '',
                    component: MenuManagementComponent,
                    data: {
                        title: 'Menu'
                    }
                },
                {
                    path: 'Edit/:menuId',
                    component: MenuCreationViewComponent,
                    data: {
                        title: 'Edit'
                    }
                }
            ]
        }
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class SettingsRoutingModule {

}
