import { Component, OnInit } from '@angular/core';
import {HttpService} from "../_services/http.service";
import {CommonService} from "../_services/common.service";
import { Router } from '@angular/router';
import {Message,DataTableModule,SharedModule,LazyLoadEvent,SelectItem} from 'primeng/primeng';

@Component({
    selector: 'app--menu-management',
    templateUrl: './menu-management.component.html',
    styleUrls: ['./menu-management.component.scss'],
    providers:[CommonService]
})
export class MenuManagementComponent implements OnInit {
    displayDialog: boolean;
    dataRows: any[];
    dataRow:any = {};
    totalRecords: number;
    menuFormObj:any = {};
    msgs: Message[] = [];
    constructor(private httpService:HttpService,
                private commonService:CommonService,private router:Router) {

    }
    userTypeList:any[] = [];
    jqGridData;
    ngOnInit():void {
        this.commonService.getDropdown('user types').subscribe(
            res=>{
                for(let key in res.data){
                    let obj:any ={};
                    obj.value = res.data[key].app_user_type_id;
                    obj.label = res.data[key].user_type;
                    this.userTypeList.push(obj);
                }
            }
        );
    }
    loadDataLazy(event: LazyLoadEvent) {
        this.httpService.getTable('getDataListView/Default/Settings/Menu',event).subscribe(
            res=>{
                this.dataRows = res.jqGridData.rows;
                this.totalRecords = res.jqGridData.records;
            }
        );
    }

    dialogTitle:string;
    onCreate():void {
        this.dialogTitle = 'Create Menu';
        this.dataRow = {};
        this.displayDialog = true;
    }
    onUpdate(row):void {
        this.dialogTitle = 'Update Menu';
        this.dataRow = row;
        this.displayDialog = true;
    }

    onView(row):void{
        //this.msgs = [];
       // this.msgs.push({severity:'info', summary:'Success', detail:'Data Updated'});
        this.router.navigate(['/Settings/Menus/Edit/'+row.app_menu_id]);
    }

    onDelete(row):void {

    }
    dialogClose():void{
        this.displayDialog = false;
    }


}
