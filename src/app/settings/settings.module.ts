// Module
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }  from '@angular/forms';
import { HttpModule } from '@angular/http';
import { SettingsRoutingModule } from './settings-routing.module';
import {DataTableModule,
    MultiSelectModule,
    DialogModule,
    ButtonModule,
    CalendarModule,
    GrowlModule,
    DropdownModule,
    TabViewModule,
    OrderListModule,
    PanelModule} from "primeng/primeng";

//components
import {MenuManagementComponent} from "./menu-management.component";
import {SettingsComponent} from "./settings.component";
import {MenuCreationViewComponent} from "./menu-creation-view.component";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    SettingsRoutingModule,
    DataTableModule,
    MultiSelectModule,
    DialogModule,
    ButtonModule,
    CalendarModule,
    DropdownModule,
    GrowlModule,
    TabViewModule,
    OrderListModule,
    PanelModule
  ],
  declarations: [
      MenuManagementComponent,
      MenuCreationViewComponent,
      SettingsComponent
  ]
})
export class SettingsModule { }
