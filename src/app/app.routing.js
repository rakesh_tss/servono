var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var auth_guard_1 = require("./_services/auth.guard");
//Layouts
var full_layout_component_1 = require('./layouts/full-layout.component');
var simple_layout_component_1 = require("./layouts/simple-layout.component");
var _404_component_1 = require("./pages/404.component");
exports.routes = [
    {
        path: '',
        component: full_layout_component_1.FullLayoutComponent,
        canActivate: [auth_guard_1.AuthGuard],
        data: {
            title: 'Home'
        },
        children: [
            {
                path: 'documents',
                loadChildren: 'app/documents/documents.module#DocumentsModule'
            },
            {
                path: '404',
                component: _404_component_1.p404Component
            },
            {
                path: '403',
                component: _404_component_1.p404Component
            },
            {
                path: 'Custom/Dashboards',
                loadChildren: 'app/dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'Modules',
                loadChildren: 'app/menu-module/menu-module.module#MenuModuleModule'
            },
            {
                path: 'uicomponents',
                loadChildren: 'app/ui-components/ui-components.module#UiComponentsModule'
            },
            {
                path: 'Settings',
                loadChildren: 'app/settings/settings.module#SettingsModule'
            },
            {
                path: 'Administration',
                loadChildren: 'app/administration/administration.module#AdministrationModule'
            },
            {
                path: 'tss',
                loadChildren: 'app/tss/tss.module#TssModule'
            },
            {
                path: 'Custom/Picklists',
                loadChildren: 'app/plicklists-mng/plicklists-mng.module#PlicklistsMngModule'
            },
            {
                path: 'Custom/Modules',
                loadChildren: 'app/module-mng/module-mng.module#ModuleMngModule'
            },
            {
                path: 'Custom/Menu Groups',
                loadChildren: 'app/menu-group/menu-group.module#MenuGroupModule'
            },
            {
                path: 'Custom/Menus',
                loadChildren: 'app/menu-mng/menu-mng.module#MenuMngModule'
            },
            {
                path: 'Custom/Profiles',
                loadChildren: 'app/profile-mng/profile-mng.module#ProfileMngModule'
            },
            {
                path: 'Custom/Roles',
                loadChildren: 'app/roles-mng/roles-mng.module#RolesMngModule'
            },
            {
                path: 'Custom/Sharing Rules',
                loadChildren: 'app/sharing-rules/sharing-rules.module#SharingRulesModule'
            },
            {
                path: 'Custom/User Groups',
                loadChildren: 'app/user-groups/user-groups.module#UserGroupsModule'
            },
            {
                path: 'Custom/Login Histories',
                loadChildren: 'app/login-history-mng/login-history-mng.module#LoginHistoryMngModule'
            },
            {
                path: 'Custom/Access Logs',
                loadChildren: 'app/access-log/access-log.module#AccessLogModule'
            },
            {
                path: 'Custom/Calendars',
                loadChildren: 'app/calender/calender.module#CalenderModule'
            },
            {
                path: 'Custom/Picklist Dependencies',
                loadChildren: 'app/picklist-dependencies/picklist-dependencies.module#PicklistDependenciesModule'
            },
            {
                path: 'Custom/Reports',
                loadChildren: 'app/reports-mng/reports-mng.module#ReportsMngModule'
            },
            {
                path: 'Custom/Reports/Create',
                loadChildren: 'app/reports-mng/reports-mng.module#ReportsMngModule'
            }
        ]
    },
    {
        path: '',
        component: simple_layout_component_1.SimpleLayoutComponent,
        data: {
            title: 'Pages'
        },
        children: [
            {
                path: '',
                loadChildren: 'app/pages/pages.module#PagesModule'
            }
        ]
    },
    { path: '**', redirectTo: '404' }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(exports.routes)],
            exports: [router_1.RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
})();
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app.routing.js.map