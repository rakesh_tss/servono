import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PicklistMngComponent } from './picklist-mng.component';

describe('PicklistMngComponent', () => {
  let component: PicklistMngComponent;
  let fixture: ComponentFixture<PicklistMngComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PicklistMngComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PicklistMngComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
