var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var PicklistMngComponent = (function () {
    function PicklistMngComponent(breadcrumbService, httpService, modalService) {
        this.breadcrumbService = breadcrumbService;
        this.httpService = httpService;
        this.modalService = modalService;
        this.fields = [];
        this.dataRows = [];
        this.totalRecords = 0;
        this.refreshTable = true;
        //Pick list mng modal
        //start block creation
        this.plSubmittedForm = false;
        this.parentPickList = [];
    }
    PicklistMngComponent.prototype.ngOnInit = function () {
        this.breadcrumbService.setBreadcrumb([{ label: 'Pick List Management' }]);
    };
    PicklistMngComponent.prototype.loadDataLazy = function (event) {
        var _this = this;
        this.httpService.postTable('GetAllPickListBySearch', event).subscribe(function (res) {
            if (res) {
                _this.dataRows = res.jqGridData.rows;
                _this.totalRecords = res.jqGridData.records;
            }
        });
    };
    PicklistMngComponent.prototype.tableReload = function () {
        var _this = this;
        this.refreshTable = false;
        setTimeout(function () { return _this.refreshTable = true; }, 0);
    };
    PicklistMngComponent.prototype.openPlModal = function (content, data) {
        console.log('data', data);
        this.plSubmittedForm = false;
        this.getParentPickList();
        this.plMF = new forms_1.FormGroup({
            'PickListId': new forms_1.FormControl(null),
            'Name': new forms_1.FormControl(null, forms_1.Validators.required),
            'ColorFlag': new forms_1.FormControl(null),
            'ParentId': new forms_1.FormControl(null),
            'CodeFlag': new forms_1.FormControl(null)
        });
        if (data) {
            var obj = {};
            obj.PickListId = data.pk_id;
            obj.Name = data.name;
            obj.ColorFlag = '';
            obj.ParentId = data.parent_id;
            obj.CodeFlag = '';
            this.plMF.patchValue(obj);
        }
        this.plModal = this.modalService.open(content);
    };
    PicklistMngComponent.prototype.plModalFormSubmit = function (formValue, valid) {
        var _this = this;
        this.plSubmittedForm = true;
        if (valid) {
            this.httpService.post('SaveUpdatePickList', formValue).subscribe(function (res) {
                if (res) {
                    _this.plModal.dismiss();
                    _this.plSubmittedForm = false;
                    _this.tableReload();
                }
            });
        }
    };
    PicklistMngComponent.prototype.getParentPickList = function () {
        var _this = this;
        this.parentPickList = [{ label: 'Select', value: null }];
        this.httpService.get('GetAllPickListParentsForAddEdit').subscribe(function (res) {
            if (res) {
                res.data.parents.forEach(function (item, index) {
                    var obj = {};
                    obj.label = item.name;
                    obj.value = item.pk_id;
                    _this.parentPickList.push(obj);
                });
            }
        });
    };
    PicklistMngComponent = __decorate([
        core_1.Component({
            selector: 'app-picklist-mng',
            templateUrl: './picklist-mng.component.html',
            styleUrls: ['./picklist-mng.component.css']
        })
    ], PicklistMngComponent);
    return PicklistMngComponent;
})();
exports.PicklistMngComponent = PicklistMngComponent;
//# sourceMappingURL=picklist-mng.component.js.map