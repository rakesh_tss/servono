import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlicklistsMngRoutingModule } from './plicklists-mng-routing.module';
import {CommonTssModule} from "../shared/commonTssModules";
import {CreatePicklistComponent} from "./create-picklist/create-picklist.component";
import {PicklistMngComponent} from "./picklist-mng.component";

@NgModule({
    imports: [
        CommonTssModule,
        PlicklistsMngRoutingModule
    ],
    declarations: [
        PicklistMngComponent,
        CreatePicklistComponent
    ]
})
export class PlicklistsMngModule {
}
