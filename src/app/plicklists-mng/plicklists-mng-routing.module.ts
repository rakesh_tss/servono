import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PicklistMngComponent} from "./picklist-mng.component";
import {CreatePicklistComponent} from "./create-picklist/create-picklist.component";

const routes: Routes = [
  {
    path: '',
    component: PicklistMngComponent,
    data: {
      title: 'PickList Management'
    }
  },
  {
    path: ':Id/:Name',
    component: CreatePicklistComponent,
    data: {
      title: 'PickList Management'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlicklistsMngRoutingModule { }
