var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var picklist_mng_component_1 = require("./picklist-mng.component");
var create_picklist_component_1 = require("./create-picklist/create-picklist.component");
var routes = [
    {
        path: '',
        component: picklist_mng_component_1.PicklistMngComponent,
        data: {
            title: 'PickList Management'
        }
    },
    {
        path: ':Id/:Name',
        component: create_picklist_component_1.CreatePicklistComponent,
        data: {
            title: 'PickList Management'
        }
    }
];
var PlicklistsMngRoutingModule = (function () {
    function PlicklistsMngRoutingModule() {
    }
    PlicklistsMngRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], PlicklistsMngRoutingModule);
    return PlicklistsMngRoutingModule;
})();
exports.PlicklistsMngRoutingModule = PlicklistsMngRoutingModule;
//# sourceMappingURL=plicklists-mng-routing.module.js.map