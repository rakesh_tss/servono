import { Component, OnInit } from '@angular/core';
import {LazyLoadEvent} from "primeng/primeng";
import { FormGroup, FormControl, FormBuilder, Validators,NgForm,NgModel} from '@angular/forms';
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {BreadcrumbService} from "../_services/breadcrumb.service";
import {HttpService} from "../_services/http.service";

@Component({
    selector: 'app-picklist-mng',
    templateUrl: './picklist-mng.component.html',
    styleUrls: ['./picklist-mng.component.css']
})
export class PicklistMngComponent implements OnInit {

    constructor(private breadcrumbService:BreadcrumbService, private httpService:HttpService, private modalService:NgbModal) {
    }

    fields:any = [];

    ngOnInit() {
        this.breadcrumbService.setBreadcrumb([{label: 'Pick List Management'}]);

    }

    dataRows:any = [];
    totalRecords:Number = 0;
    refreshTable:boolean = true;

    loadDataLazy(event:LazyLoadEvent) {
        this.httpService.postTable('GetAllPickListBySearch', event).subscribe(
            res=> {
                if (res) {
                    this.dataRows = res.jqGridData.rows;
                    this.totalRecords = res.jqGridData.records;
                }
            }
        );
    }

    tableReload() {
        this.refreshTable = false;
        setTimeout(() => this.refreshTable = true, 0);
    }

    //Pick list mng modal
    //start block creation
    plSubmittedForm:boolean = false;
    plMF:FormGroup;
    plModal:NgbModalRef;

    private openPlModal(content,data) {
        console.log('data',data);
        this.plSubmittedForm = false;
        //this.getParentPickList();
        this.plMF = new FormGroup({
            'PickListId':new FormControl(null),
            'Name': new FormControl(null, Validators.required),
            /*'ColorFlag': new FormControl(null),
            'ParentId': new FormControl(null),
            'CodeFlag': new FormControl(null),*/
        });

        if(data)
        {
            let obj:any = {};
            obj.PickListId = data.pk_id;
            obj.Name = data.name;
           /* obj.ColorFlag = '';
            obj.ParentId =data.parent_id;
            obj.CodeFlag = '';*/
            this.plMF.patchValue(obj);
        }
        this.plModal = this.modalService.open(content);
    }

    plModalFormSubmit(formValue, valid) {
        this.plSubmittedForm = true;
        if (valid) {
            this.httpService.post('SaveUpdatePickList', formValue).subscribe(
                res => {
                    if (res) {
                        this.plModal.dismiss();
                        this.plSubmittedForm = false;
                        this.tableReload();
                    }
                })
        }
    }

    parentPickList:any = [];

    getParentPickList() {
        this.parentPickList = [{label:'Select', value:null}];
        this.httpService.get('GetAllPickListParentsForAddEdit').subscribe(
            res => {
                if (res) {
                    res.data.parents.forEach((item, index)=> {
                        let obj:any = {};
                        obj.label = item.name;
                        obj.value = item.pk_id;
                        this.parentPickList.push(obj);
                    });
                }
            })
    }

}
