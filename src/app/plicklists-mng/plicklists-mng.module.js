var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var ng_bootstrap_1 = require('@ng-bootstrap/ng-bootstrap');
var plicklists_mng_routing_module_1 = require('./plicklists-mng-routing.module');
var commonTssModules_1 = require("../shared/commonTssModules");
var create_picklist_component_1 = require("./create-picklist/create-picklist.component");
var picklist_mng_component_1 = require("./picklist-mng.component");
var PlicklistsMngModule = (function () {
    function PlicklistsMngModule() {
    }
    PlicklistsMngModule = __decorate([
        core_1.NgModule({
            imports: [
                commonTssModules_1.CommonTssModule,
                plicklists_mng_routing_module_1.PlicklistsMngRoutingModule,
                ng_bootstrap_1.NgbModule.forRoot()
            ],
            declarations: [
                picklist_mng_component_1.PicklistMngComponent,
                create_picklist_component_1.CreatePicklistComponent
            ]
        })
    ], PlicklistsMngModule);
    return PlicklistsMngModule;
})();
exports.PlicklistsMngModule = PlicklistsMngModule;
//# sourceMappingURL=plicklists-mng.module.js.map