var testing_1 = require('@angular/core/testing');
var picklist_mng_component_1 = require('./picklist-mng.component');
describe('PicklistMngComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [picklist_mng_component_1.PicklistMngComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(picklist_mng_component_1.PicklistMngComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=picklist-mng.component.spec.js.map