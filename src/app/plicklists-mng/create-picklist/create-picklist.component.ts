import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {LazyLoadEvent,SpinnerModule} from "primeng/primeng";
import {HttpService} from "../../_services/http.service";
import {BreadcrumbService} from "../../_services/breadcrumb.service";
import {ConfirmationService} from "primeng/primeng";
import {NgbModal, ModalDismissReasons,NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {CommonService} from "../../_services/common.service";
import { FormGroup, FormControl, FormBuilder, Validators,NgForm,NgModel} from '@angular/forms';

@Component({
  selector: 'app-create-picklist',
  templateUrl: './create-picklist.component.html',
  styleUrls: ['./create-picklist.component.css']
})
export class CreatePicklistComponent implements OnInit {

  constructor(private activatedRoute:ActivatedRoute,
              private modalService:NgbModal,
              private commonService:CommonService,
              private confirmationService:ConfirmationService,
              private breadcrumbService:BreadcrumbService,private httpService:HttpService) { }

  rowId:number;
  key:string;
  ngOnInit() {
    this.activatedRoute.params.subscribe((params:Params) => {
      let categoryName = params['Name'];
      this.key = categoryName.replace(/ /g, '_');
      this.rowId = params['Id'];
      this.breadcrumbService.setBreadcrumb([{label: 'Pick List Management',routerLink:['/Custom/Picklists']},{label:categoryName}]);
    });
  }

  dataRows:any=[];
  totalRecords:Number=0;
  refreshTable:boolean=true;

  loadDataLazy(event: LazyLoadEvent) {
    event['PickListId'] = this.rowId;
    this.httpService.postTable('GetAllNewPickListValuesBySearch',event).subscribe(
        res=>{
          if(res){
            this.dataRows = res.jqGridData.rows;
            this.totalRecords = res.jqGridData.records;
          }
        }
    );
  }

  tableReload(){
    this.refreshTable = false;
    setTimeout(() => this.refreshTable = true, 0);
  }


  plSubmittedForm:boolean = false;
  plMF:FormGroup;
  plModal:NgbModalRef;


  private openPlModal(content,data) {
    console.log('data',data);
    this.plSubmittedForm = false;
    //this.getParentPickList();
    this.plMF = new FormGroup({
      'PickListValueId': new FormControl(null),
      'Value': new FormControl(null,Validators.required),
      'Color': new FormControl(null),
      'SeqOrder': new FormControl(null),
      /*'ColorFlag': new FormControl(null,Validators.pattern('^[a-zA-Z0-9 ]+$')),
       'ParentId': new FormControl(null),
       'CodeFlag': new FormControl(null),*/
    });

    if(data)
    {
      let obj:any = {};
      obj.PickListValueId = data.PickListValueId;
      obj.Value = data.Value;
      obj.SeqOrder = data.SeqOrder;
      obj.Color = data.Color;

      /* obj.ColorFlag = '';
       obj.ParentId =data.parent_id;
       obj.CodeFlag = '';*/
      this.plMF.patchValue(obj);
    }
    this.plModal = this.modalService.open(content);
  }

  plModalFormSubmit(formValue, valid) {
    this.plSubmittedForm = true;
    if (valid) {
      formValue.PickListId = this.rowId;
      console.log(formValue);
     this.httpService.post('SaveUpdateNewPickListValue', formValue).subscribe(
          res => {
            if (res) {
              this.plModal.dismiss();
              this.plSubmittedForm = false;
              this.tableReload();
            }
          })
    }
  }
  selectedItems:any = [];

  removeSelectedItemsMembers(row) {
    let ids:any = [];
    this.selectedItems.forEach((item)=>{
      ids.push(item.PickListValueId);
    })
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      header: 'Delete Confirmation',
      icon: 'fa fa-trash',
      accept: () => {
        this.httpService.post('DeleteNewPicklistValue', {ids:ids.toString()}).subscribe(res=> {
          if (res) {
            this.selectedItems = [];
            this.tableReload();
          }
        })
      }
    });
  }


 /* selectedItems:any = [];
  removeSelectedItemsMembers(row) {
    let ids:any = [];
    this.selectedItems.forEach((item)=>{
      ids.push(item.Id1);
    })
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      header: 'Delete Confirmation',
      icon: 'fa fa-trash',
      accept: () => {
        this.httpService.post('DeletePicklistValues', {ids:ids.toString()}).subscribe(res=> {
          if (res) {
            this.selectedItems = [];
            this.tableReload();
          }
        })

      }
    });
  }
  plModal:NgbModalRef;
  inputField:any = {};
  dropDownFields:any = [];
  showForm:boolean = false;

  private openPlModal(content) {
    this.httpService.get('GetParentsDataForPickListValueAddEdit/'+this.rowId+'/0').subscribe(res=>{
      if(res){
        if(res.data.parents.length > 1){
          this.dropDownFields = res.data.parents.slice(0,-1);
          this.dropDownFields.forEach((data,indx)=>{
              if(indx == 0){
                console.log('res.data.parentData',res.data.parentData);
               data.data = this.commonService.getDropdownOptions(res.data.parentData,'value','pk_id');
              }
          })
        }

        this.inputField = res.data.parents[res.data.parents.length-1];
        //this.showForm = true;
        this.fObj = {};
        this.plModal = this.modalService.open(content);
      }
    })

  }

  fObj:any = {ParentId:0,PickListValueId:0};
  save(){
    this.fObj.PickListId = this.rowId;
    this.fObj.PickListValueId = 0;
    this.httpService.post('SaveUpdatePickListValue',this.fObj).subscribe(res=>{
      if(res){
        this.plModal.close();
        this.tableReload();
      }
    })
  }
  changeDropdown(ParentId,Index){
    //console.log('Index',Index);
    //console.log('this.dropDownFields.length',this.dropDownFields.length-1);
    if((this.dropDownFields.length-1) != Index){
      this.httpService.get('GetPickListValuesByParentId/'+ParentId).subscribe(res=>{
        if(res){
          this.dropDownFields[Index+1].data =this.commonService.getDropdownOptions(res.data,'value','pk_id');
        }
      })
    }else{
      this.fObj.ParentId = ParentId;
    }
  }*/

}
