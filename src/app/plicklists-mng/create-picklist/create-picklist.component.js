var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var CreatePicklistComponent = (function () {
    function CreatePicklistComponent(activatedRoute, modalService, commonService, confirmationService, breadcrumbService, httpService) {
        this.activatedRoute = activatedRoute;
        this.modalService = modalService;
        this.commonService = commonService;
        this.confirmationService = confirmationService;
        this.breadcrumbService = breadcrumbService;
        this.httpService = httpService;
        this.cols = [];
        this.dataRows = [];
        this.totalRecords = 0;
        this.refreshTable = false;
        this.selectedItems = [];
        this.inputField = {};
        this.dropDownFields = [];
        this.showForm = false;
        this.fObj = { ParentId: 0, PickListValueId: 0 };
    }
    CreatePicklistComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            var categoryName = params['Name'];
            _this.key = categoryName.replace(/ /g, '_');
            _this.rowId = params['Id'];
            _this.getTableHeader();
            _this.breadcrumbService.setBreadcrumb([{ label: 'Pick List Management', routerLink: ['/Custom/Picklists'] }, { label: categoryName }]);
        });
    };
    CreatePicklistComponent.prototype.getTableHeader = function () {
        var _this = this;
        this.httpService.get('GetParentsForPickListById/' + this.rowId).subscribe(function (res) {
            if (res) {
                res.data.forEach(function (data) {
                    if (data.indexOf('Id') != 0) {
                        var obj = {};
                        obj.field = data;
                        obj.header = data.replace(/\_/g, ' ');
                        _this.cols.push(obj);
                        _this.tableReload();
                    }
                });
            }
        });
    };
    CreatePicklistComponent.prototype.loadDataLazy = function (event) {
        var _this = this;
        event['PickListId'] = this.rowId;
        this.httpService.postTable('GetAllPickListValuesBySearch', event).subscribe(function (res) {
            if (res) {
                _this.dataRows = res.jqGridData.rows;
                _this.totalRecords = res.jqGridData.records;
            }
        });
    };
    CreatePicklistComponent.prototype.tableReload = function () {
        var _this = this;
        this.refreshTable = false;
        setTimeout(function () { return _this.refreshTable = true; }, 0);
    };
    CreatePicklistComponent.prototype.removeSelectedItemsMembers = function (row) {
        var _this = this;
        var ids = [];
        this.selectedItems.forEach(function (item) {
            ids.push(item.Id1);
        });
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.httpService.post('DeletePicklistValues', { ids: ids.toString() }).subscribe(function (res) {
                    if (res) {
                        _this.selectedItems = [];
                        _this.tableReload();
                    }
                });
            }
        });
    };
    CreatePicklistComponent.prototype.openPlModal = function (content) {
        var _this = this;
        this.httpService.get('GetParentsDataForPickListValueAddEdit/' + this.rowId + '/0').subscribe(function (res) {
            if (res) {
                if (res.data.parents.length > 1) {
                    _this.dropDownFields = res.data.parents.slice(0, -1);
                    _this.dropDownFields.forEach(function (data, indx) {
                        if (indx == 0) {
                            console.log('res.data.parentData', res.data.parentData);
                            data.data = _this.commonService.getDropdownOptions(res.data.parentData, 'value', 'pk_id');
                        }
                    });
                }
                _this.inputField = res.data.parents[res.data.parents.length - 1];
                //this.showForm = true;
                _this.fObj = {};
                _this.plModal = _this.modalService.open(content);
            }
        });
    };
    CreatePicklistComponent.prototype.save = function () {
        var _this = this;
        this.fObj.PickListId = this.rowId;
        this.fObj.PickListValueId = 0;
        this.httpService.post('SaveUpdatePickListValue', this.fObj).subscribe(function (res) {
            if (res) {
                _this.plModal.close();
                _this.tableReload();
            }
        });
    };
    CreatePicklistComponent.prototype.changeDropdown = function (ParentId, Index) {
        var _this = this;
        //console.log('Index',Index);
        //console.log('this.dropDownFields.length',this.dropDownFields.length-1);
        if ((this.dropDownFields.length - 1) != Index) {
            this.httpService.get('GetPickListValuesByParentId/' + ParentId).subscribe(function (res) {
                if (res) {
                    _this.dropDownFields[Index + 1].data = _this.commonService.getDropdownOptions(res.data, 'value', 'pk_id');
                }
            });
        }
        else {
            this.fObj.ParentId = ParentId;
        }
    };
    CreatePicklistComponent = __decorate([
        core_1.Component({
            selector: 'app-create-picklist',
            templateUrl: './create-picklist.component.html',
            styleUrls: ['./create-picklist.component.css']
        })
    ], CreatePicklistComponent);
    return CreatePicklistComponent;
})();
exports.CreatePicklistComponent = CreatePicklistComponent;
//# sourceMappingURL=create-picklist.component.js.map