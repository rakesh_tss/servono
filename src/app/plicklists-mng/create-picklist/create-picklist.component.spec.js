var testing_1 = require('@angular/core/testing');
var create_picklist_component_1 = require('./create-picklist.component');
describe('CreatePicklistComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [create_picklist_component_1.CreatePicklistComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(create_picklist_component_1.CreatePicklistComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=create-picklist.component.spec.js.map