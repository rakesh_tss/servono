import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePicklistComponent } from './create-picklist.component';

describe('CreatePicklistComponent', () => {
  let component: CreatePicklistComponent;
  let fixture: ComponentFixture<CreatePicklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePicklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePicklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
