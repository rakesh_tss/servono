var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var MenuModuleComponent = (function () {
    function MenuModuleComponent(router, fullLayoutComponent) {
        this.router = router;
        this.fullLayoutComponent = fullLayoutComponent;
    }
    MenuModuleComponent.prototype.ngOnInit = function () {
        this.fullLayoutComponent.showBreadCrumbs = false;
        ;
        document.querySelector("body").classList.remove('sidebar-nav');
        var curretntUser = JSON.parse(localStorage.getItem('currentUser'));
        console.log('curretntUser.MenuObject', curretntUser.MenuObject);
        this.moduleList = curretntUser.MenuObject;
    };
    MenuModuleComponent.prototype.setMenuList = function (data) {
        document.querySelector("body").classList.add('sidebar-nav');
        //localStorage.setItem('currentMenu', JSON.stringify(data));
        this.fullLayoutComponent.setMenu(data);
        // window.location.reload();
        //window.location('/');
        //this.router.navigate(['Dashboard']);
        //this.router.navigateByUrl('/');
        this.fullLayoutComponent.ngOnInit();
        // window.location.reload();
    };
    MenuModuleComponent = __decorate([
        core_1.Component({
            selector: 'app-menu-module',
            templateUrl: './menu-module.component.html',
            styleUrls: ['./menu-module.component.scss'],
            providers: []
        })
    ], MenuModuleComponent);
    return MenuModuleComponent;
})();
exports.MenuModuleComponent = MenuModuleComponent;
//# sourceMappingURL=menu-module.component.js.map