import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuModuleRoutingModule } from './menu-module-routing.module';
import {MenuModuleComponent} from "./menu-module.component";
@NgModule({
  imports: [
    CommonModule,
    MenuModuleRoutingModule
  ],
  declarations: [
      MenuModuleComponent
  ]
})
export class MenuModuleModule { }
