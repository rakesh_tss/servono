import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MenuModuleComponent} from "./menu-module.component";

const routes: Routes = [
  {
    path: '',
    component:MenuModuleComponent,
    data: {
      title: 'Module'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class MenuModuleRoutingModule { }
