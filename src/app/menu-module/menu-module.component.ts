import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {FullLayoutComponent} from "../layouts/full-layout.component";
import {LocalStorageService} from "../_services/local-storage.service";


@Component({
  selector: 'app-menu-module',
  templateUrl: './menu-module.component.html',
  styleUrls: ['./menu-module.component.scss'],
  providers:[],
  })
export class MenuModuleComponent implements OnInit {

  constructor(private router:Router,private fullLayoutComponent:FullLayoutComponent, private localStorage:LocalStorageService) { }

  moduleList:any[];
  ngOnInit() {
    this.fullLayoutComponent.showBreadCrumbs = false;;
    document.querySelector("body").classList.remove('sidebar-nav');
    let curretntUser:any =  JSON.parse(this.localStorage.getItem('currentUser'));
    this.moduleList = this.fullLayoutComponent.menuObject;
  }
  setMenuList(data:any){
    document.querySelector("body").classList.add('sidebar-nav');
    //localStorage.setItem('currentMenu', JSON.stringify(data));
    this.fullLayoutComponent.setMenu(data);

   // window.location.reload();
    //window.location('/');
    //this.router.navigate(['Dashboard']);
    //this.router.navigateByUrl('/');
    this.fullLayoutComponent.ngOnInit();
   // window.location.reload();
  }

}
