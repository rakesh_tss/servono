var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var menu_module_routing_module_1 = require('./menu-module-routing.module');
var menu_module_component_1 = require("./menu-module.component");
var MenuModuleModule = (function () {
    function MenuModuleModule() {
    }
    MenuModuleModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                menu_module_routing_module_1.MenuModuleRoutingModule
            ],
            declarations: [
                menu_module_component_1.MenuModuleComponent
            ]
        })
    ], MenuModuleModule);
    return MenuModuleModule;
})();
exports.MenuModuleModule = MenuModuleModule;
//# sourceMappingURL=menu-module.module.js.map