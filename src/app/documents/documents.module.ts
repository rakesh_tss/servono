import { NgModule,Directive, ElementRef, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DocumentsRoutingModule } from './documents-routing.module';
import {DocumentsComponent} from "./documents.component";
import {CodeHighlighterModule} from 'primeng/primeng';
import {CommonTssModule} from "../shared/commonTssModules";
/*import * as Prism from 'prismjs';*/
/*declare var Prism: any;*/
@Directive({
    selector: '[pCode]'
})
export class CodeHighlighter implements OnInit {

    constructor(private el: ElementRef) {}

    ngOnInit() {
        //console.log('this.el.nativeElement',this.el.nativeElement);
        //Prism.highlightElement(this.el.nativeElement);
    }
}

@NgModule({
    imports: [
        CommonTssModule,
        DocumentsRoutingModule,
        CodeHighlighterModule
    ],
    declarations: [
        DocumentsComponent,
        CodeHighlighter
    ]
})
export class DocumentsModule {
}
