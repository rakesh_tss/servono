var testing_1 = require('@angular/core/testing');
var menu_mng_component_1 = require('./menu-mng.component');
describe('MenuMngComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [menu_mng_component_1.MenuMngComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(menu_mng_component_1.MenuMngComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=menu-mng.component.spec.js.map