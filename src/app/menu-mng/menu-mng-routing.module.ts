import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MenuMngComponent} from "./menu-mng.component";
import {MenuMngViewComponent} from "./menu-mng-view/menu-mng-view.component";

const routes: Routes = [
  {
    path:'',
    component:MenuMngComponent
  },
  {
    path:':Id',
    component:MenuMngViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class MenuMngRoutingModule { }
