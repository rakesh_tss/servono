import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuMngComponent } from './menu-mng.component';

describe('MenuMngComponent', () => {
  let component: MenuMngComponent;
  let fixture: ComponentFixture<MenuMngComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuMngComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuMngComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
