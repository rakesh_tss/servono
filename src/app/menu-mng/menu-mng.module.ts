import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuMngRoutingModule } from './menu-mng-routing.module';
import {CommonTssModule} from "../shared/commonTssModules";
import {MenuMngComponent} from "./menu-mng.component";
import {MenuMngViewComponent,LastDirective} from "./menu-mng-view/menu-mng-view.component";
import {ListboxModule,ToggleButtonModule} from "primeng/primeng";
import {IconPickerModule} from "../_components/icon-picker/icon-picker.module";

@NgModule({
    imports: [
        CommonTssModule,
        ToggleButtonModule,
        MenuMngRoutingModule,
        ListboxModule,
        IconPickerModule
    ],
    declarations: [MenuMngComponent, MenuMngViewComponent,LastDirective]
})
export class MenuMngModule {
}
