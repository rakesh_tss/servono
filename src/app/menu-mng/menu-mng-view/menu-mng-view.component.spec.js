var testing_1 = require('@angular/core/testing');
var menu_mng_view_component_1 = require('./menu-mng-view.component');
describe('MenuMngViewComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [menu_mng_view_component_1.MenuMngViewComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(menu_mng_view_component_1.MenuMngViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=menu-mng-view.component.spec.js.map