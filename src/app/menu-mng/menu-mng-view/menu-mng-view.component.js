var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var LastDirective = (function () {
    function LastDirective() {
        this.lastDone = new core_1.EventEmitter();
    }
    LastDirective.prototype.ngOnInit = function () {
        if (this.isLast) {
            this.lastDone.emit(true);
        }
    };
    __decorate([
        core_1.Input()
    ], LastDirective.prototype, "isLast");
    __decorate([
        core_1.Output()
    ], LastDirective.prototype, "lastDone");
    LastDirective = __decorate([
        core_1.Directive({ selector: '[isLast]' })
    ], LastDirective);
    return LastDirective;
})();
exports.LastDirective = LastDirective;
var MenuMngViewComponent = (function () {
    function MenuMngViewComponent(activatedRoute, httpService, breadcrumbService, confirmationService, commonService) {
        this.activatedRoute = activatedRoute;
        this.httpService = httpService;
        this.breadcrumbService = breadcrumbService;
        this.confirmationService = confirmationService;
        this.commonService = commonService;
        this.pkId = 0;
        this.menuMappedGroupList = [];
        //add menu modal
        this.addMenuGroupModal = false;
        this.menuGroupList = [];
        this.attachedMenuGroup = [];
        //add Menu popover
        //menuItem:any = {};
        this.selectedMenuGroup = { menuItem: {} };
        this.menuItemModuleList = [];
        this.attachedModuleModal = false;
        this.selectedMenuItem = {};
    }
    MenuMngViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.breadcrumbService.setBreadcrumb([{ label: 'Menu Management', routerLink: ['/' + 'Custom/Menus'] }, { label: 'Update / Create' }]);
        this.activatedRoute.params.subscribe(function (params) {
            console.log(' pkId', _this.pkId);
            _this.pkId = params['Id'];
            _this.getMenuMappedGroups();
        });
    };
    MenuMngViewComponent.prototype.getMenuMappedGroups = function () {
        var _this = this;
        this.httpService.get('GetMenuGroupsOfMenu/' + this.pkId).subscribe(function (res) {
            if (res) {
                _this.menuMappedGroupList = res.data;
            }
        });
    };
    MenuMngViewComponent.prototype.openMenuGroupModal = function () {
        var _this = this;
        this.attachedMenuGroup = [];
        this.httpService.get('DropListDataForMenuGroupAtMenu/' + this.pkId).subscribe(function (res) {
            if (res) {
                _this.addMenuGroupModal = true;
                _this.menuGroupList = _this.getOptions(res.data);
            }
        });
    };
    MenuMngViewComponent.prototype.saveMenuGroup = function (data) {
        var _this = this;
        var param = {};
        param.menu_groups = data;
        this.httpService.post('AttachMenuAndMenuGroups/' + this.pkId, param).subscribe(function (res) {
            if (res) {
                _this.addMenuGroupModal = false;
                _this.getMenuMappedGroups();
            }
        });
    };
    /* removeMenuGroup() {
     this.httpService.post('').subscribe(res=> {
     if (res) {

     }
     })
     }*/
    MenuMngViewComponent.prototype.getOptions = function (data) {
        var result = [];
        data.forEach(function (item) {
            var obj = {};
            obj.label = item.name;
            obj.value = item.pk_id;
            result.push(obj);
        });
        return result;
    };
    MenuMngViewComponent.prototype.openMenuPopover = function (popover, evn, menuGroup, menu) {
        this.selectedMenuGroup = menuGroup;
        this.selectedMenuGroup.menuItem = {};
        if (menuGroup) {
            if (menu) {
                this.selectedMenuGroup.menuItem = menu;
            }
            //this.selectedMenuGroup.menuItem.gid = this.selectedMenuGroup.pk_id;
            //this.selectedMenuGroup.menuItem.app_menu_item = '';
            //this.selectedMenuGroup.menuItem.icon_class = '';
            //this.selectedMenuGroup.menuItem.seq_order = '';
            popover.show(evn);
        }
    };
    MenuMngViewComponent.prototype.saveMenuItem = function (popover, evn, selectedMenuGroup) {
        var _this = this;
        this.httpService.post('SaveUpdateMenuItem/' + this.pkId + '/' + selectedMenuGroup.pk_id, selectedMenuGroup.menuItem).subscribe(function (res) {
            if (res) {
                selectedMenuGroup.MenuItems = _this.getMenuItemByGroupsAndMenu(selectedMenuGroup);
                popover.hide(evn);
            }
        });
    };
    MenuMngViewComponent.prototype.getMenuItemByGroupsAndMenu = function (menuGroup) {
        this.httpService.get('GetMenuItemByGroupsAndMenu/' + this.pkId + '/' + menuGroup.pk_id).subscribe(function (res) {
            if (res) {
                menuGroup.MenuItems = res.data;
            }
        });
    };
    //get attachement modules
    /**
     *
     * @param groupId
     * @param menuItem
     */
    MenuMngViewComponent.prototype.getModulesByMenuItem = function (menuGroup, menuItem) {
        menuItem.selectedMenuItemModules = [];
        menuGroup.selectedMenuItem = menuItem;
        this.httpService.get('GetModulesByMenuItem/' + menuItem.pk_id).subscribe(function (res) {
            if (res) {
                menuGroup.selectedMenuItem.selectedMenuItemModules = res.data;
                menuGroup.selectedMenuItemModules = true;
            }
        });
    };
    MenuMngViewComponent.prototype.openAttachedModuleModal = function (selectedMenuItem) {
        var _this = this;
        selectedMenuItem.attachedModules = [];
        this.selectedMenuItem = selectedMenuItem;
        this.httpService.get('DropListDataForModulesAtMenuItem/' + selectedMenuItem.pk_id).subscribe(function (res) {
            if (res) {
                _this.attachedModuleModal = true;
                _this.menuItemModuleList = _this.commonService.getDropdownOptions(res.data, 'module_label', 'pk_id', true);
            }
        });
    };
    MenuMngViewComponent.prototype.saveMenuItemModules = function (selectedMenuItem) {
        var _this = this;
        this.httpService.post('AttachModulesToMenuItem/' + selectedMenuItem.pk_id, { modules: selectedMenuItem.attachedModules }).subscribe(function (res) {
            if (res) {
                _this.reloadModules(selectedMenuItem);
                _this.attachedModuleModal = false;
            }
        });
    };
    MenuMngViewComponent.prototype.reloadModules = function (selectedMenuItem) {
        this.httpService.get('GetModulesByMenuItem/' + selectedMenuItem.pk_id).subscribe(function (res) {
            if (res) {
                selectedMenuItem.selectedMenuItemModules = res.data;
            }
        });
    };
    MenuMngViewComponent.prototype.removeMenuGroup = function (data) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.httpService.post('RemoveMenuInMenuGroup/' + _this.pkId + '/' + data.pk_id, {}).subscribe(function (res) {
                    if (res) {
                        _this.getMenuMappedGroups();
                    }
                });
            }
        });
    };
    MenuMngViewComponent.prototype.RemoveMenuItem = function (mg, menuItem) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.httpService.post('RemoveMenuItem/' + menuItem.pk_id, {}).subscribe(function (res) {
                    if (res) {
                        _this.getMenuItemByGroupsAndMenu(mg);
                    }
                });
            }
        });
    };
    MenuMngViewComponent.prototype.RemoveMenuItemModule = function (mg, module) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.httpService.post('RemoveMenuItemModule/' + module.pk_id, {}).subscribe(function (res) {
                    if (res) {
                        _this.reloadModules(mg.selectedMenuItem);
                    }
                });
            }
        });
    };
    MenuMngViewComponent = __decorate([
        core_1.Component({
            selector: 'app-menu-mng-view',
            templateUrl: './menu-mng-view.component.html',
            styleUrls: ['./menu-mng-view.component.css']
        })
    ], MenuMngViewComponent);
    return MenuMngViewComponent;
})();
exports.MenuMngViewComponent = MenuMngViewComponent;
//# sourceMappingURL=menu-mng-view.component.js.map