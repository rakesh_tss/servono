import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuMngViewComponent } from './menu-mng-view.component';

describe('MenuMngViewComponent', () => {
  let component: MenuMngViewComponent;
  let fixture: ComponentFixture<MenuMngViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuMngViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuMngViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
