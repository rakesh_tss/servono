import { Component, OnInit,Input,Output,Directive,EventEmitter } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {HttpService} from "../../_services/http.service";
import {CommonService} from "../../_services/common.service";
import {BreadcrumbService} from "../../_services/breadcrumb.service";
import {ConfirmationService} from "primeng/primeng";
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Directive({selector: '[isLast]'})
export class LastDirective{
    @Input() isLast:boolean;
    @Output() lastDone:EventEmitter<boolean> = new EventEmitter<boolean>();
    ngOnInit() {
        if(this.isLast) {
            this.lastDone.emit(true);
        }
    }
}

@Component({
    selector: 'app-menu-mng-view',
    templateUrl: './menu-mng-view.component.html',
    styleUrls: ['./menu-mng-view.component.css']

})
export class MenuMngViewComponent implements OnInit {

    constructor(private activatedRoute:ActivatedRoute, private httpService:HttpService,
                private breadcrumbService:BreadcrumbService,
                private confirmationService:ConfirmationService,
                private ngbModal:NgbModal,
                private commonService:CommonService) {


    }

    pkId:number = 0;
    menuGroupModal:NgbModalRef;

    ngOnInit() {
        this.breadcrumbService.setBreadcrumb([{label:'Menu Management',routerLink:['/'+'Custom/Menus']},{label:'Update / Create'}]);

        this.activatedRoute.params.subscribe((params:Params) => {
            this.pkId = params['Id'];
            this.getMenuMappedGroups();
        });
    }

    menuMappedGroupList:any = [];

    getMenuMappedGroups() {
        this.httpService.get('GetMenuGroupsOfMenu/' + this.pkId).subscribe(res=> {
            if (res) {
                this.menuMappedGroupList = res.data;
            }
        })
    }

    //add menu modal
    addMenuGroupModal:boolean = false;
    menuGroupList:any = [];
    attachedMenuGroup:any = [];
    openMenuGroupModal(content) {
        this.attachedMenuGroup =  [];
        this.httpService.get('DropListDataForMenuGroupAtMenu/' + this.pkId).subscribe(res=> {
            if (res) {
                this.menuGroupModal = this.ngbModal.open(content);
               // this.addMenuGroupModal = true;
                this.menuGroupList = this.getOptions(res.data);
            }
        })

    }

    saveMenuGroup(data) {
        let param:any = {};
        param.menu_groups = data;
        this.httpService.post('AttachMenuAndMenuGroups/' + this.pkId, param).subscribe(res=> {
            if (res) {
                //this.addMenuGroupModal = false;
                this.menuGroupModal.close();
                this.getMenuMappedGroups();
            }
        })
    }

    /* removeMenuGroup() {
     this.httpService.post('').subscribe(res=> {
     if (res) {

     }
     })
     }*/

    getOptions(data:any) {
        let result = [];
        data.forEach((item)=> {
            let obj:any = {};
            obj.label = item.name;
            obj.value = item.pk_id;
            result.push(obj);
        });

        return result;

    }

    //add Menu popover
    //menuItem:any = {};
    selectedMenuGroup:any = {menuItem: {}};

    openMenuPopover(popover, evn, menuGroup, menu) {
        this.selectedMenuGroup = menuGroup;
        this.selectedMenuGroup.menuItem = {};
        if (menuGroup) {
            if(menu){
                this.selectedMenuGroup.menuItem = menu;
            }
            //this.selectedMenuGroup.menuItem.gid = this.selectedMenuGroup.pk_id;
            //this.selectedMenuGroup.menuItem.app_menu_item = '';
            //this.selectedMenuGroup.menuItem.icon_class = '';
            //this.selectedMenuGroup.menuItem.seq_order = '';
            popover.show(evn);
        }

    }

    saveMenuItem(popover, evn, selectedMenuGroup) {
        this.httpService.post('SaveUpdateMenuItem/' + this.pkId + '/' + selectedMenuGroup.pk_id, selectedMenuGroup.menuItem).subscribe(res=> {
            if (res) {
                selectedMenuGroup.MenuItems = this.getMenuItemByGroupsAndMenu(selectedMenuGroup);
                popover.hide(evn);
            }
        })
    }
    getMenuItemByGroupsAndMenu(menuGroup){
       this.httpService.get('GetMenuItemByGroupsAndMenu/'+this.pkId+'/'+menuGroup.pk_id).subscribe(res=> {
            if (res) {
                menuGroup.MenuItems = res.data;
            }
        });
    }

    //get attachement modules
    /**
     *
     * @param groupId
     * @param menuItem
     */
    getModulesByMenuItem(menuGroup, menuItem) {
        menuItem.selectedMenuItemModules = [];
        menuGroup.selectedMenuItem = menuItem;
        this.httpService.get('GetModulesByMenuItem/' + menuItem.pk_id).subscribe(res=> {
            if (res) {
                menuGroup.selectedMenuItem.selectedMenuItemModules = res.data;
                menuGroup.selectedMenuItemModules = true;
            }
        });
    }
    menuItemModuleList:any = [];
    attachedModuleModal:NgbModalRef;
    selectedMenuItem:any = {};
    openAttachedModuleModal(selectedMenuItem,content){
        selectedMenuItem.attachedModules = [];
        this.selectedMenuItem = selectedMenuItem;
        this.httpService.get('DropListDataForModulesAtMenuItem/' + selectedMenuItem.pk_id).subscribe(res=> {
            if (res) {
                this.attachedModuleModal = this.ngbModal.open(content);
                this.menuItemModuleList = this.commonService.getDropdownOptions(res.data,'module_label','pk_id',true);
            }
        })
    }
    saveMenuItemModules(selectedMenuItem){
       this.httpService.post('AttachModulesToMenuItem/'+ selectedMenuItem.pk_id,{modules:selectedMenuItem.attachedModules}).subscribe(res=> {
            if (res) {
                this.reloadModules(selectedMenuItem);
                this.attachedModuleModal.close();
                //popover.hide(evn);
            }
        })
    }
    reloadModules(selectedMenuItem){
        this.httpService.get('GetModulesByMenuItem/' + selectedMenuItem.pk_id).subscribe(res=> {
            if (res) {
                selectedMenuItem.selectedMenuItemModules = res.data;
            }
        });
    }
    removeMenuGroup(data){
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
                this.httpService.post('RemoveMenuInMenuGroup/'+this.pkId+'/'+data.pk_id,{}).subscribe(res=>{
                    if(res){
                        this.getMenuMappedGroups();
                    }
                })
            }
        });
    }
    RemoveMenuItem(mg,menuItem){
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
                this.httpService.post('RemoveMenuItem/'+menuItem.pk_id,{}).subscribe(res=>{
                    if(res){
                        this.getMenuItemByGroupsAndMenu(mg);
                    }
                })

            }
        });
    }
    RemoveMenuItemModule(mg,module){
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
                this.httpService.post('RemoveMenuItemModule/'+module.pk_id,{}).subscribe(res=>{
                    if(res){
                        this.reloadModules(mg.selectedMenuItem)
                    }
                })

            }
        });
    }
}
