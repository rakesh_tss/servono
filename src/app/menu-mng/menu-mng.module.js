var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var menu_mng_routing_module_1 = require('./menu-mng-routing.module');
var commonTssModules_1 = require("../shared/commonTssModules");
var menu_mng_component_1 = require("./menu-mng.component");
var menu_mng_view_component_1 = require("./menu-mng-view/menu-mng-view.component");
var primeng_1 = require("primeng/primeng");
var icon_picker_module_1 = require("../_components/icon-picker/icon-picker.module");
var MenuMngModule = (function () {
    function MenuMngModule() {
    }
    MenuMngModule = __decorate([
        core_1.NgModule({
            imports: [
                commonTssModules_1.CommonTssModule,
                primeng_1.ToggleButtonModule,
                menu_mng_routing_module_1.MenuMngRoutingModule,
                primeng_1.ListboxModule,
                icon_picker_module_1.IconPickerModule
            ],
            declarations: [menu_mng_component_1.MenuMngComponent, menu_mng_view_component_1.MenuMngViewComponent, menu_mng_view_component_1.LastDirective]
        })
    ], MenuMngModule);
    return MenuMngModule;
})();
exports.MenuMngModule = MenuMngModule;
//# sourceMappingURL=menu-mng.module.js.map