var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var common_service_1 = require("../_services/common.service");
var MenuMngComponent = (function () {
    function MenuMngComponent(httpService, confirmationService, breadcrumbService, commonService, router) {
        this.httpService = httpService;
        this.confirmationService = confirmationService;
        this.breadcrumbService = breadcrumbService;
        this.commonService = commonService;
        this.router = router;
        this.dataRow = {};
        this.menuFormObj = {};
        this.userTypeList = [{ label: 'Select', value: null }];
        this.refreshTable = true;
    }
    MenuMngComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.breadcrumbService.setBreadcrumb([{ label: 'Menu Management' }]);
        this.httpService.get('DropListData/usertypes/0').subscribe(function (res) {
            if (res) {
                for (var key in res.data) {
                    var obj = {};
                    obj.value = res.data[key].pk_id;
                    obj.label = res.data[key].name;
                    _this.userTypeList.push(obj);
                }
            }
        });
    };
    MenuMngComponent.prototype.loadDataLazy = function (event) {
        var _this = this;
        this.httpService.postTable('GetGridList/Menus', event).subscribe(function (res) {
            _this.dataRows = res.jqGridData.rows;
            _this.totalRecords = res.jqGridData.records;
        });
    };
    MenuMngComponent.prototype.onCreate = function (data) {
        this.dialogTitle = 'Create Menu';
        if (data) {
            this.dialogTitle = 'Update Menu';
            this.dataRow = data;
        }
        else {
            this.dataRow = {};
        }
        this.displayDialog = true;
    };
    MenuMngComponent.prototype.onEdit = function (row) {
        this.dialogTitle = 'Update Menu';
        this.dataRow = row;
        this.displayDialog = true;
    };
    MenuMngComponent.prototype.onSave = function (row) {
        var _this = this;
        this.httpService.post('SaveUpdateMenu', row).subscribe(function (res) {
            if (res) {
                _this.displayDialog = false;
                _this.tableReload();
            }
        });
    };
    MenuMngComponent.prototype.onView = function (row) {
        //this.msgs = [];
        // this.msgs.push({severity:'info', summary:'Success', detail:'Data Updated'});
        this.router.navigate(['', row.app_menu_id]);
    };
    MenuMngComponent.prototype.onDelete = function (row) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.httpService.post('RemoveMenu/' + row.pk_id, {}).subscribe(function (res) {
                    if (res) {
                        _this.tableReload();
                    }
                });
            }
        });
    };
    MenuMngComponent.prototype.tableReload = function () {
        var _this = this;
        this.refreshTable = false;
        setTimeout(function () { return _this.refreshTable = true; }, 100);
    };
    MenuMngComponent.prototype.dialogClose = function () {
        this.displayDialog = false;
    };
    MenuMngComponent = __decorate([
        core_1.Component({
            selector: 'app-menu-mng',
            templateUrl: './menu-mng.component.html',
            styleUrls: ['./menu-mng.component.css'],
            providers: [common_service_1.CommonService]
        })
    ], MenuMngComponent);
    return MenuMngComponent;
})();
exports.MenuMngComponent = MenuMngComponent;
//# sourceMappingURL=menu-mng.component.js.map