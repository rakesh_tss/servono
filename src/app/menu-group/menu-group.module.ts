import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuGroupRoutingModule } from './menu-group-routing.module';
import {MenuGroupComponent} from "./menu-group.component";
import {CommonTssModule} from "../shared/commonTssModules";
import {IconPickerModule} from "../_components/icon-picker/icon-picker.module";

@NgModule({
    imports: [
        MenuGroupRoutingModule,
        CommonTssModule,
        IconPickerModule
    ],
    declarations: [MenuGroupComponent]
})
export class MenuGroupModule {
}
