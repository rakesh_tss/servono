var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var menu_group_routing_module_1 = require('./menu-group-routing.module');
var menu_group_component_1 = require("./menu-group.component");
var commonTssModules_1 = require("../shared/commonTssModules");
var icon_picker_module_1 = require("../_components/icon-picker/icon-picker.module");
var MenuGroupModule = (function () {
    function MenuGroupModule() {
    }
    MenuGroupModule = __decorate([
        core_1.NgModule({
            imports: [
                menu_group_routing_module_1.MenuGroupRoutingModule,
                commonTssModules_1.CommonTssModule,
                icon_picker_module_1.IconPickerModule
            ],
            declarations: [menu_group_component_1.MenuGroupComponent]
        })
    ], MenuGroupModule);
    return MenuGroupModule;
})();
exports.MenuGroupModule = MenuGroupModule;
//# sourceMappingURL=menu-group.module.js.map