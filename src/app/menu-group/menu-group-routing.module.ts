import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MenuGroupComponent} from "./menu-group.component";

const routes:Routes = [
    {
        path: '',
        component: MenuGroupComponent,
        data: {
            title: 'Menu Group'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class MenuGroupRoutingModule {
}
