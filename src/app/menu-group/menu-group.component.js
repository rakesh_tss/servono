var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var MenuGroupComponent = (function () {
    function MenuGroupComponent(httpService, confirmationService, breadcrumbService) {
        this.httpService = httpService;
        this.confirmationService = confirmationService;
        this.breadcrumbService = breadcrumbService;
        this.menuGroupList = [];
        this.display = false;
        this.SubmittedForm = false;
    }
    MenuGroupComponent.prototype.ngOnInit = function () {
        this.breadcrumbService.setBreadcrumb([{ label: 'Menu Group' }]);
        this.getMenuGroupList();
        /*      this.mg = new FormGroup({
                  'name': new FormControl(null, Validators.required),
                  'short_name': new FormControl(null),
                  'description': new FormControl(null),
                  'icon_class': new FormControl(null),
                  'pk_id': new FormControl(0)
              });*/
    };
    MenuGroupComponent.prototype.getMenuGroupList = function () {
        var _this = this;
        this.httpService.get('DropListData/MenuGroups/0').subscribe(function (res) {
            if (res) {
                _this.menuGroupList = res.data;
            }
        });
    };
    MenuGroupComponent.prototype.addMenuGroupList = function () {
    };
    MenuGroupComponent.prototype.showDialog = function (data) {
        this.mg = new forms_1.FormGroup({
            'name': new forms_1.FormControl(null, forms_1.Validators.required),
            'short_name': new forms_1.FormControl(null),
            'description': new forms_1.FormControl(null),
            'icon_class': new forms_1.FormControl(null),
            'pk_id': new forms_1.FormControl(0)
        });
        if (data) {
            console.log('data', data);
            this.mg.patchValue(data);
            this.icon_class = data.icon_class;
        }
        //console.log('this.mg',this.mg);
        this.display = true;
    };
    MenuGroupComponent.prototype.mgModalFormSubmit = function (mgData) {
        var _this = this;
        this.SubmittedForm = true;
        if (mgData.valid) {
            this.httpService.post('SaveUpdateMenuGroup', mgData.value).subscribe(function (res) {
                if (res) {
                    _this.display = false;
                    _this.SubmittedForm = false;
                    _this.getMenuGroupList();
                }
            });
        }
    };
    MenuGroupComponent.prototype.removeGroupMenu = function (mgData) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.httpService.post('RemoveMenuGroup/' + mgData.pk_id, {}).subscribe(function (res) {
                    if (res) {
                        _this.getMenuGroupList();
                    }
                });
            }
        });
    };
    MenuGroupComponent = __decorate([
        core_1.Component({
            selector: 'app-menu-group',
            templateUrl: './menu-group.component.html',
            styleUrls: ['./menu-group.component.css']
        })
    ], MenuGroupComponent);
    return MenuGroupComponent;
})();
exports.MenuGroupComponent = MenuGroupComponent;
//# sourceMappingURL=menu-group.component.js.map