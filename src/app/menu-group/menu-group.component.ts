import { Component, OnInit } from '@angular/core';
import {HttpService} from "../_services/http.service";
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, FormBuilder, Validators,NgForm,NgModel} from '@angular/forms';
import {ConfirmationService} from "primeng/primeng";
import {BreadcrumbService} from "../_services/breadcrumb.service";


@Component({
    selector: 'app-menu-group',
    templateUrl: './menu-group.component.html',
    styleUrls: ['./menu-group.component.css']
})
export class MenuGroupComponent implements OnInit {

    constructor(public httpService:HttpService, private confirmationService:ConfirmationService, private breadcrumbService:BreadcrumbService) {
    }

    ngOnInit() {
        this.breadcrumbService.setBreadcrumb([{label: 'Menu Group'}]);
        this.getMenuGroupList();
  /*      this.mg = new FormGroup({
            'name': new FormControl(null, Validators.required),
            'short_name': new FormControl(null),
            'description': new FormControl(null),
            'icon_class': new FormControl(null),
            'pk_id': new FormControl(0)
        });*/
    }

    menuGroupList:any = [];

    getMenuGroupList() {
        this.httpService.get('DropListData/MenuGroups/0').subscribe(res=> {
            if (res) {
                this.menuGroupList = res.data;
            }
        })

    }

    addMenuGroupList() {

    }

    display:boolean = false;

    mg:FormGroup;
    icon_class:string;
    showDialog(data) {
        this.mg=new FormGroup({
            'name': new FormControl(null, Validators.required),
            'short_name': new FormControl(null),
            'description': new FormControl(null),
            'icon_class': new FormControl(null),
            'pk_id': new FormControl(0)
        });
        if (data) {
            this.mg.patchValue(data);
            this.icon_class = data.icon_class;
        }
        //console.log('this.mg',this.mg);
        this.display = true;
    }

    SubmittedForm:boolean = false;

    mgModalFormSubmit(mgData) {

        this.SubmittedForm = true;
        if (mgData.valid) {
            this.httpService.post('SaveUpdateMenuGroup', mgData.value).subscribe(res=> {
                if (res) {
                    this.display = false;
                    this.SubmittedForm = false;
                    this.getMenuGroupList();
                }
            })
        }
    }

    removeGroupMenu(mgData) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
                this.httpService.post('RemoveMenuGroup/' + mgData.pk_id, {}).subscribe(
                    res => {
                        if (res) {
                            this.getMenuGroupList();
                        }
                    })
            }
        });
    }
}