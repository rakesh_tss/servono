import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelationModalContentComponent } from './relation-modal-content.component';

describe('RelationModalContentComponent', () => {
  let component: RelationModalContentComponent;
  let fixture: ComponentFixture<RelationModalContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelationModalContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelationModalContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
