import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import {SelectItem} from "primeng/primeng";
import {HttpService} from "../../_services/http.service";
import {LazyLoadEvent} from "primeng/primeng";
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {DynamicFormService} from "../_services/dynamic-form.service";

@Component({
    selector: 'relation-modal-content',
    templateUrl: './relation-modal-content.component.html',
    styleUrls: ['./relation-modal-content.component.css']
})
export class RelationModalContentComponent implements OnInit {


    constructor(private httpService:HttpService,
                private modalService:NgbModal, private dynamicFormService:DynamicFormService) {
    }

    @Input() field:any = {};
    @Input() type:string = 'notRel';
    @Input() bType:string = 'add';
    @Input() subRel:any = null;
    @Input() exParams:any = null;
    @Output('afterConfirm') clickConfirm:EventEmitter<any> = new EventEmitter();

    moduleName:string = '';
    rm:NgbModalRef;

    ngOnInit() {

        this.currentFieldRelation = this.field;
        if (this.field.data) {
            this.moduleName = this.field.data.to_module
        }
    }

    openRelationModalContent() {

    }

    //relation data table
    dataRows:any[];
    dataRow:any = {};
    totalRecords:number;
    displayDialog:boolean = false;
    datableEvent:any;
    relationName:string;
    showForm:boolean = false;
    cols:any[];
    tableShow:boolean = false;
    showCols:any[];
    columnOptions:SelectItem[];
    currentFieldRelation:any = this.field;
    selectRelation:string; //multiple or single
    selectedRows:any;
    relationDialogOpen(content) {

        let relationData = this.field.data;
        //let result = relationData.filter(item=>item.from_db_field == this.field.db_field_name);
        // this.fields.filter(item => item.id.indexOf(args[0]) !== -1);
        this.currentFieldRelation = this.field;
        if (!this.currentFieldRelation.relationData) {
            this.currentFieldRelation.relationData = [];
        }
        if (!this.currentFieldRelation.value) {
            this.currentFieldRelation.value = null;
        }

        this.displayDialog = true;
        //this.modalService.open(content);
        this.rm = this.modalService.open(content,{size:'lg'});
        if (relationData) {
            this.relationName = relationData.to_module;
            this.selectRelation = (relationData.to_relation == 0) ? 'multiple' : 'single'
            this.getTableHeader();
        }

    }

    onRelationRowSelect(event) {
        if (this.type == 'oneRel') {
            //this.dynamicFormService.setRelationDisplayName(this.currentFieldRelation);
        }
    }
    record_identifier:string;
    private getTableHeader() {
        this.tableShow = false;
        this.httpService.get('getModuleListMetadata/' + this.relationName).subscribe(res=> {
            if (res) {
                this.cols = res.data;
                this.showCols = [];
                this.columnOptions = [];
                for (let key in this.cols) {
                    if (!this.cols[key].is_hidden) {
                        if (this.cols[key].type !== 'Image')
                            this.showCols.push(this.cols[key]);
                    }
                    if(this.cols[key].record_identifier ==true){
                        this.record_identifier = this.cols[key].name;
                    }
                    this.columnOptions.push({label: this.cols[key].name, value: this.cols[key]});
                }

                this.tableShow = true;
            }
        })
    }


    loadDataLazy(event:LazyLoadEvent) {
        this.datableEvent = event;
        if (this.type == 'notRel') {
            this.datableEvent.NotRel = this.field.data;
        }
        if (this.type == 'oneRel') {
            this.datableEvent.OneRelMain = this.field.data;
            this.datableEvent.OneRelMain['params']=this.exParams;
        }
        //sub relation field table view
        if (this.subRel) {
            this.datableEvent.OneRel = this.subRel;
        }

        this.datableEvent.displayCols = this.cols;


        this.httpService.postTable('getModuleListData/' + this.relationName, this.datableEvent).subscribe(res=> {
            if (res) {
                this.dataRows = res.jqGridData.rows;

                if (this.currentFieldRelation.value) {
                    let pkId = this.currentFieldRelation.value.pk_id
                    if (pkId) {
                        this.dataRows.forEach((item)=> {
                            if (item.pk_id == pkId) {
                                this.selectedRows = item;
                            }
                        });
                    }
                }
                this.totalRecords = res.jqGridData.records;
            }
        })
    }

    saveRelationRows() {

        if (this.type == 'notRel') {
            let params:any = {};
            params = this.currentFieldRelation.data;
            this.currentFieldRelation.value = this.selectedRows;
            params.value = this.currentFieldRelation.value;
            this.httpService.postTable('SaveModuleRelationManyData', params).subscribe(res=> {
                this.clickConfirm.emit(this.currentFieldRelation.value);
                this.rm.close();
            })
        } else {
            if (this.type == 'oneRel') {
                this.currentFieldRelation.value = this.selectedRows;
                this.currentFieldRelation.displayName = this.currentFieldRelation.value[this.record_identifier];
                //this.dynamicFormService.setRelationDisplayName(this.currentFieldRelation);
            }
            this.clickConfirm.emit(this.currentFieldRelation.value);
            this.rm.close();
        }

    }

    tableReload() {
        this.tableShow = false;
        setTimeout(() => this.tableShow = true, 100);
    }

}
