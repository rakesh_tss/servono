var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var RelationModalContentComponent = (function () {
    function RelationModalContentComponent(httpService, modalService, dynamicFormService) {
        this.httpService = httpService;
        this.modalService = modalService;
        this.dynamicFormService = dynamicFormService;
        this.field = {};
        this.type = 'notRel';
        this.bType = 'add';
        this.subRel = null;
        this.clickConfirm = new core_1.EventEmitter();
        this.moduleName = '';
        this.dataRow = {};
        this.displayDialog = false;
        this.showForm = false;
        this.tableShow = false;
        this.currentFieldRelation = this.field;
    }
    RelationModalContentComponent.prototype.ngOnInit = function () {
        this.currentFieldRelation = this.field;
        if (this.field.data) {
            this.moduleName = this.field.data.to_module;
        }
    };
    RelationModalContentComponent.prototype.openRelationModalContent = function () {
    };
    RelationModalContentComponent.prototype.relationDialogOpen = function (content) {
        var relationData = this.field.data;
        //let result = relationData.filter(item=>item.from_db_field == this.field.db_field_name);
        // this.fields.filter(item => item.id.indexOf(args[0]) !== -1);
        this.currentFieldRelation = this.field;
        if (!this.currentFieldRelation.relationData) {
            this.currentFieldRelation.relationData = [];
        }
        if (!this.currentFieldRelation.value) {
            this.currentFieldRelation.value = null;
        }
        this.displayDialog = true;
        //this.modalService.open(content);
        this.rm = this.modalService.open(content);
        if (relationData) {
            this.relationName = relationData.to_module;
            this.selectRelation = (relationData.to_relation == 0) ? 'multiple' : 'single';
            this.getTableHeader();
        }
    };
    RelationModalContentComponent.prototype.onRelationRowSelect = function (event) {
        if (this.type == 'oneRel') {
        }
    };
    RelationModalContentComponent.prototype.getTableHeader = function () {
        var _this = this;
        this.tableShow = false;
        this.httpService.get('getModuleListMetadata/' + this.relationName).subscribe(function (res) {
            if (res) {
                _this.cols = res.data;
                _this.showCols = [];
                _this.columnOptions = [];
                for (var key in _this.cols) {
                    if (!_this.cols[key].is_hidden) {
                        if (_this.cols[key].type !== 'Image')
                            _this.showCols.push(_this.cols[key]);
                    }
                    _this.columnOptions.push({ label: _this.cols[key].name, value: _this.cols[key] });
                }
                _this.tableShow = true;
            }
        });
    };
    RelationModalContentComponent.prototype.loadDataLazy = function (event) {
        var _this = this;
        this.datableEvent = event;
        if (this.type == 'notRel') {
            this.datableEvent.NotRel = this.field.data;
        }
        if (this.subRel) {
            this.datableEvent.OneRel = this.subRel;
        }
        this.datableEvent.displayCols = this.cols;
        this.httpService.postTable('getModuleListData/' + this.relationName, this.datableEvent).subscribe(function (res) {
            if (res) {
                _this.dataRows = res.jqGridData.rows;
                if (_this.currentFieldRelation.value) {
                    var pkId = _this.currentFieldRelation.value.pk_id;
                    if (pkId) {
                        _this.dataRows.forEach(function (item) {
                            if (item.pk_id == pkId) {
                                _this.selectedRows = item;
                            }
                        });
                    }
                }
                _this.totalRecords = res.jqGridData.records;
            }
        });
    };
    RelationModalContentComponent.prototype.saveRelationRows = function () {
        var _this = this;
        if (this.type == 'notRel') {
            var params = {};
            params = this.currentFieldRelation.data;
            this.currentFieldRelation.value = this.selectedRows;
            params.value = this.currentFieldRelation.value;
            this.httpService.postTable('SaveModuleRelationManyData', params).subscribe(function (res) {
                _this.clickConfirm.emit(_this.currentFieldRelation.value);
                _this.rm.close();
            });
        }
        else {
            if (this.type == 'oneRel') {
                this.currentFieldRelation.value = this.selectedRows;
                this.dynamicFormService.setRelationDisplayName(this.currentFieldRelation);
            }
            this.clickConfirm.emit(this.currentFieldRelation.value);
            this.rm.close();
        }
    };
    RelationModalContentComponent.prototype.tableReload = function () {
        var _this = this;
        this.tableShow = false;
        setTimeout(function () { return _this.tableShow = true; }, 100);
    };
    __decorate([
        core_1.Input()
    ], RelationModalContentComponent.prototype, "field");
    __decorate([
        core_1.Input()
    ], RelationModalContentComponent.prototype, "type");
    __decorate([
        core_1.Input()
    ], RelationModalContentComponent.prototype, "bType");
    __decorate([
        core_1.Input()
    ], RelationModalContentComponent.prototype, "subRel");
    __decorate([
        core_1.Output('afterConfirm')
    ], RelationModalContentComponent.prototype, "clickConfirm");
    RelationModalContentComponent = __decorate([
        core_1.Component({
            selector: 'relation-modal-content',
            templateUrl: './relation-modal-content.component.html',
            styleUrls: ['./relation-modal-content.component.css']
        })
    ], RelationModalContentComponent);
    return RelationModalContentComponent;
})();
exports.RelationModalContentComponent = RelationModalContentComponent;
//# sourceMappingURL=relation-modal-content.component.js.map