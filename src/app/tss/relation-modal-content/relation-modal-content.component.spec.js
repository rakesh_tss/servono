var testing_1 = require('@angular/core/testing');
var relation_modal_content_component_1 = require('./relation-modal-content.component');
describe('RelationModalContentComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [relation_modal_content_component_1.RelationModalContentComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(relation_modal_content_component_1.RelationModalContentComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=relation-modal-content.component.spec.js.map