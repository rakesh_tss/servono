import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'multiSelectView'
})
export class MultiselectViewPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(value){
      value = value.toString();
      let arrayVal = value.split('__@@__');
      let resultVal= []
      arrayVal.forEach((item)=>{
        //console.log('item',item.split('__,__')[0]);
        if(item.split('__,__')[0])
          resultVal.push(item.split('__,__')[0]);
      })
      return resultVal.toString();
    }else{
      return '---';
    }
  }

}
