var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var ModuleViewPathPipe = (function () {
    function ModuleViewPathPipe() {
    }
    ModuleViewPathPipe.prototype.transform = function (value, args) {
        if (value) {
            value = value.toString();
            var arrayVal = value.split('__@@__');
            var resultVal = [];
            arrayVal.forEach(function (item) {
                //console.log('item',item.split('__,__')[0]);
                if (item.split('__,__')[0])
                    resultVal.push(item.split('__,__')[0]);
            });
            return resultVal.toString();
        }
        else {
            return '---';
        }
    };
    ModuleViewPathPipe = __decorate([
        core_1.Pipe({
            name: 'moduleViewPath'
        })
    ], ModuleViewPathPipe);
    return ModuleViewPathPipe;
})();
exports.ModuleViewPathPipe = ModuleViewPathPipe;
//# sourceMappingURL=module-view-path.pipe.js.map