import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'imagePath'
})
export class ImagePathPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    //console.log('args',args);

    if(value){
      if(args=='download'){

        return value.split('__@__')[0];

      }
      return value.split('__@__')[1];


    }else{
      return '';
    }
  }

}
