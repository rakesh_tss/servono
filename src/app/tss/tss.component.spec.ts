/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { TssComponent } from './tss.component';

describe('TssComponent', () => {
  let component: TssComponent;
  let fixture: ComponentFixture<TssComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TssComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
