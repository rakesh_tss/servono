var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var DetailsComponent = (function () {
    function DetailsComponent(activatedRoute, router, httpService, breadcrumbService) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.httpService = httpService;
        this.breadcrumbService = breadcrumbService;
        this.showForm = false;
    }
    DetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            _this.module = params['module'];
            _this.menu = params['menu'];
            _this.menuGroup = params['menuGroup'];
            _this.pkId = params['pkId'];
            //this.getFormFields();
            //breadcrumb
            _this.moduleListLink = 'tss/' + _this.menuGroup + '/' + _this.menu + '/' + _this.module;
            if (_this.pkId == 0) {
                _this.breadcrumbService.setBreadcrumb([{ label: _this.menu }, { label: _this.module, routerLink: ['/' + _this.moduleListLink] }, { label: 'Create' }]);
            }
            else {
                _this.breadcrumbService.setBreadcrumb([{ label: _this.menu }, { label: _this.module, routerLink: ['/' + _this.moduleListLink] }, { label: 'Update' }]);
            }
            _this.showForm = true;
        });
    };
    DetailsComponent.prototype.getFormFields = function () {
        var _this = this;
        this.showForm = false;
        var action = (this.pkId > 0) ? 'edit' : 'create';
        this.httpService.get('getModuleViewEditData/' + this.module + '/' + this.pkId + '/full/' + action).subscribe(function (res) {
            if (res) {
                _this.formFields = res.data;
                _this.showForm = true;
            }
        });
    };
    DetailsComponent.prototype.submitForm = function ($event) {
        this.router.navigate([this.moduleListLink]);
        /*   let param:any = {};
           param.Fields = [];//$event.blocks[0].fields;
           param.Module = this.module;
           param.Action = 'Update';
           param.pk_id = this.pkId;
           if(param.pk_id=="0" || param.pk_id == 0 || param.pk_id==''){
             param.Action = 'Create';
           }
           let fData = $event.blocks[0].fields;
           let fBlock = $event.blocks[0].block;
           for (let f in fData) {
             let obj:any = {};
             obj.name = fData[f].name;
             obj.db_field_name = fData[f].db_field_name;
             obj.supporting_field_name = fData[f].supporting_field_name;
             obj.pick_list_id = fData[f].pick_list_id;
             obj.value = fData[f].value;
             obj.type = fData[f].type;
             obj.block_pk_id = fBlock.pk_id;
             obj.rel = fData.rel;
             param.Fields.push(obj);
       
           }
           this.httpService.post('SaveUpdateModuleData', param).subscribe(res=> {
             if (res) {
               this.ngOnInit();
             }
           });*/
        //console.log('$event', $event);
    };
    DetailsComponent = __decorate([
        core_1.Component({
            selector: 'app-details',
            templateUrl: './details.component.html',
            styleUrls: ['./details.component.scss']
        })
    ], DetailsComponent);
    return DetailsComponent;
})();
exports.DetailsComponent = DetailsComponent;
//# sourceMappingURL=details.component.js.map