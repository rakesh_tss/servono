import {Router, ActivatedRoute, Params} from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {HttpService} from "../../_services/http.service";
import {Breadcrumb} from "primeng/primeng";
import {BreadcrumbService} from "../../_services/breadcrumb.service";
import {DynamicFormComponent} from "../dynamic-form/dynamic-form.component";
import {MenuItem} from "primeng/primeng";
import {Ng2FilterPipe} from "ng2-filter-pipe/dist/index";

@Component({
    selector: 'app-details',
    templateUrl: './details.component.html',
    styleUrls: ['./details.component.scss'],
    providers: [DynamicFormComponent]
})
export class DetailsComponent implements OnInit {

    module:string;
    menu:string;
    menuGroup:string;
    formFields:any;
    pkId:number;
    tabIndex:number;
    showForm:boolean;
    moduleListLink:string;
    steps:any[];
    stepFilter:any = {cnd_show: true, is_mandatory: true};
    activeIndex:number = 0;
    updateRelationFields:any = [];

    constructor(private activatedRoute:ActivatedRoute,
                private router:Router,
                private httpService:HttpService,
                private df:DynamicFormComponent,
                private breadcrumbService:BreadcrumbService) {
    }

    stepsReadonly:boolean = true;

    ngOnInit() {
        this.activatedRoute
            .queryParams
            .subscribe(params => {
                // Defaults to 0 if no query param provided.
                try {
                    this.tabIndex = +params['tabIndex'];
                    if(this.tabIndex){
                        this.activeIndex = this.tabIndex;
                    }
                } catch (e) {
                    console.log('e', e);
                }
            });
        this.activatedRoute.params.subscribe((params:Params) => {
            this.module = params['module'];
            this.menu = params['menu'];
            this.menuGroup = params['menuGroup'];
            this.pkId = params['pkId'];
            //this.getFormFields();
            //breadcrumb
            this.moduleListLink = 'tss/' + this.menuGroup + '/' + this.menu + '/' + this.module;
            if (this.pkId == 0) {
                this.breadcrumbService.setBreadcrumb([{label: this.menu}, {
                    label: this.module,
                    routerLink: ['/' + this.moduleListLink]
                }, {label: 'Create'}]);
            } else {
                this.stepsReadonly = false;

                this.breadcrumbService.setBreadcrumb([{label: this.menu}, {
                    label: this.module,
                    routerLink: ['/' + this.moduleListLink]
                }, {label: 'Update'}]);
            }
            this.showForm = false;
            setTimeout(() => this.showForm = true, 100);
        });
    }

    submitLabel:string = 'Submit';
    relTableShow:boolean = false;
    currentRel:any;

    getFormFields(formFields) {
        this.formFields = formFields;
        this.updateRelationFields = formFields.updateRelationFields;
        this.steps = [
            {
                label: 'Details Form',
                cnd_show: true,
                command: (event:any) => {
                    this.activeIndex = 0;
                }
            }
        ];
        if (formFields && formFields.updateRelationFields.length > 0) {
            if(this.pkId > 0){
                this.submitLabel = 'Update & Continue';
            }else{
                this.submitLabel = 'Create & Continue';
            }

            /*   formFields.updateRelationFields.forEach((item,index)=>{
             let obj:any = {};
             obj.label = item.name;
             obj.cnd_show = item.cnd_show;
             obj.relData = item;
             obj.command = (event:any) => {
             this.activeIndex = index+1;
             this.currentRel = item;
             this.relTableShow = false;
             setTimeout(() => this.relTableShow = true, 100);
             }
             this.steps.push(obj);
             })*/

        }

        if (this.activeIndex !=0 ) {
            if (this.getTabLength().length > 0) {
                this.stageClick(this.getTabLength()[0], 1);
            }
        }

        /*this.showForm = false;
         let action = (this.pkId>0)?'edit':'create';
         this.httpService.get('getModuleViewEditData/' + this.module+'/'+this.pkId+'/full/'+action).subscribe(res=> {
         if (res) {
         this.formFields = res.data;

         this.showForm = true;
         }
         })*/
    }
    onTabChange($event){
        console.log('$event',$event);
        if($event.index == 0){
            this.stageClick(null,0)
        }else{
            let rel = this.updateRelationFields[$event.index-1];
            rel.showTable = true;
            this.stageClick(rel,$event.index);
        }

    }

    stageClick(item, index) {
        if (index == 0) {
            this.activeIndex = 0;
        } else {
            if (!this.stepsReadonly) {
                item.showTable = true;
                this.activeIndex = index;
                //this.currentRel = item;
                //this.relTableShow = false;
                //setTimeout(() => this.relTableShow = true, 100);
            }
        }

    }

    submitForm($event):void {
        if (this.pkId == 0) {
            if (this.getTabLength().length > 0) {
                this.router.navigate([this.moduleListLink + '/' + $event.pk_id], {queryParams: {tabIndex: 1}});
            } else {
                this.router.navigate([this.moduleListLink]);
            }
        } else {
            if (this.getTabLength().length > 0) {
                this.stageClick(this.getTabLength()[0], 1);
            } else {
                this.router.navigate([this.moduleListLink]);
            }
        }
    }

    getTabLength() {
        let actRelFields = new Ng2FilterPipe().transform(this.updateRelationFields, this.stepFilter);
        return actRelFields;
    }
}
