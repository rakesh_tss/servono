import { Component, OnInit,Input } from '@angular/core';
import {Router, ActivatedRoute, Params,NavigationEnd} from '@angular/router';
import {HttpService} from "../../_services/http.service";
import {BreadcrumbService} from "../../_services/breadcrumb.service";
import {PermissionService} from "../../_services/permission.service";
import {ToasterService} from "angular2-toaster/angular2-toaster";
import {Location} from '@angular/common';
import {LazyLoadEvent} from "primeng/primeng";
import {SelectItem} from "primeng/primeng";

@Component({
    selector: 'tss-excel-import',
    templateUrl: './excel-import.component.html',
    styleUrls: ['./excel-import.component.css']
})
export class ExcelImportComponent implements OnInit {

    // inputs
    @Input() module:string = null;
    @Input() subRel:any = null;

    permission:any;
    baseUrl:string;
    uploadResult:any;
    successCountP:number;
    duplicateCountP:number;
    errorCountP:number;
    uploadResData:any;
    uploadUrl:string;
    // data table var
    dataRows:any[];
    totalRecords:number;
    tableShow:boolean = false;
    cols:any[];
    showCols:any[];
    columnOptions:SelectItem[];
    datableEvent:any;
    refreshTable:boolean = true;

    constructor(private _location:Location,
                private activatedRoute:ActivatedRoute,
                private access:PermissionService,
                private router:Router,
                private breadcrumbService:BreadcrumbService,
                private toasterService:ToasterService,
                private httpService:HttpService) {

    }

    ngOnInit() {
        this.uploadUrl = this.httpService.apiUrl + 'UploadRawFiles';
        this.baseUrl = this.httpService.apiUrl;
        this.permission = this.access.permission;
    }

    uploadFile() {
        this.uploadResult = null;
        let params:any = {};
        params.fileInfo = this.uploadResData;
        params.module = this.module;
        if(this.subRel){
            params.sourceModule = this.subRel;
            params.module = this.subRel.module;
        }
        this.httpService.post('ImportExcelFileModule/' + this.module, params).subscribe(res=> {
            if (res) {
                this.uploadResData = null;
                this.uploadResult = res;
                this.successCountP = parseFloat((res.succesCount / parseInt(res.totalCount) * 100).toFixed(2));
                this.duplicateCountP = parseFloat((res.duplicateCount / parseInt(res.totalCount) * 100).toFixed(2));
                this.errorCountP = parseFloat((res.errorCount / parseInt(res.totalCount) * 100).toFixed(2));
                this.getTableHeader();
            }
        })
    }

    onBeforeUpload(ev) {
        this.httpService.pendingRequestsNumber++;
        ev.formData.append('reqData', new Blob([JSON.stringify({action: 'import', module: this.module})], {
            type: "application/json"
        }));
        //ev.formData.append('field', JSON.stringify(field));
        //ev.xhr.withCredentials = true;
        ev['Content-Type'] = undefined;
    }

    onUpload(ev) {
        this.httpService.pendingRequestsNumber--;
        let res = JSON.parse(ev.xhr.response);
        if (res.success) {
            this.uploadResData = res.data;
        } else {
            this.toasterService.pop('error', 'Error', res.message);
        }
    }

    // data table function
    getTableHeader() {
        this.tableShow = false;
        this.httpService.get('getModuleListMetadata/' + this.module).subscribe(res=> {
            if (res) {
                this.cols = res.data;
                this.showCols = [];
                this.columnOptions = [];
                for (let key in this.cols) {
                    if (!this.cols[key].is_hidden) {
                        if (this.cols[key].type !== 'Image') {
                            if (this.cols[key].derived_field_type != 1 && this.cols[key].derived_field_type != 2)
                                this.showCols.push(this.cols[key]);

                            this.columnOptions.push({label: this.cols[key].name, value: this.cols[key]});
                        }

                    }
                }
                if (this.showCols.length > 0) {
                    this.tableShow = true;
                }
            }
        })
    }


    loadDataLazy(event:LazyLoadEvent) {
        this.datableEvent = event;
        this.datableEvent.displayCols = this.cols;
        if (this.uploadResult && this.uploadResult.fileInfo) {
            this.datableEvent.UUID = this.uploadResult.fileInfo.UUID
        }

        this.httpService.postTable('getModuleListData/' + this.module, this.datableEvent).subscribe(res=> {
            if (res) {
                this.dataRows = res.jqGridData.rows;
                this.totalRecords = res.jqGridData.records;
            }
        })
    }

    tableReload() {
        this.tableShow = false;
        setTimeout(() => this.tableShow = true, 100);
    }

}
