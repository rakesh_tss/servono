import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule}  from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TssRoutingModule } from './tss-routing.module';
import {TssComponent} from './tss.component';
import {
    SplitButtonModule,
    RatingModule,
    GMapModule,
    TabMenuModule,
    ChipsModule,
    AutoCompleteModule,
    ChartModule,
    CarouselModule,
    StepsModule,
    ProgressBarModule
} from "primeng/primeng";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {DetailsComponent} from "./details/details.component";
import {ViewComponent} from "./view/view.component";
import {FormFieldComponent} from "./form-field/form-field.component";
import {RelationModalContent} from "./form-field/form-field.component";
import { RelationModalContentComponent } from './relation-modal-content/relation-modal-content.component';
import {DatexPipe} from "../_pipe/datex.pipe";
import {ImagePathPipe} from "./_pipe/image-path.pipe";
import {DynamicFormComponent} from "./dynamic-form/dynamic-form.component";
import { ModuleViewPathPipe } from './_pipe/module-view-path.pipe';
import { RelationTableComponent } from './relation-table/relation-table.component';
import {PermissionService} from "../_services/permission.service";
import {CommonTssModule} from "../shared/commonTssModules";
import { TableColFieldViewComponent } from './table-col-field-view/table-col-field-view.component';
import {MaterialModule} from '@angular/material';
import {TssGuard} from "./_services/tss.guard";
import {DynamicFormService} from "./_services/dynamic-form.service";
import {TssService} from "./_services/tss.service";
import {StageViewTemplateComponent} from "./stage-view-template/stage-view-template.component";
import { SafeHtmlPipe } from './_pipe/safe-html.pipe';
import {DynamicComponentModule} from "angular2-dynamic-component/DynamicComponentModule";
import {RoundProgressModule} from "angular-svg-round-progressbar/dist/index";
import {Daterangepicker} from "ng2-daterangepicker/index";
import { MultiselectViewPipe } from './_pipe/multiselect-view.pipe';
import { ExcelImportComponent } from './excel-import/excel-import.component';
import { ExcelImpoartPageComponent } from './excel-impoart-page/excel-impoart-page.component';
import {CustomComponentModule} from "./custom-component/custom-component.module";
import {DynamicFormModule} from "./dynamic-form/dynamic-form.module";
import { ViewTemplatesComponent } from './view/view-templates/view-templates.component';
import { KanbanComponent } from './table-template/kanban/kanban.component';
import {DndModule} from "ng2-dnd/index";
import { Template1Component } from './table-template/template-1/template-1.component';
import { Template2Component } from './table-template/template-2/template-2.component';
import { Template3Component } from './table-template/template-3/template-3.component';
import { CalendarViewComponent } from './table-template/calendar-view/calendar-view.component';
import {CreateMeetingComponent} from "./view/view-templates/create-meeting/create-meeting.component";
@NgModule({
    imports: [
        CommonTssModule,
        TssRoutingModule,
        SplitButtonModule,
        RatingModule,
        TabMenuModule,
        ChipsModule,
        AutoCompleteModule,
        MaterialModule,
        ChartModule,
        DynamicComponentModule,
        CarouselModule,
        StepsModule,
        RoundProgressModule,
        ProgressBarModule,
        CustomComponentModule,
        DynamicFormModule,
        DndModule.forRoot()
    ],
    declarations: [
        TssComponent,
        DetailsComponent,
        ViewComponent,
        RelationTableComponent,
        StageViewTemplateComponent,
        SafeHtmlPipe,
        ExcelImportComponent,
        ExcelImpoartPageComponent,
        ViewTemplatesComponent,
        KanbanComponent,
        Template1Component,
        Template2Component,
        Template3Component,
        CalendarViewComponent,
        CreateMeetingComponent
    ],

    providers: [DynamicFormService,TssGuard,PermissionService,TssService]
})
export class TssModule {
}
