import { Component, OnInit,ElementRef,ViewChild,NgZone } from '@angular/core';
import {TssComponent} from "../../tss.component";

@Component({
  selector: 'app-kanban',
  templateUrl: './kanban.component.html',
  styleUrls: ['./kanban.component.scss']
})
export class KanbanComponent implements OnInit {

  constructor(private ngZone:NgZone,private tc:TssComponent) {

  }
  @ViewChild('customTree') mainDiv: ElementRef;
  myHeight:any;
  myWidth:any;
  setStyles(){
    this.myHeight = window.innerHeight;
    this.myWidth = window.innerWidth;
    let styles={
      height:this.myHeight-280+'px',
    }
    return styles;
  }
  listData:any = [
    {
      label:'LD#2017',
      title:'Dell Technologies',
      phone:'9876543212',
      date:'05 July, 2017',
      time:'51 hr'
    },
    {
      label:'LD#2017',
      title:'Test Lead',
      phone:'',
      date:'05 July, 2017',
      time:'10 hr'
    },
    {
      label:'LD#2017',
      title:'Wanda Test2',
      phone:'9876543212',
      date:'05 July, 2017',
      time:'15 hr'
    },
    {
      label:'LD#2017',
      title:'QA',
      phone:'9876543212',
      date:'05 July, 2017',
      time:'1 hr'
    },
    {
      label:'LD#2017',
      title:'Marny Wilkerson',
      phone:'9876543212',
      date:'05 July, 2017',
      time:'12 hr'
    },
    {
      label:'LD#2017',
      title:'Marny Wilkerson',
      phone:'9876543212',
      date:'05 July, 2017',
      time:'12 hr'
    }
  ];
  stages:any =[
    {label:'Stage-7',data:this.listData
    },
    {label:'Stage-2',data:[]},
    {label:'Stage-3',data:[]},
    {label:'Stage-4',data:[]},
    {label:'Stage-5',data:[]}
  ];

  stageList:any =[];
  rowsList:any = [];
  ngOnInit() {
    this.stageList = Object.assign([] ,this.tc.templateInfo.App_Stage.data);
    this.rowsList = Object.assign([] ,this.tc.dataRows);
    let stageKey = this.tc.templateInfo.App_Stage.name;
    this.stageList.forEach((stage)=>{
      stage.data = [];
    });
    this.rowsList.forEach((row)=>{
      this.stageList.forEach((stage)=>{
        if(!stage.data){
          stage.data = [];
        }
        if(stage.value == row[stageKey]){
          stage.data.push(row);
        }

      })

    })
    //console.log('this.stageList ',this.stageList );
    //console.log('this.dataRows ',this.dataRows );

  }

}
