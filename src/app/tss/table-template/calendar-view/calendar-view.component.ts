import { Component, OnInit } from '@angular/core';
import {SelectItem} from 'primeng/primeng';
@Component({
  selector: 'app-calendar-view',
  templateUrl: './calendar-view.component.html',
  styleUrls: ['./calendar-view.component.scss']
})
export class CalendarViewComponent implements OnInit {
  cities: SelectItem[];

  selectedCity: string;

  constructor() {
    this.cities = [];
    this.cities.push({label:'Weekly on Tuesday', value:null});
    this.cities.push({label:'Sunday', value:{id:1, name: 'New York', code: 'NY'}});
    this.cities.push({label:'Monday', value:{id:2, name: 'Rome', code: 'RM'}});
    this.cities.push({label:'Wednesday', value:{id:3, name: 'London', code: 'LDN'}});
    this.cities.push({label:'Thursday', value:{id:4, name: 'Istanbul', code: 'IST'}});
    this.cities.push({label:'Friday', value:{id:5, name: 'Paris', code: 'PRS'}});
  }
  ngOnInit() {
  }
  foods = [
    {value: 'steak-0', viewValue: 'Monthly'},
    {value: 'pizza-1', viewValue: 'Quarterly'},
    {value: 'tacos-2', viewValue: 'Yearly'}
  ];
  years = [
    {value: 'steak-0', viewValue:2017},
    {value: 'pizza-1', viewValue:2018},
    {value: 'tacos-2', viewValue:2019}
  ];
}
