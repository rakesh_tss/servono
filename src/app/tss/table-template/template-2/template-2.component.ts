import { Component, OnInit } from '@angular/core';
import {TssComponent} from "../../tss.component";

@Component({
  selector: 'table-template-2',
  templateUrl: './template-2.component.html',
  styleUrls: ['./template-2.component.scss']
})
export class Template2Component implements OnInit {

  constructor(private tc:TssComponent) {

  }

  ngOnInit() {
  }

}
