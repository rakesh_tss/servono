/* tslint:disable:no-unused-variable */
var testing_1 = require('@angular/core/testing');
var tss_component_1 = require('./tss.component');
describe('TssComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [tss_component_1.TssComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(tss_component_1.TssComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=tss.component.spec.js.map