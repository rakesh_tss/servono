var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var tss_component_1 = require('./tss.component');
var details_component_1 = require("./details/details.component");
var view_component_1 = require("./view/view.component");
var tss_guard_1 = require("../_services/tss.guard");
var routes = [
    {
        path: ":menuGroup/:menu/:module",
        canActivate: [tss_guard_1.TssGuard],
        data: {
            breadcrumb: 'List',
            action: 'View'
        },
        component: tss_component_1.TssComponent
    },
    {
        path: ':menuGroup/:menu/:module/:pkId/:action',
        component: view_component_1.ViewComponent,
        canActivate: [tss_guard_1.TssGuard],
        data: {
            breadcrumb: 'List',
            action: 'View'
        }
    },
    {
        path: ':menuGroup/:menu/:module/:pkId',
        component: details_component_1.DetailsComponent,
        canActivate: [tss_guard_1.TssGuard],
        data: {
            breadcrumb: 'List',
            action: 'Edit'
        }
    }
];
var TssRoutingModule = (function () {
    function TssRoutingModule() {
    }
    TssRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule],
            providers: []
        })
    ], TssRoutingModule);
    return TssRoutingModule;
})();
exports.TssRoutingModule = TssRoutingModule;
//# sourceMappingURL=tss-routing.module.js.map