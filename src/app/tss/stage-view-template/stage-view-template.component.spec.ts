import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StageViewTemplateComponent } from './stage-view-template.component';

describe('StageViewTemplateComponent', () => {
  let component: StageViewTemplateComponent;
  let fixture: ComponentFixture<StageViewTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StageViewTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StageViewTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
