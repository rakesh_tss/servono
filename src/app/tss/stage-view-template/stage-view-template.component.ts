import { Component, OnInit,Input,Renderer} from '@angular/core';

@Component({
    selector: 'tss-stage-view-template',
    templateUrl: './stage-view-template.component.html',
    styleUrls: ['./stage-view-template.component.css']
})
export class StageViewTemplateComponent implements OnInit {

    @Input() stages:any = [];
    @Input() activeStages:any = [];
    @Input() moreInfo:boolean = false;

    constructor(private _renderer:Renderer) {
    }

    stageInfo:any = [];
    stageColors:string[] = [];
    searchT:any = {active: true};
    searchF:any = {active: false};

    ngOnInit() {
        if (this.stages) {
            this.stageInfo = [];
            this.stages.forEach((stage, i)=> {
                let obj:any = {};
                obj.backGroundColor = '#ffffff';
                obj.fontColor = this.invertColor(obj.backGroundColor);
                obj.date = '---';
                obj.active = false;
                obj.status = '---';
                this.stageInfo.push(obj);
                if (this.activeStages) {
                    this.activeStages.forEach((history, index)=> {
                        if (stage.pk_id == history.stage_id) {
                            this.stageInfo[i].backGroundColor = stage.color;
                            this.stageInfo[i].fontColor = this.invertColor(stage.color);
                            this.stageInfo[i].status = history.status;
                            this.stageInfo[i].happened_on = history.happened_on;
                            if (this.activeStages.length - 1 == index) {
                                this.stageInfo[i].active = true;
                            }
                        }
                    })
                }
            })
        }

    }

    changeStageListClass(content) {
        if (content.classList.contains('list-show')) {
            this._renderer.setElementClass(content, 'list-show', false);
        } else {
            this._renderer.setElementClass(content, 'list-show', true);
        }
    }

    invertColor(hexTripletColor) {
        var color = hexTripletColor;
        color = color.substring(1);           // remove #
        color = parseInt(color, 16);          // convert to integer
        color = 0xFFFFFF ^ color;             // invert three bytes
        color = color.toString(16);           // convert to hex
        color = ("000000" + color).slice(-6); // pad with leading zeros
        color = "#" + color;                  // prepend #
        return color;
    }

}
