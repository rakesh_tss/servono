var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var primeng_1 = require("primeng/primeng");
var TssComponent = (function () {
    function TssComponent(activatedRoute, access, router, breadcrumbService, confirmationService, httpService) {
        this.activatedRoute = activatedRoute;
        this.access = access;
        this.router = router;
        this.breadcrumbService = breadcrumbService;
        this.confirmationService = confirmationService;
        this.httpService = httpService;
        this.tableShow = false;
        this.dataRow = {};
        this.formFields = [];
        this.showForm = false;
        this.baseUrl = '';
        this.refreshTable = true;
        this.gbs = null;
        this.status = { isopen: false };
        this.actionItem = [];
    }
    TssComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.baseUrl = this.httpService.apiUrl;
        this.activatedRoute.params.subscribe(function (params) {
            _this.permission = _this.access.permission;
            _this.module = params['module'];
            _this.menu = params['menu'];
            _this.menuGroup = params['menuGroup'];
            _this.getTableHeader();
            //this.getFormFields();
            _this.currentUrl = 'tss/' + _this.menuGroup + '/' + _this.menu + '/' + _this.module;
            _this.breadcrumbService.setBreadcrumb([{ label: _this.menu }, { label: _this.module }]);
        });
    };
    TssComponent.prototype.formReload = function () {
        var _this = this;
        this.showForm = false;
        setTimeout(function () { return _this.showForm = true; }, 100);
        /*  this.httpService.get('getModuleAddEditMetaData/' + this.module + '/Quick').subscribe(res=> {
         if (res) {
         this.formFields = res.data;
         this.showForm = true;
         }
         })*/
    };
    TssComponent.prototype.getTableHeader = function () {
        var _this = this;
        this.tableShow = false;
        this.httpService.get('getModuleListMetadata/' + this.module).subscribe(function (res) {
            if (res) {
                _this.cols = res.data;
                _this.showCols = [];
                _this.columnOptions = [];
                for (var key in _this.cols) {
                    if (!_this.cols[key].is_hidden) {
                        if (_this.cols[key].type !== 'Image')
                            _this.showCols.push(_this.cols[key]);
                    }
                    _this.columnOptions.push({ label: _this.cols[key].name, value: _this.cols[key] });
                }
                if (_this.showCols.length > 0) {
                    _this.tableShow = true;
                    _this.formReload();
                }
            }
        });
    };
    TssComponent.prototype.tableReload = function () {
        var _this = this;
        this.refreshTable = false;
        setTimeout(function () { return _this.refreshTable = true; }, 100);
    };
    TssComponent.prototype.loadDataLazy = function (event) {
        var _this = this;
        this.datableEvent = event;
        if (this.gbs) {
            this.datableEvent.globalSearch = this.gbs;
        }
        this.datableEvent.displayCols = this.cols;
        this.httpService.postTable('getModuleListData/' + this.module, this.datableEvent).subscribe(function (res) {
            if (res) {
                _this.dataRows = res.jqGridData.rows;
                _this.totalRecords = res.jqGridData.records;
            }
        });
    };
    TssComponent.prototype.onCreate = function () {
        var params = {};
        params.menuGroup = this.menuGroup;
        params.menu = this.menu;
        params.module = this.module;
        params.pkId = 0;
        var url = 'tss/' + this.menuGroup + '/' + this.menu + '/' + this.module + '/0';
        this.router.navigate([this.currentUrl + '/0']);
    };
    TssComponent.prototype.onView = function (row) {
        console.log('row', row);
        if (this.permission.View) {
            this.router.navigate([this.currentUrl + '/' + row.pk_id + '/view']);
        }
    };
    TssComponent.prototype.onUpdate = function (row) {
        this.router.navigate([this.currentUrl + '/' + row.pk_id]);
        //this.onCreate();
        /* this.dialogTitle = 'Update Menu';
         console.log('row', row);
         this.dataRow = row;
         this.displayDialog = true;*/
    };
    TssComponent.prototype.onDelete = function (row) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: function () {
                //Actual logic to perform a confirmation
            }
        });
    };
    TssComponent.prototype.dialogClose = function () {
        this.displayDialog = false;
    };
    TssComponent.prototype.submitForm = function ($event) {
        this.tableReload();
        this.formReload();
        /* let param:any = {};
         param.Fields = [];//$event.blocks[0].fields;
         param.Module = this.module;
         param.Action = 'Create';
         let fData = $event.blocks[0].fields;
         let fBlock = $event.blocks[0].block;
         for (let f in fData) {
         let obj:any = {};
         obj.name = fData[f].name;
         obj.db_field_name = fData[f].db_field_name;
         obj.supporting_field_name = fData[f].supporting_field_name;
         obj.pick_list_id = fData[f].pick_list_id;
         obj.value = fData[f].value;
         obj.type = fData[f].type;
         obj.block_pk_id = fBlock.pk_id;
         param.Fields.push(obj);

         }
         this.httpService.post('SaveUpdateModuleData', param).subscribe(res=> {
         if (res) {
         this.ngOnInit();
         }
         });*/
        //console.log('$event', $event);
    };
    TssComponent.prototype.keys = function (data) {
        // console.log('Object.keys(data)',Object.keys(data));
        return Object.keys(data);
    };
    TssComponent.prototype.getRandomInt = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    TssComponent.prototype.toggled = function (open) {
    };
    TssComponent.prototype.toggleDropdown = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        this.status.isopen = !this.status.isopen;
    };
    TssComponent.prototype.getMenuForActions = function (contain, event, row) {
        var _this = this;
        this.actionItem = [];
        if (this.permission.View) {
            this.actionItem.push({
                label: 'View',
                icon: 'fa-eye',
                routerLink: ['/' + this.currentUrl + '/' + row.pk_id + '/view']
            });
        }
        if (this.permission.Edit) {
            this.actionItem.push({
                label: 'Update',
                icon: 'fa-pencil',
                routerLink: ['/' + this.currentUrl + '/' + row.pk_id]
            });
        }
        if (this.permission.Delete) {
            this.actionItem.push({
                label: 'Delete', icon: 'fa-trash', command: function (event) {
                    _this.delete(row);
                }
            });
        }
        contain.toggle(event);
        //return this.actionItem;
    };
    TssComponent.prototype.delete = function (row) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Do you want to delete this record?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.httpService.post('DeleteModuleRow' + '/' + _this.module + '/' + row.pk_id, {}).subscribe(function (res) {
                    if (res) {
                        _this.tableReload();
                    }
                });
            }
        });
    };
    TssComponent = __decorate([
        core_1.Component({
            selector: 'app-tss',
            templateUrl: './tss.component.html',
            styleUrls: ['./tss.component.scss'],
            providers: [primeng_1.ConfirmationService]
        })
    ], TssComponent);
    return TssComponent;
})();
exports.TssComponent = TssComponent;
//# sourceMappingURL=tss.component.js.map