import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExcelImpoartPageComponent } from './excel-impoart-page.component';

describe('ExcelImpoartPageComponent', () => {
  let component: ExcelImpoartPageComponent;
  let fixture: ComponentFixture<ExcelImpoartPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExcelImpoartPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExcelImpoartPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
