import { Component, OnInit } from '@angular/core';
import {BreadcrumbService} from "../../_services/breadcrumb.service";
import {Router, ActivatedRoute, Params,NavigationEnd} from '@angular/router';
import {PermissionService} from "../../_services/permission.service";
import {Location} from '@angular/common';

@Component({
    selector: 'app-excel-impoart-page',
    templateUrl: './excel-impoart-page.component.html',
    styleUrls: ['./excel-impoart-page.component.css']
})
export class ExcelImpoartPageComponent implements OnInit {

    module:string;
    menu:string;
    menuGroup:string;
    moduleListLink:string;
    permission:any;

    constructor(private _location:Location,
                private activatedRoute:ActivatedRoute,
                private access:PermissionService,
                private router:Router,
                private breadcrumbService:BreadcrumbService) {
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params:Params) => {

            this.permission = this.access.permission;
            this.module = params['module'];
            this.menu = params['menu'];
            this.menuGroup = params['menuGroup'];
            //module list link
            this.moduleListLink = 'tss/' + this.menuGroup + '/' + this.menu + '/' + this.module;

            this.breadcrumbService.setBreadcrumb([{label: this.menu}, {
                label: this.module,
                routerLink: ['/' + this.moduleListLink]
            }, {label: 'Import'}]);

        });
    }


}
