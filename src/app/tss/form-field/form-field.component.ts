import { Component, Injectable,OnInit, Input, Output, EventEmitter,Pipe} from '@angular/core';
import {NgbModal, ModalDismissReasons,NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
@Component({
    selector: 'relation-modal-content2',
    templateUrl: './relation-modal-content.html'
})
export class RelationModalContent {
    @Input() name;
    tableShow:boolean= false;
    constructor(private activeModal:NgbActiveModal) {

    }
}


@Component({
    selector: 'form-field',
    templateUrl: './form-field.component.html',
    styleUrls: ['./form-field.component.css']
})
export class FormFieldComponent implements OnInit {
    @Input() field:any = {};
    @Input() viewType:string = 'view';
    @Input() module:string;
    @Input() baseUrl:string;


    constructor(private modalService:NgbModal) {

    }

    f:any = {}
    defaultValue = '---';

    ngOnInit() {
        this.f = this.field;
    }
    relationModelOpen(field) {
        const modalRef = this.modalService.open(RelationModalContent);
        modalRef.componentInstance.field = field;
    }

    saveField(f, pInplace) {
    }

}
