var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var RelationModalContent = (function () {
    function RelationModalContent(activeModal) {
        this.activeModal = activeModal;
        this.tableShow = false;
    }
    __decorate([
        core_1.Input()
    ], RelationModalContent.prototype, "name");
    RelationModalContent = __decorate([
        core_1.Component({
            selector: 'relation-modal-content2',
            templateUrl: './relation-modal-content.html'
        })
    ], RelationModalContent);
    return RelationModalContent;
})();
exports.RelationModalContent = RelationModalContent;
var FormFieldComponent = (function () {
    function FormFieldComponent(modalService) {
        this.modalService = modalService;
        this.field = {};
        this.viewType = 'view';
        this.f = {};
        this.defaultValue = '---';
    }
    FormFieldComponent.prototype.ngOnInit = function () {
        this.f = this.field;
    };
    FormFieldComponent.prototype.relationModelOpen = function (field) {
        var modalRef = this.modalService.open(RelationModalContent);
        modalRef.componentInstance.field = field;
    };
    FormFieldComponent.prototype.saveField = function (f, pInplace) {
    };
    __decorate([
        core_1.Input()
    ], FormFieldComponent.prototype, "field");
    __decorate([
        core_1.Input()
    ], FormFieldComponent.prototype, "viewType");
    __decorate([
        core_1.Input()
    ], FormFieldComponent.prototype, "module");
    __decorate([
        core_1.Input()
    ], FormFieldComponent.prototype, "baseUrl");
    FormFieldComponent = __decorate([
        core_1.Component({
            selector: 'form-field',
            templateUrl: './form-field.component.html',
            styleUrls: ['./form-field.component.css']
        })
    ], FormFieldComponent);
    return FormFieldComponent;
})();
exports.FormFieldComponent = FormFieldComponent;
//# sourceMappingURL=form-field.component.js.map