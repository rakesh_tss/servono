var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var tss_routing_module_1 = require('./tss-routing.module');
var tss_component_1 = require('./tss.component');
var primeng_1 = require("primeng/primeng");
var ng_bootstrap_1 = require('@ng-bootstrap/ng-bootstrap');
var details_component_1 = require("./details/details.component");
var view_component_1 = require("./view/view.component");
var form_field_component_1 = require("./form-field/form-field.component");
var form_field_component_2 = require("./form-field/form-field.component");
var dynamic_form_service_1 = require("../_services/dynamic-form.service");
var relation_modal_content_component_1 = require('./relation-modal-content/relation-modal-content.component');
var image_path_pipe_1 = require("./_pipe/image-path.pipe");
var dynamic_form_component_1 = require("./dynamic-form/dynamic-form.component");
var module_view_path_pipe_1 = require('./_pipe/module-view-path.pipe');
var relation_table_component_1 = require('./relation-table/relation-table.component');
var tss_guard_1 = require("../_services/tss.guard");
var permission_service_1 = require("../_services/permission.service");
var commonTssModules_1 = require("../shared/commonTssModules");
var table_col_field_view_component_1 = require('./table-col-field-view/table-col-field-view.component');
var material_1 = require('@angular/material');
var TssModule = (function () {
    function TssModule() {
    }
    TssModule = __decorate([
        core_1.NgModule({
            imports: [
                commonTssModules_1.CommonTssModule,
                tss_routing_module_1.TssRoutingModule,
                primeng_1.SplitButtonModule,
                primeng_1.RatingModule,
                primeng_1.GMapModule,
                primeng_1.TabMenuModule,
                primeng_1.ChipsModule,
                primeng_1.AutoCompleteModule,
                ng_bootstrap_1.NgbModule.forRoot(),
                material_1.MaterialModule
            ],
            declarations: [
                tss_component_1.TssComponent,
                details_component_1.DetailsComponent,
                dynamic_form_component_1.DynamicFormComponent,
                view_component_1.ViewComponent,
                form_field_component_1.FormFieldComponent,
                form_field_component_2.RelationModalContent,
                relation_modal_content_component_1.RelationModalContentComponent,
                image_path_pipe_1.ImagePathPipe,
                module_view_path_pipe_1.ModuleViewPathPipe,
                relation_table_component_1.RelationTableComponent,
                table_col_field_view_component_1.TableColFieldViewComponent
            ],
            entryComponents: [form_field_component_2.RelationModalContent],
            providers: [dynamic_form_service_1.DynamicFormService, tss_guard_1.TssGuard, permission_service_1.PermissionService]
        })
    ], TssModule);
    return TssModule;
})();
exports.TssModule = TssModule;
//# sourceMappingURL=tss.module.js.map