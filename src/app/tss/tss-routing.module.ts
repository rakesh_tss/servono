import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TssComponent} from './tss.component';
import {DetailsComponent} from "./details/details.component";
import {ViewComponent} from "./view/view.component";
import {TssGuard} from "./_services/tss.guard";
import {ExcelImportComponent} from "./excel-import/excel-import.component";
import {ExcelImpoartPageComponent} from "./excel-impoart-page/excel-impoart-page.component";
const routes:Routes = <Routes>[
    {
        path: ":menuGroup/:menu/:module",
        canActivate: [TssGuard],
        data: {
            breadcrumb: 'List',
            action:'View'
        },
        component: TssComponent,

       /* children: [
            {
                path: 'details/:menuGroup/:menu/:module',
                component: DetailsComponent,
                data: {
                    title: 'Details'
                }
            }
        ]*/
    },
    {
        path: ':menuGroup/:menu/:module/view/:pkId',
        component: ViewComponent,
        canActivate: [TssGuard],
        data: {
            breadcrumb: 'View',
            action:'View'

        }
    },
    {
        path: ':menuGroup/:menu/:module/view/:pkId/:tabName',
        component: ViewComponent,
        canActivate: [TssGuard],
        data: {
            breadcrumb: 'View',
            action:'View'

        }
    },
    {
        path: ':menuGroup/:menu/:module/:pkId',
        component: DetailsComponent,
        canActivate: [TssGuard],
        data: {
            breadcrumb: 'List',
            action:'Edit'
        }
    },
    {
        path: ':module/0',
        component: DetailsComponent,
        data: {
            breadcrumb: 'List',
            action:'Edit'
        }
    },
    {
        path: ':menuGroup/:menu/:module/excel-import/:tabName',
        component: ExcelImpoartPageComponent,
        canActivate: [TssGuard],
        data: {
            breadcrumb: 'View',
            action:'Edit'

        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class TssRoutingModule {

}
