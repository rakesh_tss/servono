import { Component, OnInit,Input } from '@angular/core';
import {HttpService} from "../../_services/http.service";

@Component({
  selector: 'tss-table-col-field-view',
  templateUrl: './table-col-field-view.component.html',
  styleUrls: ['./table-col-field-view.component.css']
})
export class TableColFieldViewComponent implements OnInit {

  constructor(private hs:HttpService) { }
  @Input() row:any;
  @Input() col:any;
  @Input() module:string;
  baseUrl:string;
  defaultValue:string='---';
  ngOnInit() {
    this.baseUrl = this.hs.apiUrl;
  }

}
