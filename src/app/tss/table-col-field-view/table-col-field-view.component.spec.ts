import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableColFieldViewComponent } from './table-col-field-view.component';

describe('TableColFieldViewComponent', () => {
  let component: TableColFieldViewComponent;
  let fixture: ComponentFixture<TableColFieldViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableColFieldViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableColFieldViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
