import { TestBed, inject } from '@angular/core/testing';

import { TssService } from './tss.service';

describe('TssService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TssService]
    });
  });

  it('should ...', inject([TssService], (service: TssService) => {
    expect(service).toBeTruthy();
  }));
});
