var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var datex_pipe_1 = require("../../_pipe/datex.pipe");
var DynamicFormService = (function () {
    function DynamicFormService() {
        this.buildFields = [];
        this.fields = [];
    }
    DynamicFormService.prototype.setDynamicFormFieldFormat = function (fieldData, type) {
        var _this = this;
        try {
            this.fields = fieldData;
            fieldData.relationFields = [];
            fieldData.updateRelationFields = [];
            fieldData.headerFields = [];
            fieldData.showRelationDetails = [];
            fieldData.featuredImage = '';
            fieldData.blocks.forEach(function (b, index) {
                b.cnd_show = true;
                _this.checkConditions(b, 'block');
                b.showFields = [];
                b.fields.forEach(function (fData, fIndex) {
                    //fData.cnd_show = true;
                    fData.cnd_show = true;
                    if (fData.FieldType) {
                        fData.type = fData.FieldType;
                    }
                    fData.display_label = (fData.display_label) ? fData.display_label : fData.name;
                    var skipField = false;
                    //condition for if form type quick only show create and required filed
                    //fData.is_hidden === true || fData.is_fixed_field === true
                    if (fData.is_hidden) {
                        skipField = true;
                    }
                    if (fData.header_view) {
                        fieldData.headerFields.push(fData);
                    }
                    if (fData.type == 'Multi-Select Combo Box' || fData.type == 'Pick List') {
                        _this.getOptionsList(fData);
                    }
                    else if (fData.type == 'Relation') {
                        _this.setRelationBindData(fData);
                        if (fData.rel == 1) {
                            _this.setRelationDisplayName(fData);
                        }
                        else {
                            skipField = true;
                            /*
                             if (type == 'view') {
                             skipField = true;
                             }else if(!fData.is_mandatory){
                             skipField = true;
                             }*/
                            fData.col = 12;
                            if (fData.data) {
                                //for edit / update form
                                if (fData.is_mandatory == true) {
                                    fieldData.updateRelationFields.push(fData);
                                }
                                if (fData.show_in_details) {
                                    fieldData.showRelationDetails.push(fData);
                                }
                                else {
                                    fieldData.relationFields.push(fData);
                                }
                            }
                        }
                    }
                    else if (fData.type == 'Time' || fData.type == 'Date' || fData.type == 'Time Stamp') {
                        if (!fData.value || fData.value == "") {
                            fData.value = '';
                        }
                        else {
                            fData.displayName = new datex_pipe_1.DatexPipe().transform(fData.value, fData.type);
                            //console.log('fData.value',fData.value);
                            fData.value = new Date(fData.value);
                        }
                    }
                    else if (fData.type == 'Image') {
                        if (type == 'view') {
                            skipField = true;
                        }
                        //console.log('fData',fData);
                        if (fData.value)
                            fieldData.featuredImage = fData.value;
                    }
                    else if (fData.type == 'Text Area' || fData.type == 'Html/Text Editor') {
                        fData.col = 12;
                    }
                    else if (fData.type == 'Owner') {
                        if (type != 'view') {
                            skipField = true;
                        }
                    }
                    else {
                    }
                    _this.checkConditions(fData, 'field');
                    if (!fData.extra_options) {
                        fData.extra_options = {};
                        fData.extra_options.lg = 3;
                        fData.extra_options.md = 6;
                        fData.extra_options.sm = 12;
                    }
                    else {
                        fData.extra_options = JSON.parse(fData.extra_options);
                    }
                    if (type == 'quick') {
                        fData.extra_options.lg = 12;
                        if (fData.quick_create || fData.is_mandatory || !fData.cnd_show) {
                        }
                        else {
                            skipField = true;
                        }
                    }
                    if (type == 'formBuilder') {
                        if (fData.is_fixed_field) {
                            skipField = true;
                        }
                    }
                    if (!skipField) {
                        b.showFields.push(fData);
                    }
                });
            });
            return fieldData;
        }
        catch (e) {
            console.log('e', e);
        }
    };
    DynamicFormService.prototype.setRelationBindData = function (field) {
        var _this = this;
        if (this.fields['Relations Data']) {
            this.fields['Relations Data'].forEach(function (data, index) {
                if (data.from_field == field.name) {
                    field.displayName = "";
                    if (_this.fields.pk_id) {
                        field.current_module = {};
                        field.current_module.module = _this.fields.module.module_label;
                        field.current_module.module_id = _this.fields.module.pk_id;
                        field.current_module.id = _this.fields.pk_id;
                    }
                    else {
                        field.current_module = {};
                        field.current_module.module = _this.fields.module.module_label;
                        field.current_module.UUID = _this.fields.UUID;
                    }
                    field.data = data;
                }
            });
        }
    };
    DynamicFormService.prototype.setRelationDisplayName = function (field) {
        if (field.value) {
            var i = 0;
            for (var key in field.value) {
                if (i == 1) {
                    if (field.value[key]) {
                        field.displayName = field.value[key];
                    }
                    return false;
                }
                i++;
            }
        }
    };
    DynamicFormService.prototype.getOptionsList = function (field) {
        try {
            var result = [];
            //field.value = '';
            var _data = [];
            var key = field.name.toString() + ' Data';
            var data = this.fields['Pick List Data'][key];
            if (field.type == 'Pick List') {
                var obj = {};
                obj.label = 'Select';
                obj.value = null;
                _data.push(obj);
            }
            if (field.type == 'Multi-Select Combo Box') {
                var preSelected = [];
                field.displayName = null;
                if (field.value) {
                    var preSelected_1 = field.value;
                    var displayNameArry = [];
                    var _value = [];
                    for (var _i = 0, _a = field.value; _i < _a.length; _i++) {
                        var v = _a[_i];
                        var obj = {};
                        obj.pk_id = v.pk_id;
                        obj.value = v.value;
                        _value.push(v.pk_id);
                        displayNameArry.push(obj.value);
                    }
                    field.displayName = displayNameArry.toString();
                    field.value = _value;
                }
            }
            for (var _b = 0; _b < data.length; _b++) {
                var v = data[_b];
                var obj = {};
                obj.label = v.value;
                obj.value = { pk_id: v.pk_id, value: v.value };
                if (field.type == 'Multi-Select Combo Box') {
                    obj.value = v.pk_id; //JSON.stringify({'pk_id': v.pk_id});
                }
                _data.push(obj);
            }
            // set default value when field value null or not exist
            if (!field.value && field.type == 'Pick List') {
                if (_data.length == 2) {
                    field.value = _data[1].value;
                }
            }
            field.data = _data;
        }
        catch (e) {
            console.log('e', e);
        }
    };
    DynamicFormService.prototype.checkConditions = function (data, type) {
        if (this.fields.BlkFldDepedency) {
            if (type == 'block') {
                if (this.fields.BlkFldDepedency.blks.indexOf(data.block.pk_id) != -1) {
                    data.cnd_show = false;
                    this.evalBlockConditions(data);
                }
                else {
                    data.cnd_show = true;
                }
            }
            if (type == 'field') {
                if (this.fields.BlkFldDepedency.flds.indexOf(data.pk_id) != -1) {
                    data.cnd_show = false;
                    this.evalFieldConditions(data);
                }
                else {
                    data.cnd_show = true;
                }
            }
        }
    };
    DynamicFormService.prototype.evalBlockConditions = function (block) {
        if (!this.fields.BlkFldDepedency.blkCnd[block.block.pk_id])
            return;
        var cnd_show = true;
        for (var _i = 0, _a = this.fields.BlkFldDepedency.blkCnd[block.block.pk_id]; _i < _a.length; _i++) {
            var fldItem = _a[_i];
            var actFld = this.getBlockOrFieldById(fldItem.app_module_field_id, 'field');
            if (!fldItem.equals && ((actFld.type == 'Check Box' && JSON.parse(fldItem.value).indexOf(actFld.value == undefined ? false : actFld.value) == -1) ||
                (actFld.type == 'Pick List' && JSON.parse(fldItem.value).indexOf(actFld.value == undefined ? 0 : actFld.value.pk_id) == -1))) {
            }
            else if (fldItem.equals && ((actFld.type == 'Check Box' && JSON.parse(fldItem.value).indexOf(actFld.value == undefined ? false : actFld.value) != -1) ||
                (actFld.type == 'Pick List' && JSON.parse(fldItem.value).indexOf(actFld.value == undefined ? 0 : actFld.value.pk_id) != -1))) {
            }
            else {
                cnd_show = false;
            }
        }
        block.cnd_show = cnd_show;
    };
    DynamicFormService.prototype.evalFieldConditions = function (field) {
        try {
            if (!this.fields.BlkFldDepedency.fldCnd[field.pk_id])
                return;
            var is_mandatory = false, cnd_show = true, is_readonly = false;
            for (var access in this.fields.BlkFldDepedency.fldCnd[field.pk_id]) {
                for (var _i = 0, _a = this.fields.BlkFldDepedency.fldCnd[field.pk_id][access]; _i < _a.length; _i++) {
                    var fldItem = _a[_i];
                    var actFld = this.getBlockOrFieldById(fldItem.app_module_field_id, 'field');
                    if (!fldItem.equals && ((actFld.type == 'Check Box' && JSON.parse(fldItem.value).indexOf(actFld.value == undefined ? false : actFld.value) == -1) ||
                        (actFld.type == 'Pick List' && JSON.parse(fldItem.value).indexOf(actFld.value == undefined ? 0 : actFld.value.pk_id) == -1))) {
                    }
                    else if (fldItem.equals && ((actFld.type == 'Check Box' && JSON.parse(fldItem.value).indexOf(actFld.value == undefined ? false : actFld.value) != -1) ||
                        (actFld.type == 'Pick List' && JSON.parse(fldItem.value).indexOf(actFld.value == undefined ? 0 : actFld.value.pk_id) != -1))) {
                    }
                    else {
                        cnd_show = false;
                    }
                }
                if (cnd_show) {
                    if (access == 'R') {
                        is_readonly = true;
                    }
                    else if (access == 'R/W') {
                    }
                    else {
                        if (is_readonly != true) {
                            is_mandatory = true;
                        }
                    }
                }
            }
            if (cnd_show == true) {
                field.cnd_show = true;
                field.is_readonly = is_readonly;
                field.is_mandatory = is_mandatory;
            }
            else {
                field.cnd_show = false;
                field.is_readonly = false;
                field.is_mandatory = false;
            }
        }
        catch (e) {
            console.log('e', e);
        }
    };
    DynamicFormService.prototype.getBlockOrFieldById = function (id, type) {
        var obj = null;
        if (type == 'block') {
            this.fields.blocks.forEach(function (b, index) {
                if (id == b.block.pk_id) {
                    obj = b;
                    return false;
                }
            });
        }
        else {
            this.fields.blocks.forEach(function (b, index) {
                b.fields.forEach(function (fData, fIndex) {
                    if (id == fData.pk_id) {
                        obj = fData;
                        return false;
                    }
                });
            });
        }
        return obj;
    };
    DynamicFormService = __decorate([
        core_1.Injectable()
    ], DynamicFormService);
    return DynamicFormService;
})();
exports.DynamicFormService = DynamicFormService;
//# sourceMappingURL=dynamic-form.service.js.map