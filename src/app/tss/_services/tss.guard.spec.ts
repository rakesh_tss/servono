import { TestBed, async, inject } from '@angular/core/testing';

import { TssGuard } from './tss.guard';

describe('TssGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TssGuard]
    });
  });

  it('should ...', inject([TssGuard], (guard: TssGuard) => {
    expect(guard).toBeTruthy();
  }));
});
