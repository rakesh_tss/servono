var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var TssGuard = (function () {
    function TssGuard(router, ps) {
        this.router = router;
        this.ps = ps;
    }
    TssGuard.prototype.canActivate = function (next, state) {
        //console.log('next',state.url);
        // console.log('state',next.data.action);
        var params = next.params;
        var currentModule = params['module'];
        var moduleInf = this.getCurrentModulePermission(currentModule);
        var access;
        var _permission = {
            View: false, Create: false, Edit: false, Delete: false
        };
        if (moduleInf) {
            access = moduleInf.access.split(',');
            _permission.View = (access.indexOf('View') === -1) ? false : true;
            _permission.Create = (access.indexOf('Create') === -1) ? false : true;
            _permission.Edit = (access.indexOf('Edit') === -1) ? false : true;
            _permission.Delete = (access.indexOf('Delete') === -1) ? false : true;
            _permission.show_full_form = (moduleInf.show_full_form) ? true : false;
            _permission.show_quick_form = (moduleInf.show_quick_form) ? true : false;
        }
        if (_permission[next.data.action] === true) {
            this.ps.set(_permission);
            return true;
        }
        else {
            this.router.navigate(['/403']);
            return false;
        }
    };
    TssGuard.prototype.getCurrentModulePermission = function (currentModule) {
        var activeModule = currentModule;
        var result;
        var access = '';
        var local = JSON.parse(localStorage.getItem('currentMenu'));
        if (local) {
            local.menu.forEach(function (item) {
                if (item.sub) {
                    item.sub.forEach(function (subMenu) {
                        if (subMenu.module == activeModule) {
                            result = subMenu;
                        }
                    });
                }
                else {
                    if (item.module == activeModule) {
                        result = item;
                    }
                }
            });
        }
        if (result.access) {
            return result;
        }
        else {
            return false;
        }
    };
    TssGuard = __decorate([
        core_1.Injectable()
    ], TssGuard);
    return TssGuard;
})();
exports.TssGuard = TssGuard;
//# sourceMappingURL=tss.guard.js.map