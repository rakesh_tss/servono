import { Injectable } from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {PermissionService} from "../../_services/permission.service";
import {LocalStorageService} from "../../_services/local-storage.service";
//declare var localStorage : any;

@Injectable()
export class TssGuard implements CanActivate {
  constructor(private router: Router,private ps:PermissionService,private localStorage:LocalStorageService ) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    //console.log('next',state.url);
   // console.log('state',next.data.action);
    let params:any = next.params;
    let currentModule = params['module'];
    let moduleInf = this.getCurrentModulePermission(currentModule);
    let access:any;
    let _permission:any = {
      View:false,Create:false,Edit:false,Delete:false
    };
    if(moduleInf){
      access = moduleInf.access.split(',');
      _permission.View = (access.indexOf('View') === -1)?false:true;
      _permission.Create = (access.indexOf('Create') === -1)?false:true;
      _permission.Edit = (access.indexOf('Edit') === -1)?false:true;
      _permission.Delete = (access.indexOf('Delete') === -1)?false:true;
      _permission.Export = (access.indexOf('Export') === -1)?false:true;
      _permission.Import = (access.indexOf('Import') === -1)?false:true;
      _permission.show_full_form = (moduleInf.show_full_form)?true:false;
      _permission.show_quick_form = (moduleInf.show_quick_form)?true:false;
    }

    if (_permission[next.data.action] === true) {
      this.ps.set(_permission);
      return true;
    }else{
      this.router.navigate(['/403']);
      return false;
    }
  }
  getCurrentModulePermission(currentModule){
    let activeModule:string = currentModule;
    let result:any ={} ;
    let access:string = '';
    let local =  JSON.parse(this.localStorage.getItem('currentMenu'));
    if (local) {
      local.menu.forEach((item)=>{
        if(item.sub){
          item.sub.forEach((subMenu)=>{
            if(subMenu.module == activeModule){
              result = subMenu;
            }
          })
        }else{
          if(item.module == activeModule){
            result = item;
            //access = item.access;
          }
        }
      })
    }
    if(result.access){
      return result;
    }else{
      return false;
    }

  }
}
