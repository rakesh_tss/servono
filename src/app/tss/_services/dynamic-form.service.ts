import { Injectable } from '@angular/core';
import {SelectItem} from "primeng/primeng";
import {DatexPipe} from "../../_pipe/datex.pipe";


@Injectable()
export class DynamicFormService {

    constructor() {
    }

    buildFields:any = [];
    fields:any = [];

    public setDynamicFormFieldFormat(fieldData, type?:any) {
        try {


            this.fields = fieldData;
            fieldData.relationFields = [];
            fieldData.updateRelationFields = [];
            fieldData.headerFields = [];
            fieldData.showRelationDetails = [];
            fieldData.featuredImage = '';
            fieldData.blocks.forEach((b, index)=> {
                if(b.block.extra_options && type != 'formBuilder'){
                    b.block.extra_options = JSON.parse(b.block.extra_options);
                }
                b.cnd_show = true;
                this.checkConditions(b, 'block');
                b.showFields = [];
                b.fields.forEach((fData, fIndex)=> {
                    //fData.cnd_show = true;
                    fData.cnd_show = true;
                    if (fData.FieldType) {
                        fData.type = fData.FieldType;
                    }

                    fData.display_label = (fData.display_label) ? fData.display_label : fData.name;

                    let skipField = false;
                    //condition for if form type quick only show create and required filed

                    //fData.is_hidden === true || fData.is_fixed_field === true


                    if (fData.is_hidden) {
                        skipField = true;
                    }


                    if (fData.header_view) {
                        fieldData.headerFields.push(fData);
                    }

                    if (fData.type == 'Multi-Select Combo Box' || fData.type == 'Pick List') {
                        this.getOptionsList(fData);
                        // b.showFields.push(fData);
                    } else if (fData.type == 'Relation') {

                        this.setRelationBindData(fData);

                        if (fData.rel == 1) {
                            this.setRelationDisplayName(fData);
                            // b.showFields.push(fData);
                        } else {

                            skipField = true;
                            /*
                             if (type == 'view') {
                             skipField = true;
                             }else if(!fData.is_mandatory){
                             skipField = true;
                             }*/

                            fData.col = 12;
                            if (fData.data) {
                                //for edit / update form
                                if (fData.is_mandatory == true) {
                                    fieldData.updateRelationFields.push(fData);
                                }

                                if (fData.show_in_details) {
                                    fieldData.showRelationDetails.push(fData);
                                } else {
                                    fieldData.relationFields.push(fData);
                                }
                            }

                        }
                    } else if (fData.type == 'Time' || fData.type == 'Date' || fData.type == 'Time Stamp') {
                        if (!fData.value || fData.value == "") {
                            fData.value = '';
                        } else {
                            fData.displayName = new DatexPipe().transform(fData.value, fData.type);
                            //console.log('fData.value',fData.value);
                            fData.value = new Date(fData.value);
                        }
                        //b.showFields.push(fData);
                    } else if (fData.type == 'Image') {
                        if (type == 'view') {
                            skipField = true;
                            // b.showFields.push(fData);
                        }
                        //console.log('fData',fData);
                        if (fData.value)
                            fieldData.featuredImage = fData.value;

                    } else if (fData.type == 'Text Area' || fData.type == 'Html/Text Editor') {
                        fData.col = 12;
                    } else if (fData.type == 'Owner') {
                        if (type != 'view') {
                            // skipField = true;
                            // b.showFields.push(fData);
                        }
                    } else {
                        //b.showFields.push(fData);
                    }

                    this.checkConditions(fData, 'field');


                    if (!fData.extra_options) {
                        fData.extra_options = {};
                        fData.extra_options.lg = 3;
                        fData.extra_options.md = 6;
                        fData.extra_options.sm = 12;
                    } else {
                        fData.extra_options = JSON.parse(fData.extra_options);
                    }

                    if (type == 'quick') {
                        fData.extra_options.lg = fData.extra_options.sm;
                        if (fData.quick_create || fData.is_mandatory || !fData.cnd_show) {

                        } else {
                            skipField = true;
                        }
                    }
                    if (type == 'formBuilder') {
                        if (fData.is_fixed_field && fData.type != 'Owner') {
                            skipField = true;
                        }
                    }


                    if (!skipField) {
                        b.showFields.push(fData);
                    }


                })
            })
            return fieldData;
        } catch (e) {
            console.log('e', e);
        }
    }

    private setRelationBindData(field) {
        if (this.fields['Relations Data']) {
            this.fields['Relations Data'].forEach((data, index)=> {
                if (data.from_field == field.name) {
                    field.displayName = "";
                    if (this.fields.pk_id) {
                        field.current_module = {};
                        field.current_module.module = this.fields.module.module_label;
                        field.current_module.module_id = this.fields.module.pk_id;
                        field.current_module.id = this.fields.pk_id;
                    } else {
                        field.current_module = {};
                        field.current_module.module = this.fields.module.module_label;
                        field.current_module.UUID = this.fields.UUID;

                    }
                    field.data = data;
                }
            })
        }

    }

    public setRelationDisplayName(field) {
        if (field.value) {
            let i = 0;
            for (let key in field.value) {
                if (i == 1) {
                    if (field.value[key]) {
                        field.displayName = field.value[key];
                    }
                    return false;
                }
                i++;
            }
        }
    }

    private getOptionsList(field):any {
        try {
            let result:any = [];
            //field.value = '';
            let _data:SelectItem[] = [];
            let key = field.name.toString() + ' Data';
            let data = this.fields['Pick List Data'][key];
            if (field.type == 'Pick List') {
                let obj:any = {};
                obj.label = 'Select';
                obj.value = null;
                _data.push(obj);
                //set default value

            }
            if (field.type == 'Multi-Select Combo Box') {

                let preSelected:any = [];
                field.displayName = null;
                if (field.value) {
                    let preSelected = field.value;

                    let displayNameArry:string[] = [];
                    let _value:string[] = [];
                    for (let v of field.value) {
                        let obj:any = {};
                        obj.pk_id = v.pk_id;
                        obj.value = v.value;
                        _value.push(v.pk_id);
                        displayNameArry.push(obj.value);
                    }
                    field.displayName = displayNameArry.toString();
                    field.value = _value;
                }
            }

            for (let v of data) {
                let obj:any = {};
                obj.label = v.value;
                obj.value = {pk_id: v.pk_id, value: v.value};
                if (field.type == 'Multi-Select Combo Box') {
                    obj.value = v.pk_id;//JSON.stringify({'pk_id': v.pk_id});
                }

                _data.push(obj);
            }

            // set default value when field value null or not exist
            if (!field.value && field.type == 'Pick List') {
                if (_data.length == 2) {
                    field.value = _data[1].value;
                }
            }
            field.data = _data;
            //return field;
        } catch (e) {
            console.log('e', e);
        }
    }

    checkConditions(data, type) {
        if (this.fields.BlkFldDepedency) {
            if (type == 'block') {
                if (this.fields.BlkFldDepedency.blks.indexOf(data.block.pk_id) != -1) {
                    data.cnd_show = false;
                    this.evalBlockConditions(data);
                } else {
                    data.cnd_show = true;
                }
            }
            if (type == 'field') {
                if (this.fields.BlkFldDepedency.flds.indexOf(data.pk_id) != -1) {
                    data.cnd_show = false;
                    this.evalFieldConditions(data);
                } else {
                    data.cnd_show = true;
                }
            }
        }
    }

    evalBlockConditions(block) {
        if (!this.fields.BlkFldDepedency.blkCnd[block.block.pk_id])return;
        let cnd_show:boolean = true;
        for (let fldItem of this.fields.BlkFldDepedency.blkCnd[block.block.pk_id]) {
            let actFld:any = this.getBlockOrFieldById(fldItem.app_module_field_id, 'field');
            if (!fldItem.equals && ((actFld.type == 'Check Box' && JSON.parse(fldItem.value).indexOf(actFld.value == undefined ? false : actFld.value) == -1) ||
                (actFld.type == 'Pick List' && JSON.parse(fldItem.value).indexOf(actFld.value == undefined ? 0 : actFld.value.pk_id) == -1))) {

            } else if (fldItem.equals && ((actFld.type == 'Check Box' && JSON.parse(fldItem.value).indexOf(actFld.value == undefined ? false : actFld.value) != -1) ||
                (actFld.type == 'Pick List' && JSON.parse(fldItem.value).indexOf(actFld.value == undefined ? 0 : actFld.value.pk_id) != -1))) {

            } else {
                cnd_show = false;
            }
        }
        block.cnd_show = cnd_show;
    }

    evalFieldConditions(field) {
        try {
            if (!this.fields.BlkFldDepedency.fldCnd[field.pk_id])return;
            let is_mandatory:boolean = false, cnd_show:boolean = true, is_readonly:boolean = false;
            for (let access in this.fields.BlkFldDepedency.fldCnd[field.pk_id]) {
                for (let fldItem of this.fields.BlkFldDepedency.fldCnd[field.pk_id][access]) {
                    let actFld:any = this.getBlockOrFieldById(fldItem.app_module_field_id, 'field');
                    if (!fldItem.equals && ((actFld.type == 'Check Box' && JSON.parse(fldItem.value).indexOf(actFld.value == undefined ? false : actFld.value) == -1) ||
                        (actFld.type == 'Pick List' && JSON.parse(fldItem.value).indexOf(actFld.value == undefined ? 0 : actFld.value.pk_id) == -1))) {

                    } else if (fldItem.equals && ((actFld.type == 'Check Box' && JSON.parse(fldItem.value).indexOf(actFld.value == undefined ? false : actFld.value) != -1) ||
                        (actFld.type == 'Pick List' && JSON.parse(fldItem.value).indexOf(actFld.value == undefined ? 0 : actFld.value.pk_id) != -1))) {

                    } else {
                        cnd_show = false;
                    }
                }
                if (cnd_show) {
                    if (access == 'R') {
                        is_readonly = true;
                    } else if (access == 'R/W') {

                    } else {
                        if (is_readonly != true) {
                            is_mandatory = true;
                        }
                    }
                }
            }
            if (cnd_show == true) {
                field.cnd_show = true;
                field.is_readonly = is_readonly;
                field.is_mandatory = is_mandatory;
            } else {
                field.cnd_show = false;
                field.is_readonly = false;
                field.is_mandatory = false;
            }
        } catch (e) {
            console.log('e', e);
        }
    }

    getBlockOrFieldById(id, type) {
        let obj = null;
        if (type == 'block') {
            this.fields.blocks.forEach((b, index)=> {
                if (id == b.block.pk_id) {
                    obj = b;
                    return false;
                }
            });
        } else {
            this.fields.blocks.forEach((b, index)=> {
                b.fields.forEach((fData, fIndex)=> {
                    if (id == fData.pk_id) {
                        obj = fData;
                        return false;
                    }
                });
            });
        }
        return obj;
    }

}
