import {Router, ActivatedRoute, Params} from '@angular/router';
import { Component, OnInit,Pipe } from '@angular/core';
import {HttpService} from "../../_services/http.service";
import {LazyLoadEvent} from "primeng/primeng";
import {BreadcrumbService} from "../../_services/breadcrumb.service";
import {DynamicFormService} from "../_services/dynamic-form.service";
import {TssService} from "../_services/tss.service";
import {PermissionService} from "../../_services/permission.service";
import {ConfirmationService} from "primeng/primeng";

@Component({
    selector: 'app-view',
    templateUrl: './view.component.html',
    styleUrls: ['./view.component.scss']
})

export class ViewComponent implements OnInit {

    constructor(private activatedRoute:ActivatedRoute,
                private access:PermissionService,
                private router:Router,
                private breadcrumbService:BreadcrumbService,
                private httpService:HttpService,
                private tssService:TssService,
                private confirmationService:ConfirmationService,
                private dynamicFormService:DynamicFormService) {
    }
    idx:number;
    module:string;
    menu:string;
    menuGroup:string;
    pkId:number;
    details:any;
    tabViewItem:any = [];
    tags:string[] = [];
    baseUrl:string;
    displayTag:number=4;
    moduleListLink:string;
    permission:any;
    selectedIndex: number = 0;
    tabName:string;
    filterField:any ={
        cnd_show:true
    }
    paramData:any  = {};
    showEditForm:boolean=false;
    ngOnInit() {
        this.selectedIndex = 0;
        this.tabViewItem = [{label: 'Stats'},
            {label: 'Calendar', icon: 'fa-calendar'},
            {label: 'Documentation', icon: 'fa-book'},
            {label: 'Support', icon: 'fa-support'},
            {label: 'Social', icon: 'fa-twitter'}
        ];
        this.baseUrl = this.httpService.apiUrl;
        this.activatedRoute.params.subscribe((params:Params) => {
            this.permission = this.access.permission;
            this.module = params['module'];
            this.menu = params['menu'];
            this.menuGroup = params['menuGroup'];
            this.pkId = params['pkId'];
            this.tabName = params['tabName'];
            Object.assign(this.paramData,params);
            this.paramData.baseUrl=this.baseUrl;
            this.getRowDetail();

            //module list link
           this.moduleListLink = 'tss/' + this.menuGroup + '/' + this.menu + '/' + this.module;

            this.breadcrumbService.setBreadcrumb([{label: this.menu}, {
                label: this.module,
                routerLink: ['/'+this.moduleListLink]
            }, {label: this.module+' Details'}]);

            this.setNextPreviewBtn();
        });
    }

    formFields:any = [];
    relationFields:any = [];
    blocks:any = [];
    headerFields:any = [];
    showRelationDetails:any = [];
    featuredImage:string = '';
    moduleData:any = {};
    stages:any = [];
    appStagesHistory:any = [];
    showViewPage:boolean = false;
    dynamicFormFieldFormat:any;
    getRowDetail() {
        this.showViewPage = false;
        this.httpService.get('getModuleViewEditData/' + this.module + '/' + this.pkId + '/full/view').subscribe(res=> {
            if (res.data) {
                this.relationFields = [];
                this.dynamicFormFieldFormat = this.dynamicFormService.setDynamicFormFieldFormat(res.data, 'view');
                this.moduleData = res.data.module;
                if(this.moduleData.stage_applicable){
                    if(res.data['Pick List Data']){
                        this.stages = res.data['Pick List Data'][this.moduleData.StagePickListFieldName+' Data'];
                    }
                    if(res.data.App_Stages_History){
                        this.appStagesHistory = res.data.App_Stages_History;
                    }
                }

                this.blocks = this.dynamicFormFieldFormat.blocks;
                this.headerFields = res.data.headerFields;
                //this.relationFields = res.data.relationFields;
                //this.showRelationDetails = res.data.showRelationDetails;
                this.relationFields = this.dynamicFormFieldFormat.relationFields;
                this.showRelationDetails = this.dynamicFormFieldFormat.showRelationDetails;
                //active tab if tabname
                if(this.tabName){
                    this.relationFields.forEach((item,indx)=>{
                        if(item.name == this.tabName){
                            this.selectedIndex = indx+1;
                            item.showTable = true;
                        }
                    })
                }else{
                    this.selectedIndex = 0;
                }

                this.featuredImage = res.data.featuredImage;
                this.showViewPage = true;
                this.getTags();
            }
        })
    }

    tableShow:boolean = false;
    showRelationDiv:boolean = true;

    onTabChange($event) {
        this.tableShow = false;
        if ($event.index == 0) {
            this.showRelationDiv = true;
        } else {
            this.showRelationDiv = false;
        }
        let inx = ($event.index - 1);
        if (inx >= 0) {
            let relationData = this.relationFields[inx];
            relationData.formShow = false;
            relationData.showTable = true;
        }
    }

    //tag function
    tagList:any = [];
    openAddTagPopover(popover,evn) {
        this.tags = [];
        this.chip = null;
        popover.show(evn);
    }
    tag:any = {};
    openUpdateTagPopover(popover,evn,item){
        this.tag = Object.assign({}, item);
        popover.show(evn);
    }

    addTags(data, popover) {
        let param:any = {};
        param.tags = data;
        if(data.length>0){
            this.httpService.post('addUpdateTags/' + this.moduleData.pk_id + '/' + this.pkId, param).subscribe(res=> {
                popover.hide();
                this.getTags()
            });
        }

        //this.tagList.push(data);

    }

    getTags() {
        this.httpService.get('getTags/' + this.moduleData.pk_id + '/' + this.pkId).subscribe(res=> {
            if (res) {
                this.tagList = res.data;
            }
        });
    }

    removeTag(data) {
        this.httpService.post('removeTag', data).subscribe(res=> {
            if (res) {
                this.getTags();
            }
        });
    }

    updateTag(data,popover) {
        this.httpService.post('updateTag', data).subscribe(res=> {
            if (res) {
                this.getTags();
                popover.hide();
            }
        });
    }
    filteredTags:any[];
    filterTags(event){
        let query = event.query;

        this.httpService.get('getTags?key='+query).subscribe(res=>{
            if(res){
                this.filteredTags = res.data;
            }
        })
    }
    chip:string = null;
    selectedSuggestion($event){
        if( $event.name){
            this.addChip($event.name);
        }
    }
    addChip(key){
        if(key && typeof key === 'string' && this.tags.indexOf(key) === -1){
            this.tags.push(key);
            this.resetAutoComplete();
        }
    }
    showAutoComplete:boolean = true;
    resetAutoComplete(){
        this.chip = null;
        this.showAutoComplete = false;
        setTimeout(() => this.showAutoComplete = true, 100);
    }
    nextLink:string;
    prevLink:string;
    setNextPreviewBtn(){
        this.nextLink = null;
        this.prevLink = null;
        let rows:any =  this.tssService.dataRows;
        rows.forEach((data,indx)=>{
            if(data.pk_id == this.pkId){
                if(rows.length != indx+1)
                    this.nextLink = '/'+this.moduleListLink+'/view/'+rows[indx+1].pk_id;
                if(indx != 0)
                    this.prevLink = '/'+this.moduleListLink+'/view/'+rows[indx-1].pk_id;
            }
        })
    }
    actionItem:any = [];
    getMenuForActions(contain, event) {
        this.actionItem = [];
/*        if (this.permission.View) {
            this.actionItem.push({
                label: 'View',
                icon: 'fa-eye',
                routerLink: ['/' + this.currentUrl + '/' + this.pkId + '/view']
            });
        }*/

        if (this.permission.Edit) {
            this.actionItem.push({
                label: 'Update',
                icon: 'fa-pencil',
                routerLink: ['/' + this.moduleListLink + '/' + this.pkId]
            });
        }
       /* if (this.permission.Delete) {
            this.actionItem.push({
                label: 'Delete', icon: 'fa-trash', command: (event) => {
                    this.delete(row)
                }
            });
        }*/
        contain.toggle(event);
        //return this.actionItem;
    }

    stageDatabind(data){
        if(data['Pick List Data']){
            this.stages = data['Pick List Data'][this.moduleData.StagePickListName+' Data'];
            this.stages.forEach((stage)=>{
                let color = '#fff';
                stage.status = null;
                stage.happened_on = null;
                if(data.App_Stages_History){
                    data.App_Stages_History.forEach((history)=>{
                        if (stage.pk_id == history.stage_id) {
                            color = stage.color;
                            stage.status = history.status;
                            stage.happened_on = history.happened_on;
                        }
                    })
                }
                stage.color = color;
            })
        }
    }

    afterUpdateForm(){
        this.showEditForm = false;
        this.getRowDetail();
    }


    SWIPE_ACTION = { LEFT: 'swipeleft', RIGHT: 'swiperight' };

    // Action triggered when user swipes
    swipe(selectedIndex: number, action = this.SWIPE_ACTION.RIGHT) {
        // Out of range
        if (this.selectedIndex < 0 || this.selectedIndex > this.relationFields.length ) return;

        // Swipe left, next tab
        if (action === this.SWIPE_ACTION.LEFT) {
            const isLast = this.selectedIndex === this.relationFields.length ;
            this.selectedIndex = isLast ? 0 : this.selectedIndex + 1;
        }

        // Swipe right, previous tab
        if (action === this.SWIPE_ACTION.RIGHT) {
            const isFirst = this.selectedIndex === 0;
            this.selectedIndex = isFirst ? 1 : this.selectedIndex - 1;
        }
    }
    delete() {
        this.confirmationService.confirm({
            message: 'Do you want to delete this record?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
                this.httpService.post('DeleteModuleRow' + '/' + this.module + '/' + this.pkId, {}).subscribe(res=> {
                    if (res) {
                        if(this.nextLink){
                            this.router.navigate([this.nextLink]);
                        }else{
                            this.router.navigate([this.moduleListLink]);
                        }
                    }
                })
            }
        });
    }

}
