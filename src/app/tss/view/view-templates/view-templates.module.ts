import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/primeng';
import {DataTableModule,SharedModule} from 'primeng/primeng';
import {ChartModule} from "primeng/primeng";
import {GMapModule} from "primeng/primeng";
import { CreateMeetingComponent } from './create-meeting/create-meeting.component';
@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    DataTableModule,
    SharedModule,
    GMapModule,
    ChartModule
  ],
  declarations: [CreateMeetingComponent]
})
export class ViewTemplatesModule { }
