import { Component, OnInit,Input } from '@angular/core';
import {OverlayPanel} from "primeng/primeng";

@Component({
    selector: 'app-view-templates',
    templateUrl: './view-templates.component.html',
    styleUrls: ['./view-templates.component.scss']
})
export class ViewTemplatesComponent implements OnInit {
    constructor() {}
    @Input() rowData:any = {};
    @Input() params:any = {};

    overVIew:any[];
    blocks:any = [];
    headerFields:any = [];
    data:any;
    options:any;
    overlays:any[];


    ngOnInit() {

        this.blocks = this.rowData.blocks;
        this.headerFields = this.rowData.headerFields;
        this.overVIew = [
            {brand: 'Gram panchayath', lastYearSale: 'Archer'},
            {brand: 'State', lastYearSale: 'Godhavari'},
            {brand: 'Area', lastYearSale: '156.9 hectares'},
            {brand: 'Gram panchayath', lastYearSale: 'Archer'},
            {brand: 'State', lastYearSale: 'Godhavari'},
            {brand: 'Area', lastYearSale: '156.9 hectares'},
            {brand: 'State', lastYearSale: 'Godhavari'}
        ];
        this.data = {
            labels: ['SENSITIVITY_social ASPECT', 'SENSITIVITY_Economic ASPECT', 'Adaptive_social ASPECT', 'Adaptive_Economic ASPECT'],
            datasets: [
                {
                    label: 'District',
                    backgroundColor: '#dd2c00',
                    borderColor: '#dd2c00',
                    data: [0.15, 0.65, 0.55, 0.1]
                },
                {
                    label: 'Taluk',
                    backgroundColor: '#008e3d',
                    borderColor: '#008e3d',
                    data: [0.55, 0.8, 0.6, 0.4]
                },
                {
                    label: 'Village',
                    backgroundColor: '#ffb300',
                    borderColor: '#ffb300',
                    data: [0.25, 0.7, 0.74, 0.3]
                }
            ]
        }
        this.options = {
            center: {lat: 36.890257, lng: 30.707417},
            zoom: 12
        };
    }

    selectedRelField:any = {};
    showRelPopover:boolean = false;

    openRelPopOverlay(event, field, overlaypanel:OverlayPanel) {
        if (field.type == 'Relation') {
            this.showRelPopover = false;
            this.selectedRelField = field;
            overlaypanel.toggle(event);
            setTimeout(() => this.showRelPopover = true, 100);
        }

    }

    getValue(field, block) {
        if (field.value && (field.value.value || field.value.value=='0')) {
            let value = field.value.value;
            if (block.extra_options && block.extra_options.option) {
                block.extra_options.option.forEach((data)=> {
                    if (+data.range[0] <= +value && +data.range[1] >= +value) {
                        field.displayName = data.label;
                        field.color = data.color;
                        return false;
                    }
                })
            }
        }else{
            field.color = '#ccc';
            field.displayName = '---';
        }
    }

}
