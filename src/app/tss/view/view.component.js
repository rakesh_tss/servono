var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var ViewComponent = (function () {
    function ViewComponent(activatedRoute, router, breadcrumbService, httpService, dynamicFormService) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.breadcrumbService = breadcrumbService;
        this.httpService = httpService;
        this.dynamicFormService = dynamicFormService;
        this.tabViewItem = [];
        this.displayTag = 4;
        this.formFields = [];
        this.relationFields = [];
        this.blocks = [];
        this.headerFields = [];
        this.showRelationDetails = [];
        this.featuredImage = '';
        this.moduleData = {};
        this.showViewPage = false;
        this.tableShow = false;
        this.showRelationDiv = true;
        //tag function
        this.tagList = [];
        this.tag = {};
        this.chip = null;
    }
    ViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.tabViewItem = [{ label: 'Stats' },
            { label: 'Calendar', icon: 'fa-calendar' },
            { label: 'Documentation', icon: 'fa-book' },
            { label: 'Support', icon: 'fa-support' },
            { label: 'Social', icon: 'fa-twitter' }
        ];
        this.baseUrl = this.httpService.apiUrl;
        this.activatedRoute.params.subscribe(function (params) {
            _this.module = params['module'];
            _this.menu = params['menu'];
            _this.menuGroup = params['menuGroup'];
            _this.pkId = params['pkId'];
            _this.getRowDetail();
            //module list link
            var moduleListLink = 'tss/' + _this.menuGroup + '/' + _this.menu + '/' + _this.module;
            _this.breadcrumbService.setBreadcrumb([{ label: _this.menu }, {
                    label: _this.module,
                    routerLink: ['/' + moduleListLink]
                }, { label: 'Details' }]);
        });
    };
    ViewComponent.prototype.getRowDetail = function () {
        var _this = this;
        this.httpService.get('getModuleViewEditData/' + this.module + '/' + this.pkId + '/full/view').subscribe(function (res) {
            if (res.data) {
                _this.relationFields = [];
                var dynamicFormFieldFormat = _this.dynamicFormService.setDynamicFormFieldFormat(res.data, 'view');
                _this.moduleData = res.data.module;
                _this.blocks = res.data.blocks;
                _this.headerFields = res.data.headerFields;
                //this.relationFields = res.data.relationFields;
                //this.showRelationDetails = res.data.showRelationDetails;
                _this.relationFields = dynamicFormFieldFormat.relationFields;
                _this.showRelationDetails = dynamicFormFieldFormat.showRelationDetails;
                _this.featuredImage = res.data.featuredImage;
                _this.showViewPage = true;
                _this.getTags();
            }
        });
    };
    ViewComponent.prototype.onTabChange = function ($event) {
        console.log('$event', $event);
        this.tableShow = false;
        if ($event.index == 0) {
            this.showRelationDiv = true;
        }
        else {
            this.showRelationDiv = false;
        }
        var inx = ($event.index - 1);
        if (inx >= 0) {
            var relationData = this.relationFields[inx];
            relationData.showTable = true;
        }
    };
    ViewComponent.prototype.openAddTagPopover = function (popover, evn) {
        this.tags = [];
        popover.show(evn);
    };
    ViewComponent.prototype.openUpdateTagPopover = function (popover, evn, item) {
        this.tag = Object.assign({}, item);
        popover.show(evn);
    };
    ViewComponent.prototype.addTags = function (data, popover) {
        var _this = this;
        var param = {};
        param.tags = data;
        if (data.length > 0) {
            this.httpService.post('addUpdateTags/' + this.moduleData.pk_id + '/' + this.pkId, param).subscribe(function (res) {
                popover.hide();
                _this.getTags();
            });
        }
        //this.tagList.push(data);
    };
    ViewComponent.prototype.getTags = function () {
        var _this = this;
        this.httpService.get('getTags/' + this.moduleData.pk_id + '/' + this.pkId).subscribe(function (res) {
            if (res) {
                _this.tagList = res.data;
            }
        });
    };
    ViewComponent.prototype.removeTag = function (data) {
        var _this = this;
        this.httpService.post('removeTag', data).subscribe(function (res) {
            if (res) {
                _this.getTags();
            }
        });
    };
    ViewComponent.prototype.updateTag = function (data, popover) {
        var _this = this;
        this.httpService.post('updateTag', data).subscribe(function (res) {
            if (res) {
                _this.getTags();
                popover.hide();
            }
        });
    };
    ViewComponent.prototype.filterTags = function (event) {
        var _this = this;
        var query = event.query;
        this.httpService.get('getTags?key=' + query).subscribe(function (res) {
            if (res) {
                _this.filteredTags = res.data;
                console.log('res', res.data);
            }
        });
    };
    ViewComponent.prototype.selectedSuggestion = function (item) {
        if (item.name) {
            this.addChip(item.name);
        }
    };
    ViewComponent.prototype.addChip = function (key) {
        if (key && this.tags.indexOf(key) === -1) {
            this.tags.push(key);
        }
        this.chip = null;
    };
    ViewComponent = __decorate([
        core_1.Component({
            selector: 'app-view',
            templateUrl: './view.component.html',
            styleUrls: ['./view.component.scss']
        })
    ], ViewComponent);
    return ViewComponent;
})();
exports.ViewComponent = ViewComponent;
//# sourceMappingURL=view.component.js.map