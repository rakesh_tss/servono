var testing_1 = require('@angular/core/testing');
var relation_table_component_1 = require('./relation-table.component');
describe('RelationTableComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [relation_table_component_1.RelationTableComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(relation_table_component_1.RelationTableComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=relation-table.component.spec.js.map