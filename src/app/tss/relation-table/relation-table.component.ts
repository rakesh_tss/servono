import { Component, OnInit,Input } from '@angular/core';
import {LazyLoadEvent} from "primeng/primeng";
import {HttpService} from "../../_services/http.service";
import {PermissionService} from "../../_services/permission.service";
import {ConfirmationService} from "primeng/primeng";
import {Router, ActivatedRoute, Params} from '@angular/router';
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'relation-table',
    templateUrl: './relation-table.component.html',
    styleUrls: ['./relation-table.component.css']
})
export class RelationTableComponent implements OnInit {

    @Input() relData:any = {};
    @Input() formData:any = {};
    @Input() pkId:Number = 0;//parent module pk id
    @Input() tempId:string = null;
    @Input() subRel:any=null;
    @Input() showAddBtn:boolean = true;

    formShow:boolean = false;
    constructor(private httpService:HttpService,
                private confirmationService:ConfirmationService,
                private router:Router,
                private modalService:NgbModal,
                private ps:PermissionService) {
    }

    currentActiveRelationData:any = {};
    permission:any;
    parentModule:string;
    sampleDownloadLink:string;
    downloadLink:string;
    module:string;
    subRelData:any = {};
    showImport: boolean = false;
    ngOnInit() {
        //console.log('formData',this.formData);
        this.permission = this.ps.get();
        this.currentActiveRelationData = this.relData;
        this.subRelData.module = {'pk_id':this.relData.current_module.id};
        //this.relData.data
        let relObj = this.relData.data;
        this.subRelData.field ={
            to_block:relObj.to_block,
            to_field:relObj.to_field,
            from_db_field:relObj.from_db_field,
            to_db_field:relObj.to_db_field,
            xref_table_name:relObj.xref_table_name,
            from_relation:relObj.from_relation
        } ;

        this.parentModule=this.relData.data.from_module;
        this.module = this.currentActiveRelationData.data.to_module;
        this.getTableHeader(this.currentActiveRelationData.data.to_module);
        this.downloadLink = this.httpService.apiUrl + 'exportModuleData' + '/' + this.module+'?field='+relObj.from_field+'&from_block='+relObj.from_block+'&pk_id='+this.relData.current_module.id+'&parentModule='+this.relData.data.from_module;
        this.sampleDownloadLink = this.httpService.apiUrl+'DownloadSampleImportExcelTemplate'+'/'+this.relData.name+'/full?parentModule='+this.relData.data.from_module;
    }

    tableShow:boolean = false;
    cols:any = [];
    showCols:any = [];
    columnOptions:any = [];
    relationModule:string = null;

    private getTableHeader(module) {
        this.relationModule = module;
        this.tableShow = false;
        this.httpService.get('getModuleListMetadata/' + module+'?parentModule='+ this.parentModule).subscribe(res=> {
            if (res) {
                this.cols = res.data;
                this.showCols = [];
                this.columnOptions = [];
                for (let key in this.cols) {
                    if (!this.cols[key].is_hidden) {
                        if (this.cols[key].type !== 'Image')
                            this.showCols.push(this.cols[key]);
                    }
                    this.columnOptions.push({label: this.cols[key].name, value: this.cols[key]});
                }

                this.tableShow = true;
            }
        })
    }

    datableEvent:any;
    gbs:string = null;
    dataRows:any;
    totalRecords:number = 0;

    loadDataLazy(event:LazyLoadEvent) {
        this.datableEvent = event;
        let rel:any = this.currentActiveRelationData.data;
        rel.pk_id = this.pkId;

        this.datableEvent.Rel = rel;

        /* if(this.gbs){
         this.datableEvent.globalSearch = this.gbs;
         }*/
        this.datableEvent.displayCols = this.cols;
        this.httpService.postTable('getModuleListData/' + this.relationModule, this.datableEvent).subscribe(res=> {
            if (res) {
                this.dataRows = res.jqGridData.rows;
                this.totalRecords = res.jqGridData.records;
            }
        })
    }
    rowPkId:number;
    onCreate(){
        this.rowPkId = null;
        this.formShow = true;
    }
    onUpdate(pk_id){
        this.rowPkId = pk_id;
        this.formShow = true;
    }

    addNewRelation(evn) {
        this.reloadTab();
        /*let params:any = {};
        params = this.currentActiveRelationData.data;
        params.value = [{'pk_id': evn.pk_id}];
        this.httpService.postTable('SaveModuleRelationManyData', params).subscribe(res=> {
            if (res) {
                this.reloadTab();
            } else {
                this.reloadTab();
            }
        })*/
    }

    reloadTab() {
        this.formShow = false;
        this.tableShow = false;
        setTimeout(() => this.tableShow = true, 0);
    }

    importExcel() {
        this.downloadSampleExcel();

    }

    downloadSampleExcel() {
        this.httpService.get('DownloadSampleImportExcelTemplate'+'/'+this.relData.name+'/full?parentModule='+this.relData.data.from_module).subscribe(res=> {
            if (res) {

            }
        })
    }
    //force delete
    forceDelete(row) {
        this.confirmationService.confirm({
            message: 'Do you want to force delete this record?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
                this.httpService.post('DeleteModuleRow' + '/' + this.module+ '/' + row.pk_id, {}).subscribe(res=> {
                    if (res) {
                        this.reloadTab();
                    }
                })
            }
        });
    }


    selectedRel:any;

    deleteRels():void {
        let delObj:any = {
            relData:this.relData,
            pkId :this.pkId,
            relPkIds:[],
        };
        this.selectedRel.forEach((item)=>{
            delObj.relPkIds.push(item.pk_id);
        })
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
                this.httpService.post('DeleteRelModule',delObj).subscribe(res=>{
                    if(res){
                        this.selectedRel=[];
                        this.reloadTab();
                    }
                })
            }
        });
    }

    openImportModel(content) {
        this.modalService.open(content,{size:'lg'}).result.then((result) => {
            //this.closeResult = `Closed with: ${result}`;
            this.reloadTab();
        }, (reason) => {
            this.reloadTab();
            //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }


}
