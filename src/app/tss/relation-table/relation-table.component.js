var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var RelationTableComponent = (function () {
    function RelationTableComponent(httpService, ps) {
        this.httpService = httpService;
        this.ps = ps;
        this.relData = {};
        this.pkId = 0;
        this.tempId = null;
        this.subRel = null;
        this.showAddBtn = true;
        this.currentActiveRelationData = {};
        this.tableShow = false;
        this.cols = [];
        this.showCols = [];
        this.columnOptions = [];
        this.relationModule = null;
        this.gbs = null;
        this.totalRecords = 0;
        this.formShow = false;
    }
    RelationTableComponent.prototype.ngOnInit = function () {
        this.permission = this.ps.get();
        this.currentActiveRelationData = this.relData;
        this.parentModule = this.relData.data.from_module;
        this.module = this.currentActiveRelationData.data.to_module;
        this.getTableHeader(this.currentActiveRelationData.data.to_module);
        this.sampleDownloadLink = this.httpService.apiUrl + 'DownloadSampleImportExcelTemplate' + '/' + this.relData.name + '/full?parentModule=' + this.relData.data.from_module;
    };
    RelationTableComponent.prototype.getTableHeader = function (module) {
        var _this = this;
        this.relationModule = module;
        this.tableShow = false;
        this.httpService.get('getModuleListMetadata/' + module + '?parentModule=' + this.parentModule).subscribe(function (res) {
            if (res) {
                _this.cols = res.data;
                _this.showCols = [];
                _this.columnOptions = [];
                for (var key in _this.cols) {
                    if (!_this.cols[key].is_hidden) {
                        if (_this.cols[key].type !== 'Image')
                            _this.showCols.push(_this.cols[key]);
                    }
                    _this.columnOptions.push({ label: _this.cols[key].name, value: _this.cols[key] });
                }
                _this.tableShow = true;
            }
        });
    };
    RelationTableComponent.prototype.loadDataLazy = function (event) {
        var _this = this;
        this.datableEvent = event;
        var rel = this.currentActiveRelationData.data;
        rel.pk_id = this.pkId;
        this.datableEvent.Rel = rel;
        /* if(this.gbs){
         this.datableEvent.globalSearch = this.gbs;
         }*/
        this.datableEvent.displayCols = this.cols;
        this.httpService.postTable('getModuleListData/' + this.relationModule, this.datableEvent).subscribe(function (res) {
            if (res) {
                _this.dataRows = res.jqGridData.rows;
                _this.totalRecords = res.jqGridData.records;
            }
        });
    };
    RelationTableComponent.prototype.addNewRelation = function (evn) {
        var _this = this;
        var params = {};
        params = this.currentActiveRelationData.data;
        params.value = [{ 'pk_id': evn.pk_id }];
        this.httpService.postTable('SaveModuleRelationManyData', params).subscribe(function (res) {
            if (res) {
                _this.reloadTab();
            }
            else {
                _this.reloadTab();
            }
        });
    };
    RelationTableComponent.prototype.reloadTab = function () {
        var _this = this;
        this.formShow = false;
        this.tableShow = false;
        setTimeout(function () { return _this.tableShow = true; }, 0);
    };
    RelationTableComponent.prototype.importExcel = function () {
        this.downloadSampleExcel();
    };
    RelationTableComponent.prototype.downloadSampleExcel = function () {
        this.httpService.get('DownloadSampleImportExcelTemplate' + '/' + this.relData.name + '/full?parentModule=' + this.relData.data.from_module).subscribe(function (res) {
            if (res) {
            }
        });
    };
    __decorate([
        core_1.Input()
    ], RelationTableComponent.prototype, "relData");
    __decorate([
        core_1.Input()
    ], RelationTableComponent.prototype, "pkId");
    __decorate([
        core_1.Input()
    ], RelationTableComponent.prototype, "tempId");
    __decorate([
        core_1.Input()
    ], RelationTableComponent.prototype, "subRel");
    __decorate([
        core_1.Input()
    ], RelationTableComponent.prototype, "showAddBtn");
    RelationTableComponent = __decorate([
        core_1.Component({
            selector: 'relation-table',
            templateUrl: './relation-table.component.html',
            styleUrls: ['./relation-table.component.css']
        })
    ], RelationTableComponent);
    return RelationTableComponent;
})();
exports.RelationTableComponent = RelationTableComponent;
//# sourceMappingURL=relation-table.component.js.map