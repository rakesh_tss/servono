import {Router, ActivatedRoute, Params} from '@angular/router';
import { Component, OnInit,Pipe, PipeTransform,HostListener,ViewChild,ElementRef,Inject } from '@angular/core';
import {HttpService} from "../_services/http.service";
import { DOCUMENT } from "@angular/platform-browser";
import {Message,DataTableModule,SharedModule,LazyLoadEvent,SelectItem} from 'primeng/primeng';
import {ConfirmationService} from "primeng/primeng";
import {BreadcrumbService} from "../_services/breadcrumb.service";
import {PermissionService} from "../_services/permission.service";
import {TssService} from "./_services/tss.service";
import {ChartModule,PanelModule} from "primeng/primeng";
import {RoundProgressModule} from "angular-svg-round-progressbar/dist/index";
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import * as jQuery from 'jquery';
import {CommonService} from "../_services/common.service";
(window as any).jQuery = (window as any).$ = jQuery;

@Component({
    selector: 'app-tss',
    templateUrl: './tss.component.html',
    styleUrls: ['./tss.component.scss'],
    providers: [ConfirmationService]
})
export class TssComponent implements OnInit {
    self = this;
    dataRows:any[];
    selectedRows:any[]=[];
    tableShow:boolean = false;
    dataRow:any = {};
    totalRecords:number;
    cols:any[];
    showCols:any[];
    module:string;
    menu:string;
    menuGroup:string;
    columnOptions:SelectItem[];
    public formFields:any = [];
    showForm:boolean = false;
    currentUrl:string;
    templateInfo:any = {};
    keysGetter = Object.keys;
    datableEvent:any;
    gbs:string = null;
    baseUrl:string = '';
    permission:any;
    widgets:any = [];
    topWidgets:any = [];
    rightWidgets:any = [];
    downloadLink:string = '';
    sampleDownloadLink:string;
    dynamicExtraModules = [ChartModule, RoundProgressModule, PanelModule];
    refreshTable:boolean = true;
    dialogTitle:string;
    displayDialog:boolean;
    actionItem:any = [];
    selectedFilters:any = [];
    i:number = 0;
    kanbanView:boolean = false;
    colClassName:string;

    constructor(private activatedRoute:ActivatedRoute, private access:PermissionService,
                private router:Router, private breadcrumbService:BreadcrumbService,
                private confirmationService:ConfirmationService,
                private tssService:TssService,
                private httpService:HttpService,
                private commonService:CommonService,
                private modalService:NgbModal,
                @Inject(DOCUMENT) private doc:Document) {
    }

    @ViewChild('divRight') divRight:ElementRef;
    @ViewChild('dashBoard') dashBoard:ElementRef;
    scrollTop:any;
    isSpecial:boolean = false;

    @HostListener("window:scroll", [])
    onWindowScroll(event) {
        try {
            let windowWidth = window.screen.width;
            if (windowWidth > 1200) {
                let num = this.doc.body.scrollTop;
                /*this.scrollTop = this.doc.body.scrollTop;*/
                let rightHeight = this.dashBoard.nativeElement.offsetHeight;
                /*let rightTop = this.divRight.nativeElement.offsetTop;*/
                if (num > rightHeight) {
                    this.isSpecial = true;
                }
                else if (num <= rightHeight / 4) {
                    this.isSpecial = false;
                }
            }
        } catch (e) {
            //console.log('e', e);
        }


    }

    /*setStyles(){

     let styles= {
     position: this.isSpecial ? 'fixed':'relative',
     bottom: this.isSpecial ? '10px':'isSpecial',
     right: this.isSpecial? '10px':'isSpecial'
     }
     console.log(styles);
     return styles;
     }*/

    ngOnInit() {

        this.baseUrl = this.httpService.apiUrl;
        this.activatedRoute.params.subscribe((params:Params) => {
            this.selectedRows = [];
            this.topWidgets = [];
            this.rightWidgets = [];
            this.permission = this.access.permission;
            this.module = params['module'];
            this.menu = params['menu'];
            this.menuGroup = params['menuGroup'];
            this.tableShow = false;
            this.showForm = false;
            this.kanbanView = false;
            this.colClassName = 'with-out-image';
            this.getTableHeader();

            //this.getFormFields();
            this.currentUrl = 'tss/' + this.menuGroup + '/' + this.menu + '/' + this.module;
            this.breadcrumbService.setBreadcrumb([{label: this.menu}, {label: this.module}]);
            this.downloadLink = this.httpService.apiUrl + 'exportModuleData' + '/' + this.module;
            this.sampleDownloadLink = this.httpService.apiUrl + 'DownloadSampleImportExcelTemplate' + '/' + this.module + '/full';
        });

    }

    formReload() {
        this.showForm = false;
        setTimeout(() => this.showForm = true, 100);
    }

    getTableHeader() {
        this.httpService.get('getModuleListMetadata/' + this.module).subscribe(res=> {
            if (res) {
                try {
                    this.cols = res.data;
                    this.showCols = [];
                    this.columnOptions = [];
                    this.templateInfo = res.moduleAndTemplateData;
                    this.widgets = res.widgets;
                    let tWidgets:any = [];
                    this.widgets.forEach((w)=> {
                        if (w.widget_type == 1) {
                            tWidgets.push(w);
                        } else {
                            this.rightWidgets.push(w);
                        }
                        this.getWidgetInfo(w);
                    });
                    this.topWidgets = tWidgets;
                    for (let key in this.cols) {
                        if (!this.cols[key].is_hidden) {
                            if (this.cols[key].type !== 'Image') {
                                if (this.cols[key].derived_field_type != 1 && this.cols[key].derived_field_type != 2){
                                    this.showCols.push(this.cols[key]);
                                }
                                this.columnOptions.push({label: this.cols[key].name, value: this.cols[key]});
                            }
                        }
                    }
                    if (this.showCols.length > 0) {
                        if(this.cols[0] && this.cols[0].type == 'Image'){
                            this.colClassName = 'with-image';
                        }
                        this.tableShow = true;
                        this.formReload();
                    }
                } catch (e) {
                    console.log('e', e);
                }
            }
        })
    }


    tableReload() {
        this.refreshTable = false;
        setTimeout(() => this.refreshTable = true, 100);
    }


    loadDataLazy(event:LazyLoadEvent) {
        this.datableEvent = event;
        if (this.templateInfo.template_name == 'Template 3') {
            this.datableEvent.ManyRelCount = true;
        }

        this.datableEvent.displayCols = this.cols;
        this.httpService.postTable('getModuleListData/' + this.module, this.datableEvent).subscribe(res=> {
            if (res) {
                this.dataRows = res.jqGridData.rows;
                this.tssService.dataRows = this.dataRows;
                this.totalRecords = res.jqGridData.records;
            }
        })
    }


    onCreate():void {
        let params:any = {};
        params.menuGroup = this.menuGroup;
        params.menu = this.menu;
        params.module = this.module;
        params.pkId = 0;
        let url = 'tss/' + this.menuGroup + '/' + this.menu + '/' + this.module + '/0'
        this.router.navigate([this.currentUrl + '/0']);
    }

    onView(row):void {
        if (this.permission.View) {
            this.router.navigate([this.currentUrl + '/view/' + row.pk_id]);
        }
    }

    onImport():void {
        if (this.permission.View) {
            this.router.navigate([this.currentUrl + '/excel-import/0']);
        }
    }

    tabViewNav(row, key) {
        if (this.permission.View && row.Many_Rel_Content[key].rel_module) {
            //row.Many_Rel_Content[key].rel_module
            this.router.navigate([this.currentUrl + '/view/' + row.pk_id + '/' + key]);
        }
    }

    onUpdate(row):void {
        this.router.navigate([this.currentUrl + '/' + row.pk_id]);
        //this.onCreate();
        /* this.dialogTitle = 'Update Menu';
         console.log('row', row);
         this.dataRow = row;
         this.displayDialog = true;*/
    }


    onDelete(row):void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: () => {

                //Actual logic to perform a confirmation
            }
        });
    }

    dialogClose():void {
        this.displayDialog = false;
    }

    submitForm($event):void {
        this.tableReload();
        this.formReload();
    }

    keys(data:any):Array<string> {
        // console.log('Object.keys(data)',Object.keys(data));
        return Object.keys(data);
    }

    getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    @ViewChild('actionMenuContain') actionMenuContain:ElementRef;
    getMenuForActions(contain,event, row) {
        this.actionItem = [];
        if (this.permission.View) {
            this.actionItem.push({
                label: 'View',
                icon: 'fa-eye',
                routerLink: ['/' + this.currentUrl + '/view/' + row.pk_id]
            });
        }

        if (this.permission.Edit) {
            this.actionItem.push({
                label: 'Update',
                icon: 'fa-pencil',
                routerLink: ['/' + this.currentUrl + '/' + row.pk_id]
            });
        }
        if (this.permission.Delete) {
            this.actionItem.push({
                label: 'Delete', icon: 'fa-trash', command: (event) => {
                    this.delete(row)
                }
            });
        }
        contain.toggle(event);
        //return this.actionItem;
    }

    delete(row) {
        this.confirmationService.confirm({
            message: 'Do you want to delete this record?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
                this.httpService.post('DeleteModuleRow' + '/' + this.module + '/' + row.pk_id, {}).subscribe(res=> {
                    if (res) {
                        this.tableReload();
                    }
                })
            }
        });
    }

    dbFilter($event, searchKey, type) {
        if (searchKey) {
            let filtersObj:any = {};
            if (this.selectedFilters.length > 0) {
                this.selectedFilters.forEach((data)=> {
                    filtersObj[data.name] = {value: searchKey};
                });

                this.datableEvent.filters = filtersObj;
            } else {
                this.datableEvent.globalSearch = searchKey;
            }

            this.loadDataLazy(this.datableEvent);
        } else {

        }
    }

    clearFilter() {
        this.gbs = null;
        delete this.datableEvent.globalSearch;
        this.datableEvent.filters = {}
        this.selectedFilters = [];
        this.loadDataLazy(this.datableEvent);
    }

    changeSortOrder(event) {
        if (event.value) {
            this.datableEvent.sortField = event.value.name;
            this.datableEvent.sortOrder = (this.datableEvent.sortOrder == 1) ? -1 : 1;
            this.loadDataLazy(this.datableEvent)
        }
    }

    getWidgetInfo(w) {
        if (w.call_required === true) {
            this.httpService.get('GetWidgetData/' + w.pk_id).subscribe(res=> {
                if (res) {
                    w.data = res;
                    let returnResult = res.data;
                    try {
                        if (w.js_code) {
                            var fun = new Function('arg', w.js_code);
                            returnResult = fun(res.data);
                        }
                    } catch (e) {
                        console.log('e', e);
                    }
                    w.data = returnResult;
                }
            });
        } else {
            if (w.js_code) {
                var fun = new Function('arg', w.js_code);
                let returnResult = fun(null);
                w.data = returnResult;
            }
        }
    }
    selectAll(event){
        console.log('event',event);
        if(event){
            this.selectedRows = this.dataRows;
        }else{
            this.selectedRows = [];
        }
    }
    selectRow(event){
        if(event){
            this.selectedRows.push(event);
        }else{

        }
    }

    //assignModal funciton
    assignModal:NgbModalRef;
    userList:any[];
    assignTo:any;

    openAssignModal(contant) {

        this.httpService.get('GetAssignableOwners/User').subscribe(res=> {
            if (res) {
                this.userList = this.commonService.getDropdownOptions(res.data, 'login_email', 'pk_id', true);
            }
        })
        this.assignModal = this.modalService.open(contant);
    }

    saveAssignUser() {
        let params:any = {};
        params.module = this.module;
        let idsArray:any = [];
        this.selectedRows.forEach((item)=> {
            idsArray.push(item.pk_id);
        })
        params.ids = idsArray;
        params.assignTo = this.assignTo;
        params.assignToType = 'User';
       // console.log('params', params);
        this.httpService.post('AssignModuleOwner', params).subscribe(res=> {
            if (res) {
                if (res) {
                    this.selectedRows = [];
                    this.assignModal.close();
                }
            }
        })
    }

}
