import { Component, OnInit,Input,Output,EventEmitter} from '@angular/core';
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {HttpService} from "../../_services/http.service";
import {ToasterService} from "angular2-toaster/angular2-toaster";
import {TableColFieldViewComponent} from "../table-col-field-view/table-col-field-view.component";
import {CommonService} from "../../_services/common.service";
import {ConfirmationService} from "primeng/primeng";
import {MenuItem} from "primeng/primeng";
import {SelectItem} from "primeng/primeng";
import {Location} from '@angular/common';

@Component({
    selector: 'tss-custom-component',
    templateUrl: './custom-component.component.html',
    styleUrls: ['./custom-component.component.css']
})
export class CustomComponentComponent implements OnInit {

    @Input() formData:any = {};
    @Input() field:any = {};
    @Output('afterSave') afterSave:EventEmitter<any> = new EventEmitter();
    // table var
    tableShow:boolean = false;
    cols:any = [];
    showCols:any = [];
    dataRows:any = [];
    columnOptions:any = [];
    relationModule:string = null;
    module:string = null;

    constructor(private modalService:NgbModal,
                private location:Location,
                private commonService:CommonService,
                private toasterService:ToasterService,
                private confirmationService:ConfirmationService,
                private httpService:HttpService) {
    }

    componentName:string;
    WarehouseProcessDiv:boolean = false;
    qaProcessDiv:boolean = false;

    ngOnInit() {
        if (this.formData.module && this.formData.module.custom_component) {
            this.componentName = this.formData.module.custom_component;
            switch (this.componentName) {
                case 'WarehouseProcess':
                    this.wareHouseProcess();
                    break;
            }
        }
    }

    checkCustomComponent() {

    }

    fieldInfo:any = {};

    wareHouseProcess() {


        this.httpService.get('testService/WarehouseProcess_CustomActions').subscribe(res=> {
            if (res) {
                this.checkCnd(res.data);
            }
        })

        if (this.field) {
            this.fieldInfo.module = {'pk_id': this.field.current_module.id};
            //this.relData.data
            let relObj = this.field.data;
            this.fieldInfo.field = {
                to_block: relObj.to_block,
                to_field: relObj.to_field,
                from_db_field: relObj.from_db_field,
                to_db_field: relObj.to_db_field,
                xref_table_name: relObj.xref_table_name,
                from_relation: relObj.from_relation
            };
        }
        /*
         if (this.field && this.field.name.toLowerCase() == 'items') {
         this.fieldInfo.module = {'pk_id': this.field.current_module.id};
         //this.relData.data
         let relObj = this.field.data;
         this.fieldInfo.field = {
         to_block: relObj.to_block,
         to_field: relObj.to_field,
         from_db_field: relObj.from_db_field,
         to_db_field: relObj.to_db_field,
         xref_table_name: relObj.xref_table_name,
         from_relation: relObj.from_relation
         };

         let stageInfo = this.getFieldByName('stage');
         let statusInfo = this.getFieldByName('status');
         if ((stageInfo.value && stageInfo.value.value == 'GRN')
         && (statusInfo.value && statusInfo.value.value == 'Received – Hold Inventory')) {
         this.field.can_add = false;
         this.field.can_create = false;
         this.field.can_delete = false;
         this.field.can_export = false;
         this.field.can_import = false;
         this.field.can_export = false;
         this.WarehouseProcessDiv = true;
         }
         if ((stageInfo.value && stageInfo.value.value == 'QA')
         && (statusInfo.value && statusInfo.value.value == 'Available for testing')) {
         this.field.can_add = false;
         this.field.can_create = false;
         this.field.can_delete = false;
         this.field.can_export = false;
         this.field.can_import = false;
         this.field.can_export = false;
         this.qaProcessDiv = true;
         }

         }*/

    }

    buttons:any = {};

    checkCnd(res) {
        try {
            if (res.ManyTab) {
                //loop tabs
                for (let tab in res.ManyTab) {
                    let fData = this.getFieldByName(tab); //get filed data
                    let tabData = res.ManyTab[tab];
                    for (let button in tabData.Buttons) {
                        let checkCnd = this.checkParentFieldValues(tabData.Buttons[button]);
                        if (checkCnd) {
                            let obj = tabData.Buttons[button];
                            this.buttons[button] = obj
                            if (obj.DefaultButtonsToShow) {
                                for (let showBtn in obj.DefaultButtonsToShow) {
                                    fData[showBtn] = obj.DefaultButtonsToShow[showBtn];
                                }
                            }
                        }
                    }
                }
            }
        } catch (e) {
            console.log('e', e);
        }

        //console.log('res', res['ManyTab']['']);
    }

    checkParentFieldValues(data) {
        let returnResult = true;
        data.ParentFieldValues.forEach((item)=> {
            //get field value
            let field = this.getFieldByName(item.FieldName);
            /*            console.log('field.value.value',field.value.value);
             console.log('item.value',item.Value);
             console.log('item.Value.indexOf(field.value.value)',item.Value.indexOf(field.value.value));*/
            if (!field.value || (item.Value.indexOf(field.value.value) == -1)) {
                returnResult = false;
                return;
            }

        })

        return returnResult;
    }

    type:string = "verify";
    modalTitle:string;
    verifiedModal:NgbModalRef;

    openVerified(content, type) {
        this.type = type;

        if (type == 'verify') {
            this.modalTitle = 'Verify';
            this.getQAOptions('Received Status');
        } else {
            this.modalTitle = 'QA Verify';
            this.getQAOptions('QA Status');

        }
        this.verifiedModal = this.modalService.open(content, {size: 'lg', backdrop: 'static'});
        this.dataRows = [];
        this.getTableHeader();
    }

    search:string;

    searchMacId() {
        let params:any = {};
        params.displayCols = this.cols;
        params.rows = 1;
        params.sourceModule = this.fieldInfo;
        params.unique_filter = {cust_cond: 'wh_verify', value: this.search, name: 'mac_id'};
        this.httpService.postTable('getModuleListData/Items', params).subscribe(res=> {
            if (res) {
                if (res.jqGridData && this.checkVerifyRow(res.jqGridData.rows)) {
                    this.dataRows.push(res.jqGridData.rows[0]);
                    this.search = null;
                    this.reloadTable();
                }
            }
        })
    }

    checkVerifyRow(rows) {
        let result = true;
        if (rows.length > 0) {
            let row = rows[0];
            if (this.type == 'verify') {
                if (row.Received == true) {
                    this.search = null;
                    this.toasterService.pop('info', '', 'Mac Id already verified');
                    result = false;
                } else {
                    this.dataRows.forEach(item=> {
                        if (item.pk_id == row.pk_id) {
                            this.toasterService.pop('info', '', 'Mac Id already Added');
                            this.search = null;
                            result = false;
                            return false;
                        }
                    })
                }
            } else {
                if (row['Received Status'] != 'Received' && row.Received == true) {
                    this.search = null;
                    this.toasterService.pop('info', '', 'Mac Id Rejected / Damaged');
                    result = false;
                } else if (row.QA == true) {
                    this.search = null;
                    this.toasterService.pop('info', '', 'QA already done for this Mac Id');
                    result = false;
                } else {
                    this.dataRows.forEach(item=> {
                        if (item.pk_id == row.pk_id) {
                            this.toasterService.pop('info', '', 'Mac Id already Added');
                            this.search = null;
                            result = false;
                            return false;
                        }
                    })
                }
            }

        } else {
            this.toasterService.pop('info', '', 'Mac Id invalid');
            result = false;
        }

        return result;

    }

    /*  checkQAVerifyRow(rows) {
     let result = true;
     if (rows.length > 0) {
     let row = rows[0];
     if(this.type=='verify'){
     if (row.Received == true) {
     this.search = null;
     this.toasterService.pop('info', '', 'Mac Id already verified');
     result = false;
     } else {
     this.dataRows.forEach(item=> {
     if (item.pk_id == row.pk_id) {
     this.toasterService.pop('info', '', 'Mac Id already Added');
     this.search = null;
     result = false;
     return false;
     }
     })
     }
     }else{
     if (row.Received == true && row.QA == true) {
     this.search = null;
     this.toasterService.pop('info', '', 'Mac Id already verified');
     result = false;
     } else {
     this.dataRows.forEach(item=> {
     if (item.pk_id == row.pk_id) {
     this.toasterService.pop('info', '', 'Mac Id already Added');
     this.search = null;
     result = false;
     return false;
     }
     })
     }
     }

     } else {
     this.toasterService.pop('info', '', 'Mac Id invalid');
     result = false;
     }

     return result;

     }*/

    reloadTable() {
        this.tableShow = false;
        setTimeout(() => this.tableShow = true, 0);
    }

    getFieldByName(fName) {
        let returnField:any;
        if (this.formData && this.formData.blocks) {
            this.formData.blocks.forEach(block=> {
                block.fields.forEach(field=> {
                    if (field.name.toLowerCase() == fName || field.name == fName) {
                        returnField = field;
                        return false;
                    }
                })
            })
        }
        return returnField;
    }

    deleteRow(row) {
        let index = this.dataRows.indexOf(row);
        this.dataRows.splice(index, 1);
        this.reloadTable();
    }

    // warehouse get header information
    private getTableHeader() {
        this.module = 'Items';
        this.tableShow = false;
        this.httpService.get('getModuleListMetadata/Items?parentModule=ASNs').subscribe(res=> {
            if (res) {
                this.cols = res.data;
                this.showCols = [];
                this.columnOptions = [];
                for (let key in this.cols) {
                    if (!this.cols[key].is_hidden) {
                        if (this.cols[key].type !== 'Image')
                            this.showCols.push(this.cols[key]);
                    }
                    this.columnOptions.push({label: this.cols[key].name, value: this.cols[key]});
                }

                this.tableShow = true;
            }
        })
    }

    qaStatusOptions:SelectItem[];
    qa_status:string = null;

    getQAOptions(key) {

        this.httpService.post('GetAllPickListData', {PickLists: [key]}).subscribe(res=> {
            if (res) {
                if (res.data[key + ' Data']) {
                    let data = res.data[key + ' Data'];
                    this.qaStatusOptions = this.commonService.getDropdownOptions(data, 'value', 'pk_id');
                }
            }
        })
    }

    qa_comments:string;

    saveVerifyRows() {
        let params:any = {
            data: []
        };

        this.dataRows.forEach(item=> {
            params.data.push(item.pk_id);
        })
        if (this.type == 'verify') {
            params.qa_status = this.qa_status;
            this.httpService.post('Warehouse/VerifyItems', params).subscribe(res=> {
                if (res) {
                    this.afterSave.emit();
                    this.verifiedModal.close();
                }
            })
        } else {
            params.qa_status = this.qa_status;
            params.qa_comments = this.qa_comments;
            this.httpService.post('Warehouse/Qa', params).subscribe(res=> {
                if (res) {
                    this.afterSave.emit();
                    this.verifiedModal.close();
                    //this.location;
                }
            })
        }


    }

    moveToInventory() {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: () => {
                let params:any = {};
                params.pk_id = this.field.current_module.id;
                let relObj = this.field.data;
                params.field = {
                    to_block: relObj.to_block,
                    to_field: relObj.to_field,
                    from_db_field: relObj.from_db_field,
                    to_db_field: relObj.to_db_field,
                    xref_table_name: relObj.xref_table_name,
                    from_relation: relObj.from_relation
                };
                this.httpService.post('Warehouse/moveToInventory', params).subscribe(res=> {
                    if (res) {
                        this.afterSave.emit();
                    }
                })
            }
        });
    }

}
