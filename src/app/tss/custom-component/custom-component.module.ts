import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CommonTssModule} from "../../shared/commonTssModules";
import {CustomComponentComponent} from "./custom-component.component";

@NgModule({
  imports: [
    CommonTssModule
  ],
  declarations: [
      CustomComponentComponent
  ],
  exports:[
    CustomComponentComponent
  ]
})
export class CustomComponentModule { }
