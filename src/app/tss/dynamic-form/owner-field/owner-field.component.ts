import { Component, OnInit,Input } from '@angular/core';
import {HttpService} from "../../../_services/http.service";
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {DynamicFormService} from "../../_services/dynamic-form.service";
import {CommonService} from "../../../_services/common.service";

@Component({
  selector: 'owner-field',
  templateUrl: './owner-field.component.html',
  styleUrls: ['./owner-field.component.scss']
})
export class OwnerFieldComponent implements OnInit {

  constructor(private httpService:HttpService,
              private modalService:NgbModal, private commonService:CommonService,private dynamicFormService:DynamicFormService) {
  }
  @Input() field:any = {};

  assignModal:NgbModalRef;
  userList:any[];
  assignTo:any={};
  displayName:String;
  ngOnInit() {
    let currentUser = this.commonService.getUser();
    if(!this.field.value){
      this.assignTo = {pk_id:currentUser.userId};
      this.displayName  = currentUser.UserName;
      this.field.value = {'pk_id':currentUser.userId,'type':'User','display_name':currentUser.UserName};
    }else{
      //this.assignTo = this.field.value;
      let value = this.field.value;
      this.assignTo = {'pk_id':+value.pk_id};
      this.displayName  = this.field.value.display_name;
    }
    console.log('this.field.value',this.field.value)
  }

  openAssignModal(contant) {


    this.httpService.get('GetAssignableOwners/User').subscribe(res=> {
      if (res) {
        this.userList = [];
        res.data.forEach((data)=>{
          this.userList.push({label:data.login_email,value:data});
        })
        //this.userList = this.commonService.getDropdownOptions(res.data, 'login_email', 'pk_id', true);
      }
    })
    this.assignModal = this.modalService.open(contant);
  }
  saveAssignUser() {
    if(this.assignTo){
      this.field.value = {'pk_id':this.assignTo.pk_id,'type':'User','display_name':this.assignTo.login_email};
      this.displayName = this.assignTo.login_email;
      this.assignModal.close();
    }
  }

}
