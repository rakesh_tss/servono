var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var dynamic_form_service_1 = require("../_services/dynamic-form.service");
var dynamic_form_component_1 = require("./dynamic-form.component");
var relation_modal_content_component_1 = require("../relation-modal-content/relation-modal-content.component");
var table_col_field_view_component_1 = require("../table-col-field-view/table-col-field-view.component");
var primeng_1 = require("primeng/primeng");
var image_path_pipe_1 = require("../_pipe/image-path.pipe");
var index_1 = require("ng2-daterangepicker/index");
var multiselect_view_pipe_1 = require("../_pipe/multiselect-view.pipe");
var module_view_path_pipe_1 = require("../_pipe/module-view-path.pipe");
var commonTssModules_1 = require("../../shared/commonTssModules");
var form_field_component_1 = require("../form-field/form-field.component");
var DynamicFormModule = (function () {
    function DynamicFormModule() {
    }
    DynamicFormModule = __decorate([
        core_1.NgModule({
            imports: [
                commonTssModules_1.CommonTssModule,
                primeng_1.FieldsetModule,
                index_1.Daterangepicker
            ],
            declarations: [
                dynamic_form_component_1.DynamicFormComponent,
                relation_modal_content_component_1.RelationModalContentComponent,
                table_col_field_view_component_1.TableColFieldViewComponent,
                image_path_pipe_1.ImagePathPipe,
                multiselect_view_pipe_1.MultiselectViewPipe,
                module_view_path_pipe_1.ModuleViewPathPipe,
                form_field_component_1.FormFieldComponent
            ], providers: [
                dynamic_form_service_1.DynamicFormService
            ],
            exports: [
                primeng_1.FieldsetModule,
                index_1.Daterangepicker,
                dynamic_form_component_1.DynamicFormComponent,
                relation_modal_content_component_1.RelationModalContentComponent,
                table_col_field_view_component_1.TableColFieldViewComponent,
                form_field_component_1.FormFieldComponent,
                image_path_pipe_1.ImagePathPipe,
                multiselect_view_pipe_1.MultiselectViewPipe,
                module_view_path_pipe_1.ModuleViewPathPipe
            ]
        })
    ], DynamicFormModule);
    return DynamicFormModule;
})();
exports.DynamicFormModule = DynamicFormModule;
//# sourceMappingURL=dynamic-form.module.js.map