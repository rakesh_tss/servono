var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var common_service_1 = require("../../_services/common.service");
var DynamicFormComponent = (function () {
    function DynamicFormComponent(toasterService, _fb, router, httpService, commonService, modalService, dyanmicService) {
        this.toasterService = toasterService;
        this._fb = _fb;
        this.router = router;
        this.httpService = httpService;
        this.commonService = commonService;
        this.modalService = modalService;
        this.dyanmicService = dyanmicService;
        this.fields = {};
        this.module = null;
        this.pkId = 0;
        this.autoSave = false;
        this.formType = 'quick';
        this.addMoreField = false;
        this.addMoreFieldUrl = '';
        this.col = 12;
        this.subRel = null;
        this.parentModule = null;
        this.submitLabel = 'Submit';
        this.showBlogTitle = false;
        this.submitted = new core_1.EventEmitter();
        this.loadData = new core_1.EventEmitter();
        this.afterSave = new core_1.EventEmitter();
        //form: FormGroup;
        this.buildFields = [];
        this.formShow = false;
        this.filterField = {
            cnd_show: true
        };
        this.regex = {
            email: "^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$",
            url: "^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"
        };
        this.formFields = [];
        //daterangeInput picker options
        this.daterangeOptions = {
            locale: { format: 'YYYY-MM-DD' },
            alwaysShowCalendars: false
        };
        this.datetimerangeOptions = {
            timePicker: true,
            locale: { format: 'YYYY-MM-DD' },
            alwaysShowCalendars: false
        };
        this.timerangeOptions = {
            format: 'HH:ii p',
            autoclose: true,
            // todayHighlight: true,
            showMeridian: true,
            startView: 1,
            maxView: 1
        };
        this.submittedForm = false;
        this.saveButtonDisable = false;
        this.uploadUrl = this.httpService.apiUrl + 'UploadRawFiles';
        //picklist dependency check
        this.appFirstLoad = true;
    }
    DynamicFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.myForm = this._fb.group({});
        //this.myForm.patchValue(this.getStoreFormValue())
        this.myForm.valueChanges.subscribe(function (data) {
            //console.log('data', data);
            _this.setStoreFormValue(data);
        });
        var param = '';
        if (this.parentModule) {
            param = '?parentModule=' + this.parentModule;
        }
        if (this.fields.blocks) {
            this.formFields = this.dyanmicService.setDynamicFormFieldFormat(this.fields);
            this.setFormControl();
            this.formShow = true;
        }
        else {
            var action = (this.pkId > 0) ? 'edit' : 'create';
            if (this.pkId > 0) {
                this.httpService.get('getModuleViewEditData/' + this.module + '/' + this.pkId + '/' + this.formType + '/' + action + param).subscribe(function (res) {
                    if (res) {
                        _this.formFields = _this.dyanmicService.setDynamicFormFieldFormat(res.data, _this.formType);
                        _this.setFormControl();
                        _this.formShow = true;
                        // this.checkBlockAndFieldConditions();
                        _this.loadData.emit(_this.formFields);
                    }
                });
            }
            else {
                this.httpService.get('getModuleAddEditMetaData/' + this.module + '/' + this.formType + param).subscribe(function (res) {
                    if (res) {
                        _this.formFields = _this.dyanmicService.setDynamicFormFieldFormat(res.data, _this.formType);
                        _this.setFormControl();
                        _this.formShow = true;
                        //this.checkBlockAndFieldConditions();
                        _this.loadData.emit(_this.formFields);
                    }
                });
            }
        }
    };
    //add validation for field
    DynamicFormComponent.prototype.setFormControl = function () {
        var _this = this;
        this.formFields.blocks.forEach(function (block) {
            block.showFields.forEach(function (fData) {
                var myValidators = [];
                if (fData.is_mandatory) {
                }
                var newCtl = new forms_1.FormControl(_this.getStoreFormValue(fData), myValidators);
                if (fData.cnd_show) {
                    _this.myForm.addControl(fData.db_field_name, newCtl);
                }
                else {
                    _this.myForm.removeControl(fData.db_field_name);
                }
            });
        });
        // console.log('fData.db_field_name',fData.db_field_name);
        //this.myForm.patchValue(this.getStoreFormValue());
    };
    DynamicFormComponent.prototype.dynamicFormSave = function (isValid) {
        var _this = this;
        this.submittedForm = true;
        if (isValid) {
            if (this.autoSave) {
                this.saveButtonDisable = true;
                var param = {};
                param.Fields = []; //$event.blocks[0].fields;
                param.Module = this.module;
                param.Action = 'Create';
                if (this.pkId > 0) {
                    param.Action = 'Update';
                    param.pk_id = this.pkId;
                }
                else {
                    if (this.subRel) {
                        param.sourceModule = this.subRel;
                    }
                    if (this.subRel && this.subRel.UUID) {
                        param.UUID = this.subRel.UUID;
                    }
                }
                this.formFields.blocks.forEach(function (block) {
                    block.showFields.forEach(function (fData) {
                        var obj = {};
                        obj.name = fData.name;
                        obj.db_field_name = fData.db_field_name;
                        obj.supporting_field_name = fData.supporting_field_name;
                        obj.pick_list_id = fData.pick_list_id;
                        obj.value = fData.value;
                        obj.type = fData.type;
                        obj.block_pk_id = block.block.pk_id;
                        obj.rel = fData.rel;
                        /* if(fData.rel){
                         obj.rel = fData.rel;
                         }*/
                        param.Fields.push(obj);
                    });
                });
                this.httpService.post('SaveUpdateModuleData', param).subscribe(function (res) {
                    _this.saveButtonDisable = false;
                    if (res) {
                        _this.formCacheClear();
                        _this.afterSave.emit(res.data);
                    }
                    else {
                    }
                });
            }
            else {
                this.submitted.emit(this.fields);
            }
        }
    };
    DynamicFormComponent.prototype.gotoAddMoreFields = function () {
        if (this.addMoreFieldUrl) {
            this.router.navigate([this.addMoreFieldUrl]);
        }
    };
    DynamicFormComponent.prototype.onBeforeUpload = function (ev, field) {
        this.httpService.pendingRequestsNumber++;
        ev.formData.append('reqData', new Blob([JSON.stringify({ field: field.name, module: this.module })], {
            type: "application/json"
        }));
        //ev.formData.append('field', JSON.stringify(field));
        ev.xhr.withCredentials = true;
        ev['Content-Type'] = undefined;
    };
    DynamicFormComponent.prototype.onUpload = function (ev, field) {
        this.httpService.pendingRequestsNumber--;
        var res = JSON.parse(ev.xhr.response);
        if (res.success) {
            field.value = res.data;
        }
        else {
            this.toasterService.pop('error', 'Error', res.message);
        }
    };
    DynamicFormComponent.prototype.roleChange = function (evn, fieldD) {
        var _this = this;
        this.formFields.blocks.forEach(function (block) {
            block.showFields.forEach(function (field) {
                if (field.db_field_name == 'reports_to') {
                    if (evn) {
                        _this.bindRepoarts(evn, field);
                    }
                }
            });
        });
    };
    DynamicFormComponent.prototype.bindRepoarts = function (evn, bfield) {
        bfield.data = [];
        bfield.data.push({ label: 'Select', value: null });
        if (evn.pk_id) {
            this.httpService.get('GetReportingToUsers/' + evn.pk_id).subscribe(function (res) {
                if (res) {
                    res.data.forEach(function (v) {
                        var obj = {};
                        obj.label = v.value;
                        obj.value = { pk_id: v.pk_id, value: v.value };
                        bfield.data.push(obj);
                    });
                }
            });
        }
    };
    DynamicFormComponent.prototype.checkDependency = function (event, field) {
        var _this = this;
        this.onCheckConditions(event, field);
        if (field.dependent_to && event) {
            this.httpService.get('PickList/getDependentData/' + field.pick_list_id + '/' + event.pk_id).subscribe(function (res) {
                if (res) {
                    try {
                        for (var key in res.data) {
                            if (field.pick_list_id != key || (_this.pkId > 0 && _this.appFirstLoad)) {
                                //this.appFirstLoad = false;
                                _this.dependentDataBind(key, res.data[key]);
                            }
                            if (field.pick_list_id == key && _this.pkId > 0 && _this.appFirstLoad) {
                                var obj = Object.assign({}, field.value);
                                /*    if (field.value) {

                                 obj.pk_id = field.value.pk_id;
                                 obj.value = field.value.value;
                                 }*/
                                //res.data[key].push(obj);
                                _this.dependentDataBind(key, res.data[key]);
                            }
                        }
                        if (_this.appFirstLoad) {
                            _this.appFirstLoad = false;
                        }
                    }
                    catch (e) {
                        console.log('e', e);
                    }
                }
            });
        }
        else {
            if (field.dependent_to) {
                var dependentList = field.dependent_to.split(',');
                for (var _i = 0; _i < dependentList.length; _i++) {
                    var key = dependentList[_i];
                    if (key) {
                        var pickListId = key.split(':')[1];
                        if (field.pick_list_id != pickListId) {
                            this.dependentDataBind(pickListId, []);
                        }
                    }
                }
            }
        }
        this.setFormControl();
    };
    DynamicFormComponent.prototype.dependentDataBind = function (pickListId, data) {
        this.formFields.blocks.forEach(function (b, index) {
            b.showFields.forEach(function (fData, fIndex) {
                if (fData.pick_list_id == pickListId) {
                    fData.data = [{ label: 'Select', value: null }];
                    var matchValue = false;
                    for (var _i = 0; _i < data.length; _i++) {
                        var v = data[_i];
                        var obj = {};
                        obj.pk_id = v.pk_id;
                        obj.value = v.value;
                        if (fData.value && fData.value.pk_id == v.pk_id) {
                            if (!matchValue) {
                                matchValue = true;
                            }
                        }
                        fData.data.push({ label: v.value, value: obj });
                    }
                    if (!matchValue) {
                        //fData.value = fData.data[0].value;
                        fData.value = null;
                    }
                }
            });
        });
    };
    DynamicFormComponent.prototype.onCheckConditions = function (event, field) {
        field.value = event;
        //this.checkBlockAndFieldCondition s();
        try {
            if (this.formFields.BlkFldDepedency && this.formFields.BlkFldDepedency.depFlds) {
                for (var key in this.formFields.BlkFldDepedency.depFlds) {
                    if (key == field.pk_id) {
                        for (var _i = 0, _a = this.formFields.BlkFldDepedency.depFlds[key]; _i < _a.length; _i++) {
                            var inKey = _a[_i];
                            var objType = inKey.split(" ")[0];
                            var objId = inKey.split(" ")[1];
                            if (objType == 'B') {
                                this.dyanmicService.evalBlockConditions(this.dyanmicService.getBlockOrFieldById(objId, 'block'));
                            }
                            else {
                                this.dyanmicService.evalFieldConditions(this.dyanmicService.getBlockOrFieldById(objId, 'field'));
                            }
                        }
                    }
                }
            }
        }
        catch (e) {
            console.log('e', e);
        }
    };
    DynamicFormComponent.prototype.checkConditions = function (data, type) {
        if (this.formFields.BlkFldDepedency) {
            if (type == 'block') {
                if (this.formFields.BlkFldDepedency.blks.indexOf(data.block.pk_id) != -1) {
                    data.cnd_show = false;
                    this.dyanmicService.evalBlockConditions(data);
                }
                else {
                    data.cnd_show = true;
                }
            }
            if (type == 'field') {
                if (this.formFields.BlkFldDepedency.flds.indexOf(data.pk_id) != -1) {
                    data.cnd_show = false;
                    this.dyanmicService.evalFieldConditions(data);
                }
                else {
                    data.cnd_show = true;
                }
            }
        }
    };
    //date picker
    DynamicFormComponent.prototype.selectedDateRange = function (event, field) {
        field.value = event;
    };
    DynamicFormComponent.prototype.setStoreFormValue = function (data) {
        var obj = JSON.parse(localStorage.getItem(this.router.url));
        var mObj = {};
        for (var key in obj) {
            //console.log('key',key);
            mObj[key] = obj[key];
        }
        for (var key in data) {
            mObj[key] = data[key];
        }
        localStorage.setItem(this.router.url, JSON.stringify(mObj));
    };
    DynamicFormComponent.prototype.getStoreFormValue = function (fData) {
        var obj = JSON.parse(localStorage.getItem(this.router.url));
        var fKey = fData['db_field_name'];
        if (obj && obj[fKey]) {
            //console.log(fKey,obj[fKey]);
            if (['Text', 'Email', 'Url', 'Integer'].indexOf(fData.type) != -1) {
                fData.value = obj[fKey];
            }
        }
    };
    DynamicFormComponent.prototype.formCacheClear = function () {
        localStorage.removeItem(this.router.url);
    };
    __decorate([
        core_1.Input()
    ], DynamicFormComponent.prototype, "fields");
    __decorate([
        core_1.Input()
    ], DynamicFormComponent.prototype, "module");
    __decorate([
        core_1.Input()
    ], DynamicFormComponent.prototype, "pkId");
    __decorate([
        core_1.Input()
    ], DynamicFormComponent.prototype, "autoSave");
    __decorate([
        core_1.Input()
    ], DynamicFormComponent.prototype, "formType");
    __decorate([
        core_1.Input()
    ], DynamicFormComponent.prototype, "addMoreField");
    __decorate([
        core_1.Input()
    ], DynamicFormComponent.prototype, "addMoreFieldUrl");
    __decorate([
        core_1.Input()
    ], DynamicFormComponent.prototype, "col");
    __decorate([
        core_1.Input()
    ], DynamicFormComponent.prototype, "subRel");
    __decorate([
        core_1.Input()
    ], DynamicFormComponent.prototype, "parentModule");
    __decorate([
        core_1.Input()
    ], DynamicFormComponent.prototype, "submitLabel");
    __decorate([
        core_1.Input()
    ], DynamicFormComponent.prototype, "showBlogTitle");
    __decorate([
        core_1.Output('send')
    ], DynamicFormComponent.prototype, "submitted");
    __decorate([
        core_1.Output('loadData')
    ], DynamicFormComponent.prototype, "loadData");
    __decorate([
        core_1.Output('afterSave')
    ], DynamicFormComponent.prototype, "afterSave");
    DynamicFormComponent = __decorate([
        core_1.Component({
            selector: 'dynamic-form',
            templateUrl: './dynamic-form.component.html',
            styleUrls: ['./dynamic-form.component.scss'],
            providers: [common_service_1.CommonService]
        })
    ], DynamicFormComponent);
    return DynamicFormComponent;
})();
exports.DynamicFormComponent = DynamicFormComponent;
//# sourceMappingURL=dynamic-form.component.js.map