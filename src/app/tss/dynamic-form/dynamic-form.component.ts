import { Component, Injectable,OnInit, Input, Output, EventEmitter,Pipe} from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators,NgForm,NgModel} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import {HttpService} from "../../_services/http.service";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {ToasterService} from "angular2-toaster/angular2-toaster";
import {DynamicFormService} from "../_services/dynamic-form.service";
import {CommonService} from "../../_services/common.service";
import {LocalStorageService} from "../../_services/local-storage.service";


@Component({
    selector: 'dynamic-form',
    templateUrl: './dynamic-form.component.html',
    styleUrls: ['./dynamic-form.component.scss'],
    providers: [CommonService]
})
export class DynamicFormComponent implements OnInit {

    @Input() fields:any = {};
    @Input() module:string = null;
    @Input() pkId:number = 0;
    @Input() autoSave:boolean = false;
    @Input() formType:string = 'quick';
    @Input() addMoreField:boolean = false;
    @Input() addMoreFieldUrl:string = '';
    @Input() col:Number = 12;
    @Input() subRel:any = null;
    @Input() exParams:any = null;
    @Input() parentModule:string = null;
    @Input() submitLabel:string = 'Submit';
    @Input() showBlogTitle:boolean = false;
    @Output('send') submitted:EventEmitter<any> = new EventEmitter();
    @Output('loadData') loadData:EventEmitter<any> = new EventEmitter();
    @Output('afterSave') afterSave:EventEmitter<any> = new EventEmitter();

    //form: FormGroup;
    buildFields:any[] = [];
    dynamicFormGroup:FormGroup;
    formShow:boolean = false;
    text1:string;
    filterField:any = {
        cnd_show: true
    }
    regex:any = {
        email: "^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$",
        url: "^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"
    }

    public myForm:FormGroup;

    constructor(private toasterService:ToasterService,
                private _fb:FormBuilder,
                private router:Router,
                private httpService:HttpService,
                private commonService:CommonService,
                private modalService:NgbModal, private dyanmicService:DynamicFormService, private localStorage:LocalStorageService) {

    }

    public sampleFormGroup:FormGroup;
    //options:any;
    overlays:any[];
    formFields:any = [];
    //daterangeInput picker options
    public daterangeOptions:any = {
        locale: {format: 'YYYY-MM-DD'},
        alwaysShowCalendars: false,
    };
    public datetimerangeOptions:any = {
        timePicker: true,
        locale: {format: 'YYYY-MM-DD'},
        alwaysShowCalendars: false,
    };
    public timerangeOptions:any = {
        format: 'HH:ii p',
        autoclose: true,
        // todayHighlight: true,
        showMeridian: true,
        startView: 1,
        maxView: 1
    }

    ngOnInit() {
        this.myForm = this._fb.group({});
        //this.myForm.patchValue(this.getStoreFormValue())
        this.myForm.valueChanges.subscribe(data => {
            //console.log('data', data);
            this.setStoreFormValue(data);
        });
        let param:string = '';
        if (this.parentModule) {
            param = '?parentModule=' + this.parentModule;
        }
        try{
            if (this.fields.blocks) {
                this.formFields = this.dyanmicService.setDynamicFormFieldFormat(this.fields);
                this.setFormControl();
                this.formShow = true;
            } else {
                let action = (this.pkId > 0) ? 'edit' : 'create';
                if (this.pkId > 0) {
                    this.httpService.get('getModuleViewEditData/' + this.module + '/' + this.pkId + '/' + this.formType + '/' + action + param).subscribe(res=> {
                        if (res) {
                            this.formFields = this.dyanmicService.setDynamicFormFieldFormat(res.data, this.formType);
                            this.setFormControl();
                            this.formShow = true;
                            // this.checkBlockAndFieldConditions();
                            this.loadData.emit(this.formFields);
                        }
                    })
                } else {
                    this.httpService.get('getModuleAddEditMetaData/' + this.module + '/' + this.formType + param).subscribe(res=> {
                        if (res) {
                            this.formFields = this.dyanmicService.setDynamicFormFieldFormat(res.data, this.formType);
                            this.setFormControl();
                            this.formShow = true;
                            //this.checkBlockAndFieldConditions();
                            this.loadData.emit(this.formFields);

                        }
                    })
                }
            }
        }catch (e){
            console.log('e',e)
        }

    }

    //add validation for field
    setFormControl() {
        this.formFields.blocks.forEach((block)=> {
            block.showFields.forEach((fData)=> {
                let myValidators = [];
                if (fData.is_mandatory) {
                    //  myValidators.push(Validators.required);
                }
                let newCtl = new FormControl(this.getStoreFormValue(fData), myValidators);
                if (fData.cnd_show) {
                    this.myForm.addControl(fData.db_field_name, newCtl);
                } else {
                    this.myForm.removeControl(fData.db_field_name);
                }

            })
        })
        // console.log('fData.db_field_name',fData.db_field_name);
        //this.myForm.patchValue(this.getStoreFormValue());
    }

    submittedForm:boolean = false;
    saveButtonDisable:boolean = false;

    dynamicFormSave(isValid:boolean) {
        this.submittedForm = true;
        if (isValid) {
            if (this.autoSave) {
                this.saveButtonDisable = true;
                let param:any = {};
                param.Fields = [];//$event.blocks[0].fields;
                param.Module = this.module;
                param.Action = 'Create';
                if (this.pkId > 0) {
                    param.Action = 'Update';
                    param.pk_id = this.pkId;
                } else {
                    if (this.subRel) {
                        param.sourceModule = this.subRel;
                    }
                    if (this.subRel && this.subRel.UUID) {
                        param.UUID = this.subRel.UUID;
                    }
                }
                this.formFields.blocks.forEach((block)=> {
                    block.showFields.forEach((fData)=> {
                        let obj:any = {};
                        obj.name = fData.name;
                        obj.db_field_name = fData.db_field_name;
                        obj.supporting_field_name = fData.supporting_field_name;
                        obj.pick_list_id = fData.pick_list_id;
                        obj.value = fData.value;
                        obj.type = fData.type;
                        obj.block_pk_id = block.block.pk_id;
                        obj.rel = fData.rel;
                        /* if(fData.rel){
                         obj.rel = fData.rel;
                         }*/
                        param.Fields.push(obj);
                    })
                })

                this.httpService.post('SaveUpdateModuleData', param).subscribe(res=> {
                    this.saveButtonDisable = false;
                    if (res) {
                        this.formCacheClear();
                        this.afterSave.emit(res.data);
                    } else {
                        //console.log('res', res);
                    }
                });
            } else {
                this.submitted.emit(this.fields);
            }


        }
    }

    gotoAddMoreFields() {
        if (this.addMoreFieldUrl) {
            this.router.navigate([this.addMoreFieldUrl]);
        }
    }

    uploadUrl:string = this.httpService.apiUrl + 'UploadRawFiles';

    onBeforeUpload(ev, field) {
        this.httpService.pendingRequestsNumber++;
        ev.formData.append('reqData', new Blob([JSON.stringify({field: field.name, module: this.module})], {
            type: "application/json"
        }));
        //ev.formData.append('field', JSON.stringify(field));
        ev.xhr.withCredentials = true;
        ev['Content-Type'] = undefined;
    }

    onUpload(ev, field) {
        this.httpService.pendingRequestsNumber--;
        let res = JSON.parse(ev.xhr.response);
        if (res.success) {
            field.value = res.data;
        } else {
            this.toasterService.pop('error', 'Error', res.message);
        }
    }

    roleChange(evn, fieldD) {
        this.formFields.blocks.forEach((block)=> {
            block.showFields.forEach((field)=> {
                if (field.db_field_name == 'reports_to') {
                    if (evn) {
                        this.bindRepoarts(evn, field);
                    }
                }
            })
        })
    }

    bindRepoarts(evn, bfield) {
        bfield.data = [];
        bfield.data.push({label: 'Select', value: null});
        if (evn.pk_id) {
            this.httpService.get('GetReportingToUsers/' + evn.pk_id).subscribe(res=> {
                if (res) {
                    res.data.forEach((v)=> {
                        let obj:any = {};
                        obj.label = v.value;
                        obj.value = {pk_id: v.pk_id, value: v.value};
                        bfield.data.push(obj);
                    });
                }
            })
        }

    }

    //picklist dependency check
    appFirstLoad:boolean = true;

    checkDependency(event, field) {
        this.onCheckConditions(event, field);
        if (field.dependent_to && event) {
            this.httpService.get('PickList/getDependentData/' + field.pick_list_id + '/' + event.pk_id).subscribe(res=> {
                if (res) {
                    try {
                        for (let key in res.data) {
                            if (field.pick_list_id != key || (this.pkId > 0 && this.appFirstLoad )) {
                                //this.appFirstLoad = false;
                                this.dependentDataBind(key, res.data[key])
                            }
                            if (field.pick_list_id == key && this.pkId > 0 && this.appFirstLoad) {
                                let obj:any = Object.assign({}, field.value);
                                /*    if (field.value) {

                                 obj.pk_id = field.value.pk_id;
                                 obj.value = field.value.value;
                                 }*/

                                //res.data[key].push(obj);
                                this.dependentDataBind(key, res.data[key]);
                            }

                        }

                        if (this.appFirstLoad) {
                            this.appFirstLoad = false;
                        }
                    } catch (e) {
                        console.log('e', e);
                    }


                }
            })
        } else {
            if (field.dependent_to) {
                let dependentList:any = field.dependent_to.split(',');
                for (let key of dependentList) {
                    if (key) {
                        let pickListId = key.split(':')[1];
                        if (field.pick_list_id != pickListId) {
                            this.dependentDataBind(pickListId, []);
                        }
                    }

                }
            }
        }
        this.setFormControl();
    }

    dependentDataBind(pickListId, data) {
        this.formFields.blocks.forEach((b, index)=> {
            b.showFields.forEach((fData, fIndex)=> {
                if (fData.pick_list_id == pickListId) {
                    fData.data = [{label: 'Select', value: null}];
                    let matchValue = false;
                    for (let v of data) {
                        let obj:any = {};
                        obj.pk_id = v.pk_id;
                        obj.value = v.value;
                        if (fData.value && fData.value.pk_id == v.pk_id) {
                            if (!matchValue) {
                                matchValue = true;
                            }
                        }
                        fData.data.push({label: v.value, value: obj});

                    }

                    if (!matchValue) {
                        //fData.value = fData.data[0].value;
                        fData.value = null;
                    }
                }

            })
        });
    }

    onCheckConditions(event, field) {
        field.value = event;
        //this.checkBlockAndFieldCondition s();
        try {
            if (this.formFields.BlkFldDepedency && this.formFields.BlkFldDepedency.depFlds) {
                for (let key in this.formFields.BlkFldDepedency.depFlds) {
                    if (key == field.pk_id) {
                        for (let inKey of this.formFields.BlkFldDepedency.depFlds[key]) {
                            let objType:String = inKey.split(" ")[0];
                            let objId:Number = inKey.split(" ")[1];
                            if (objType == 'B') {
                                this.dyanmicService.evalBlockConditions(this.dyanmicService.getBlockOrFieldById(objId, 'block'));
                            } else {
                                this.dyanmicService.evalFieldConditions(this.dyanmicService.getBlockOrFieldById(objId, 'field'));
                            }
                        }
                    }
                }
            }
        } catch (e) {
            console.log('e', e);
        }

    }


    checkConditions(data, type) {
        if (this.formFields.BlkFldDepedency) {
            if (type == 'block') {
                if (this.formFields.BlkFldDepedency.blks.indexOf(data.block.pk_id) != -1) {
                    data.cnd_show = false;
                    this.dyanmicService.evalBlockConditions(data);
                } else {
                    data.cnd_show = true;
                }
            }
            if (type == 'field') {
                if (this.formFields.BlkFldDepedency.flds.indexOf(data.pk_id) != -1) {
                    data.cnd_show = false;
                    this.dyanmicService.evalFieldConditions(data);
                } else {
                    data.cnd_show = true;
                }
            }
        }
    }

    //date picker
    selectedDateRange(event, field) {
        field.value = event;
    }
    setStoreFormValue(data) {
        let obj:any = JSON.parse(this.localStorage.getItem(this.router.url));
        let mObj:any ={};
        for(let key in obj){
            //console.log('key',key);
            mObj[key] = obj[key];
        }
        for(let key in data){
            mObj[key] = data[key];
        }
        this.localStorage.setItem(this.router.url, JSON.stringify(mObj));
    }

    getStoreFormValue(fData) {
        let obj:any = JSON.parse(this.localStorage.getItem(this.router.url));
        let fKey = fData['db_field_name'];
        if (obj && obj[fKey]) {
            //console.log(fKey,obj[fKey]);
            if(['Text','Email','Url','Integer'].indexOf(fData.type) !=-1){
                fData.value = obj[fKey];
            }
        }

    }
    formCacheClear(){
        this.localStorage.removeItem(this.router.url);
    }


}








