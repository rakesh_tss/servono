import { NgModule } from '@angular/core';
import {DynamicFormService} from "../_services/dynamic-form.service";
import {DynamicFormComponent} from "./dynamic-form.component";
import {RelationModalContentComponent} from "../relation-modal-content/relation-modal-content.component";
import {TableColFieldViewComponent} from "../table-col-field-view/table-col-field-view.component";
import {FieldsetModule} from "primeng/primeng";
import {ImagePathPipe} from "../_pipe/image-path.pipe";
import {Daterangepicker} from "ng2-daterangepicker/index";
import {MultiselectViewPipe} from "../_pipe/multiselect-view.pipe";
import {ModuleViewPathPipe} from "../_pipe/module-view-path.pipe";
import {CommonTssModule} from "../../shared/commonTssModules";
import {FormFieldComponent} from "../form-field/form-field.component";
import {TssGeoModule} from "../../_components/tss-geo/tss-geo.module";
import {OwnerFieldComponent} from "./owner-field/owner-field.component";

@NgModule({
    imports: [
        CommonTssModule,
        FieldsetModule,
        Daterangepicker,
        TssGeoModule
    ],
    declarations: [
        DynamicFormComponent,
        OwnerFieldComponent,
        RelationModalContentComponent,
        TableColFieldViewComponent,
        ImagePathPipe,
        MultiselectViewPipe,
        ModuleViewPathPipe,
        FormFieldComponent
    ], providers: [
        DynamicFormService
    ],
    exports: [
        FieldsetModule,
        Daterangepicker,
        DynamicFormComponent,
        RelationModalContentComponent,
        TableColFieldViewComponent,
        OwnerFieldComponent,
        FormFieldComponent,
        ImagePathPipe,
        MultiselectViewPipe,
        ModuleViewPathPipe
    ]

})
export class DynamicFormModule {
}
