import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuleMngRoutingModule } from './module-mng-routing.module';
import {CommonTssModule} from "../shared/commonTssModules";
import {ModuleMngComponent} from "./module-mng.component";
import {CreateModuleComponent} from "./create-module/create-module.component";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { WorkflowsComponent } from './workflows/workflows.component';
import { BlockAndFieldsComponent } from './block-and-fields/block-and-fields.component';
import { RelationshipsComponent } from './relationships/relationships.component';
import { ModulePropetriesComponent } from './module-propetries/module-propetries.component';
import {IconPickerModule} from "../_components/icon-picker/icon-picker.module";
import {DndModule} from 'ng2-dnd';
import {MdGridListModule,MdCardModule} from '@angular/material';
import { GridManagementComponent} from './grid-management/grid-management.component';
import { DependentFiledsBlockComponent } from './dependent-fileds-block/dependent-fileds-block.component';
import { FieldsUniqueIndexComponent } from './fields-unique-index/fields-unique-index.component';
import {FormBuilderComponent } from './form-builder/form-builder.component';
import {DynamicFormComponent} from "../tss/dynamic-form/dynamic-form.component";
import {RelationModalContentComponent} from "../tss/relation-modal-content/relation-modal-content.component";
import {Daterangepicker} from "ng2-daterangepicker/index";
import {TableColFieldViewComponent} from "../tss/table-col-field-view/table-col-field-view.component";
import {ImagePathPipe} from "../tss/_pipe/image-path.pipe";
import {MultiselectViewPipe} from "../tss/_pipe/multiselect-view.pipe";
import {ModuleViewPathPipe} from "../tss/_pipe/module-view-path.pipe";
import {DynamicFormService} from "../tss/_services/dynamic-form.service";
import {TssService} from "../tss/_services/tss.service";
import {DynamicFormModule} from "../tss/dynamic-form/dynamic-form.module";
import {MaterialModule,MdSlideToggleModule,MdSliderModule} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import {ChipsModule,
    StepsModule,
    ConfirmationService,
    AutoCompleteModule,
    DataTableModule,
    SharedModule,
    OrderListModule,
    SpinnerModule,
    FieldsetModule} from "primeng/primeng";
@NgModule({
    imports: [
        ModuleMngRoutingModule,
        CommonTssModule,
        StepsModule,
        ChipsModule,
        IconPickerModule,
        AutoCompleteModule,
        OrderListModule,
        NgbModule.forRoot(),
        DndModule.forRoot(),
        MdGridListModule,
        MdCardModule,
        MaterialModule,
        FieldsetModule,
        DynamicFormModule,
        MdSlideToggleModule,
        MdSliderModule,
        FlexLayoutModule,
        DataTableModule,
        SharedModule,
        SpinnerModule
    ],
    declarations: [
        ModuleMngComponent,
        CreateModuleComponent,
        WorkflowsComponent,
        BlockAndFieldsComponent,
        RelationshipsComponent,
        ModulePropetriesComponent,
        GridManagementComponent,
        DependentFiledsBlockComponent,
        FieldsUniqueIndexComponent,
        FormBuilderComponent

    ], providers: [
        ConfirmationService,
        DynamicFormService,
        TssService
    ]

})
export class ModuleMngModule {
}
