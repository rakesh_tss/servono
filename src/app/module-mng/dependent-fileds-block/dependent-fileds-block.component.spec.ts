import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DependentFiledsBlockComponent } from './dependent-fileds-block.component';

describe('DependentFiledsBlockComponent', () => {
  let component: DependentFiledsBlockComponent;
  let fixture: ComponentFixture<DependentFiledsBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DependentFiledsBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DependentFiledsBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
