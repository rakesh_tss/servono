import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators,NgForm,NgModel} from '@angular/forms';
import {CreateModuleComponent} from "../create-module/create-module.component";
import {CommonService} from "../../_services/common.service";
import {HttpService} from "../../_services/http.service";
import {LazyLoadEvent,SelectItem,ConfirmationService,SpinnerModule} from "primeng/primeng";
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-dependent-fileds-block',
    templateUrl: './dependent-fileds-block.component.html',
    styleUrls: ['./dependent-fileds-block.component.css']
}) export class DependentFiledsBlockComponent implements OnInit {

    constructor(private activatedRoute:ActivatedRoute
        ,private createModule:CreateModuleComponent
        ,private commonService:CommonService
        ,private router:Router
        ,private httpService:HttpService
        ,private modalService:NgbModal
        ,private confirmationService:ConfirmationService
    ) {

    }

    moduleId:string = '';
    moduleData:any = {};

    ngOnInit() {
        this.createModule.activeIndex = 5;
        this.activatedRoute.parent.params.subscribe((params:Params) => {
            this.moduleId = params['moduleId'];
            this.getModuleBlogData();
        });
    }

    dataRows:any = [];

    getModuleBlogData() {
        this.httpService.get('getModuleAddEditMetaData/' + this.moduleId + '/full').subscribe(res => {
            if (res) {
                this.moduleData = res.data;
                //console.log(this.moduleData['Pick List Data']);
                this.getModuleDependentFields();
            }
        })
    }

    getModuleDependentFields() {
        this.httpService.get('GetAllModuleBlockFieldDependencies/' + this.moduleData.module.pk_id).subscribe(res=> {
            if (res) {
                this.dataRows = res.data;
                //this.totalRecords = res.jqGridData.records;
            }
        });
    }

    tableReload() {
        this.getModuleDependentFields();
    }

    getDependencyConditions(row) {
        this.httpService.get('GetAllModuleBlockFieldDependencyConditions/' + row.pk_id).subscribe(res=> {
            if (res) {
                row.data = res.data;
            }
        })
    }

    itemModal:NgbModalRef;
    itemFG:FormGroup;
    subItemFG:FormGroup;
    accessTypes:any = [];
    itemSubmittedForm:boolean = false;
    blockFields:any = [];
    conditionFields:any = [];
    conditionValues:any = [];

    getBlockFields()
    {
        this.blockFields = [];
        this.conditionFields = [];
        this.moduleData.blocks.forEach((block)=>{
            this.blockFields.push({label:block.block.name,value:{object_id:block.block.pk_id,object_type:1}});
            block.fields.forEach((field)=>{
                this.blockFields.push({label:field.name,value:{object_id:field.pk_id,object_type:2}});
                if(field.type == 'Pick List' || field.type == 'Check Box')
                {
                    this.conditionFields.push({label:field.name,value:{object_id:field.pk_id,object_type:field.type,object_name:field.name}});
                }
            });
        });
    }

    openItemModal(content, data) {
        this.getBlockFields();
        this.accessTypes = [];
        this.accessTypes.push({label: 'Show', value: null});
        this.itemSubmittedForm = false;
        this.itemFG = new FormGroup({
            'pk_id': new FormControl(null),
            'dependency_field': new FormControl(null,Validators.required),
            'access': new FormControl(null),
            'is_mandatory': new FormControl(false)
        });

        if (data) {
            data.dependency_field = {object_id:data.object_id,object_type:data.object_type};
            if(data.object_type == 2)
            {
                this.accessTypes = [];
                this.accessTypes.push({label: 'Show (Read only)', value: false});
                this.accessTypes.push({label: 'Show (Read + Write)', value: true});
            }
            this.itemFG.patchValue(data);
        }
        else
        {
            if(this.blockFields.length > 0)
            {
                this.itemFG.patchValue({dependency_field:this.blockFields[0].value});
                this.dependencyFieldChange(this.blockFields[0].value);
            }
        }

        this.itemModal = this.modalService.open(content);
    }
    dependencyFieldChange(item)
    {
        this.accessTypes = [];
        if(item.object_type==2)
        {
            this.accessTypes.push({label: 'Show (Read only)', value: false});
            this.accessTypes.push({label: 'Show (Read + Write)', value: true});
            this.itemFG.patchValue({access:false});
        }
        else
        {
            this.accessTypes.push({label: 'Show', value: ''});
            this.itemFG.patchValue({access:''});
        }
    }

    itemModalFormSubmit(formValue, valid) {
       this.itemSubmittedForm = true;
        if (valid) {
            formValue.app_module_id = this.moduleData.module.pk_id;
            formValue.object_id = formValue.dependency_field.object_id;
            formValue.object_type = formValue.dependency_field.object_type;
            delete formValue.dependency_field;
            this.httpService.post('SaveOrUpdateDependencyFieldBlock', formValue).subscribe(
                res => {
                    if (res) {
                        this.itemModal.dismiss();
                        this.itemSubmittedForm = false;
                        this.tableReload();
                    }
                })
        }
    }

    removeSelectedItem(row) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
               this.httpService.get('DeleteDependencyFieldBlock/'+row.pk_id,'').subscribe(res=> {
                    if (res) {
                        this.tableReload();
                    }
                })
            }
        });
    }

    selectedValues:any;
    isCheckBox:boolean = false;
    currentRow:any;
    openSubItemModal(content, data,row) {
        this.currentRow = data;
        this.selectedValues = [];
        this.getBlockFields();
        this.conditionValues = [];
        this.conditionValues.push({label: 'Equals', value: true});
        this.conditionValues.push({label: 'Not Equals', value: false});
        this.itemSubmittedForm = false;
        this.subItemFG = new FormGroup({
            'app_dependency_field_block_id': new FormControl(data.pk_id),
            'pk_id': new FormControl(null),
            'app_module_field': new FormControl(null, Validators.required),
            'equals': new FormControl(true),
            'selectedValues': new FormControl(null,Validators.required)
        });
        if (row) {
            row.app_module_field = {object_id:row.app_module_field_id,object_type:row.field_type,object_name:row.field_name};
            this.moduleFieldChange(row.app_module_field);
            if(row.field_type=='Pick List')
            {
                row.value.forEach((val)=>{
                    this.selectedValues.push(val.pk_id);
                });
            }
            else
            {
                if(row.value.length > 0)
                {
                    this.selectedValues = row.value[0].value;
                    row.selectedValues = row.value[0].value;
                }
            }

            this.subItemFG.patchValue(row);
        }
        else
        {
            this.subItemFG.patchValue({app_module_field:this.conditionFields[0].value});
            this.moduleFieldChange(this.conditionFields[0].value);
        }
        this.itemModal = this.modalService.open(content);
    }

    picklistValues:any = [];

    moduleFieldChange(item)
    {
        this.picklistValues = [];
        this.isCheckBox = false;
        if(item.object_type=='Pick List')
        {
            this.selectedValues = [];
            this.picklistValues = this.moduleData['Pick List Data'][item.object_name+' Data'];
            this.picklistValues = this.commonService.getDropdownOptions(this.picklistValues,'value','pk_id',true);
        }
        else if(item.object_type=='Check Box')
        {
            this.isCheckBox = true;
            this.picklistValues.push({label: 'True', value: true});
            this.picklistValues.push({label: 'False', value: false});
            this.selectedValues = 'true';
        }
    }

    subItemModalFormSubmit(formValue, valid)
    {
        this.itemSubmittedForm = true;
        if (valid) {
            formValue.app_module_field_id = formValue.app_module_field.object_id;
            delete formValue.app_module_field;
            formValue.value = formValue.selectedValues.toString();
            delete formValue.selectedValues;
            this.httpService.post('SaveOrUpdateDependencyFieldBlockCondition', formValue).subscribe(
                res => {
                    if (res) {
                        this.itemModal.dismiss();
                        this.itemSubmittedForm = false;
                        this.getDependencyConditions(this.currentRow);
                    }
                })
        }
    }

    removeCondition(row,prow)
    {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
                this.httpService.get('DeleteDependencyFieldBlockCondition/'+prow.pk_id,'').subscribe(res=> {
                    if (res) {
                        this.getDependencyConditions(row);
                    }
                })
            }
        });
    }


}
