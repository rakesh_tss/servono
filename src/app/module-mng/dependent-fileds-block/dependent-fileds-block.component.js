var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var DependentFiledsBlockComponent = (function () {
    function DependentFiledsBlockComponent(activatedRoute, createModule, commonService, router, httpService, modalService, confirmationService) {
        this.activatedRoute = activatedRoute;
        this.createModule = createModule;
        this.commonService = commonService;
        this.router = router;
        this.httpService = httpService;
        this.modalService = modalService;
        this.confirmationService = confirmationService;
        this.moduleId = '';
        this.moduleData = {};
        this.dataRows = [];
        this.accessTypes = [];
        this.itemSubmittedForm = false;
        this.blockFields = [];
        this.conditionFields = [];
        this.conditionValues = [];
        this.isCheckBox = false;
        this.picklistValues = [];
    }
    DependentFiledsBlockComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createModule.activeIndex = 5;
        this.activatedRoute.parent.params.subscribe(function (params) {
            _this.moduleId = params['moduleId'];
            _this.getModuleBlogData();
        });
    };
    DependentFiledsBlockComponent.prototype.getModuleBlogData = function () {
        var _this = this;
        this.httpService.get('getModuleAddEditMetaData/' + this.moduleId + '/full').subscribe(function (res) {
            if (res) {
                _this.moduleData = res.data;
                //console.log(this.moduleData['Pick List Data']);
                _this.getModuleDependentFields();
            }
        });
    };
    DependentFiledsBlockComponent.prototype.getModuleDependentFields = function () {
        var _this = this;
        this.httpService.get('GetAllModuleBlockFieldDependencies/' + this.moduleData.module.pk_id).subscribe(function (res) {
            if (res) {
                _this.dataRows = res.data;
            }
        });
    };
    DependentFiledsBlockComponent.prototype.tableReload = function () {
        this.getModuleDependentFields();
    };
    DependentFiledsBlockComponent.prototype.getDependencyConditions = function (row) {
        this.httpService.get('GetAllModuleBlockFieldDependencyConditions/' + row.pk_id).subscribe(function (res) {
            if (res) {
                row.data = res.data;
            }
        });
    };
    DependentFiledsBlockComponent.prototype.getBlockFields = function () {
        var _this = this;
        this.blockFields = [];
        this.conditionFields = [];
        this.moduleData.blocks.forEach(function (block) {
            _this.blockFields.push({ label: block.block.name, value: { object_id: block.block.pk_id, object_type: 1 } });
            block.fields.forEach(function (field) {
                _this.blockFields.push({ label: field.name, value: { object_id: field.pk_id, object_type: 2 } });
                if (field.type == 'Pick List' || field.type == 'Check Box') {
                    _this.conditionFields.push({ label: field.name, value: { object_id: field.pk_id, object_type: field.type, object_name: field.name } });
                }
            });
        });
    };
    DependentFiledsBlockComponent.prototype.openItemModal = function (content, data) {
        this.getBlockFields();
        this.accessTypes = [];
        this.accessTypes.push({ label: 'Show', value: null });
        this.itemSubmittedForm = false;
        this.itemFG = new forms_1.FormGroup({
            'pk_id': new forms_1.FormControl(null),
            'dependency_field': new forms_1.FormControl(null, forms_1.Validators.required),
            'access': new forms_1.FormControl(null),
            'is_mandatory': new forms_1.FormControl(false)
        });
        if (data) {
            data.dependency_field = { object_id: data.object_id, object_type: data.object_type };
            if (data.object_type == 2) {
                this.accessTypes = [];
                this.accessTypes.push({ label: 'Show (Read only)', value: false });
                this.accessTypes.push({ label: 'Show (Read + Write)', value: true });
            }
            this.itemFG.patchValue(data);
        }
        else {
            if (this.blockFields.length > 0) {
                this.itemFG.patchValue({ dependency_field: this.blockFields[0].value });
                this.dependencyFieldChange(this.blockFields[0].value);
            }
        }
        this.itemModal = this.modalService.open(content);
    };
    DependentFiledsBlockComponent.prototype.dependencyFieldChange = function (item) {
        this.accessTypes = [];
        if (item.object_type == 2) {
            this.accessTypes.push({ label: 'Show (Read only)', value: false });
            this.accessTypes.push({ label: 'Show (Read + Write)', value: true });
            this.itemFG.patchValue({ access: false });
        }
        else {
            this.accessTypes.push({ label: 'Show', value: '' });
            this.itemFG.patchValue({ access: '' });
        }
    };
    DependentFiledsBlockComponent.prototype.itemModalFormSubmit = function (formValue, valid) {
        var _this = this;
        this.itemSubmittedForm = true;
        if (valid) {
            formValue.app_module_id = this.moduleData.module.pk_id;
            formValue.object_id = formValue.dependency_field.object_id;
            formValue.object_type = formValue.dependency_field.object_type;
            delete formValue.dependency_field;
            this.httpService.post('SaveOrUpdateDependencyFieldBlock', formValue).subscribe(function (res) {
                if (res) {
                    _this.itemModal.dismiss();
                    _this.itemSubmittedForm = false;
                    _this.tableReload();
                }
            });
        }
    };
    DependentFiledsBlockComponent.prototype.removeSelectedItem = function (row) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.httpService.get('DeleteDependencyFieldBlock/' + row.pk_id, '').subscribe(function (res) {
                    if (res) {
                        _this.tableReload();
                    }
                });
            }
        });
    };
    DependentFiledsBlockComponent.prototype.openSubItemModal = function (content, data, row) {
        var _this = this;
        this.currentRow = data;
        this.selectedValues = [];
        this.getBlockFields();
        this.conditionValues = [];
        this.conditionValues.push({ label: 'Equals', value: true });
        this.conditionValues.push({ label: 'Not Equals', value: false });
        this.itemSubmittedForm = false;
        this.subItemFG = new forms_1.FormGroup({
            'app_dependency_field_block_id': new forms_1.FormControl(data.pk_id),
            'pk_id': new forms_1.FormControl(null),
            'app_module_field': new forms_1.FormControl(null, forms_1.Validators.required),
            'equals': new forms_1.FormControl(true),
            'selectedValues': new forms_1.FormControl(null, forms_1.Validators.required)
        });
        if (row) {
            row.app_module_field = { object_id: row.app_module_field_id, object_type: row.field_type, object_name: row.field_name };
            this.moduleFieldChange(row.app_module_field);
            if (row.field_type == 'Pick List') {
                row.value.forEach(function (val) {
                    _this.selectedValues.push(val.pk_id);
                });
            }
            else {
                if (row.value.length > 0) {
                    this.selectedValues = row.value[0].value;
                    row.selectedValues = row.value[0].value;
                }
            }
            this.subItemFG.patchValue(row);
        }
        else {
            this.subItemFG.patchValue({ app_module_field: this.conditionFields[0].value });
            this.moduleFieldChange(this.conditionFields[0].value);
        }
        this.itemModal = this.modalService.open(content);
    };
    DependentFiledsBlockComponent.prototype.moduleFieldChange = function (item) {
        this.picklistValues = [];
        this.isCheckBox = false;
        if (item.object_type == 'Pick List') {
            this.selectedValues = [];
            this.picklistValues = this.moduleData['Pick List Data'][item.object_name + ' Data'];
            this.picklistValues = this.commonService.getDropdownOptions(this.picklistValues, 'value', 'pk_id', true);
        }
        else if (item.object_type == 'Check Box') {
            this.isCheckBox = true;
            this.picklistValues.push({ label: 'True', value: true });
            this.picklistValues.push({ label: 'False', value: false });
            this.selectedValues = 'true';
        }
    };
    DependentFiledsBlockComponent.prototype.subItemModalFormSubmit = function (formValue, valid) {
        var _this = this;
        this.itemSubmittedForm = true;
        if (valid) {
            formValue.app_module_field_id = formValue.app_module_field.object_id;
            delete formValue.app_module_field;
            formValue.value = formValue.selectedValues.toString();
            delete formValue.selectedValues;
            this.httpService.post('SaveOrUpdateDependencyFieldBlockCondition', formValue).subscribe(function (res) {
                if (res) {
                    _this.itemModal.dismiss();
                    _this.itemSubmittedForm = false;
                    _this.getDependencyConditions(_this.currentRow);
                }
            });
        }
    };
    DependentFiledsBlockComponent.prototype.removeCondition = function (row, prow) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.httpService.get('DeleteDependencyFieldBlockCondition/' + prow.pk_id, '').subscribe(function (res) {
                    if (res) {
                        _this.getDependencyConditions(row);
                    }
                });
            }
        });
    };
    DependentFiledsBlockComponent = __decorate([
        core_1.Component({
            selector: 'app-dependent-fileds-block',
            templateUrl: './dependent-fileds-block.component.html',
            styleUrls: ['./dependent-fileds-block.component.css']
        })
    ], DependentFiledsBlockComponent);
    return DependentFiledsBlockComponent;
})();
exports.DependentFiledsBlockComponent = DependentFiledsBlockComponent;
//# sourceMappingURL=dependent-fileds-block.component.js.map