var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var module_mng_routing_module_1 = require('./module-mng-routing.module');
var commonTssModules_1 = require("../shared/commonTssModules");
var module_mng_component_1 = require("./module-mng.component");
var create_module_component_1 = require("./create-module/create-module.component");
var ng_bootstrap_1 = require('@ng-bootstrap/ng-bootstrap');
var workflows_component_1 = require('./workflows/workflows.component');
var block_and_fields_component_1 = require('./block-and-fields/block-and-fields.component');
var relationships_component_1 = require('./relationships/relationships.component');
var module_propetries_component_1 = require('./module-propetries/module-propetries.component');
var icon_picker_module_1 = require("../_components/icon-picker/icon-picker.module");
var primeng_1 = require("primeng/primeng");
var ng2_dnd_1 = require('ng2-dnd');
var material_1 = require('@angular/material');
var grid_management_component_1 = require('./grid-management/grid-management.component');
var dependent_fileds_block_component_1 = require('./dependent-fileds-block/dependent-fileds-block.component');
var fields_unique_index_component_1 = require('./fields-unique-index/fields-unique-index.component');
var material_2 = require('@angular/material');
var form_builder_component_1 = require('./form-builder/form-builder.component');
var dynamic_form_service_1 = require("../tss/_services/dynamic-form.service");
var tss_service_1 = require("../tss/_services/tss.service");
var dynamic_form_module_1 = require("../tss/dynamic-form/dynamic-form.module");
var material_3 = require('@angular/material');
var flex_layout_1 = require('@angular/flex-layout');
var primeng_2 = require('primeng/primeng');
var ModuleMngModule = (function () {
    function ModuleMngModule() {
    }
    ModuleMngModule = __decorate([
        core_1.NgModule({
            imports: [
                module_mng_routing_module_1.ModuleMngRoutingModule,
                commonTssModules_1.CommonTssModule,
                primeng_1.StepsModule,
                primeng_1.ChipsModule,
                icon_picker_module_1.IconPickerModule,
                primeng_1.AutoCompleteModule,
                primeng_1.OrderListModule,
                ng_bootstrap_1.NgbModule.forRoot(),
                ng2_dnd_1.DndModule.forRoot(),
                material_1.MdGridListModule,
                material_1.MdCardModule,
                material_2.MaterialModule,
                primeng_1.FieldsetModule,
                dynamic_form_module_1.DynamicFormModule,
                material_3.MdSlideToggleModule,
                material_3.MdSliderModule,
                flex_layout_1.FlexLayoutModule,
                primeng_2.DataTableModule,
                primeng_2.SharedModule
            ],
            declarations: [
                module_mng_component_1.ModuleMngComponent,
                create_module_component_1.CreateModuleComponent,
                workflows_component_1.WorkflowsComponent,
                block_and_fields_component_1.BlockAndFieldsComponent,
                relationships_component_1.RelationshipsComponent,
                module_propetries_component_1.ModulePropetriesComponent,
                grid_management_component_1.GridManagementComponent,
                dependent_fileds_block_component_1.DependentFiledsBlockComponent,
                fields_unique_index_component_1.FieldsUniqueIndexComponent,
                form_builder_component_1.FormBuilderComponent
            ], providers: [
                primeng_1.ConfirmationService,
                dynamic_form_service_1.DynamicFormService,
                tss_service_1.TssService
            ]
        })
    ], ModuleMngModule);
    return ModuleMngModule;
})();
exports.ModuleMngModule = ModuleMngModule;
//# sourceMappingURL=module-mng.module.js.map