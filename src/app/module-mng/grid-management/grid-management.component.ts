import { Component, OnInit } from '@angular/core';
import {CreateModuleComponent} from "../create-module/create-module.component";
import { FormGroup, FormControl, FormBuilder, Validators,NgForm,NgModel} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {HttpService} from "../../_services/http.service";
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {LazyLoadEvent,SelectItem,ConfirmationService,SpinnerModule} from "primeng/primeng";
import {CommonService} from "../../_services/common.service";
import {DynamicFormService} from "../../tss/_services/dynamic-form.service";

@Component({
    selector: 'module-grid-management',
    templateUrl: './grid-management.component.html',
    styleUrls: ['./grid-management.component.scss']
})
export class GridManagementComponent implements OnInit {

    constructor(private createModule:CreateModuleComponent,
                private activatedRoute:ActivatedRoute,
                private modalService:NgbModal,
                private confirmationService:ConfirmationService,
                private cs:CommonService,
                private dynamicFormService:DynamicFormService,
                private httpService:HttpService) {
    }

    /**/


    moduleId:string = '';
    ModuleDbTableName:string;
    currentTemplateId:string;
    //templateList:any =[{label:'Template 1',value:'Template 1'},{label:'Template 2',value:'Template 2'}]
    templateList:any = [];
    rowsData:any = [{}, {}];
    numCols:any = [1, 2, 3, 4, 5, 6];//Array(5).fill().map((x,i)=>i); // [0,1,2,3,4]
    col:any = [];

    ngOnInit() {
        this.createModule.activeIndex = 2;
        this.activatedRoute.parent.params.subscribe((params:Params) => {
            this.moduleId = params['moduleId'];
            this.getTemplates();
            this.getModuleListManagement();
            //this.getModuleBlogData();
            //this.getModuleBlogFullData();
        });
    }

    private moduleData:any = {};
    moduleFields:any = [];

    getTemplates() {
        this.httpService.get('DropListData/modulelisttemplate/0' + '').subscribe(res => {
            if (res) {
                this.templateList = this.cs.getDropdownOptions(res.data, 'name', 'pk_id', true);
            }
        })
    }


    colFields:any = [];
    unColFields:any = [];

    getModuleBlogFullData() {
        this.httpService.get('GetModuleByName/' + this.moduleId).subscribe(res => {
            if (res) {
                //this.blocks = res.data.blocks;
                //this.moduleData = res.data.module;
                let formateData = this.dynamicFormService.setDynamicFormFieldFormat(res.data);

                formateData.blocks.forEach((b)=> {
                    b.fields.forEach(f=> {
                        this.allFields.push(f);
                        if (f.display_in_list_view) {
                            this.colFields.push(f)
                        } else {
                            this.unColFields.push(f)
                        }

                    })
                })
                //set module fields
                //let formateData = this.dynamicFormService.setDynamicFormFieldFormat(res.data);
                //console.log('formateData', formateData);

                // this.moduleFields = formateData.blocks;
                //  this.ModuleDbTableName = this.moduleData.db_table_name;
            }
        })
    }

    allFields:any = {available_fields: [], list_fields: []};
    module_pk_id:any;

    getModuleListManagement() {
        this.httpService.get('GetModuleListManagement/' + this.moduleId).subscribe(res => {
            if (res) {
                this.allFields = res.data;
                this.currentTemplateId = res.data.template_id;
                this.module_pk_id = res.data.module_id;
                this.tableReload();
                /*this.selectedFields = res.data;
                 //set module fields
                 //this.moduleFields = res.data.blocks;
                 this.currentTemplateId = res.moduleAndTemplateData.template_id;
                 console.log(res.moduleAndTemplateData.template_id);
                 //this.getFieldsList();
                 //this.ModuleDbTableName = this.moduleData.db_table_name;
                 this.tableReload();*/
            }
        })
    }

    getModuleBlogData() {
        this.httpService.get('getModuleListMetadata/' + this.moduleId).subscribe(res => {
            if (res) {
                this.moduleData = res.moduleAndTemplateData;
                this.selectedFields = res.data;
                //set module fields
                //this.moduleFields = res.data.blocks;
                this.currentTemplateId = res.moduleAndTemplateData.template_id;
                console.log(res.moduleAndTemplateData.template_id);
                //this.getFieldsList();
                //this.ModuleDbTableName = this.moduleData.db_table_name;
                this.tableReload();
            }
        })
    }

    fieldList:any[] = [];
    selectedFields:any[] = [];

    getFieldsList() {
        this.moduleFields.forEach((block)=> {
            block.fields.forEach((field)=> {
                let valueObj:any = {};
                valueObj.label = field.name;
                valueObj.pk_id = field.pk_id;
                this.fieldList.push({label: field.name, value: valueObj});
                if (field.display_in_list_view) {
                    this.selectedFields.push({label: field.name, value: valueObj});
                }
            })
        })

    }

    saveModuleTemplate(temp) {
        let formValues:any = {};
        this.currentTemplateId = temp.value;
        formValues.app_module_id = this.moduleData.app_module_id;
        formValues.template_id = this.currentTemplateId;
        if (formValues.template_id) {
            this.httpService.post('saveOrUpdateModuleList', formValues).subscribe(
                res => {
                    if (res) {
                    }
                })
        }
    }


    dataRows:any = [];
    totalRecords:Number = 0;
    refreshTable:boolean = false;

    loadDataLazy(event:LazyLoadEvent) {
        event['app_module_id'] = this.module_pk_id;
        this.httpService.postTable('GetGridList/modulepagewidget', event).subscribe(
            res=> {
                if (res) {
                    this.dataRows = res.jqGridData.rows;
                    this.totalRecords = res.jqGridData.records;
                }
            }
        );
    }

    tableReload() {
        this.refreshTable = false;
        setTimeout(() => this.refreshTable = true, 0);
    }

    itemSubmittedForm:boolean = false;
    itemFG:FormGroup;
    itemModal:NgbModalRef;
    widgetTypes:any = [];

    private openItemModal(content, data) {
        this.widgetTypes = [];
        this.widgetTypes.push({label: 'Top', value: 'Top'});
        this.widgetTypes.push({label: 'Right', value: 'Right'});
        console.log('data', data);
        this.itemSubmittedForm = false;
        //this.getParentPickList();
        this.itemFG = new FormGroup({
            'pk_id': new FormControl(null),
            'name': new FormControl(null, Validators.required),
            'widget_type': new FormControl('Top'),
            'html_template': new FormControl(null),
            'js_function_name': new FormControl(null),
            'js_code': new FormControl(null),
            'query_code': new FormControl(null),
            'call_required': new FormControl(false),
            'seq_order': new FormControl(null, Validators.required)
        });

        if (data) {
            this.itemFG.patchValue(data);
        }
        this.itemModal = this.modalService.open(content, {size: 'lg'});
    }

    itemModalFormSubmit(formValue, valid) {
        this.itemSubmittedForm = true;
        if (valid) {
            formValue.app_module_id = this.moduleData.app_module_id;
            this.httpService.post('saveUpdateModulePageWidgets', formValue).subscribe(
                res => {
                    if (res) {
                        this.itemModal.dismiss();
                        this.itemSubmittedForm = false;
                        this.tableReload();
                    }
                })
        }
    }

    removeSelectedItem(row) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
                this.httpService.post('removeModulePageWidgets/' + row.pk_id, '').subscribe(res=> {
                    if (res) {
                        this.tableReload();
                    }
                })
            }
        });
    }

    removeColumn(f, indx) {
        console.log('indx', indx);
        f.display_in_list_view = false;
        this.colFields.splice(indx, 1);
        this.unColFields.push(f);
    }

    allowDropFunction():any {

        //console.log('this.colFields.length',(this.colFields.length <= 6)?true:false);
        return this.colFields.length <= 6;
    }

    onDropSuccess($event, type, onlyTemplate) {

        let params:any = {};
        Object.assign(params, this.allFields);
        params.onlyTemplate = false;
        if (onlyTemplate) {
            params.onlyTemplate = true;
            params.template_id = onlyTemplate.value;
            this.currentTemplateId = onlyTemplate.value;
            delete  params.available_fields;
            delete  params.list_fields;
        }
        if (type == 'colField') {
            params.list_fields.forEach((data, index)=> {
                data.list_order = index + 1;
            })
        }


        this.httpService.post('SaveUpdateModuleListFields', params).subscribe(res=> {
            if (res) {
            }
        })


        /* if(type == 'colField'){
         if(!event.display_in_list_view)
         this.changeFieldStatus(event,'display_in_list_view');

         this.reOrder();

         }else{
         if(event.display_in_list_view)
         this.changeFieldStatus(event,'display_in_list_view');
         }*/


    }


    reOrder() {
        this.colFields.forEach((field, key)=> {
            field.seq_order = key + 1;
        });
        console.log('colFields', this.colFields);
        let params:any = {};
        params.data = this.colFields;
        this.httpService.post('updateOderOfModuleListFields', params).subscribe(res=> {
            if (res) {

            }
        })
    }

    //cahgne
    changeFieldStatus(field, key) {
        let keyValue:boolean = true;
        if (field[key]) {
            keyValue = false;
        }
        let param:any = {};
        param.key = key;
        param.value = keyValue;
        param.pk_id = field.pk_id;
        param.user_meta_field = field.user_meta_field;
        param.module_id = this.moduleData.pk_id;
        this.httpService.post('UpdateModuleFieldQuick', param).subscribe(
            res => {
                if (res) {
                    field[key] = keyValue;
                }
            })

    }


}
