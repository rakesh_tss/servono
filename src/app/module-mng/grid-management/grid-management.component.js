var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var GridManagementComponent = (function () {
    function GridManagementComponent(createModule, activatedRoute, modalService, confirmationService, cs, dynamicFormService, httpService) {
        this.createModule = createModule;
        this.activatedRoute = activatedRoute;
        this.modalService = modalService;
        this.confirmationService = confirmationService;
        this.cs = cs;
        this.dynamicFormService = dynamicFormService;
        this.httpService = httpService;
        /**/
        this.moduleId = '';
        //templateList:any =[{label:'Template 1',value:'Template 1'},{label:'Template 2',value:'Template 2'}]
        this.templateList = [];
        this.rowsData = [{}, {}];
        this.numCols = [1, 2, 3, 4, 5, 6]; //Array(5).fill().map((x,i)=>i); // [0,1,2,3,4]
        this.col = [];
        this.moduleData = {};
        this.moduleFields = [];
        this.allFields = [];
        this.colFields = [];
        this.unColFields = [];
        this.fieldList = [];
        this.selectedFields = [];
        this.dataRows = [];
        this.totalRecords = 0;
        this.refreshTable = false;
        this.itemSubmittedForm = false;
        this.widgetTypes = [];
    }
    GridManagementComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createModule.activeIndex = 2;
        this.activatedRoute.parent.params.subscribe(function (params) {
            _this.moduleId = params['moduleId'];
            _this.getTemplates();
            _this.getModuleBlogData();
            _this.getModuleBlogFullData();
        });
    };
    GridManagementComponent.prototype.getTemplates = function () {
        var _this = this;
        this.httpService.get('DropListData/modulelisttemplate/0' + '').subscribe(function (res) {
            if (res) {
                _this.templateList = _this.cs.getDropdownOptions(res.data, 'name', 'pk_id', true);
            }
        });
    };
    GridManagementComponent.prototype.getModuleBlogFullData = function () {
        var _this = this;
        this.httpService.get('GetModuleByName/' + this.moduleId).subscribe(function (res) {
            if (res) {
                //this.blocks = res.data.blocks;
                //this.moduleData = res.data.module;
                var formateData = _this.dynamicFormService.setDynamicFormFieldFormat(res.data);
                formateData.blocks.forEach(function (b) {
                    b.fields.forEach(function (f) {
                        _this.allFields.push(f);
                        if (f.display_in_list_view) {
                            _this.colFields.push(f);
                        }
                        else {
                            _this.unColFields.push(f);
                        }
                    });
                });
            }
        });
    };
    GridManagementComponent.prototype.getModuleBlogData = function () {
        var _this = this;
        this.httpService.get('getModuleListMetadata/' + this.moduleId).subscribe(function (res) {
            if (res) {
                _this.moduleData = res.moduleAndTemplateData;
                _this.selectedFields = res.data;
                //set module fields
                //this.moduleFields = res.data.blocks;
                _this.currentTemplateId = res.moduleAndTemplateData.template_id;
                console.log(res.moduleAndTemplateData.template_id);
                //this.getFieldsList();
                //this.ModuleDbTableName = this.moduleData.db_table_name;
                _this.tableReload();
            }
        });
    };
    GridManagementComponent.prototype.getFieldsList = function () {
        var _this = this;
        this.moduleFields.forEach(function (block) {
            block.fields.forEach(function (field) {
                var valueObj = {};
                valueObj.label = field.name;
                valueObj.pk_id = field.pk_id;
                _this.fieldList.push({ label: field.name, value: valueObj });
                if (field.display_in_list_view) {
                    _this.selectedFields.push({ label: field.name, value: valueObj });
                }
            });
        });
    };
    GridManagementComponent.prototype.saveModuleTemplate = function (temp) {
        var formValues = {};
        this.currentTemplateId = temp.value;
        formValues.app_module_id = this.moduleData.app_module_id;
        formValues.template_id = this.currentTemplateId;
        if (formValues.template_id) {
            this.httpService.post('saveOrUpdateModuleList', formValues).subscribe(function (res) {
                if (res) {
                }
            });
        }
    };
    GridManagementComponent.prototype.loadDataLazy = function (event) {
        var _this = this;
        console.log(this.moduleData);
        event['app_module_id'] = this.moduleData.app_module_id;
        this.httpService.postTable('GetGridList/modulepagewidget', event).subscribe(function (res) {
            if (res) {
                _this.dataRows = res.jqGridData.rows;
                _this.totalRecords = res.jqGridData.records;
            }
        });
    };
    GridManagementComponent.prototype.tableReload = function () {
        var _this = this;
        this.refreshTable = false;
        setTimeout(function () { return _this.refreshTable = true; }, 0);
    };
    GridManagementComponent.prototype.openItemModal = function (content, data) {
        this.widgetTypes = [];
        this.widgetTypes.push({ label: 'Top', value: 'Top' });
        this.widgetTypes.push({ label: 'Right', value: 'Right' });
        console.log('data', data);
        this.itemSubmittedForm = false;
        //this.getParentPickList();
        this.itemFG = new forms_1.FormGroup({
            'pk_id': new forms_1.FormControl(null),
            'name': new forms_1.FormControl(null, forms_1.Validators.required),
            'widget_type': new forms_1.FormControl('Top'),
            'html_template': new forms_1.FormControl(null),
            'js_function_name': new forms_1.FormControl(null),
            'js_code': new forms_1.FormControl(null),
            'query_code': new forms_1.FormControl(null),
            'call_required': new forms_1.FormControl(false),
            'seq_order': new forms_1.FormControl(null, forms_1.Validators.required)
        });
        if (data) {
            this.itemFG.patchValue(data);
        }
        this.itemModal = this.modalService.open(content, { size: 'lg' });
    };
    GridManagementComponent.prototype.itemModalFormSubmit = function (formValue, valid) {
        var _this = this;
        this.itemSubmittedForm = true;
        if (valid) {
            formValue.app_module_id = this.moduleData.app_module_id;
            this.httpService.post('saveUpdateModulePageWidgets', formValue).subscribe(function (res) {
                if (res) {
                    _this.itemModal.dismiss();
                    _this.itemSubmittedForm = false;
                    _this.tableReload();
                }
            });
        }
    };
    GridManagementComponent.prototype.removeSelectedItem = function (row) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.httpService.post('removeModulePageWidgets/' + row.pk_id, '').subscribe(function (res) {
                    if (res) {
                        _this.tableReload();
                    }
                });
            }
        });
    };
    GridManagementComponent.prototype.removeColumn = function (f, indx) {
        console.log('indx', indx);
        f.display_in_list_view = false;
        this.colFields.splice(indx, 1);
        this.unColFields.push(f);
    };
    GridManagementComponent.prototype.allowDropFunction = function () {
        //console.log('this.colFields.length',(this.colFields.length <= 6)?true:false);
        return this.colFields.length <= 6;
    };
    GridManagementComponent.prototype.onDropSuccess = function (event, type) {
        if (type == 'colField') {
            if (!event.display_in_list_view)
                this.changeFieldStatus(event, 'display_in_list_view');
            this.reOrder();
        }
        else {
            if (event.display_in_list_view)
                this.changeFieldStatus(event, 'display_in_list_view');
        }
    };
    GridManagementComponent.prototype.reOrder = function () {
        this.colFields.forEach(function (field, key) {
            field.seq_order = key + 1;
        });
        console.log('colFields', this.colFields);
        var params = {};
        params.data = this.colFields;
        this.httpService.post('updateOderOfModuleListFields', params).subscribe(function (res) {
            if (res) {
            }
        });
    };
    //cahgne
    GridManagementComponent.prototype.changeFieldStatus = function (field, key) {
        var keyValue = true;
        if (field[key]) {
            keyValue = false;
        }
        var param = {};
        param.key = key;
        param.value = keyValue;
        param.pk_id = field.pk_id;
        param.user_meta_field = field.user_meta_field;
        param.module_id = this.moduleData.pk_id;
        this.httpService.post('UpdateModuleFieldQuick', param).subscribe(function (res) {
            if (res) {
                field[key] = keyValue;
            }
        });
    };
    GridManagementComponent = __decorate([
        core_1.Component({
            selector: 'module-grid-management',
            templateUrl: './grid-management.component.html',
            styleUrls: ['./grid-management.component.scss']
        })
    ], GridManagementComponent);
    return GridManagementComponent;
})();
exports.GridManagementComponent = GridManagementComponent;
//# sourceMappingURL=grid-management.component.js.map