var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var module_mng_component_1 = require("./module-mng.component");
var create_module_component_1 = require("./create-module/create-module.component");
var relationships_component_1 = require("./relationships/relationships.component");
var module_propetries_component_1 = require("./module-propetries/module-propetries.component");
var workflows_component_1 = require("./workflows/workflows.component");
var grid_management_component_1 = require("./grid-management/grid-management.component");
var dependent_fileds_block_component_1 = require("./dependent-fileds-block/dependent-fileds-block.component");
var fields_unique_index_component_1 = require("./fields-unique-index/fields-unique-index.component");
var form_builder_component_1 = require("./form-builder/form-builder.component");
var routes = [
    {
        path: '',
        component: module_mng_component_1.ModuleMngComponent,
        data: {
            title: 'Module Management'
        }
    },
    {
        path: ':Create/:moduleId',
        component: create_module_component_1.CreateModuleComponent,
        data: {
            title: 'Module Create/Update'
        },
        children: [
            {
                path: '0',
                component: module_propetries_component_1.ModulePropetriesComponent
            },
            {
                path: '1',
                component: form_builder_component_1.FormBuilderComponent
            },
            {
                path: '2',
                component: grid_management_component_1.GridManagementComponent
            },
            {
                path: '3',
                component: relationships_component_1.RelationshipsComponent
            },
            {
                path: '4',
                component: workflows_component_1.WorkflowsComponent
            },
            {
                path: '5',
                component: dependent_fileds_block_component_1.DependentFiledsBlockComponent
            },
            {
                path: '6',
                component: fields_unique_index_component_1.FieldsUniqueIndexComponent
            }
        ]
    }
];
var ModuleMngRoutingModule = (function () {
    function ModuleMngRoutingModule() {
    }
    ModuleMngRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule],
            providers: []
        })
    ], ModuleMngRoutingModule);
    return ModuleMngRoutingModule;
})();
exports.ModuleMngRoutingModule = ModuleMngRoutingModule;
//# sourceMappingURL=module-mng-routing.module.js.map