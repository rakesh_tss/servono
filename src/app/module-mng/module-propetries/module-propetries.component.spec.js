var testing_1 = require('@angular/core/testing');
var module_propetries_component_1 = require('./module-propetries.component');
describe('ModulePropetriesComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [module_propetries_component_1.ModulePropetriesComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(module_propetries_component_1.ModulePropetriesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=module-propetries.component.spec.js.map