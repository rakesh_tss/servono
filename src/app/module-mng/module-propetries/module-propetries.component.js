var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var pluralize = require('pluralize');
var ModulePropetriesComponent = (function () {
    function ModulePropetriesComponent(_fb, createModule, activatedRoute, router, modalService, httpService) {
        this._fb = _fb;
        this.createModule = createModule;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.modalService = modalService;
        this.httpService = httpService;
        this.moduleId = null;
        this.module_icon = null;
        this.moduleData = {};
        this.submittedForm = false;
        this.dataRows = [];
        this.totalRecords = 0;
        this.refreshTable = false;
        this.itemSubmittedForm = false;
    }
    ModulePropetriesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createModule.activeIndex = 0;
        this.activatedRoute.parent.params.subscribe(function (params) {
            if (params['moduleId'] && params['moduleId'] != 0) {
                _this.moduleId = params['moduleId'];
            }
        });
        this.moduleForm = this._fb.group({
            'ModuleLabel': new forms_1.FormControl('', [forms_1.Validators.required, forms_1.Validators.pattern('^[a-zA-Z0-9 ]+$')]),
            'SingularModuleLabel': new forms_1.FormControl('', [forms_1.Validators.required, forms_1.Validators.pattern('^[a-zA-Z0-9 ]+$')]),
            'module_icon': new forms_1.FormControl('fa-connectdevelop', forms_1.Validators.required),
            'show_quick_form': new forms_1.FormControl(''),
            'show_full_form': new forms_1.FormControl(''),
            'can_import': new forms_1.FormControl(''),
            'can_export': new forms_1.FormControl(''),
            'is_user': new forms_1.FormControl(false),
            'conditions': new forms_1.FormControl(),
            'stage_applicable': new forms_1.FormControl(),
            'status_applicable': new forms_1.FormControl(),
            'StatusPickListName': new forms_1.FormControl(),
            'StagePickListName': new forms_1.FormControl()
        });
        if (this.moduleId) {
            this.getModuleData();
        }
    };
    ModulePropetriesComponent.prototype.getModuleData = function () {
        var _this = this;
        this.httpService.get('GetModuleByName/' + this.moduleId).subscribe(function (res) {
            if (res) {
                _this.moduleData = res.data.module;
                _this.moduleForm.addControl('ModuleId', new forms_1.FormControl(_this.moduleData.pk_id));
                _this.moduleForm.patchValue({
                    'ModuleLabel': _this.moduleData.module_label,
                    'SingularModuleLabel': _this.moduleData.module_label_singular,
                    'module_icon': _this.moduleData.module_icon,
                    'is_user': _this.moduleData.is_user,
                    'show_quick_form': _this.moduleData.show_quick_form,
                    'show_full_form': _this.moduleData.show_full_form,
                    'can_import': _this.moduleData.can_import,
                    'can_export': _this.moduleData.can_export,
                    'stage_applicable': _this.moduleData.stage_applicable,
                    'status_applicable': _this.moduleData.status_applicable,
                    'StatusPickListName': _this.moduleData.StatusPickListName,
                    'StagePickListName': _this.moduleData.StagePickListName,
                    'conditions': _this.moduleData.conditions
                });
                _this.tableReload();
            }
        });
    };
    ModulePropetriesComponent.prototype.moduleFormSave = function (formValue, valid) {
        var _this = this;
        this.submittedForm = true;
        if (valid) {
            this.httpService.post('SaveUpdateModule', formValue).subscribe(function (res) {
                if (res) {
                    _this.router.navigate(['Custom/Modules/Create/' + formValue.ModuleLabel + '/1']);
                    _this.submittedForm = false;
                }
            });
        }
    };
    ModulePropetriesComponent.prototype.onBlurModuleLabel = function () {
        var moduleLabel = this.moduleForm.value.ModuleLabel;
        /*    let singularLabel=this.moduleForm.value.ModuleLabel;
            console.log('moduleLabel.substr(moduleLabel.length-2)',pluralize.singular('tests'));
            if((moduleLabel.charAt(moduleLabel.length-2) !='s' && moduleLabel.charAt(moduleLabel.length-2) !='S' )&&(moduleLabel.charAt(moduleLabel.length-1)=='s' || moduleLabel.charAt(moduleLabel.length-1)=='S')){
               singularLabel = moduleLabel.substr(0, moduleLabel.length - 1);
            }*/
        if (this.moduleForm.value.SingularModuleLabel == '' || this.moduleForm.value.SingularModuleLabel == null) {
            this.moduleForm.patchValue({ 'SingularModuleLabel': pluralize.singular(moduleLabel) });
        }
    };
    ModulePropetriesComponent.prototype.loadDataLazy = function (event) {
        var _this = this;
        event['app_module_id'] = this.moduleData.pk_id;
        this.httpService.postTable('GetModuleNumberingBySearch', event).subscribe(function (res) {
            if (res) {
                _this.dataRows = res.jqGridData.rows;
                _this.totalRecords = res.jqGridData.records;
            }
        });
    };
    ModulePropetriesComponent.prototype.tableReload = function () {
        var _this = this;
        this.refreshTable = false;
        setTimeout(function () { return _this.refreshTable = true; }, 0);
    };
    ModulePropetriesComponent.prototype.openItemModal = function (content) {
        this.itemSubmittedForm = false;
        this.itemFG = new forms_1.FormGroup({
            'app_module_id': new forms_1.FormControl(this.moduleData.pk_id),
            'prefix': new forms_1.FormControl(null, forms_1.Validators.required),
            'seq': new forms_1.FormControl(null, forms_1.Validators.required)
        });
        this.itemModal = this.modalService.open(content);
    };
    ModulePropetriesComponent.prototype.itemModalFormSubmit = function (formValue, valid) {
        var _this = this;
        this.itemSubmittedForm = true;
        if (valid) {
            this.httpService.post('SaveModuleNumbering', formValue).subscribe(function (res) {
                if (res) {
                    _this.itemModal.dismiss();
                    _this.itemSubmittedForm = false;
                    _this.tableReload();
                }
            });
        }
    };
    ModulePropetriesComponent = __decorate([
        core_1.Component({
            selector: 'app-module-propetries',
            templateUrl: './module-propetries.component.html',
            styleUrls: ['./module-propetries.component.css']
        })
    ], ModulePropetriesComponent);
    return ModulePropetriesComponent;
})();
exports.ModulePropetriesComponent = ModulePropetriesComponent;
//# sourceMappingURL=module-propetries.component.js.map