import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModulePropetriesComponent } from './module-propetries.component';

describe('ModulePropetriesComponent', () => {
  let component: ModulePropetriesComponent;
  let fixture: ComponentFixture<ModulePropetriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModulePropetriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModulePropetriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
