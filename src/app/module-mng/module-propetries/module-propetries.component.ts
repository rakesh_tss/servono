import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {HttpService} from "../../_services/http.service";
import { FormGroup, FormControl, FormBuilder, Validators,NgForm,NgModel} from '@angular/forms';
import {CreateModuleComponent} from "../create-module/create-module.component";
import {LazyLoadEvent,SpinnerModule} from "primeng/primeng";
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import *as pluralize from 'pluralize';

@Component({
  selector: 'app-module-propetries',
  templateUrl: './module-propetries.component.html',
  styleUrls: ['./module-propetries.component.css']
})
export class ModulePropetriesComponent implements OnInit {

  constructor(private _fb:FormBuilder,
              private createModule:CreateModuleComponent,
              private activatedRoute:ActivatedRoute,
              private router:Router,
              private modalService:NgbModal,
              private httpService:HttpService) { }

  moduleForm:FormGroup;
  moduleId:string = null;
  module_icon:string=null;
  ngOnInit() {
    this.createModule.activeIndex = 0;
    this.activatedRoute.parent.params.subscribe((params:Params) => {
      if( params['moduleId'] && params['moduleId'] != 0   ){
        this.moduleId = params['moduleId'];
      }
    });
    this.moduleForm = this._fb.group({
      'ModuleLabel': new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$')]),
      'SingularModuleLabel': new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$')]),
      'module_icon': new FormControl('fa-connectdevelop', Validators.required),
      'show_quick_form': new FormControl(''),
      'show_full_form': new FormControl(''),
      'can_import': new FormControl(''),
      'can_export': new FormControl(''),
      'is_user': new FormControl(false),
      'conditions': new FormControl(),
      'stage_applicable': new FormControl(),
      'status_applicable': new FormControl(),
      'StatusPickListName': new FormControl(),
      'StagePickListName': new FormControl()
    });
    if(this.moduleId){
      this.getModuleData();
    }

  }
  private moduleData:any = {};
  getModuleData() {
    this.httpService.get('GetModuleByName/' + this.moduleId).subscribe(res => {
      if (res) {
        this.moduleData = res.data.module;
        this.moduleForm.addControl('ModuleId', new FormControl(this.moduleData.pk_id));
        this.moduleForm.patchValue({
          'ModuleLabel': this.moduleData.module_label,
          'SingularModuleLabel': this.moduleData.module_label_singular,
          'module_icon': this.moduleData.module_icon,
          'is_user': this.moduleData.is_user,
          'show_quick_form': this.moduleData.show_quick_form,
          'show_full_form': this.moduleData.show_full_form,
          'can_import': this.moduleData.can_import,
          'can_export': this.moduleData.can_export,
          'stage_applicable': this.moduleData.stage_applicable,
          'status_applicable': this.moduleData.status_applicable,
          'StatusPickListName': this.moduleData.StatusPickListName,
          'StagePickListName': this.moduleData.StagePickListName,
          'conditions': this.moduleData.conditions
        });
        this.tableReload();
      }

    })
  }
  submittedForm:boolean = false;
  moduleFormSave(formValue, valid) {
    this.submittedForm = true;
    if (valid) {
      this.httpService.post('SaveUpdateModule', formValue).subscribe(
          res => {
            if (res) {
              this.router.navigate(['Custom/Modules/Create/' + formValue.ModuleLabel + '/1']);
              this.submittedForm = false;
            }

          })
    }
  }
  onBlurModuleLabel(){
    let moduleLabel = this.moduleForm.value.ModuleLabel;
/*    let singularLabel=this.moduleForm.value.ModuleLabel;
    console.log('moduleLabel.substr(moduleLabel.length-2)',pluralize.singular('tests'));
    if((moduleLabel.charAt(moduleLabel.length-2) !='s' && moduleLabel.charAt(moduleLabel.length-2) !='S' )&&(moduleLabel.charAt(moduleLabel.length-1)=='s' || moduleLabel.charAt(moduleLabel.length-1)=='S')){
       singularLabel = moduleLabel.substr(0, moduleLabel.length - 1);
    }*/
    if(this.moduleForm.value.SingularModuleLabel == '' || this.moduleForm.value.SingularModuleLabel==null){
      this.moduleForm.patchValue({'SingularModuleLabel': pluralize.singular(moduleLabel)});
    }

  }

  dataRows:any=[];
  totalRecords:number=0;
  refreshTable:boolean=false;

  loadDataLazy(event: LazyLoadEvent) {
    event['app_module_id'] = this.moduleData.pk_id;
    this.httpService.postTable('GetModuleNumberingBySearch',event).subscribe(
        res=>{
          if(res){
            this.dataRows = res.jqGridData.rows;
            this.totalRecords = res.jqGridData.records;
          }
        }
    );
  }

  tableReload(){
    this.refreshTable = false;
    setTimeout(() => this.refreshTable = true, 0);
  }

  itemFG:FormGroup;
  itemModal:NgbModalRef;
  itemSubmittedForm:boolean = false;

  openItemModal(content)
  {
    this.itemSubmittedForm = false;
    this.itemFG = new FormGroup({
      'app_module_id': new FormControl(this.moduleData.pk_id),
      'prefix': new FormControl(null,Validators.required),
      'seq': new FormControl(null,Validators.required),
    });
    this.itemModal = this.modalService.open(content);
  }

  itemModalFormSubmit(formValue, valid) {
    this.itemSubmittedForm = true;
    if (valid) {
      this.httpService.post('SaveModuleNumbering', formValue).subscribe(
          res => {
            if (res) {
              this.itemModal.dismiss();
              this.itemSubmittedForm = false;
              this.tableReload();
            }
          })
    }
  }
}
