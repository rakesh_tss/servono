import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {ConfirmationService} from "primeng/primeng";
import {BreadcrumbService} from "../_services/breadcrumb.service";
import {HttpService} from "../_services/http.service";

@Component({
    selector: 'app-module-mng',
    templateUrl: './module-mng.component.html',
    styleUrls: ['./module-mng.component.scss']
})
export class ModuleMngComponent implements OnInit {

    constructor(private breadcrumbService:BreadcrumbService ,private httpService:HttpService, private activatedRoute:ActivatedRoute, private router:Router, private confirmationService:ConfirmationService) {
    }
    moduleSearchKey:any={};
    ngOnInit() {
        this.getModuleList();
        this.breadcrumbService.setBreadcrumb([{label:'Module Management'}]);
    }

    checked:boolean;
    public moduleList:any = [];

    private getModuleList() {
        this.httpService.get('GetModules').subscribe(
            res => {
                if (res) {
                    this.moduleList = res.data;
                }
            });
    }

    redirect(pagename:string) {
        this.router.navigate([pagename]);
    }

    moduleDelete(m) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: () => {
                let param:any = {};
                param.module = m.module_label;
                this.httpService.post('deleteModule', param).subscribe(
                    res => {
                        if(res){
                            this.getModuleList();
                        }
                    })
            }
        });
    }
}
