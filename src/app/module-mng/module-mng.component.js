var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var ModuleMngComponent = (function () {
    function ModuleMngComponent(breadcrumbService, httpService, activatedRoute, router, confirmationService) {
        this.breadcrumbService = breadcrumbService;
        this.httpService = httpService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.confirmationService = confirmationService;
        this.moduleSearchKey = {};
        this.moduleList = [];
    }
    ModuleMngComponent.prototype.ngOnInit = function () {
        this.getModuleList();
        this.breadcrumbService.setBreadcrumb([{ label: 'Module Management' }]);
    };
    ModuleMngComponent.prototype.getModuleList = function () {
        var _this = this;
        this.httpService.get('GetModules').subscribe(function (res) {
            if (res) {
                _this.moduleList = res.data;
            }
        });
    };
    ModuleMngComponent.prototype.redirect = function (pagename) {
        this.router.navigate([pagename]);
    };
    ModuleMngComponent.prototype.moduleDelete = function (m) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: function () {
                var param = {};
                param.module = m.module_label;
                _this.httpService.post('deleteModule', param).subscribe(function (res) {
                    if (res) {
                        _this.getModuleList();
                    }
                });
            }
        });
    };
    ModuleMngComponent = __decorate([
        core_1.Component({
            selector: 'app-module-mng',
            templateUrl: './module-mng.component.html',
            styleUrls: ['./module-mng.component.scss']
        })
    ], ModuleMngComponent);
    return ModuleMngComponent;
})();
exports.ModuleMngComponent = ModuleMngComponent;
//# sourceMappingURL=module-mng.component.js.map