import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ModuleMngComponent} from "./module-mng.component";
import {CreateModuleComponent} from "./create-module/create-module.component";
import {BlockAndFieldsComponent} from "./block-and-fields/block-and-fields.component";
import {RelationshipsComponent} from "./relationships/relationships.component";
import {ModulePropetriesComponent} from "./module-propetries/module-propetries.component";
import {WorkflowsComponent} from "./workflows/workflows.component";
import {GridManagementComponent} from "./grid-management/grid-management.component";
import {DependentFiledsBlockComponent} from "./dependent-fileds-block/dependent-fileds-block.component";
import {FieldsUniqueIndexComponent} from "./fields-unique-index/fields-unique-index.component";
import {FormBuilderComponent} from "./form-builder/form-builder.component";

const routes: Routes = [
  {
    path: '',
    component: ModuleMngComponent,
    data: {
      title: 'Module Management'
    }
  },
  {
    path: ':Create/:moduleId',
    component: CreateModuleComponent,
    data: {
      title: 'Module Create/Update'
    },
    children:[
      {
        path: '0',
        component: ModulePropetriesComponent,
      },
      {
        path: '1',
        component: FormBuilderComponent,
      },
      {
        path: '2',
        component: GridManagementComponent,
      },
      {
        path: '3',
        component: RelationshipsComponent,
      },
      {
        path: '4',
        component: WorkflowsComponent,
      },
      {
        path : '5',
        component: DependentFiledsBlockComponent
      },
      {
        path : '6',
        component:FieldsUniqueIndexComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class ModuleMngRoutingModule { }
