import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockAndFieldsComponent } from './block-and-fields.component';

describe('BlockAndFieldsComponent', () => {
  let component: BlockAndFieldsComponent;
  let fixture: ComponentFixture<BlockAndFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockAndFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockAndFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
