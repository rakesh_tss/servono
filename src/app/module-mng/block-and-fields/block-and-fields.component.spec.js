var testing_1 = require('@angular/core/testing');
var block_and_fields_component_1 = require('./block-and-fields.component');
describe('BlockAndFieldsComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [block_and_fields_component_1.BlockAndFieldsComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(block_and_fields_component_1.BlockAndFieldsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=block-and-fields.component.spec.js.map