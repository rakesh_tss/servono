var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var BlockAndFieldsComponent = (function () {
    function BlockAndFieldsComponent(activatedRoute, modalService, router, _fb, httpService, confirmationService, createModule) {
        this.activatedRoute = activatedRoute;
        this.modalService = modalService;
        this.router = router;
        this._fb = _fb;
        this.httpService = httpService;
        this.confirmationService = confirmationService;
        this.createModule = createModule;
        this.moduleId = '';
        this.moduleData = {};
        this.moduleFields = [];
        //addField Modal
        this.fieldTypeList = [
            { label: 'Select Type', value: null },
        ];
        this.afmSubmittedForm = false;
        this.field = {
            PickListValues: []
        };
        //start block creation
        this.bmSubmittedForm = false;
        this.prePickListValues = [];
        this.defaultPicklist = [];
    }
    BlockAndFieldsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createModule.activeIndex = 1;
        this.activatedRoute.parent.params.subscribe(function (params) {
            _this.moduleId = params['moduleId'];
            _this.getModuleBlogData();
        });
    };
    BlockAndFieldsComponent.prototype.getModuleBlogData = function () {
        var _this = this;
        this.httpService.get('GetModuleByName/' + this.moduleId).subscribe(function (res) {
            if (res) {
                _this.moduleData = res.data.module;
                //set module fields
                _this.moduleFields = res.data.blocks;
                _this.ModuleDbTableName = _this.moduleData.db_table_name;
            }
        });
    };
    BlockAndFieldsComponent.prototype.openAddFieldModal = function (content, blockDetails) {
        var _this = this;
        this.defaultPicklist = [];
        this.defaultPicklist.push({ label: 'Choose', value: null });
        this.prePickListValues = [];
        this.field = {};
        this.field.AppModuleId = blockDetails.app_module_id;
        this.field.AppModuleBlockId = blockDetails.pk_id;
        this.field.DbTableName = this.ModuleDbTableName;
        /* this.addFieldModalForm = this._fb.group({
         'AppModuleId': new FormControl(blockDetails.app_module_id),
         'AppModuleBlockId': new FormControl(blockDetails.pk_id),
         'DbTableName': new FormControl(this.ModuleDbTableName),
         'ModuleFieldId': new FormControl(''),
         'FieldType': new FormControl('', Validators.required),
         'Name': new FormControl('', Validators.required),
         'Length': new FormControl(''),
         'Default': new FormControl(''),
         'Precision': new FormControl(''),
         'IsMandatory': new FormControl('false'),
         'QuickCreate': new FormControl(''),
         'MassEdit': new FormControl(''),
         'HeaderView': new FormControl(''),
         'IsKeyField': new FormControl(''),
         'PickListValues': new FormControl('')
         });*/
        this.httpService.get('GetFieldTypes').subscribe(function (res) {
            _this.fieldTypeList = [
                { label: 'Select Type', value: null },
            ];
            var fieldTypeListData = res.data;
            for (var _i = 0; _i < fieldTypeListData.length; _i++) {
                var ty = fieldTypeListData[_i];
                if (ty.is_available == 0) {
                    var fObj = {};
                    fObj.label = ty.name;
                    fObj.value = { 'id': ty.pk_id, 'name': ty.name };
                    _this.fieldTypeList.push(fObj);
                }
            }
        });
        this.mr = this.modalService.open(content);
    };
    BlockAndFieldsComponent.prototype.addFieldFormSubmit = function (data) {
        var _this = this;
        this.afmSubmittedForm = true;
        if (this.field) {
            var fType = this.field.selectedFieldType;
            var params = this.field;
            params.FieldType = fType.id;
            params.FieldTypeName = fType.name;
            /*let fieldOrgValue = addFieldModalForm.value.FieldType;
            addFieldModalForm.value.FieldType = fieldOrgValue.id;
            addFieldModalForm.value.FieldTypeName = fieldOrgValue.name;*/
            this.httpService.post('SaveUpdateModuleField', params).subscribe(function (res) {
                if (res) {
                    _this.mr.close();
                    _this.getModuleBlogData();
                    _this.afmSubmittedForm = false;
                }
            });
        }
    };
    BlockAndFieldsComponent.prototype.multiplePickList = function (data, defaul) {
        data = ['a', 'b', 'c'];
        var mList = [];
        if (defaul) {
            mList.push({ label: 'Choose', value: null });
        }
        if (data) {
            for (var _i = 0; _i < data.length; _i++) {
                var d = data[_i];
                var fObj = {};
                fObj.label = d;
                fObj.value = d;
                mList.push(fObj);
            }
        }
        if (this.prePickListValues) {
            for (var _a = 0, _b = this.prePickListValues; _a < _b.length; _a++) {
                var p = _b[_a];
                var fObj = {};
                fObj.label = p;
                fObj.value = p;
                mList.push(fObj);
            }
        }
        return mList;
    };
    BlockAndFieldsComponent.prototype.checkDropDownFieldType = function (data) {
        var ft = this.field.selectedFieldType;
        if (ft && data) {
            if (data.indexOf(ft.name) != -1)
                return true;
        }
        return false;
    };
    BlockAndFieldsComponent.prototype.changeFieldStatus = function (field, key) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: function () {
                var keyValue = true;
                if (field[key]) {
                    keyValue = false;
                }
                var param = {};
                param.key = key;
                param.value = keyValue;
                param.pk_id = field.pk_id;
                param.user_meta_field = field.user_meta_field;
                param.module_id = _this.moduleData.pk_id;
                _this.httpService.post('UpdateModuleFieldQuick', param).subscribe(function (res) {
                    if (res) {
                        field[key] = keyValue;
                        if (key == 'record_identifier') {
                            _this.getModuleBlogData();
                        }
                    }
                });
            }
        });
    };
    BlockAndFieldsComponent.prototype.openBlockModal = function (content) {
        this.bmSubmittedForm = false;
        this.blockMF = new forms_1.FormGroup({
            'Name': new forms_1.FormControl(null, forms_1.Validators.required)
        });
        this.bm = this.modalService.open(content);
    };
    BlockAndFieldsComponent.prototype.blockModalFormSubmit = function (formValue, valid) {
        var _this = this;
        this.bmSubmittedForm = true;
        if (valid) {
            formValue.ModuleId = this.moduleData.pk_id;
            formValue.ModuleBlockId = 0;
            this.httpService.post('SaveUpdateModuleBlock', formValue).subscribe(function (res) {
                if (res) {
                    _this.bm.dismiss();
                    _this.bmSubmittedForm = false;
                    _this.getModuleBlogData();
                }
            });
        }
    };
    BlockAndFieldsComponent.prototype.managePickList = function () {
        this.defaultPicklist = [];
        this.defaultPicklist.push({ label: 'Choose', value: null });
        if (this.prePickListValues) {
            for (var _i = 0, _a = this.prePickListValues; _i < _a.length; _i++) {
                var d = _a[_i];
                this.defaultPicklist.push({ label: d, value: d });
            }
        }
        if (this.field.PickListValues) {
            for (var _b = 0, _c = this.field.PickListValues; _b < _c.length; _b++) {
                var d = _c[_b];
                this.defaultPicklist.push({ label: d, value: d });
            }
        }
    };
    BlockAndFieldsComponent.prototype.onBlurName = function (value) {
        var _this = this;
        if (value) {
            this.httpService.get('GetPickListValuesByPickListName/' + value).subscribe(function (res) {
                if (res) {
                    _this.prePickListValues = [];
                    if (res.data) {
                        _this.prePickListValues = [];
                        res.data.forEach(function (data) {
                            if (data.value) {
                                _this.prePickListValues.push(data.value);
                                _this.managePickList();
                            }
                        });
                        _this.managePickList();
                    }
                }
            });
        }
        else {
            this.prePickListValues = [];
            this.managePickList();
        }
    };
    BlockAndFieldsComponent.prototype.pickListSelected = function (field, event) {
        field.PickListName = event.name;
    };
    BlockAndFieldsComponent.prototype.filterPickList = function (event) {
        var _this = this;
        var query = event.query;
        this.httpService.get('getPickListAutocompleter?key=' + query).subscribe(function (res) {
            if (res) {
                _this.filteredPickList = res.data;
            }
        });
    };
    BlockAndFieldsComponent.prototype.reOrder = function () {
        this.moduleFields.forEach(function (block) {
            block.fields.forEach(function (field, key) {
                field.seq_order = key + 1;
            });
        });
        var params = {};
        params.data = this.moduleFields;
        this.httpService.post('UpdateModuleFieldsOrder', params).subscribe(function (res) {
            if (res) {
            }
        });
    };
    BlockAndFieldsComponent.prototype.onBlurDisplayLabel = function (value) {
        if (!this.field.Name)
            this.field.Name = value.replace(/[&\/\\#,@^+()$~%.'":*?<>{}]/g, '_');
        ;
    };
    BlockAndFieldsComponent.prototype.deleteModuleField = function (f) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.httpService.post('deleteModuleField', f).subscribe(function (res) {
                    if (res) {
                        _this.getModuleBlogData();
                    }
                });
            }
        });
    };
    BlockAndFieldsComponent = __decorate([
        core_1.Component({
            selector: 'module-block-and-fields',
            templateUrl: './block-and-fields.component.html',
            styleUrls: ['./block-and-fields.component.scss']
        })
    ], BlockAndFieldsComponent);
    return BlockAndFieldsComponent;
})();
exports.BlockAndFieldsComponent = BlockAndFieldsComponent;
//# sourceMappingURL=block-and-fields.component.js.map