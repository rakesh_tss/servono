import { Component, OnInit } from '@angular/core';
import {CreateModuleComponent} from "../create-module/create-module.component";
import { FormGroup, FormControl, FormBuilder, Validators,NgForm,NgModel} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {HttpService} from "../../_services/http.service";
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ConfirmDialogModule,ConfirmationService,SelectItem} from 'primeng/primeng';

@Component({
    selector: 'module-block-and-fields',
    templateUrl: './block-and-fields.component.html',
    styleUrls: ['./block-and-fields.component.scss']
})
export class BlockAndFieldsComponent implements OnInit {

    constructor(private activatedRoute:ActivatedRoute,
                private modalService:NgbModal,
                private router:Router,
                private _fb:FormBuilder,
                private httpService:HttpService,
                private confirmationService:ConfirmationService,
                private createModule:CreateModuleComponent) {
    }

    moduleId:string = '';
    ModuleDbTableName:string;

    ngOnInit() {
        this.createModule.activeIndex = 1;
        this.activatedRoute.parent.params.subscribe((params:Params) => {
            this.moduleId = params['moduleId'];
            this.getModuleBlogData();
        });
    }

    private moduleData:any = {};
    moduleFields:any = [];

    getModuleBlogData() {
        this.httpService.get('GetModuleByName/' + this.moduleId).subscribe(res => {
            if (res) {
                this.moduleData = res.data.module;
                //set module fields
                this.moduleFields = res.data.blocks;
                this.ModuleDbTableName = this.moduleData.db_table_name;
            }
        })
    }

    //addField Modal
    public fieldTypeList:SelectItem[] = [
        {label: 'Select Type', value: null},
    ];
    //addFieldModalForm:FormGroup;
    public mr:NgbModalRef;
    afmSubmittedForm:boolean = false;
    field:any = {
        PickListValues: []
    };

    openAddFieldModal(content, blockDetails) {
        this.defaultPicklist = [];
        this.defaultPicklist.push({label: 'Choose', value: null});
        this.prePickListValues = [];
        this.field = {};
        this.field.AppModuleId = blockDetails.app_module_id;
        this.field.AppModuleBlockId = blockDetails.pk_id;
        this.field.DbTableName = this.ModuleDbTableName;
        /* this.addFieldModalForm = this._fb.group({
         'AppModuleId': new FormControl(blockDetails.app_module_id),
         'AppModuleBlockId': new FormControl(blockDetails.pk_id),
         'DbTableName': new FormControl(this.ModuleDbTableName),
         'ModuleFieldId': new FormControl(''),
         'FieldType': new FormControl('', Validators.required),
         'Name': new FormControl('', Validators.required),
         'Length': new FormControl(''),
         'Default': new FormControl(''),
         'Precision': new FormControl(''),
         'IsMandatory': new FormControl('false'),
         'QuickCreate': new FormControl(''),
         'MassEdit': new FormControl(''),
         'HeaderView': new FormControl(''),
         'IsKeyField': new FormControl(''),
         'PickListValues': new FormControl('')
         });*/
        this.httpService.get('GetFieldTypes').subscribe(
            res=> {
                this.fieldTypeList = [
                    {label: 'Select Type', value: null},
                ];
                let fieldTypeListData = res.data;
                for (let ty of fieldTypeListData) {
                    if (ty.is_available == 0) {
                        let fObj:any = {};
                        fObj.label = ty.name;
                        fObj.value = {'id': ty.pk_id, 'name': ty.name};
                        this.fieldTypeList.push(fObj);
                    }
                }
            }
        );

        this.mr = this.modalService.open(content);
    }

    addFieldFormSubmit(data) {
        this.afmSubmittedForm = true;
        if (this.field) {
            let fType:any = this.field.selectedFieldType;
            let params = this.field;
            params.FieldType = fType.id;
            params.FieldTypeName = fType.name;
             /*let fieldOrgValue = addFieldModalForm.value.FieldType;
             addFieldModalForm.value.FieldType = fieldOrgValue.id;
             addFieldModalForm.value.FieldTypeName = fieldOrgValue.name;*/
            this.httpService.post('SaveUpdateModuleField', params).subscribe(res=> {
                if (res) {
                    this.mr.close();
                    this.getModuleBlogData();
                    this.afmSubmittedForm = false;
                }

            })
        }
    }

    multiplePickList(data, defaul) {

        data = ['a','b','c'];
        let mList:any = [];
        if (defaul) {
            mList.push({label: 'Choose', value: null});
        }
        if (data) {
            for (let d of data) {
                let fObj:any = {};
                fObj.label = d;
                fObj.value = d;
                mList.push(fObj);
            }
        }
        if(this.prePickListValues)
        {
            for (let p of this.prePickListValues) {
                let fObj:any = {};
                fObj.label = p;
                fObj.value = p;
                mList.push(fObj);
            }
        }
        return mList;
    }

    checkDropDownFieldType(data:any) {
        let ft = this.field.selectedFieldType;
        if (ft && data) {
            if (data.indexOf(ft.name) != -1)
                return true;
        }
        return false;
    }

    changeFieldStatus(field, key) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: () => {
                let keyValue:boolean = true;
                if (field[key]) {
                    keyValue = false;
                }
                let param:any = {};
                param.key = key;
                param.value = keyValue;
                param.pk_id = field.pk_id;
                param.user_meta_field = field.user_meta_field;
                param.module_id = this.moduleData.pk_id;
                this.httpService.post('UpdateModuleFieldQuick', param).subscribe(
                    res => {
                        if (res) {
                            field[key] = keyValue;
                            if(key=='record_identifier'){
                                this.getModuleBlogData();
                            }
                        }
                    })
            }
        });

    }

    //start block creation
    bmSubmittedForm:boolean = false;
    blockMF:FormGroup;
    bm:NgbModalRef;

    openBlockModal(content) {
        this.bmSubmittedForm = false;
        this.blockMF = new FormGroup({
            'Name': new FormControl(null, Validators.required),
        });

        this.bm = this.modalService.open(content);
    }

    blockModalFormSubmit(formValue, valid) {
        this.bmSubmittedForm = true;
        if (valid) {
            formValue.ModuleId = this.moduleData.pk_id;
            formValue.ModuleBlockId = 0;
            this.httpService.post('SaveUpdateModuleBlock', formValue).subscribe(
                res => {
                    if (res) {
                        this.bm.dismiss();
                        this.bmSubmittedForm = false;
                        this.getModuleBlogData();
                    }
                })
        }
    }

    prePickListValues:any = [];
    defaultPicklist:any = [];

    managePickList()
    {
        this.defaultPicklist = [];
        this.defaultPicklist.push({label: 'Choose', value: null});
        if (this.prePickListValues) {
            for (let d of this.prePickListValues) {
                this.defaultPicklist.push({label:d,value:d});
            }
        }
        if(this.field.PickListValues)
        {
            for(let d of this.field.PickListValues)
            {
                this.defaultPicklist.push({label:d,value:d});
            }
        }
    }

    onBlurName(value) {
        if (value) {
            this.httpService.get('GetPickListValuesByPickListName/' + value).subscribe(res=> {
                if (res) {
                    this.prePickListValues = [];
                    if(res.data)
                    {
                        this.prePickListValues = [];
                        res.data.forEach((data)=> {
                            if (data.value) {
                                this.prePickListValues.push(data.value);
                                this.managePickList();
                            }
                        });
                        this.managePickList();
                    }
                }
            })
        }else{
            this.prePickListValues = [];
            this.managePickList();
        }


    }

    pickListSelected(field, event) {
        field.PickListName = event.name;
    }


    filteredPickList:any[];

    filterPickList(event) {
        let query = event.query;

        this.httpService.get('getPickListAutocompleter?key=' + query).subscribe(res=> {
            if (res) {
                this.filteredPickList = res.data;
            }
        })
    }

    reOrder() {
        this.moduleFields.forEach((block)=> {
            block.fields.forEach((field, key)=> {
                field.seq_order = key + 1;
            });

        })
        let params:any = {};
        params.data = this.moduleFields;
        this.httpService.post('UpdateModuleFieldsOrder', params).subscribe(res=> {
            if (res) {

            }
        })
    }
    onBlurDisplayLabel(value){
        if(!this.field.Name)
            this.field.Name = value.replace(/[&\/\\#,@^+()$~%.'":*?<>{}]/g,'_');;
    }

    deleteModuleField(f):void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
                this.httpService.post('deleteModuleField', f).subscribe(
                    res => {
                        if (res) {
                            this.getModuleBlogData();
                        }
                    })
            }
        });
    }

}
