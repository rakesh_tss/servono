import { Component, OnInit,Inject, HostListener,ViewChild,ElementRef } from '@angular/core';
import { DOCUMENT } from "@angular/platform-browser";
import {CreateModuleComponent} from "../create-module/create-module.component";
import { FormGroup, FormControl, FormBuilder, Validators,NgForm,NgModel} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {HttpService} from "../../_services/http.service";
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ConfirmDialogModule,ConfirmationService,SelectItem} from 'primeng/primeng';
import {CommonService} from "../../_services/common.service";
import {Ng2FilterPipe} from "ng2-filter-pipe/dist/index";
import {DynamicFormService} from "../../tss/_services/dynamic-form.service";
import { DomSanitizer, SafeResourceUrl, SafeUrl,} from '@angular/platform-browser';
import {APP_BASE_HREF,PlatformLocation,Location, LocationStrategy, PathLocationStrategy } from '@angular/common';

@Component({
    selector: 'app-form-builder',
    templateUrl: './form-builder.component.html',
    styleUrls: ['./form-builder.component.scss']
})
export class FormBuilderComponent implements OnInit {

    baseUrl:string;
    constructor(private activatedRoute:ActivatedRoute,
                private modalService:NgbModal,
                private commonService:CommonService,
                private router:Router,
                private _fb:FormBuilder,
                private httpService:HttpService,
                private confirmationService:ConfirmationService,
                private createModule:CreateModuleComponent,
                private sanitizer:DomSanitizer,
                private platformLocation:PlatformLocation,
                private location:Location,
                @Inject(DOCUMENT) private doc:Document,
                private dynamicFormService:DynamicFormService) {
        //console.log(this.platformLocation);
        //console.log(this.doc.location.href);
      //  console.log(this.doc.location.origin);
        console.log(this.doc.location);
        this.baseUrl = this.httpService.baseUrl;
       // console.log(platformLocation.location.href);
       // console.log(platformLocation.location.origin);
    }

    /*public fixed: boolean = false;
    @HostListener("window:scroll", [])
    onWindowScroll() {
        let num = this.doc.body.scrollTop;
        console.log('num', num);
        if (num > 200) {
            this.fixed = true;
        } else if (this.fixed && num < 5) {
            this.fixed = false;
        }
    }*/
    @ViewChild('divLeft') divLeft: ElementRef;
    viewHeight: number;
    scrollTop:any;
    isSpecial:boolean = false;
    @HostListener("window:scroll", [])
    onWindowScroll() {
        let num = this.doc.body.scrollTop;
        this.scrollTop = this.doc.body.scrollTop;
        let leftHeight = this.divLeft.nativeElement.offsetHeight;
        let leftTop = this.divLeft.nativeElement.offsetTop;
        if(num > 200){
            this.isSpecial = true;
            /*console.log(this.scroll)*/
        }
        else {
            this.isSpecial = false;
        }
    }

    setStyles(){

        let styles= {
            position: this.isSpecial ? 'isSpecial':'relative',
            top : this.isSpecial ? (this.scrollTop-200)+'px' : '0px'
        }
        //console.log(styles);
        return styles;
    }

    showFieldsObjKey:string='showFields';
    moduleId:string = '';
    ModuleDbTableName:string;
    previewSize:number = 1300;
    sf:any = {more: false}; //selected filed
    previewUrl:SafeResourceUrl;

    ngOnInit() {
        this.createModule.activeIndex = 1;
        this.activatedRoute.parent.params.subscribe((params:Params) => {
            this.moduleId = params['moduleId'];
            this.getModuleBlogData();
            this.getFieldTypeList();
            let pUrl:string = this.baseUrl+"tss/"+ this.moduleId + "/0";
            console.log('pUrl',pUrl);
            this.previewUrl = this.sanitizer.bypassSecurityTrustResourceUrl(pUrl);
            //console.log('this.previewUrl', platformLocation.location.href);
        });
    }

    private moduleData:any = {};
    moduleFields:any = [];

    getModuleBlogData() {
        this.httpService.get('GetModuleByName/' + this.moduleId).subscribe(res => {
            if (res) {
                this.moduleData = res.data.module;
                //set module fields
                let formateData = this.dynamicFormService.setDynamicFormFieldFormat(res.data,'formBuilder');
                console.log('formateData', formateData);

                this.moduleFields = formateData.blocks;
                this.ModuleDbTableName = this.moduleData.db_table_name;
            }
        })
    }

    //addField Modal
    public fieldTypeList:SelectItem[] = [];
    //addFieldModalForm:FormGroup;
    public mr:NgbModalRef;
    afmSubmittedForm:boolean = false;
    field:any = {
        PickListValues: []
    };

    getFieldTypeList() {
        this.httpService.get('GetFieldTypes').subscribe(
            res=> {

                this.fieldTypeList = [];
                let fieldTypeListData = res.data;
                //let fieldTypeListData =  new Ng2FilterPipe().transform(res.data,{is_available:0});
                //this.fieldTypeList = this.commonService.getDropdownOptions(fieldTypeListData,'name','pk_id');
                for (let ty of fieldTypeListData) {
                    if (ty.is_available == 0) {
                        let fObj:any = {};
                        fObj.label = ty.name;
                        fObj.value = {'id': ty.pk_id, 'name': ty.name};
                        this.fieldTypeList.push(fObj);
                    }
                }
            }
        );
    }

    openAddFieldModal(content, blockDetails, event) {
        console.log('blockDetails',blockDetails);
        console.log('event',event);
        this.defaultPicklist = [];
        this.defaultPicklist.push({label: 'Choose', value: null});
        this.prePickListValues = [];
        this.field = {};

        this.field.AppModuleId = blockDetails.app_module_id;
        this.field.AppModuleBlockId = blockDetails.pk_id;
        this.field.DbTableName = this.ModuleDbTableName;
        this.field.selectedFieldType = event.dragData;
        /* this.addFieldModalForm = this._fb.group({
         'AppModuleId': new FormControl(blockDetails.app_module_id),
         'AppModuleBlockId': new FormControl(blockDetails.pk_id),
         'DbTableName': new FormControl(this.ModuleDbTableName),
         'ModuleFieldId': new FormControl(''),
         'FieldType': new FormControl('', Validators.required),
         'Name': new FormControl('', Validators.required),
         'Length': new FormControl(''),
         'Default': new FormControl(''),
         'Precision': new FormControl(''),
         'IsMandatory': new FormControl('false'),
         'QuickCreate': new FormControl(''),
         'MassEdit': new FormControl(''),
         'HeaderView': new FormControl(''),
         'IsKeyField': new FormControl(''),
         'PickListValues': new FormControl('')
         });*/

        this.mr = this.modalService.open(content);
    }

    addFieldFormSubmit(data) {
        this.afmSubmittedForm = true;
        if (this.field) {
            let fType:any = this.field.selectedFieldType;
            let params = this.field;
            params.FieldType = fType.id;
            params.FieldTypeName = fType.name;
            /*let fieldOrgValue = addFieldModalForm.value.FieldType;
             addFieldModalForm.value.FieldType = fieldOrgValue.id;
             addFieldModalForm.value.FieldTypeName = fieldOrgValue.name;*/
            this.httpService.post('SaveUpdateModuleField', params).subscribe(res=> {
                if (res) {
                    this.mr.close();
                    this.getModuleBlogData();
                    this.afmSubmittedForm = false;
                }

            })
        }
    }

    multiplePickList(data, defaul) {

        data = ['a', 'b', 'c'];
        let mList:any = [];
        if (defaul) {
            mList.push({label: 'Choose', value: null});
        }
        if (data) {
            for (let d of data) {
                let fObj:any = {};
                fObj.label = d;
                fObj.value = d;
                mList.push(fObj);
            }
        }
        if (this.prePickListValues) {
            for (let p of this.prePickListValues) {
                let fObj:any = {};
                fObj.label = p;
                fObj.value = p;
                mList.push(fObj);
            }
        }
        return mList;
    }

    checkDropDownFieldType(data:any) {
        let ft = this.field.selectedFieldType;
        if (ft && data) {
            if (data.indexOf(ft.name) != -1)
                return true;
        }
        return false;
    }
    conditionModal:NgbModalRef;

    changeFieldStatus(field, key,content) {
        if(key=='conditions'){

            this.conditionModal = this.modalService.open(content);
        }else{
            this.confirmationService.confirm({
                message: 'Are you sure that you want to perform this action?',
                accept: () => {
                    let keyValue:boolean = true;
                    if (field[key]) {
                        keyValue = false;
                    }
                    let param:any = {};
                    param.key = key;
                    param.value = keyValue;
                    param.pk_id = field.pk_id;
                    param.user_meta_field = field.user_meta_field;
                    param.module_id = this.moduleData.pk_id;
                    this.httpService.post('UpdateModuleFieldQuick', param).subscribe(
                        res => {
                            if (res) {
                                field[key] = keyValue;
                                if (key == 'record_identifier') {
                                    this.getModuleBlogData();
                                }
                            }
                        })
                }
            });
        }


    }
    conditionSave(){

        let param:any = {};
        param.key = 'conditions';
        param.value = this.sf['conditions'];
        param.pk_id = this.sf.pk_id;
        param.user_meta_field = this.sf.user_meta_field;
        param.module_id = this.moduleData.pk_id;
        this.httpService.post('UpdateModuleFieldQuick', param).subscribe(
            res => {
                if (res) {
                    this.conditionModal.close();
                }
            })
    }

    saveExtraOptions(field, key, data) {
        let param:any = {};
        param.key = key;
        param.value = JSON.stringify(data);
        param.pk_id = field.pk_id;
        param.user_meta_field = field.user_meta_field;
        param.module_id = this.moduleData.pk_id;
        this.httpService.post('UpdateModuleFieldQuick', param).subscribe(
            res => {
                if (res) {

                }
            })

    }

    //start block creation
    bmSubmittedForm:boolean = false;
    blockMF:FormGroup;
    bm:NgbModalRef;
    blockModalTitle:string;
    openBlockModal(content,data) {
        this.bmSubmittedForm = false;
        this.blockMF = new FormGroup({
            'Name': new FormControl(null, Validators.required),
            'ModuleBlockId': new FormControl(0),
            'ModuleId': new FormControl(null),
            'extra_options': new FormControl(null),
            'SeqOrder': new FormControl(null),
        });
        this.blockModalTitle = 'Create';
        if(data){
            console.log('data',data);
            this.blockModalTitle = 'Update';
            let blockObj:any = {};
            blockObj.Name = data.name;
            blockObj.ModuleBlockId = data.pk_id;
            blockObj.ModuleId = data.app_module_id;
            blockObj.extra_options = data.extra_options;
            blockObj.SeqOrder = data.seq_order;
            this.blockMF.patchValue(blockObj);
        }

        this.bm = this.modalService.open(content);
    }

    blockModalFormSubmit(formValue, valid) {
        //console.log('formValue',formValue);
        this.bmSubmittedForm = true;
        if (valid) {
            formValue.ModuleId = this.moduleData.pk_id;
            this.httpService.post('SaveUpdateModuleBlock', formValue).subscribe(
                res => {
                    if (res) {
                        this.bm.dismiss();
                        this.bmSubmittedForm = false;
                        this.getModuleBlogData();
                    }
                })
        }
    }

    prePickListValues:any = [];
    defaultPicklist:any = [];

    managePickList() {
        this.defaultPicklist = [];
        this.defaultPicklist.push({label: 'Choose', value: null});
        if (this.prePickListValues) {
            for (let d of this.prePickListValues) {
                this.defaultPicklist.push({label: d, value: d});
            }
        }
        if (this.field.PickListValues) {
            for (let d of this.field.PickListValues) {
                this.defaultPicklist.push({label: d, value: d});
            }
        }
    }

    onBlurName(value) {
        if (value) {
            this.httpService.get('GetPickListValuesByPickListName/' + value).subscribe(res=> {
                if (res) {
                    this.prePickListValues = [];
                    if (res.data) {
                        this.prePickListValues = [];
                        res.data.forEach((data)=> {
                            if (data.value) {
                                this.prePickListValues.push(data.value);
                                this.managePickList();
                            }
                        });
                        this.managePickList();
                    }
                }
            })
        } else {
            this.prePickListValues = [];
            this.managePickList();
        }


    }

    pickListSelected(field, event) {
        console.log('field',field);
        console.log('event',event);
        //field.PickListName = event.name;
        //pickListSelectedfield.PickListName = event.name;
    }


    filteredPickList:string[];

    filterPickList(event) {
        let query = event.query;

        this.httpService.get('getPickListAutocompleter?key=' + query).subscribe(res=> {
            if (res) {
                this.filteredPickList = [];
                res.data.forEach((data)=>{
                    this.filteredPickList.push(data.name);
                })
            }
        })
    }

    reOrder() {
        this.moduleFields.forEach((block)=> {
            block[this.showFieldsObjKey].forEach((field, key)=> {
                field.seq_order = key + 1;
            });
        })
        let params:any = {};
        params.data = this.moduleFields;
        this.httpService.post('UpdateModuleFieldsOrder', params).subscribe(res=> {
            if (res) {

            }
        })
    }

    onBlurDisplayLabel(value) {
        if (!this.field.Name)
            this.field.Name = value.replace(/[&\/\\#,@^+()$~%.'":*?<>{}]/g, '_');
        ;
    }

    deleteModuleField(f):void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
                this.httpService.post('deleteModuleField', f).subscribe(
                    res => {
                        if (res) {
                            this.getModuleBlogData();
                            this.sf = {};
                        }
                    })
            }
        });
    }

    deleteBlog(blog):void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
                this.httpService.post('removeModuleBlock/' + blog.pk_id).subscribe(
                    res => {
                        if (res) {
                            this.getModuleBlogData();
                        }
                    })
            }
        });
    }

    openPreviewModal(content) {
        this.modalService.open(content, {size: 'lg', windowClass: 'dynamic-form-preview'});
    }
    selectedTab:number = 0;
    onFieldSelect(field) {

        /*        if(!field.extra_options){
         field.extra_options = {};
         field.extra_options.lg=3;
         field.extra_options.md=6;
         field.extra_options.sm=12;
         }*/
        this.sf = field;
        this.selectedTab = 1;
    }

    displayFormFields(event) {
        this.sf = {};
        if (event.checked) {
            this.showFieldsObjKey = 'fields';
        } else {
            this.showFieldsObjKey = 'showFields';
        }

    }

    onScroll(event) {
        console.log('event', event);
    }

    /*onDrop(event){
     console.log('event',event);
     }*/
}
