var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('@angular/core');
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require('@angular/forms');
var FormBuilderComponent = (function () {
    function FormBuilderComponent(activatedRoute, modalService, commonService, router, _fb, httpService, confirmationService, createModule, sanitizer, platformLocation, location, doc, dynamicFormService) {
        this.activatedRoute = activatedRoute;
        this.modalService = modalService;
        this.commonService = commonService;
        this.router = router;
        this._fb = _fb;
        this.httpService = httpService;
        this.confirmationService = confirmationService;
        this.createModule = createModule;
        this.sanitizer = sanitizer;
        this.platformLocation = platformLocation;
        this.location = location;
        this.doc = doc;
        this.dynamicFormService = dynamicFormService;
        this.isSpecial = false;
        this.showFieldsObjKey = 'showFields';
        this.moduleId = '';
        this.previewSize = 1300;
        this.sf = { more: false }; //selected filed
        this.moduleData = {};
        this.moduleFields = [];
        //addField Modal
        this.fieldTypeList = [];
        this.afmSubmittedForm = false;
        this.field = {
            PickListValues: []
        };
        //start block creation
        this.bmSubmittedForm = false;
        this.prePickListValues = [];
        this.defaultPicklist = [];
        //console.log(this.platformLocation);
        //console.log(this.doc.location.href);
        //  console.log(this.doc.location.origin);
        // console.log(this.doc.location);
        this.baseUrl = this.doc.location.origin + '/stp/#/';
        // console.log(platformLocation.location.href);
        // console.log(platformLocation.location.origin);
    }
    FormBuilderComponent.prototype.onWindowScroll = function () {
        var num = this.doc.body.scrollTop;
        this.scrollTop = this.doc.body.scrollTop;
        var leftHeight = this.divLeft.nativeElement.offsetHeight;
        var leftTop = this.divLeft.nativeElement.offsetTop;
        if (num > 200) {
            this.isSpecial = true;
        }
        else {
            this.isSpecial = false;
        }
    };
    FormBuilderComponent.prototype.setStyles = function () {
        var styles = {
            position: this.isSpecial ? 'isSpecial' : 'relative',
            top: this.isSpecial ? (this.scrollTop - 200) + 'px' : '0px'
        };
        //console.log(styles);
        return styles;
    };
    FormBuilderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createModule.activeIndex = 1;
        this.activatedRoute.parent.params.subscribe(function (params) {
            _this.moduleId = params['moduleId'];
            _this.getModuleBlogData();
            _this.getFieldTypeList();
            var pUrl = _this.baseUrl + "tss/" + _this.moduleId + "/0";
            console.log('pUrl', pUrl);
            _this.previewUrl = _this.sanitizer.bypassSecurityTrustResourceUrl(pUrl);
            //console.log('this.previewUrl', platformLocation.location.href);
        });
    };
    FormBuilderComponent.prototype.getModuleBlogData = function () {
        var _this = this;
        this.httpService.get('GetModuleByName/' + this.moduleId).subscribe(function (res) {
            if (res) {
                _this.moduleData = res.data.module;
                //set module fields
                var formateData = _this.dynamicFormService.setDynamicFormFieldFormat(res.data, 'formBuilder');
                console.log('formateData', formateData);
                _this.moduleFields = formateData.blocks;
                _this.ModuleDbTableName = _this.moduleData.db_table_name;
            }
        });
    };
    FormBuilderComponent.prototype.getFieldTypeList = function () {
        var _this = this;
        this.httpService.get('GetFieldTypes').subscribe(function (res) {
            _this.fieldTypeList = [];
            var fieldTypeListData = res.data;
            //let fieldTypeListData =  new Ng2FilterPipe().transform(res.data,{is_available:0});
            //this.fieldTypeList = this.commonService.getDropdownOptions(fieldTypeListData,'name','pk_id');
            for (var _i = 0; _i < fieldTypeListData.length; _i++) {
                var ty = fieldTypeListData[_i];
                if (ty.is_available == 0) {
                    var fObj = {};
                    fObj.label = ty.name;
                    fObj.value = { 'id': ty.pk_id, 'name': ty.name };
                    _this.fieldTypeList.push(fObj);
                }
            }
        });
    };
    FormBuilderComponent.prototype.openAddFieldModal = function (content, blockDetails, event) {
        this.defaultPicklist = [];
        this.defaultPicklist.push({ label: 'Choose', value: null });
        this.prePickListValues = [];
        this.field = {};
        this.field.AppModuleId = blockDetails.app_module_id;
        this.field.AppModuleBlockId = blockDetails.pk_id;
        this.field.DbTableName = this.ModuleDbTableName;
        this.field.selectedFieldType = event.dragData;
        /* this.addFieldModalForm = this._fb.group({
         'AppModuleId': new FormControl(blockDetails.app_module_id),
         'AppModuleBlockId': new FormControl(blockDetails.pk_id),
         'DbTableName': new FormControl(this.ModuleDbTableName),
         'ModuleFieldId': new FormControl(''),
         'FieldType': new FormControl('', Validators.required),
         'Name': new FormControl('', Validators.required),
         'Length': new FormControl(''),
         'Default': new FormControl(''),
         'Precision': new FormControl(''),
         'IsMandatory': new FormControl('false'),
         'QuickCreate': new FormControl(''),
         'MassEdit': new FormControl(''),
         'HeaderView': new FormControl(''),
         'IsKeyField': new FormControl(''),
         'PickListValues': new FormControl('')
         });*/
        this.mr = this.modalService.open(content);
    };
    FormBuilderComponent.prototype.addFieldFormSubmit = function (data) {
        var _this = this;
        this.afmSubmittedForm = true;
        if (this.field) {
            var fType = this.field.selectedFieldType;
            var params = this.field;
            params.FieldType = fType.id;
            params.FieldTypeName = fType.name;
            /*let fieldOrgValue = addFieldModalForm.value.FieldType;
             addFieldModalForm.value.FieldType = fieldOrgValue.id;
             addFieldModalForm.value.FieldTypeName = fieldOrgValue.name;*/
            this.httpService.post('SaveUpdateModuleField', params).subscribe(function (res) {
                if (res) {
                    _this.mr.close();
                    _this.getModuleBlogData();
                    _this.afmSubmittedForm = false;
                }
            });
        }
    };
    FormBuilderComponent.prototype.multiplePickList = function (data, defaul) {
        data = ['a', 'b', 'c'];
        var mList = [];
        if (defaul) {
            mList.push({ label: 'Choose', value: null });
        }
        if (data) {
            for (var _i = 0; _i < data.length; _i++) {
                var d = data[_i];
                var fObj = {};
                fObj.label = d;
                fObj.value = d;
                mList.push(fObj);
            }
        }
        if (this.prePickListValues) {
            for (var _a = 0, _b = this.prePickListValues; _a < _b.length; _a++) {
                var p = _b[_a];
                var fObj = {};
                fObj.label = p;
                fObj.value = p;
                mList.push(fObj);
            }
        }
        return mList;
    };
    FormBuilderComponent.prototype.checkDropDownFieldType = function (data) {
        var ft = this.field.selectedFieldType;
        if (ft && data) {
            if (data.indexOf(ft.name) != -1)
                return true;
        }
        return false;
    };
    FormBuilderComponent.prototype.changeFieldStatus = function (field, key) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: function () {
                var keyValue = true;
                if (field[key]) {
                    keyValue = false;
                }
                var param = {};
                param.key = key;
                param.value = keyValue;
                param.pk_id = field.pk_id;
                param.user_meta_field = field.user_meta_field;
                param.module_id = _this.moduleData.pk_id;
                _this.httpService.post('UpdateModuleFieldQuick', param).subscribe(function (res) {
                    if (res) {
                        field[key] = keyValue;
                        if (key == 'record_identifier') {
                            _this.getModuleBlogData();
                        }
                    }
                });
            }
        });
    };
    FormBuilderComponent.prototype.saveExtraOptions = function (field, key, data) {
        var param = {};
        param.key = key;
        param.value = JSON.stringify(data);
        param.pk_id = field.pk_id;
        param.user_meta_field = field.user_meta_field;
        param.module_id = this.moduleData.pk_id;
        this.httpService.post('UpdateModuleFieldQuick', param).subscribe(function (res) {
            if (res) {
            }
        });
    };
    FormBuilderComponent.prototype.openBlockModal = function (content) {
        this.bmSubmittedForm = false;
        this.blockMF = new forms_1.FormGroup({
            'Name': new forms_1.FormControl(null, forms_1.Validators.required)
        });
        this.bm = this.modalService.open(content);
    };
    FormBuilderComponent.prototype.blockModalFormSubmit = function (formValue, valid) {
        var _this = this;
        this.bmSubmittedForm = true;
        if (valid) {
            formValue.ModuleId = this.moduleData.pk_id;
            formValue.ModuleBlockId = 0;
            this.httpService.post('SaveUpdateModuleBlock', formValue).subscribe(function (res) {
                if (res) {
                    _this.bm.dismiss();
                    _this.bmSubmittedForm = false;
                    _this.getModuleBlogData();
                }
            });
        }
    };
    FormBuilderComponent.prototype.managePickList = function () {
        this.defaultPicklist = [];
        this.defaultPicklist.push({ label: 'Choose', value: null });
        if (this.prePickListValues) {
            for (var _i = 0, _a = this.prePickListValues; _i < _a.length; _i++) {
                var d = _a[_i];
                this.defaultPicklist.push({ label: d, value: d });
            }
        }
        if (this.field.PickListValues) {
            for (var _b = 0, _c = this.field.PickListValues; _b < _c.length; _b++) {
                var d = _c[_b];
                this.defaultPicklist.push({ label: d, value: d });
            }
        }
    };
    FormBuilderComponent.prototype.onBlurName = function (value) {
        var _this = this;
        if (value) {
            this.httpService.get('GetPickListValuesByPickListName/' + value).subscribe(function (res) {
                if (res) {
                    _this.prePickListValues = [];
                    if (res.data) {
                        _this.prePickListValues = [];
                        res.data.forEach(function (data) {
                            if (data.value) {
                                _this.prePickListValues.push(data.value);
                                _this.managePickList();
                            }
                        });
                        _this.managePickList();
                    }
                }
            });
        }
        else {
            this.prePickListValues = [];
            this.managePickList();
        }
    };
    FormBuilderComponent.prototype.pickListSelected = function (field, event) {
        field.PickListName = event.name;
    };
    FormBuilderComponent.prototype.filterPickList = function (event) {
        var _this = this;
        var query = event.query;
        this.httpService.get('getPickListAutocompleter?key=' + query).subscribe(function (res) {
            if (res) {
                _this.filteredPickList = res.data;
            }
        });
    };
    FormBuilderComponent.prototype.reOrder = function () {
        var _this = this;
        this.moduleFields.forEach(function (block) {
            block[_this.showFieldsObjKey].forEach(function (field, key) {
                field.seq_order = key + 1;
            });
        });
        var params = {};
        params.data = this.moduleFields;
        this.httpService.post('UpdateModuleFieldsOrder', params).subscribe(function (res) {
            if (res) {
            }
        });
    };
    FormBuilderComponent.prototype.onBlurDisplayLabel = function (value) {
        if (!this.field.Name)
            this.field.Name = value.replace(/[&\/\\#,@^+()$~%.'":*?<>{}]/g, '_');
        ;
    };
    FormBuilderComponent.prototype.deleteModuleField = function (f) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.httpService.post('deleteModuleField', f).subscribe(function (res) {
                    if (res) {
                        _this.getModuleBlogData();
                        _this.sf = {};
                    }
                });
            }
        });
    };
    FormBuilderComponent.prototype.deleteBlog = function (blog) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.httpService.post('removeModuleBlock/' + blog.pk_id).subscribe(function (res) {
                    if (res) {
                        _this.getModuleBlogData();
                    }
                });
            }
        });
    };
    FormBuilderComponent.prototype.openPreviewModal = function (content) {
        this.modalService.open(content, { size: 'lg', windowClass: 'dynamic-form-preview' });
    };
    FormBuilderComponent.prototype.onFieldSelect = function (field) {
        /*        if(!field.extra_options){
         field.extra_options = {};
         field.extra_options.lg=3;
         field.extra_options.md=6;
         field.extra_options.sm=12;
         }*/
        this.sf = field;
    };
    FormBuilderComponent.prototype.displayFormFields = function (event) {
        this.sf = {};
        if (event.checked) {
            this.showFieldsObjKey = 'fields';
        }
        else {
            this.showFieldsObjKey = 'showFields';
        }
    };
    FormBuilderComponent.prototype.onScroll = function (event) {
        console.log('event', event);
    };
    __decorate([
        core_1.ViewChild('divLeft')
    ], FormBuilderComponent.prototype, "divLeft");
    __decorate([
        core_1.HostListener("window:scroll", [])
    ], FormBuilderComponent.prototype, "onWindowScroll");
    FormBuilderComponent = __decorate([
        core_1.Component({
            selector: 'app-form-builder',
            templateUrl: './form-builder.component.html',
            styleUrls: ['./form-builder.component.scss']
        }),
        __param(11, core_1.Inject(platform_browser_1.DOCUMENT))
    ], FormBuilderComponent);
    return FormBuilderComponent;
})();
exports.FormBuilderComponent = FormBuilderComponent;
//# sourceMappingURL=form-builder.component.js.map