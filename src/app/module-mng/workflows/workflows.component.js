var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var WorkflowsComponent = (function () {
    function WorkflowsComponent(activatedRoute, createModule, commonService, router, httpService) {
        this.activatedRoute = activatedRoute;
        this.createModule = createModule;
        this.commonService = commonService;
        this.router = router;
        this.httpService = httpService;
        this.moduleId = '';
        this.dataRow = {};
        this.refreshTable = true;
        this.showForm = false;
        this.orConditions = [
            {}, {}
        ];
        this.fieldList = [];
        this.conditionList = [{ label: 'empty', value: 'or' }, { label: 'not empty', value: 'not empty' }];
        this.andConditions = [];
        this.actionItems = [
            { label: 'Send Email', icon: 'fa-envelope-o' },
            { label: 'Invoke Custom Function', icon: 'fa-code' },
            { label: 'Update Fields', icon: 'fa-refresh' },
            { label: 'Create Records', icon: 'fa-refresh' },
            { label: 'Web hook', icon: 'fa-link' }
        ];
        this.allFieldsTypeConditions = [];
    }
    WorkflowsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createModule.activeIndex = 4;
        this.activatedRoute.parent.params.subscribe(function (params) {
            _this.moduleId = params['moduleId'];
            _this.getAllFieldTypeConditions();
            _this.getModuleFields();
        });
    };
    WorkflowsComponent.prototype.loadDataLazy = function (event) {
        /*this.httpService.postTable('', event).subscribe(
         res=> {
         if(res){
         //this.dataRows = res.jqGridData.rows;
         //this.totalRecords = res.jqGridData.records;
         }
         }
         );*/
    };
    WorkflowsComponent.prototype.getModuleFields = function () {
        var _this = this;
        this.httpService.get('getModuleFieldsForWorkflow/' + this.moduleId).subscribe(function (res) {
            if (res) {
                _this.fieldList = [];
                res.data.blocks.forEach(function (block) {
                    _this.fieldList.push({ is_block: true, label: block.BlockName });
                    block.Fields.forEach(function (field) {
                        _this.fieldList.push({
                            group: block.BlockName,
                            disabled: true,
                            label: field.name + '(' + block.BlockName + ')',
                            value: { value: field.db_field_name, type: field.type }
                        });
                    });
                });
            }
        });
    };
    WorkflowsComponent.prototype.tableReload = function () {
        var _this = this;
        this.refreshTable = false;
        setTimeout(function () { return _this.refreshTable = true; }, 100);
    };
    WorkflowsComponent.prototype.onCreate = function () {
        this.showForm = true;
        console.log('this.showForm', this.showForm);
    };
    WorkflowsComponent.prototype.addCondition = function (data) {
        data.push({});
    };
    WorkflowsComponent.prototype.deleteCondition = function (items, index) {
        items.splice(index, 1);
    };
    WorkflowsComponent.prototype.getAllFieldTypeConditions = function () {
        var _this = this;
        this.httpService.get('GetListOfWorkflowConditionTypes').subscribe(function (res) {
            if (res) {
                _this.allFieldsTypeConditions = res.data;
            }
        });
    };
    WorkflowsComponent.prototype.onChangeCondition = function (field, row) {
        var obj = [];
        obj = this.allFieldsTypeConditions.filter(function (item) {
            return item.type == field.type;
        });
        console.log('obj', obj);
        row.data = this.commonService.getDropdownOptions(obj, 'condition', 'condition_id');
    };
    WorkflowsComponent = __decorate([
        core_1.Component({
            selector: 'app-workflows',
            templateUrl: './workflows.component.html',
            styleUrls: ['./workflows.component.css']
        })
    ], WorkflowsComponent);
    return WorkflowsComponent;
})();
exports.WorkflowsComponent = WorkflowsComponent;
//# sourceMappingURL=workflows.component.js.map