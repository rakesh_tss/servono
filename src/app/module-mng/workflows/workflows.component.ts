import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {HttpService} from "../../_services/http.service";
import {LazyLoadEvent} from "primeng/primeng";
import {CreateModuleComponent} from "../create-module/create-module.component";
import {CommonService} from "../../_services/common.service";

@Component({
    selector: 'app-workflows',
    templateUrl: './workflows.component.html',
    styleUrls: ['./workflows.component.css']
})
export class WorkflowsComponent implements OnInit {

    constructor(private activatedRoute:ActivatedRoute,
                private createModule:CreateModuleComponent,
                private commonService:CommonService,
                private router:Router, private httpService:HttpService) {
    }

    moduleId:string = '';

    ngOnInit() {
        this.createModule.activeIndex = 4;
        this.activatedRoute.parent.params.subscribe((params:Params) => {
            this.moduleId = params['moduleId'];
            this.getAllFieldTypeConditions();
            this.getModuleFields();
        });
    }

    dataRows:any[];
    dataRow:any = {};
    totalRecords:number;
    refreshTable:boolean = true;

    loadDataLazy(event:LazyLoadEvent) {
        /*this.httpService.postTable('', event).subscribe(
         res=> {
         if(res){
         //this.dataRows = res.jqGridData.rows;
         //this.totalRecords = res.jqGridData.records;
         }
         }
         );*/
    }

    getModuleFields() {

        this.httpService.get('getModuleFieldsForWorkflow/' + this.moduleId).subscribe(res=> {
            if (res) {
                this.fieldList = [];
                res.data.blocks.forEach((block)=> {
                    this.fieldList.push({is_block:true, label: block.BlockName});
                    block.Fields.forEach((field)=> {
                        this.fieldList.push({
                            group: block.BlockName,
                            disabled:true,
                            label: field.name + '(' + block.BlockName + ')',
                            value: {value:field.db_field_name,type:field.type}
                        });
                    })
                })
            }
        })
    }

    tableReload() {
        this.refreshTable = false;
        setTimeout(() => this.refreshTable = true, 100);
    }

    showForm:boolean = false;

    onCreate() {
        this.showForm = true;
        console.log('this.showForm', this.showForm);
    }

    orConditions:any = [
        {}, {}
    ];
    fieldList:any = [];
    conditionList:any = [{label: 'empty', value: 'or'}, {label: 'not empty', value: 'not empty'}];
    andConditions:any = [];
    actionItems:any = [
        {label: 'Send Email', icon: 'fa-envelope-o'},
        {label: 'Invoke Custom Function', icon: 'fa-code'},
        {label: 'Update Fields', icon: 'fa-refresh'},
        {label: 'Create Records', icon: 'fa-refresh'},
        {label: 'Web hook', icon: 'fa-link'}
    ];

    addCondition(data) {
        data.push({});
    }

    deleteCondition(items, index) {
        items.splice(index, 1);
    }
    allFieldsTypeConditions:any = [];
    getAllFieldTypeConditions() {
        this.httpService.get('GetListOfWorkflowConditionTypes').subscribe(res=>{
            if(res){
                this.allFieldsTypeConditions = res.data;
            }
        })
    }
    onChangeCondition(field,row){
        let obj:any = [];
        obj = this.allFieldsTypeConditions.filter((item)=>{
            return item.type==field.type
        });
        console.log('obj',obj);
        row.data = this.commonService.getDropdownOptions(obj,'condition','condition_id');
    }

}
