var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var forms_1 = require('@angular/forms');
var core_1 = require('@angular/core');
var RelationshipsComponent = (function () {
    function RelationshipsComponent(modalService, createModule, activatedRoute, router, _fb, httpServer, confirmationService) {
        this.modalService = modalService;
        this.createModule = createModule;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this._fb = _fb;
        this.httpServer = httpServer;
        this.confirmationService = confirmationService;
        this.submittedForm = false;
        this.moduleId = '';
        this.ModuleRelationData = [];
        this.moduleCurrentBlockList = [];
        this.moduleRelationBlockList = [];
        this.moduleList = [];
        this.rmSubmittedForm = false;
    }
    RelationshipsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createModule.activeIndex = 3;
        this.activatedRoute.parent.params.subscribe(function (params) {
            _this.moduleId = params['moduleId'];
            console.log('Rel', _this.moduleId);
            _this.getModuleList();
            _this.getRelationModule();
            _this.getCurrentModuleBlock(_this.moduleId);
        });
    };
    RelationshipsComponent.prototype.getModuleList = function () {
        var _this = this;
        this.moduleList = [{ label: '--Select--', value: null }];
        this.httpServer.get('GetModules').subscribe(function (res) {
            if (res) {
                for (var _i = 0, _a = res.data; _i < _a.length; _i++) {
                    var d = _a[_i];
                    var obj = {};
                    obj.value = d.module_label;
                    obj.label = d.module_label;
                    _this.moduleList.push(obj);
                }
            }
        });
    };
    RelationshipsComponent.prototype.getRelationModule = function () {
        var _this = this;
        this.httpServer.get('GetModuleRelations/' + this.moduleId).subscribe(function (res) {
            if (res) {
                _this.ModuleRelationData = res.data;
            }
        });
    };
    RelationshipsComponent.prototype.getCurrentModuleBlock = function (module_lable) {
        var _this = this;
        this.moduleCurrentBlockList = [{ label: '--Select--', value: null }];
        this.httpServer.get('GetModuleBlocks/' + module_lable).subscribe(function (res) {
            if (res) {
                for (var _i = 0, _a = res.data; _i < _a.length; _i++) {
                    var d = _a[_i];
                    var obj = {};
                    obj.value = d.pk_id;
                    obj.label = d.name;
                    _this.moduleCurrentBlockList.push(obj);
                }
            }
        });
    };
    RelationshipsComponent.prototype.getRelationModuleBlock = function (evn) {
        var _this = this;
        var module_lable = evn.value;
        if (module_lable) {
            this.moduleRelationBlockList = [{ label: '--Select--', value: null }];
            this.httpServer.get('GetModuleBlocks/' + module_lable).subscribe(function (res) {
                if (res) {
                    for (var _i = 0, _a = res.data; _i < _a.length; _i++) {
                        var d = _a[_i];
                        var obj = {};
                        obj.value = d.pk_id;
                        obj.label = d.name;
                        _this.moduleRelationBlockList.push(obj);
                    }
                }
            });
        }
    };
    RelationshipsComponent.prototype.openAddRelationModal = function (content) {
        this.rmSubmittedForm = false;
        this.relationMF = new forms_1.FormGroup({
            'module_label': new forms_1.FormControl(null, forms_1.Validators.required),
            'relationship_type': new forms_1.FormControl('', forms_1.Validators.required),
            'module_block': new forms_1.FormControl(null, forms_1.Validators.required),
            'relation_module_block': new forms_1.FormControl(null, forms_1.Validators.required),
            'module_record': new forms_1.FormControl('', forms_1.Validators.required),
            'relation_module_record': new forms_1.FormControl('', forms_1.Validators.required)
        });
        //this.relationMF.setValue([{relationship_type:"1-1"}]);
        this.rm = this.modalService.open(content);
    };
    RelationshipsComponent.prototype.addRelationFormSubmit = function (formValue, valid) {
        var _this = this;
        this.rmSubmittedForm = true;
        if (valid) {
            this.httpServer.post('SaveModuleRelation', formValue).subscribe(function (res) {
                if (res) {
                    _this.rm.dismiss();
                    _this.rmSubmittedForm = false;
                    _this.getRelationModule();
                }
            });
        }
    };
    RelationshipsComponent = __decorate([
        core_1.Component({
            selector: 'module-relationships',
            templateUrl: './relationships.component.html',
            styleUrls: ['./relationships.component.scss']
        })
    ], RelationshipsComponent);
    return RelationshipsComponent;
})();
exports.RelationshipsComponent = RelationshipsComponent;
//# sourceMappingURL=relationships.component.js.map