import {Router, ActivatedRoute, Params} from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators,NgForm,NgModel} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {SelectItem} from "primeng/primeng";
import {ConfirmDialogModule,ConfirmationService} from 'primeng/primeng';
import {HttpService} from "../../_services/http.service";
import {CreateModuleComponent} from "../create-module/create-module.component";

@Component({
  selector: 'module-relationships',
  templateUrl: './relationships.component.html',
  styleUrls: ['./relationships.component.scss']
})
export class RelationshipsComponent implements OnInit {


  constructor(private modalService:NgbModal,
              private createModule:CreateModuleComponent,
              private activatedRoute:ActivatedRoute,
              private router:Router, private _fb:FormBuilder,
              private httpServer:HttpService, private confirmationService:ConfirmationService) {

  }

  submittedForm:boolean = false;
  moduleId:string = '';
  ModuleRelationData:any = [];
  moduleCurrentBlockList:any = [];
  moduleRelationBlockList:any = [];

  ngOnInit() {
    this.createModule.activeIndex = 3;
    this.activatedRoute.parent.params.subscribe((params:Params) => {
      this.moduleId = params['moduleId'];
      console.log('Rel',this.moduleId);
      this.getModuleList();
      this.getRelationModule();
      this.getCurrentModuleBlock(this.moduleId);
    });
  }

  moduleList:any = [];

  private getModuleList() {
    this.moduleList = [{label: '--Select--', value: null}];
    this.httpServer.get('GetModules').subscribe(res=> {
      if (res) {
        for (let d of res.data) {
          let obj:any = {};
          obj.value = d.module_label;
          obj.label = d.module_label;
          this.moduleList.push(obj);
        }

      }
    })
  }

  private getRelationModule() {
    this.httpServer.get('GetModuleRelations/' + this.moduleId).subscribe(res=> {
      if (res) {
        this.ModuleRelationData = res.data;

      }
    })
  }


  private getCurrentModuleBlock(module_lable) {
    this.moduleCurrentBlockList = [{label: '--Select--', value: null}];
    this.httpServer.get('GetModuleBlocks/' + module_lable).subscribe(res => {
      if (res) {
        for (let d of res.data) {
          let obj:any = {};
          obj.value = d.pk_id;
          obj.label = d.name;
          this.moduleCurrentBlockList.push(obj);
        }
      }
    })
  }

  private getRelationModuleBlock(evn) {
    let module_lable = evn.value;
    if (module_lable) {
      this.moduleRelationBlockList = [{label: '--Select--', value: null}];
      this.httpServer.get('GetModuleBlocks/' + module_lable).subscribe(res => {
        if (res) {
          for (let d of res.data) {
            let obj:any = {};
            obj.value = d.pk_id;
            obj.label = d.name;
            this.moduleRelationBlockList.push(obj);
          }
        }
      })
    }

  }
  //add relation field
  public rm:NgbModalRef;
  relationMF:FormGroup;
  rmSubmittedForm:boolean = false;

  openAddRelationModal(content) {
    this.rmSubmittedForm = false;
    this.relationMF = new FormGroup({
      'module_label': new FormControl(null, Validators.required),
      'relationship_type': new FormControl('', Validators.required),
      'module_block': new FormControl(null, Validators.required),
      'relation_module_block': new FormControl(null, Validators.required),
      'module_record': new FormControl('', Validators.required),
      'relation_module_record': new FormControl('', Validators.required),
    });

    //this.relationMF.setValue([{relationship_type:"1-1"}]);
    this.rm = this.modalService.open(content);
  }

  addRelationFormSubmit(formValue, valid) {
    this.rmSubmittedForm = true;
    if (valid) {
      this.httpServer.post('SaveModuleRelation', formValue).subscribe(
          res => {
            if (res) {
              this.rm.dismiss();
              this.rmSubmittedForm = false;
              this.getRelationModule();
            }
          })
    }
  }

}
