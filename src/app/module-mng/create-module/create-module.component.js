var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
//import current = SyntaxKind.current;
var CreateModuleComponent = (function () {
    function CreateModuleComponent(modalService, activatedRoute, breadcrumbService, router, _fb, httpServer, confirmationService) {
        this.modalService = modalService;
        this.activatedRoute = activatedRoute;
        this.breadcrumbService = breadcrumbService;
        this.router = router;
        this._fb = _fb;
        this.httpServer = httpServer;
        this.confirmationService = confirmationService;
        this.activeIndex = 0;
        this.submittedForm = false;
        this.moduleId = null;
        this.stepId = 0;
        this.stepReadonly = true;
    }
    CreateModuleComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            if (params['moduleId'] && params['moduleId'] != 0) {
                _this.moduleId = params['moduleId'];
            }
            _this.stepId = params['stepId'];
            //setBreadcrumb
            if (!_this.moduleId) {
                _this.breadcrumbService.setBreadcrumb([{ label: 'Module Management', routerLink: ['/Custom/Modules'] }, { label: 'Create' }]);
            }
            else {
                _this.breadcrumbService.setBreadcrumb([{ label: 'Module Management', routerLink: ['/Custom/Modules'] }, { label: _this.moduleId + ' Update' }]);
            }
            if (_this.moduleId) {
                _this.stepReadonly = false;
                _this.activeIndex = Number(_this.stepId);
            }
            var currentUrl = 'Custom/Modules/Create/' + _this.moduleId;
            _this.stepItems = [{
                    label: 'Module Properties',
                    command: function (event) {
                        //this.activeIndex = 0;
                        if (_this.stepId != 0)
                            _this.router.navigate([currentUrl + '/0']);
                    }
                },
                {
                    label: 'Block & Fields',
                    command: function (event) {
                        //this.activeIndex = 1;
                        if (_this.stepId != 1)
                            _this.router.navigate([currentUrl + '/1']);
                    }
                },
                {
                    label: 'Grid Management',
                    command: function (event) {
                        //this.activeIndex = 2;
                        if (_this.stepId != 2)
                            _this.router.navigate([currentUrl + '/2']);
                    }
                },
                {
                    label: 'Relationships',
                    command: function (event) {
                        _this.activeIndex = 3;
                        if (_this.stepId != 3)
                            _this.router.navigate([currentUrl + '/3']);
                    }
                },
                {
                    label: 'Workflows',
                    command: function (event) {
                        //this.activeIndex = 4;
                        if (_this.stepId != 4)
                            _this.router.navigate([currentUrl + '/4']);
                    }
                },
                {
                    label: 'Dependent Fields/Block',
                    command: function (event) {
                        //this.activeIndex = 4;
                        if (_this.stepId != 5)
                            _this.router.navigate([currentUrl + '/5']);
                    }
                },
                {
                    label: 'Fields Unique Index',
                    command: function (event) {
                        //this.activeIndex = 4;
                        if (_this.stepId != 6)
                            _this.router.navigate([currentUrl + '/6']);
                    }
                }
            ];
            if (_this.stepId != 0 && _this.moduleId == null) {
            }
        });
    };
    CreateModuleComponent = __decorate([
        core_1.Component({
            selector: 'app-create-module',
            templateUrl: './create-module.component.html',
            styleUrls: ['./create-module.component.scss']
        })
    ], CreateModuleComponent);
    return CreateModuleComponent;
})();
exports.CreateModuleComponent = CreateModuleComponent;
//# sourceMappingURL=create-module.component.js.map