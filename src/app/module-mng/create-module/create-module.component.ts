import {Router, ActivatedRoute, Params} from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators,NgForm,NgModel} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {SelectItem} from "primeng/primeng";
import {ConfirmDialogModule,ConfirmationService} from 'primeng/primeng';
import {HttpService} from "../../_services/http.service";
import {BreadcrumbService} from "../../_services/breadcrumb.service";
//import current = SyntaxKind.current;

@Component({
    selector: 'app-create-module',
    templateUrl: './create-module.component.html',
    styleUrls: ['./create-module.component.scss']
})
export class CreateModuleComponent implements OnInit {

    stepItems:any;
    activeIndex:number = 0;

    constructor(private modalService:NgbModal, private activatedRoute:ActivatedRoute,
                private breadcrumbService:BreadcrumbService,
                private router:Router, private _fb:FormBuilder,
                private httpServer:HttpService, private confirmationService:ConfirmationService) {

    }

    submittedForm:boolean = false;
    moduleId:string = null;
    stepId:number = 0;
    stepReadonly:boolean = true;


    ngOnInit() {

        this.activatedRoute.params.subscribe((params:Params) => {
            if( params['moduleId'] && params['moduleId'] != 0   ){
                this.moduleId = params['moduleId'];

            }
            this.stepId = params['stepId'];
            //setBreadcrumb
            if(!this.moduleId){
                this.breadcrumbService.setBreadcrumb([{label:'Module Management',routerLink:['/Custom/Modules']},{label:'Create'}]);
            }else{
                this.breadcrumbService.setBreadcrumb([{label:'Module Management',routerLink:['/Custom/Modules']},{label:this.moduleId+' Update'}]);
            }

            if (this.moduleId) {
                this.stepReadonly = false;
                this.activeIndex = Number(this.stepId);
            }

            let currentUrl = 'Custom/Modules/Create/' + this.moduleId;
            this.stepItems = [{
                label: 'Module Properties',
                command: (event:any) => {
                    //this.activeIndex = 0;
                    if (this.stepId != 0)
                        this.router.navigate([currentUrl + '/0']);
                }
            },
                {
                    label: 'Block & Fields',
                    command: (event:any) => {
                        //this.activeIndex = 1;
                        if (this.stepId != 1)
                            this.router.navigate([currentUrl + '/1']);
                    }
                },
                {
                    label: 'Grid Management',
                    command: (event:any) => {
                        //this.activeIndex = 2;
                        if (this.stepId != 2)
                            this.router.navigate([currentUrl + '/2']);
                    }
                },
                {
                    label: 'Relationships',
                    command: (event:any) => {
                        this.activeIndex = 3;
                        if (this.stepId != 3)
                            this.router.navigate([currentUrl + '/3']);
                    }
                },
                {
                    label: 'Workflows',
                    command: (event:any) => {
                        //this.activeIndex = 4;
                        if (this.stepId != 4)
                            this.router.navigate([currentUrl + '/4']);
                    }
                },
                {
                    label: 'Dependent Fields/Block',
                    command: (event:any) => {
                        //this.activeIndex = 4;
                        if (this.stepId != 5)
                            this.router.navigate([currentUrl + '/5']);
                    }
                },
                {
                    label: 'Fields Unique Index',
                    command: (event:any) => {
                        //this.activeIndex = 4;
                        if (this.stepId != 6)
                            this.router.navigate([currentUrl + '/6']);
                    }
                }
            ];

            if(this.stepId !=0 && this.moduleId==null){
                //this.router.navigate(['Custom/Modules/Create/0/0']);
            }
        });

    }

}
