import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldsUniqueIndexComponent } from './fields-unique-index.component';

describe('FieldsUniqueIndexComponent', () => {
  let component: FieldsUniqueIndexComponent;
  let fixture: ComponentFixture<FieldsUniqueIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldsUniqueIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldsUniqueIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
