import { Component, OnInit } from '@angular/core';
import {CreateModuleComponent} from "../create-module/create-module.component";
import {Router, ActivatedRoute, Params} from '@angular/router';
import {HttpService} from "../../_services/http.service";
import {LazyLoadEvent,SelectItem,ConfirmationService,SpinnerModule} from "primeng/primeng";
import {CommonService} from "../../_services/common.service";
import { FormGroup, FormControl, FormBuilder, Validators,NgForm,NgModel} from '@angular/forms';
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-fields-unique-index',
    templateUrl: './fields-unique-index.component.html',
    styleUrls: ['./fields-unique-index.component.css']
})
export class FieldsUniqueIndexComponent implements OnInit {

    constructor(private createModule:CreateModuleComponent,
                private activatedRoute:ActivatedRoute,
                private httpService:HttpService,
                private cs:CommonService,
                private modalService:NgbModal,
                private confirmationService:ConfirmationService) {
    }

    moduleId:string = '';
    moduleData:any = {};
    app_module_id:number = 0;

    ngOnInit() {
        this.createModule.activeIndex = 6;
        this.activatedRoute.parent.params.subscribe((params:Params) => {
            this.moduleId = params['moduleId'];
            this.getModuleBlogData();
        });
    }

    getModuleBlogData() {
        this.httpService.get('getModuleListMetadata/' + this.moduleId).subscribe(res => {
            if (res) {
                this.moduleData = res.moduleAndTemplateData;
                this.app_module_id = this.moduleData.app_module_id;
                this.tableReload();
                this.getModuleFields();
            }
        })
    }

    moduleFields:any = [];

    getModuleFields() {
        this.moduleFields = [];
        this.httpService.get('GetCustomModuleFieldsById/' + this.app_module_id).subscribe(res => {
            if (res) {
                this.moduleFields = this.cs.getDropdownOptions(res.data, 'name', 'pk_id', true);
                /*res.data.forEach(row => {
                 this.moduleFields.push({label:});
                 });*/
            }
        });
    }

    dataRows:any = [];
    totalRecords:number = 0;
    refreshTable:boolean = false;

    loadDataLazy(event:LazyLoadEvent) {
        event['app_module_id'] = this.app_module_id;
        this.httpService.postTable('GetModuleFiledsUnieIndexBySearch', event).subscribe(
            res=> {
                if (res) {
                    this.dataRows = res.jqGridData.rows;
                    this.totalRecords = res.jqGridData.records;
                }
            }
        );

    }

    tableReload() {
        this.refreshTable = false;
        setTimeout(() => this.refreshTable = true, 0);
    }

    itemSubmittedForm:boolean = false;
    itemFG:FormGroup;
    itemModal:NgbModalRef;
    fields:any = [];

    openItemModal(content, fdata) {
        this.fields = [];
        let data :any={}
        Object.assign(data,fdata);

        //this.getModuleFields();
        this.itemSubmittedForm = false;
        this.itemFG = new FormGroup({
            'pk_id': new FormControl(null)
            , 'name': new FormControl(null, Validators.required)
            , 'fields': new FormControl(null, Validators.required)
        });
        if (data) {
            let selectedFields:any = [];
            selectedFields = data.fields;
            if(selectedFields){
                data.fields = [];
                selectedFields.forEach((field)=> {
                    data.fields.push(field.pk_id);
                });
            }

            this.itemFG.patchValue(data);
        }
        this.itemModal = this.modalService.open(content);

    }

    itemModalFormSubmit(formValue, valid) {
        this.itemSubmittedForm = true;
        if (valid) {
            formValue.app_module_id = this.app_module_id;
            formValue.fields = formValue.fields.toString();
            this.httpService.post('SaveModuleFieldUniqueIndex', formValue).subscribe(
                res => {
                    if (res) {
                        this.itemModal.dismiss();
                        this.itemSubmittedForm = false;
                        this.tableReload();
                    }
                })
        }
    }

    removeSelectedItem(row) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
                this.httpService.get('DeleteModuleFieldsUniqueIndex/' + row.pk_id, '').subscribe(res=> {
                    if (res) {
                        this.tableReload();
                    }
                })
            }
        });
    }

}
