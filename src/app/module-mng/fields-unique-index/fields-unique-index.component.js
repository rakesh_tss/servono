var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var FieldsUniqueIndexComponent = (function () {
    function FieldsUniqueIndexComponent(createModule, activatedRoute, httpService, cs, modalService, confirmationService) {
        this.createModule = createModule;
        this.activatedRoute = activatedRoute;
        this.httpService = httpService;
        this.cs = cs;
        this.modalService = modalService;
        this.confirmationService = confirmationService;
        this.moduleId = '';
        this.moduleData = {};
        this.app_module_id = 0;
        this.moduleFields = [];
        this.dataRows = [];
        this.totalRecords = 0;
        this.refreshTable = false;
        this.itemSubmittedForm = false;
        this.fields = [];
    }
    FieldsUniqueIndexComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createModule.activeIndex = 6;
        this.activatedRoute.parent.params.subscribe(function (params) {
            _this.moduleId = params['moduleId'];
            _this.getModuleBlogData();
        });
    };
    FieldsUniqueIndexComponent.prototype.getModuleBlogData = function () {
        var _this = this;
        this.httpService.get('getModuleListMetadata/' + this.moduleId).subscribe(function (res) {
            if (res) {
                _this.moduleData = res.moduleAndTemplateData;
                _this.app_module_id = _this.moduleData.app_module_id;
                _this.tableReload();
                _this.getModuleFields();
            }
        });
    };
    FieldsUniqueIndexComponent.prototype.getModuleFields = function () {
        var _this = this;
        this.moduleFields = [];
        this.httpService.get('GetCustomModuleFieldsById/' + this.app_module_id).subscribe(function (res) {
            if (res) {
                _this.moduleFields = _this.cs.getDropdownOptions(res.data, 'name', 'pk_id', true);
            }
        });
    };
    FieldsUniqueIndexComponent.prototype.loadDataLazy = function (event) {
        var _this = this;
        event['app_module_id'] = this.app_module_id;
        this.httpService.postTable('GetModuleFiledsUnieIndexBySearch', event).subscribe(function (res) {
            if (res) {
                _this.dataRows = res.jqGridData.rows;
                _this.totalRecords = res.jqGridData.records;
            }
        });
    };
    FieldsUniqueIndexComponent.prototype.tableReload = function () {
        var _this = this;
        this.refreshTable = false;
        setTimeout(function () { return _this.refreshTable = true; }, 0);
    };
    FieldsUniqueIndexComponent.prototype.openItemModal = function (content, data) {
        this.fields = [];
        //this.getModuleFields();
        this.itemSubmittedForm = false;
        this.itemFG = new forms_1.FormGroup({
            'pk_id': new forms_1.FormControl(null),
            'name': new forms_1.FormControl(null, forms_1.Validators.required),
            'fields': new forms_1.FormControl(null, forms_1.Validators.required)
        });
        //this.itemFG.patchValue({fields:['940']});
        if (data) {
            var selectedFields = [];
            selectedFields = data.fields;
            data.fields = [];
            selectedFields.forEach(function (field) {
                data.fields.push(field.pk_id);
            });
            this.itemFG.patchValue(data);
            console.log('data', data);
        }
        this.itemModal = this.modalService.open(content);
    };
    FieldsUniqueIndexComponent.prototype.itemModalFormSubmit = function (formValue, valid) {
        var _this = this;
        console.log(formValue);
        this.itemSubmittedForm = true;
        if (valid) {
            formValue.app_module_id = this.app_module_id;
            formValue.fields = formValue.fields.toString();
            this.httpService.post('SaveModuleFieldUniqueIndex', formValue).subscribe(function (res) {
                if (res) {
                    _this.itemModal.dismiss();
                    _this.itemSubmittedForm = false;
                    _this.tableReload();
                }
            });
        }
    };
    FieldsUniqueIndexComponent.prototype.removeSelectedItem = function (row) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.httpService.get('DeleteModuleFieldsUniqueIndex/' + row.pk_id, '').subscribe(function (res) {
                    if (res) {
                        _this.tableReload();
                    }
                });
            }
        });
    };
    FieldsUniqueIndexComponent = __decorate([
        core_1.Component({
            selector: 'app-fields-unique-index',
            templateUrl: './fields-unique-index.component.html',
            styleUrls: ['./fields-unique-index.component.css']
        })
    ], FieldsUniqueIndexComponent);
    return FieldsUniqueIndexComponent;
})();
exports.FieldsUniqueIndexComponent = FieldsUniqueIndexComponent;
//# sourceMappingURL=fields-unique-index.component.js.map