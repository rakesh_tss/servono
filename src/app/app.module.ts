import { NgModule,APP_INITIALIZER,ErrorHandler} from '@angular/core';
import { FormsModule,ReactiveFormsModule }    from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { LocationStrategy, HashLocationStrategy,PathLocationStrategy } from '@angular/common';
import { HttpModule } from '@angular/http';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { NAV_DROPDOWN_DIRECTIVES } from './shared/nav-dropdown.directive';
import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/sidebar.directive';
import { AsideToggleDirective } from './shared/aside.directive';
import { BreadcrumbsComponent } from './shared/breadcrumb.component';
import { SmartResizeDirective } from './shared/smart-resize.directive';

// Routing Module
import { AppRoutingModule } from './app.routing';

//Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import {SimpleLayoutComponent} from "./layouts/simple-layout.component";
import {AuthGuard} from "./_services/auth.guard";
import {AuthenticationService} from "./_services/authentication.service";
import {HttpService} from "./_services/http.service";
import {LayoutService} from "./_services/layout.service";
import {BreadcrumbModule,
    InputTextModule,
    ButtonModule,MenuModule,
    OverlayPanelModule,
    DialogModule,
    PasswordModule} from "primeng/primeng";
import {BreadcrumbService} from "./_services/breadcrumb.service";
import {p404Component} from "./pages/404.component";
import {PermissionService} from "./_services/permission.service";
import {AppConfig} from "./app.config";
import {APP_BASE_HREF} from '@angular/common';
import {GlobalErrorHandler} from "./_services/error-handler.service";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginModalComponent } from './_components/login-modal/login-modal.component';
import {LocalStorageService} from "./_services/local-storage.service";
import { DropdownModule, CheckboxModule } from 'primeng/primeng';
import {MdMenuModule} from '@angular/material';
export function initialConfigLoad(config: AppConfig) {
    return () => config.load();
};
@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        AppRoutingModule,
        HttpModule,
        ToasterModule,
        MenuModule,
        OverlayPanelModule,
        BreadcrumbModule,
        BrowserAnimationsModule,
        DialogModule,
        PasswordModule,
        InputTextModule,
        ButtonModule,
        NgbModule.forRoot(),
        DropdownModule,
        MdMenuModule,
        CheckboxModule
    ],
    declarations: [
        AppComponent,
        FullLayoutComponent,
        SimpleLayoutComponent,
        NAV_DROPDOWN_DIRECTIVES,
        BreadcrumbsComponent,
        SIDEBAR_TOGGLE_DIRECTIVES,
        AsideToggleDirective,
        SmartResizeDirective,
        p404Component,
        LoginModalComponent
    ],
    entryComponents:[
        LoginModalComponent
    ],
    providers: [
        {
            provide: ErrorHandler,
            useClass: GlobalErrorHandler
        },
        {
            provide: APP_BASE_HREF, useValue : '/'
        },
        {
            provide: LocationStrategy,
            useClass: HashLocationStrategy
        },
        AppConfig,
        { provide: APP_INITIALIZER, useFactory: initialConfigLoad, deps: [AppConfig], multi: true },
         AuthGuard,AuthenticationService, HttpService, LayoutService,BreadcrumbService,PermissionService,LocalStorageService],
    bootstrap: [AppComponent]
})
/*
 {
 provide: LocationStrategy,
 useClass: HashLocationStrategy
 }
 */
export class AppModule {
}
