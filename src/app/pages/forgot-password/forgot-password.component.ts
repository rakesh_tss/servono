import { Component, OnInit } from '@angular/core';
import {HttpService} from "../../_services/http.service";
import {Router, ActivatedRoute, Params} from '@angular/router';
import {SimpleLayoutComponent} from "../../layouts/simple-layout.component";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private httpService:HttpService,
              private sl:SimpleLayoutComponent,
              private router:Router,private activatedRoute:ActivatedRoute) { }
  fp:any = {};
  ngOnInit() {
  }
  sendMail(){
    this.sl.loading = true;
    this.httpService.post('SendForgotPasswordMail',this.fp).subscribe(res=>{
      this.sl.loading = false;
      if(res){
        this.router.navigate([ '/login']);
      }
    },error=>{
      this.sl.loading = false;
    })
  }

}
