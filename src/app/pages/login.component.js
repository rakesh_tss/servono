var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var full_layout_component_1 = require("../layouts/full-layout.component");
var LoginComponent = (function () {
    function LoginComponent(route, router, authenticationService, fullLayoutComponent) {
        this.route = route;
        this.router = router;
        this.authenticationService = authenticationService;
        this.fullLayoutComponent = fullLayoutComponent;
        this.model = {};
        this.loading = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        // reset login status
        if (localStorage.getItem('currentUser')) {
            this.authenticationService.logout();
        }
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.loading = true;
        this.authenticationService.login(this.model.email, this.model.password).subscribe(function (res) {
            //var result =result;
            if (res) {
                var result = {};
                result = res;
                _this.menuObject = _this.fullLayoutComponent.setMenuFormat(result.Data.MenuObject);
                result.Data.MenuObject = _this.menuObject;
                // result.Data.MenuObject = this.fullLayoutComponent.setMenuFormat(result.Data.MenuObject);
                //console.log('result.Data.MenuObject',result.Data.MenuObject);
                localStorage.setItem('currentUser', JSON.stringify(result.Data));
                if (_this.menuObject.length > 1) {
                    document.querySelector("body").classList.remove('sidebar-nav');
                    _this.router.navigate(['Modules']);
                }
                else {
                    document.querySelector("body").classList.add('sidebar-nav');
                    _this.fullLayoutComponent.setMenu(_this.menuObject[0]);
                }
            }
            else {
                _this.loading = false;
            }
        }, function (err) {
            _this.loading = false;
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            templateUrl: 'login.component.html',
            providers: [full_layout_component_1.FullLayoutComponent],
            styleUrls: ['./login.component.scss']
        })
    ], LoginComponent);
    return LoginComponent;
})();
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map