var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var _500_component_1 = require('./500.component');
var login_component_1 = require('./login.component');
var register_component_1 = require('./register.component');
var pages_routing_module_1 = require('./pages-routing.module');
var user_service_1 = require("../_services/user.service");
var alert_service_1 = require("../_services/alert.service");
var authentication_service_1 = require("../_services/authentication.service");
var http_service_1 = require("../_services/http.service");
var primeng_1 = require("primeng/primeng");
var forgot_password_component_1 = require('./forgot-password/forgot-password.component');
var update_password_component_1 = require('./update-password/update-password.component');
var PagesModule = (function () {
    function PagesModule() {
    }
    PagesModule = __decorate([
        core_1.NgModule({
            imports: [
                forms_1.FormsModule,
                http_1.HttpModule,
                primeng_1.PanelModule,
                pages_routing_module_1.PagesRoutingModule, primeng_1.InputTextModule, primeng_1.ButtonModule],
            declarations: [
                _500_component_1.p500Component,
                login_component_1.LoginComponent,
                register_component_1.RegisterComponent,
                forgot_password_component_1.ForgotPasswordComponent,
                update_password_component_1.UpdatePasswordComponent
            ],
            providers: [user_service_1.UserService, alert_service_1.AlertService, authentication_service_1.AuthenticationService, http_service_1.HttpService]
        })
    ], PagesModule);
    return PagesModule;
})();
exports.PagesModule = PagesModule;
//# sourceMappingURL=pages.module.js.map