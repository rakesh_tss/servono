var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var _500_component_1 = require('./500.component');
var login_component_1 = require('./login.component');
var register_component_1 = require('./register.component');
var forgot_password_component_1 = require("./forgot-password/forgot-password.component");
var update_password_component_1 = require("./update-password/update-password.component");
var routes = [
    {
        path: '500',
        component: _500_component_1.p500Component,
        data: {
            title: 'Page 500'
        }
    },
    {
        path: 'login',
        component: login_component_1.LoginComponent,
        data: {
            title: 'Login Page'
        }
    },
    {
        path: 'register',
        component: register_component_1.RegisterComponent,
        data: {
            title: 'Register Page'
        }
    },
    {
        path: 'forgot-password',
        component: forgot_password_component_1.ForgotPasswordComponent,
        data: {
            title: 'Forgot Password'
        }
    },
    {
        path: 'update-password/:key',
        component: update_password_component_1.UpdatePasswordComponent,
        data: {
            title: 'Update Password'
        }
    }
];
var PagesRoutingModule = (function () {
    function PagesRoutingModule() {
    }
    PagesRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], PagesRoutingModule);
    return PagesRoutingModule;
})();
exports.PagesRoutingModule = PagesRoutingModule;
//# sourceMappingURL=pages-routing.module.js.map