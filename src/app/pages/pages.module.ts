import { NgModule } from '@angular/core'
import { FormsModule }    from '@angular/forms';
import { HttpModule } from '@angular/http';

import { p404Component } from './404.component';
import { p500Component } from './500.component';
import { LoginComponent } from './login.component';
import { RegisterComponent } from './register.component';

import { PagesRoutingModule } from './pages-routing.module';
import {UserService} from "../_services/user.service";
import {AlertService} from "../_services/alert.service";
import {AuthenticationService} from "../_services/authentication.service";
import {HttpService} from "../_services/http.service";
import {PanelModule,InputTextModule,ButtonModule} from "primeng/primeng";
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { UpdatePasswordComponent } from './update-password/update-password.component';

@NgModule({
    imports: [
        FormsModule,
        HttpModule,
        PanelModule,
        PagesRoutingModule,InputTextModule,ButtonModule],
    declarations: [
        p500Component,
        LoginComponent,
        RegisterComponent,
        ForgotPasswordComponent,
        UpdatePasswordComponent
    ],
    providers: [UserService,AlertService,AuthenticationService,HttpService],
})
export class PagesModule {

}
