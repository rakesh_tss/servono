import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthenticationService } from '../_services/index';
import {FullLayoutComponent} from "../layouts/full-layout.component";
import {HttpService} from "../_services/http.service";
import {SimpleLayoutComponent} from "../layouts/simple-layout.component";
import {LocalStorageService} from "../_services/local-storage.service";


@Component({
    templateUrl: 'login.component.html',
    providers: [FullLayoutComponent],
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
    model:any = {};
    returnUrl:string;
    menuObject:any[];

    constructor(private route:ActivatedRoute,
                private router:Router,
                private sl:SimpleLayoutComponent,
                private httpService:HttpService,
                private authenticationService:AuthenticationService,
                private fullLayoutComponent:FullLayoutComponent,
                private localStorage:LocalStorageService) {
    }

    ngOnInit() {
        // reset login status
        if (this.localStorage.getItem('currentUser')) {
            this.authenticationService.logout();
        }
        console.log('login Page');
        if (this.httpService.reLoginModal) {
            console.log('modal open login Page');
            this.httpService.reLoginModal.close();
        }


        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    }

    login() {
        this.sl.loading = true;
        this.httpService.post('processLogin', {
            UserName: this.model.email,
            Password: this.model.password
        }).subscribe(res=> {
            this.sl.loading = false;
            if (res) {
                let result:any = {};
                result = res;
                //result.Data.MenuObject = this.menuObject;
                this.localStorage.setItem('currentUser', JSON.stringify(result.Data));
                this.router.navigate(['/']);
                /*if(this.menuObject.length>1){
                 document.querySelector("body").classList.remove('sidebar-nav');
                 this.router.navigate(['Modules']);
                 }else{
                 document.querySelector("body").classList.add('sidebar-nav');
                 this.fullLayoutComponent.setMenu(this.menuObject[0]);
                 }*/
            }
        }, error=> {
            this.sl.loading = false;
        })


        /* this.authenticationService.login(this.model.email, this.model.password).subscribe(res=>{
         //var result =result;
         if (res) {


         }else{
         this.loading = false;
         }

         },err=>{
         this.loading = false;
         });*/
    }

}
