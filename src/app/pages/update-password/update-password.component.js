var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var UpdatePasswordComponent = (function () {
    function UpdatePasswordComponent(httpService, router, activatedRoute) {
        this.httpService = httpService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.Up = {};
    }
    UpdatePasswordComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            _this.Up.MailId = params['key'];
        });
    };
    UpdatePasswordComponent.prototype.updatePassword = function () {
        var _this = this;
        this.httpService.post('UpdateNewPasswordByMail', this.Up).subscribe(function (res) {
            if (res) {
                _this.router.navigate(['/login']);
            }
        });
    };
    UpdatePasswordComponent = __decorate([
        core_1.Component({
            selector: 'app-update-password',
            templateUrl: './update-password.component.html',
            styleUrls: ['./update-password.component.css']
        })
    ], UpdatePasswordComponent);
    return UpdatePasswordComponent;
})();
exports.UpdatePasswordComponent = UpdatePasswordComponent;
//# sourceMappingURL=update-password.component.js.map