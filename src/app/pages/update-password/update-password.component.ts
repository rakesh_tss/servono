import { Component, OnInit } from '@angular/core';
import {HttpService} from "../../_services/http.service";
import {Router, ActivatedRoute, Params} from '@angular/router';
import {SimpleLayoutComponent} from "../../layouts/simple-layout.component";

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.css']
})
export class UpdatePasswordComponent implements OnInit {

  constructor(private httpService:HttpService,
              private sl:SimpleLayoutComponent,
              private router:Router,private activatedRoute:ActivatedRoute) { }
  Up:any = {};
  ngOnInit() {
    this.activatedRoute.params.subscribe((params:Params) => {
      this.Up.MailId = params['key'];
    });
  }
  updatePassword(){
    this.sl.loading = true;
    this.httpService.post('UpdateNewPasswordByMail',this.Up).subscribe(res=>{
      this.sl.loading = false;
      if(res){
        this.router.navigate([ '/login']);
      }
    },error=>{
      this.sl.loading = false;
    })
  }



}
