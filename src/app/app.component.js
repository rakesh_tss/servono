var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var angular2_toaster_1 = require("angular2-toaster/angular2-toaster");
var AppComponent = (function () {
    function AppComponent(toasterService) {
        this.toasterService = toasterService;
        this.toasterconfig = new angular2_toaster_1.ToasterConfig({
            showCloseButton: true,
            positionClass: "toast-bottom-right",
            preventDuplicates: true
        });
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'body',
            template: '<toaster-container [toasterconfig]="toasterconfig"></toaster-container><router-outlet></router-outlet>'
        })
    ], AppComponent);
    return AppComponent;
})();
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map