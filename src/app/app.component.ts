import { Component } from '@angular/core';
import {ToasterService} from "angular2-toaster/angular2-toaster";
import {ToasterConfig} from "angular2-toaster/angular2-toaster";


@Component({
    selector: 'body',
    template: '<toaster-container [toasterconfig]="toasterconfig"></toaster-container><router-outlet></router-outlet>'
})
export class AppComponent{
    constructor(private toasterService: ToasterService) {
    }
     public toasterconfig : ToasterConfig =
        new ToasterConfig({
            showCloseButton: true,
            positionClass: "toast-bottom-right",
            preventDuplicates: true
        });
}
