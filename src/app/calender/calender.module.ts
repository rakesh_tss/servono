import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {ScheduleModule} from 'primeng/primeng';
import 'fullcalendar';

import { CalenderRoutingModule } from './calender-routing.module';
import { CalenderComponent } from './calender.component';
import {CommonTssModule} from "../shared/commonTssModules";
import {DynamicFormModule} from "../tss/dynamic-form/dynamic-form.module";

@NgModule({
  imports: [
    CommonModule,
    CalenderRoutingModule,
    ScheduleModule,
    CommonTssModule,
    DynamicFormModule
  ],
  declarations: [CalenderComponent]
})
export class CalenderModule { }
