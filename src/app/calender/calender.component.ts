import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import * as jQuery from 'jquery';
(window as any).jQuery = (window as any).$ = jQuery;
@Component({
  selector: 'app-calender',
  templateUrl: './calender.component.html',
  styleUrls: ['./calender.component.css']
})
export class CalenderComponent implements OnInit {

  events: any[];

  header: any;

  eventLimit : any;

  event: MyEvent;

  dialogVisible: boolean = false;

  idGen: number = 100;

  constructor() { }

  ngOnInit() {
   //this.eventService.getEvents().then(events => {this.events = events;});



    this.events = [
      {
        "id" : 1,
        "title": "All Day Event",
        "start": "2017-01-01",
       /* url: 'http://google.com/',*/
        "end": "2017-01-04T16:00:00",
        allDay : false
        /*editable: false*/

      },
      {
        "id" : 11,
        "title": "All Day Event1",
        "start": "2017-01-01"
      },
      {
        "id" : 2,
        "title": "Long Event",
        "start": "2017-01-07",
        "end": "2017-01-10"
      },
      {
        "id" : 3,
        "title": "Repeating Event",
        "start": "2017-01-09T16:00:00"
      },
      {
        "id" : 4,
        "title": "Repeating Event",
        "start": "2017-01-16T16:00:00"
      },
      {
        "id" : 5,
        "title": "Conference",
        "start": "2017-01-11",
        "end": "2017-01-13"
      }
    ];


    this.header = {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    };
  }

  handleDayClick(event) {
    this.event = new MyEvent();
    this.event.start = event.date.format();
    this.dialogVisible = true;

    //trigger detection manually as somehow only moving the mouse quickly after click triggers the automatic detection
    //this.cd.detectChanges();
  }

  handleEventClick(e) {

    this.event = new MyEvent();
    this.event.title = e.calEvent.title;

    let start = e.calEvent.start;
    let end = e.calEvent.end;
    if(e.view.name === 'month') {
      start.stripTime();
    }

    if(end) {
      //end.stripTime();
      this.event.end = new Date('2017-01-04T16:00:00');
    }

    this.event.id = e.calEvent.id;
    this.event.start = start.format();
    this.event.allDay = e.calEvent.allDay;
    this.dialogVisible = true;

  }

  saveEvent() {
    //update
    if(this.event.id) {
      let index: number = this.findEventIndexById(this.event.id);
      if(index >= 0) {
        this.events[index] = this.event;
      }
    }
    //new
    else {
      this.event.id = this.idGen++;
      this.events.push(this.event);
      this.event = null;
    }

    this.dialogVisible = false;
  }

  deleteEvent() {
    let index: number = this.findEventIndexById(this.event.id);
    if(index >= 0) {
      this.events.splice(index, 1);
    }
    this.dialogVisible = false;
  }

  findEventIndexById(id: number) {
    let index = -1;
    for(let i = 0; i < this.events.length; i++) {
      if(id == this.events[i].id) {
        index = i;
        break;
      }
    }

    return index;
  }


}

export class MyEvent {
  id: number;
  title: string;
  start: string;
  end: Date;
  allDay: boolean = true;
}

/*
export class EventService {

}
*/

/*export class EventService {

  constructor(private http: Http) {}

  getEvents() {
    return this.http.get('showcase/resources/data/scheduleevents.json')
        .toPromise()
        .then(res => <any[]> res.json().data)
        .then(data => { return data; });
  }
}*/
