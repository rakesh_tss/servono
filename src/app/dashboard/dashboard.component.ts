import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {BreadcrumbService} from "../_services/breadcrumb.service";

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  constructor(private breadcrumbService:BreadcrumbService ) { }


  ngOnInit(): void {
    this.breadcrumbService.setBreadcrumb([]);
  }
}
