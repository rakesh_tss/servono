var testing_1 = require('@angular/core/testing');
var profile_mng_component_1 = require('./profile-mng.component');
describe('ProfileMngComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [profile_mng_component_1.ProfileMngComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(profile_mng_component_1.ProfileMngComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=profile-mng.component.spec.js.map