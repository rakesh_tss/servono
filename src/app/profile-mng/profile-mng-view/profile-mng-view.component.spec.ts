import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileMngViewComponent } from './profile-mng-view.component';

describe('ProfileMngViewComponent', () => {
  let component: ProfileMngViewComponent;
  let fixture: ComponentFixture<ProfileMngViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileMngViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileMngViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
