var testing_1 = require('@angular/core/testing');
var profile_mng_view_component_1 = require('./profile-mng-view.component');
describe('ProfileMngViewComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [profile_mng_view_component_1.ProfileMngViewComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(profile_mng_view_component_1.ProfileMngViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=profile-mng-view.component.spec.js.map