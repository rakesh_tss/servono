var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var ProfileMngViewComponent = (function () {
    function ProfileMngViewComponent(httpService, commonService, router, breadcrumbService, activatedRoute) {
        this.httpService = httpService;
        this.commonService = commonService;
        this.router = router;
        this.breadcrumbService = breadcrumbService;
        this.activatedRoute = activatedRoute;
        this.selectedCities = [];
        this.pkId = 0;
        this.userTypeList = [];
        this.profileAccessList = [];
        this.profile = {};
    }
    ProfileMngViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.breadcrumbService.setBreadcrumb([{ label: 'Profile Management', routerLink: ['/Custom/Profiles'] }, { label: 'Update / Create' }]);
        this.activatedRoute.params.subscribe(function (params) {
            _this.pkId = params['Id'];
            _this.getUserList();
        });
        this.getProfileModule();
    };
    ProfileMngViewComponent.prototype.getUserList = function () {
        var _this = this;
        this.httpService.get('DropListData/usertypes/0').subscribe(function (res) {
            if (res) {
                _this.userTypeList = _this.commonService.getDropdownOptions(res.data, 'name', 'pk_id');
            }
        });
    };
    ProfileMngViewComponent.prototype.getProfileModule = function () {
        var _this = this;
        this.httpService.get('GetProfileAccessAddEdit/' + this.pkId).subscribe(function (res) {
            if (res) {
                _this.profileAccessList = res.data.PermissionsData;
                console.log('res.data.ProfileData', res.data.ProfileData);
                if (_this.pkId > 0 && res.data.ProfileData) {
                    _this.profile.Name = res.data.ProfileData.name;
                    _this.profile.user_type_id = res.data.ProfileData.user_type_id;
                    _this.profile.Description = res.data.ProfileData.description;
                    _this.profile.pk_id = res.data.ProfileData.pk_id;
                }
            }
        });
    };
    ProfileMngViewComponent.prototype.saveProfile = function () {
        var _this = this;
        var params = {};
        params.ProfileId = 0;
        if (this.profile.pk_id > 0) {
            params.ProfileId = this.profile.pk_id;
        }
        params.UserTypeId = this.profile.user_type_id;
        params.Permissions = this.profileAccessList;
        params.Name = this.profile.Name;
        params.Description = this.profile.Description;
        console.log('this.profileAccessList', params);
        this.httpService.post('SaveUpdateProfile', params).subscribe(function (res) {
            if (res) {
                _this.router.navigate(['Custom/Profiles']);
            }
        });
    };
    ProfileMngViewComponent.prototype.allChecked = function (evn, key) {
        var _this = this;
        this.profileAccessList.forEach(function (data) {
            if (evn) {
                data[key] = 1;
            }
            else {
                data[key] = 0;
            }
            _this.changeCheckBox(data, key);
            //console.log('data',data);
        });
    };
    ProfileMngViewComponent.prototype.changeCheckBox = function (row, key) {
        if (row[key]) {
            row[key] = 1;
        }
        else {
            row[key] = 0;
        }
        if (key != 'View') {
            row['View'] = 1;
        }
        else if (key == 'View') {
            if (!row[key]) {
                row['Edit'] = 0;
                row['Delete'] = 0;
                row['Create'] = 0;
            }
        }
    };
    ProfileMngViewComponent = __decorate([
        core_1.Component({
            selector: 'app-profile-mng-view',
            templateUrl: './profile-mng-view.component.html',
            styleUrls: ['./profile-mng-view.component.css']
        })
    ], ProfileMngViewComponent);
    return ProfileMngViewComponent;
})();
exports.ProfileMngViewComponent = ProfileMngViewComponent;
//# sourceMappingURL=profile-mng-view.component.js.map