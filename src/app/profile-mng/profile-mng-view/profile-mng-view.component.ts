import { Component, OnInit } from '@angular/core';
import {HttpService} from "../../_services/http.service";
import {BreadcrumbService} from "../../_services/breadcrumb.service";
import {Router, ActivatedRoute, Params} from '@angular/router';
import {CommonService} from "../../_services/common.service";

@Component({
    selector: 'app-profile-mng-view',
    templateUrl: './profile-mng-view.component.html',
    styleUrls: ['./profile-mng-view.component.css']
})
export class ProfileMngViewComponent implements OnInit {

    constructor(private httpService:HttpService,
                private commonService:CommonService,
                private router:Router,
                private breadcrumbService:BreadcrumbService, private activatedRoute:ActivatedRoute) {

    }


    selectedCities:string[] = [];
    pkId=0;
    ngOnInit() {
        this.breadcrumbService.setBreadcrumb([{label:'Profile Management',routerLink:['/Custom/Profiles']},{label:'Update / Create'}]);
        this.activatedRoute.params.subscribe((params: Params) => {
            this.pkId= params['Id'];
            this.getUserList();
        });

        this.getProfileModule();
    }
    userTypeList:any = [];
    getUserList(){
        this.httpService.get('DropListData/usertypes/0').subscribe(
            res=> {
                if(res){
                    this.userTypeList = this.commonService.getDropdownOptions(res.data,'name','pk_id');
                }
            }
        );
    }

    profileAccessList:any = [];
    profile:any = {};

    getProfileModule() {

        this.httpService.get('GetProfileAccessAddEdit/'+this.pkId).subscribe(res=> {
            if (res) {
                this.profileAccessList = res.data.PermissionsData;
                console.log('res.data.ProfileData',res.data.ProfileData);

                if(this.pkId > 0 && res.data.ProfileData){
                    this.profile.Name = res.data.ProfileData.name;
                    this.profile.user_type_id = res.data.ProfileData.user_type_id;
                    this.profile.Description = res.data.ProfileData.description;
                    this.profile.pk_id = res.data.ProfileData.pk_id;
                }

            }
        })

    }

    saveProfile() {
        let params:any = {};
        params.ProfileId = 0;
        if(this.profile.pk_id >0){
            params.ProfileId = this.profile.pk_id;
        }
        params.UserTypeId = this.profile.user_type_id;
        params.Permissions = this.profileAccessList;
        params.Name = this.profile.Name;
        params.Description = this.profile.Description;
        console.log('this.profileAccessList', params);

        this.httpService.post('SaveUpdateProfile', params).subscribe(res=> {
            if (res) {
                this.router.navigate(['Custom/Profiles']);
            }
        })
    }
    allChecked(evn,key){
        this.profileAccessList.forEach((data)=>{
            if(evn){
                data[key] = 1;
            }else{
                data[key] = 0;
            }
            this.changeCheckBox(data,key);
            //console.log('data',data);
        })
    }
    changeCheckBox(row, key) {
        if (row[key]) {
            row[key] = 1;
        } else {
            row[key] = 0;
        }
        if(key != 'View'){
            row['View'] = 1;
        }else if(key =='View'){
            if(!row[key]){
                row['Edit'] = 0;
                row['Delete'] = 0;
                row['Create'] = 0;
                row['Export'] = 0;
                row['Import'] = 0;
            }
        }
    }
}
