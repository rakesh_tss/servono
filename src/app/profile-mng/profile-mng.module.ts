import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProfileMngRoutingModule} from "./profile-mng-routing.module";
import {CommonTssModule} from "../shared/commonTssModules";
import {ProfileMngComponent} from "./profile-mng.component";
import { ProfileMngViewComponent } from './profile-mng-view/profile-mng-view.component';

@NgModule({
  imports: [
    ProfileMngRoutingModule,
    CommonTssModule
  ],
  declarations: [
      ProfileMngComponent,
      ProfileMngViewComponent
  ]
})
export class ProfileMngModule { }
