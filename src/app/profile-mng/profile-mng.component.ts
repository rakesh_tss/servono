import { Component, OnInit } from '@angular/core';
import {LazyLoadEvent} from "primeng/primeng";
import {HttpService} from "../_services/http.service";
import { Router } from '@angular/router';
import {BreadcrumbService} from "../_services/breadcrumb.service";

@Component({
  selector: 'app-profile-mng',
  templateUrl: './profile-mng.component.html',
  styleUrls: ['./profile-mng.component.css']
})
export class ProfileMngComponent implements OnInit {

  displayDialog:boolean;
  dataRows:any[];
  dataRow:any = {};
  totalRecords:number;
  menuFormObj:any = {};

  constructor(private httpService:HttpService, private router:Router,private breadcrumbService:BreadcrumbService) {

  }

  userTypeList:any[] = [{label:'Select',value:null}];
  jqGridData;

  ngOnInit():void {
    this.breadcrumbService.setBreadcrumb([{label:'Profile Management'}]);

    this.httpService.get('DropListData/usertypes/0').subscribe(
        res=> {
          if(res){
            for (let key in res.data) {
              let obj:any = {};
              obj.value = res.data[key].pk_id;
              obj.label = res.data[key].name;
              this.userTypeList.push(obj);
            }
          }
        }
    );
  }

  loadDataLazy(event:LazyLoadEvent) {
    this.httpService.postTable('GetGridList/profileaccess', event).subscribe(
        res=> {
          this.dataRows = res.jqGridData.rows;
          this.totalRecords = res.jqGridData.records;
        }
    );
  }

  dialogTitle:string;

  onCreate(data?:any):void {
    this.dialogTitle = 'Create Menu';
    if(data){
      this.dialogTitle = 'Update Menu';
      this.dataRow = data;
    }else{
      this.dataRow = {};
    }
    this.displayDialog = true;
  }
  onEdit(row):void{
    this.dialogTitle = 'Update Menu';
    this.dataRow = row;
    this.displayDialog = true;
  }
  onSave(row):void {
    this.httpService.post('SaveUpdateProfile',row).subscribe(res=>{
      if(res){
        this.displayDialog = false;
        this.tableReload();
      }
    })
  }

  onView(row):void {
    //this.msgs = [];
    // this.msgs.push({severity:'info', summary:'Success', detail:'Data Updated'});
    this.router.navigate(['',row.app_menu_id]);
  }

  onDelete(row):void {

  }
  refreshTable:boolean=true;
  tableReload(){
    this.refreshTable = false;
    setTimeout(() => this.refreshTable = true, 100);
  }

  dialogClose():void {
    this.displayDialog = false;
  }

}
