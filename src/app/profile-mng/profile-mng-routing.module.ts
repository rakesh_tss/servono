import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProfileMngComponent} from "./profile-mng.component";
import {ProfileMngViewComponent} from "./profile-mng-view/profile-mng-view.component";

const routes: Routes = [
  {
    path: '',
    component: ProfileMngComponent,
    data: {
      title: 'Profile Managment'
    }
  },
  {
    path: ':Id',
    component: ProfileMngViewComponent,
    data: {
      title: 'Profile View'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class ProfileMngRoutingModule { }
