var testing_1 = require('@angular/core/testing');
var roles_mng_component_1 = require('./roles-mng.component');
describe('RolesMngComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [roles_mng_component_1.RolesMngComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(roles_mng_component_1.RolesMngComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=roles-mng.component.spec.js.map