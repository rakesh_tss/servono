import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RolesMngRoutingModule } from './roles-mng-routing.module';
import { RoleCreateComponent } from './role-create/role-create.component';
import {RolesMngComponent} from "./roles-mng.component";
import {CommonTssModule} from "../shared/commonTssModules";
import {OrganizationChartModule} from "primeng/primeng";

@NgModule({
  imports: [
    CommonModule,
    RolesMngRoutingModule,
    CommonTssModule,
    OrganizationChartModule
  ],
  declarations: [RolesMngComponent, RoleCreateComponent]
})
export class RolesMngModule {
}
