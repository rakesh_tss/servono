var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var roles_mng_routing_module_1 = require('./roles-mng-routing.module');
var role_create_component_1 = require('./role-create/role-create.component');
var roles_mng_component_1 = require("./roles-mng.component");
var commonTssModules_1 = require("../shared/commonTssModules");
var primeng_1 = require("primeng/primeng");
var RolesMngModule = (function () {
    function RolesMngModule() {
    }
    RolesMngModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                roles_mng_routing_module_1.RolesMngRoutingModule,
                commonTssModules_1.CommonTssModule,
                primeng_1.TreeModule
            ],
            declarations: [roles_mng_component_1.RolesMngComponent, role_create_component_1.RoleCreateComponent]
        })
    ], RolesMngModule);
    return RolesMngModule;
})();
exports.RolesMngModule = RolesMngModule;
//# sourceMappingURL=roles-mng.module.js.map