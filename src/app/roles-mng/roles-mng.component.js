var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var RolesMngComponent = (function () {
    function RolesMngComponent(breadcrumbService, httpService, activatedRoute, commonService) {
        this.breadcrumbService = breadcrumbService;
        this.httpService = httpService;
        this.activatedRoute = activatedRoute;
        this.commonService = commonService;
        this.userTypeList = [];
    }
    RolesMngComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.breadcrumbService.setBreadcrumb([{ label: 'Roles' }]);
        this.activatedRoute.params.subscribe(function (params) {
            _this.userTypeId = +params['userTypeId']; // (+) converts string 'id' to a number
        });
        this.getUserList();
    };
    RolesMngComponent.prototype.getUserList = function () {
        var _this = this;
        this.httpService.get('DropListData/usertypes/0').subscribe(function (res) {
            if (res) {
                _this.userTypeList = _this.commonService.getDropdownOptions(res.data, 'name', 'pk_id');
                if (_this.userTypeId) {
                    _this.user_type_id = _this.userTypeId;
                    _this.getRoles(_this.userTypeId);
                }
                else {
                    if (_this.userTypeList.length > 0) {
                        console.log('this.userTypeList[0].value', _this.userTypeList);
                        _this.user_type_id = _this.userTypeList[1].value;
                        _this.getRoles(_this.user_type_id);
                    }
                }
            }
        });
    };
    RolesMngComponent.prototype.getRoles = function (userTypeId) {
        var _this = this;
        this.roles = [];
        this.httpService.get('GetRolesTree/' + userTypeId).subscribe(function (res) {
            if (res) {
                _this.roles = res.data;
                _this.expandAll();
            }
        });
    };
    RolesMngComponent.prototype.expandAll = function () {
        var _this = this;
        this.roles.forEach(function (node) {
            _this.expandRecursive(node, true);
        });
    };
    RolesMngComponent.prototype.collapseAll = function () {
        var _this = this;
        this.roles.forEach(function (node) {
            _this.expandRecursive(node, false);
        });
    };
    RolesMngComponent.prototype.expandRecursive = function (node, isExpand) {
        var _this = this;
        if (node.children.length > 0) {
            node.expanded = isExpand;
            node.children.forEach(function (childNode) {
                _this.expandRecursive(childNode, isExpand);
            });
        }
    };
    RolesMngComponent = __decorate([
        core_1.Component({
            selector: 'app-roles-mng',
            templateUrl: './roles-mng.component.html',
            styleUrls: ['./roles-mng.component.css']
        })
    ], RolesMngComponent);
    return RolesMngComponent;
})();
exports.RolesMngComponent = RolesMngComponent;
//# sourceMappingURL=roles-mng.component.js.map