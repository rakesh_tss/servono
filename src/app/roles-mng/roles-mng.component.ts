import { Component, OnInit,ElementRef,ViewChild } from '@angular/core';
import {BreadcrumbService} from "../_services/breadcrumb.service";
import {TreeNode} from "primeng/primeng";
import {HttpService} from "../_services/http.service";
import {CommonService} from "../_services/common.service";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-roles-mng',
  templateUrl: './roles-mng.component.html',
  styleUrls: ['./roles-mng.component.css']
})
export class RolesMngComponent implements OnInit {

  constructor(private breadcrumbService:BreadcrumbService,
              private httpService:HttpService,
              private activatedRoute:ActivatedRoute,
              private commonService:CommonService) {
  }

  roles:any[];
  userTypeId:Number;


  data2:any[];
  @ViewChild('customTree') mainDiv: ElementRef;
  isSpecial:boolean=true;
  myHeight:any;
  myWidth:any;
  setStyles(){
    this.myHeight = window.innerHeight;
    this.myWidth = window.innerWidth;
    let styles={
      height:this.isSpecial ? this.myHeight-190+'px':'auto',
      /*position:this.isSpecial? 'static':'static'*/
    }
    /*console.log(styles);*/
    return styles;
  }
  ngOnInit() {
    this.breadcrumbService.setBreadcrumb([{label: 'Roles'}]);
    this.activatedRoute.params.subscribe(params => {
      this.userTypeId = +params['userTypeId']; // (+) converts string 'id' to a number
    });

    this.getUserList();


    this.data2 = [{
      label2: 'F.C Barcelona',
      children: [
        {
          label2: 'F.C Barcelona',
          children: [
            {
              label: 'Chelsea FC',
              children: [
                {
                  label: 'Bayern Munich'
                },
                {
                  label: 'Real Madrid'
                }
              ]
            }
          ]
        }
      ]
    }];
  }

  userTypeList:any = [];
  user_type_id:Number;

  getUserList() {
    this.httpService.get('DropListData/usertypes/0').subscribe(
      res=> {
        if (res) {
          this.userTypeList = this.commonService.getDropdownOptions(res.data, 'name', 'pk_id');
          if (this.userTypeId) {
            this.user_type_id = this.userTypeId;
            this.getRoles(this.userTypeId);
          } else {
            if (this.userTypeList.length > 0) {
              console.log('this.userTypeList[0].value', this.userTypeList);
              this.user_type_id = this.userTypeList[1].value;
              this.getRoles(this.user_type_id);
            }
          }
        }
      }
    );
  }
  showBranches:boolean = false;
  getRoles(userTypeId) {
    this.showBranches = false;
    this.roles = [];
    this.httpService.get('GetRolesTree/' + userTypeId).subscribe(res=> {
      if (res) {
        this.roles = res.data;
          console.log('this.roles',this.roles);
          this.showBranches = true;
        this.expandAll();
      }
    })
  }

  expandAll() {
    this.roles.forEach(node => {
      this.expandRecursive(node, true);
    });
  }

  collapseAll() {
    this.roles.forEach(node => {
      this.expandRecursive(node, false);
    });
  }

  private expandRecursive(node:TreeNode, isExpand:boolean) {

    if (node.children.length > 0) {
      node.expanded = isExpand;
      node.children.forEach(childNode => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }

}
