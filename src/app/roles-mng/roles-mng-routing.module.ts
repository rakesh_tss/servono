import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RolesMngComponent} from "./roles-mng.component";
import {RoleCreateComponent} from "./role-create/role-create.component";

const routes: Routes = [
  {
    path:'',
    component:RolesMngComponent
  },
  {
    path:':userTypeId',
    component:RolesMngComponent
  },
  {
    path:':Id/:userTypeId/:parentRoleId',
    component:RoleCreateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class RolesMngRoutingModule { }
