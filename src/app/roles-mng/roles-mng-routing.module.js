var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var roles_mng_component_1 = require("./roles-mng.component");
var role_create_component_1 = require("./role-create/role-create.component");
var routes = [
    {
        path: '',
        component: roles_mng_component_1.RolesMngComponent
    },
    {
        path: ':userTypeId',
        component: roles_mng_component_1.RolesMngComponent
    },
    {
        path: ':Id/:userTypeId/:parentRoleId',
        component: role_create_component_1.RoleCreateComponent
    }
];
var RolesMngRoutingModule = (function () {
    function RolesMngRoutingModule() {
    }
    RolesMngRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule],
            providers: []
        })
    ], RolesMngRoutingModule);
    return RolesMngRoutingModule;
})();
exports.RolesMngRoutingModule = RolesMngRoutingModule;
//# sourceMappingURL=roles-mng-routing.module.js.map