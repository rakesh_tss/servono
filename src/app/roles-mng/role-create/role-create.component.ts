import { Component, OnInit } from '@angular/core';
import {BreadcrumbService} from "../../_services/breadcrumb.service";
import {Router, ActivatedRoute, Params} from '@angular/router';
import {CommonService} from "../../_services/common.service";
import {HttpService} from "../../_services/http.service";

@Component({
  selector: 'app-role-create',
  templateUrl: './role-create.component.html',
  styleUrls: ['./role-create.component.css']
})
export class RoleCreateComponent implements OnInit {

  constructor(private breadcrumbService:BreadcrumbService,
              private commonService:CommonService,
              private httpService:HttpService,
              private router:Router,
              private activatedRoute:ActivatedRoute) { }

  pkId:Number;
  userTypeId:Number;
  parentRoleId:Number;
  ngOnInit() {
    this.breadcrumbService.setBreadcrumb([{label:'Roles',routerLink:['/Custom/Roles']},{label:'Create'}]);
    this.activatedRoute.params.subscribe((params: Params) => {
      this.pkId= params['Id'];
      this.userTypeId= params['userTypeId'];
      this.parentRoleId= params['parentRoleId'];
      //this.getRoleDetails();
      if(this.pkId > 0){
        this.breadcrumbService.setBreadcrumb([{label:'Roles',routerLink:['/Custom/Roles']},{label:'Update'}]);
      }
      this.getMenuList();
      this.getProfileList();
    });

  }
  roleDetail:any ={};
  getRoleDetails(){
    this.httpService.get('GetRoleDetails/'+this.pkId+'/'+this.userTypeId+'/'+this.parentRoleId).subscribe(res=>{
      if(res){
        this.roleDetail = res.data ;
        if(!this.roleDetail.can_assign_records_to){
          this.roleDetail.can_assign_records_to = 1 ;
        }

        this.roleDetail.privileges = 1;
        this.getModulePermission();
        if(res.data.profiles){
          if(res.data.profiles.length >0){
            this.roleDetail.privileges = 2;
          }
        }
      }
    })
  }
  menuDropdownList:any = [];
  getMenuList(){
    this.httpService.get('DropListData/menus/'+this.userTypeId).subscribe(res=>{
      if(res){
         this.menuDropdownList = this.commonService.getDropdownOptions(res.data,'name','pk_id') ;
      }
    })

  }
  profileDropdownList:any = [];
  profileDropdownList2:any = [];
  getProfileList(){
    this.httpService.get('DropListData/profileaccess/'+this.userTypeId).subscribe(res=>{
      if(res){
        this.profileDropdownList = this.commonService.getDropdownOptions(res.data,'name','pk_id') ;
        this.profileDropdownList2 = this.commonService.getDropdownOptions(res.data,'name','pk_id',true) ;
        this.getRoleDetails();
      }
    })
  }
  profileAccessList:any = [];
  getModulePermission(copy_profile_id?:number){
    let type = 'Roles';
    let id = this.pkId;
    if(copy_profile_id){
      type = 'profile'
      id = copy_profile_id;
    }
    this.httpService.get('/GetAccessDetailsOfRoleOrProfiles/'+id+'/'+type).subscribe(res=>{
      if(res){
          this.roleDetail.Permissions = res.data.PermissionsData;
      }
    })
  }
  showProfileaccessTable(ids:any){
    let stringIds ;
    //Object.assign([], ids,stringIds);
    //console.log('stringIds',stringIds.toString());
    this.getModulePermission(stringIds.toString());
    this.profileaccessTable = true;
  }
  allChecked(evn,key){
    this.roleDetail.Permissions.forEach((data)=>{
      if(evn){
        data[key] = 1;
      }else{
        data[key] = 0;
      }
      this.changeCheckBox(data,key);
      //console.log('data',data);
    })
  }
  profileaccessTable:boolean = false;
  changeCheckBox(row, key) {
    if (row[key]) {
      row[key] = 1;
    } else {
      row[key] = 0;
    }
    if(key != 'View'){
      row['View'] = 1;
    }else if(key =='View'){
      if(!row[key]){
        row['Edit'] = 0;
        row['Delete'] = 0;
        row['Create'] = 0;
        row['Export'] = 0;
        row['Import'] = 0;
      }
    }
  }

  saveUpdateRole(){
    if(this.roleDetail.privileges==1){
      delete this.roleDetail.profiles;
    }else{
      delete this.roleDetail.Permissions;
    }
    this.httpService.post('SaveUpdateRole',this.roleDetail).subscribe(res=>{
      if(res){
        this.router.navigate(['Custom/Roles']);
      }
    })
  }

}
