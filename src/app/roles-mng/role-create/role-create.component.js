var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var RoleCreateComponent = (function () {
    function RoleCreateComponent(breadcrumbService, commonService, httpService, router, activatedRoute) {
        this.breadcrumbService = breadcrumbService;
        this.commonService = commonService;
        this.httpService = httpService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.roleDetail = {};
        this.menuDropdownList = [];
        this.profileDropdownList = [];
        this.profileDropdownList2 = [];
        this.profileAccessList = [];
        this.profileaccessTable = false;
    }
    RoleCreateComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.breadcrumbService.setBreadcrumb([{ label: 'Roles', routerLink: ['/Custom/Roles'] }, { label: 'Create' }]);
        this.activatedRoute.params.subscribe(function (params) {
            _this.pkId = params['Id'];
            _this.userTypeId = params['userTypeId'];
            _this.parentRoleId = params['parentRoleId'];
            _this.getRoleDetails();
            _this.getMenuList();
            _this.getProfileList();
        });
    };
    RoleCreateComponent.prototype.getRoleDetails = function () {
        var _this = this;
        this.httpService.get('GetRoleDetails/' + this.pkId + '/' + this.userTypeId + '/' + this.parentRoleId).subscribe(function (res) {
            if (res) {
                _this.roleDetail = res.data;
                if (!_this.roleDetail.can_assign_records_to) {
                    _this.roleDetail.can_assign_records_to = 1;
                }
                _this.roleDetail.privileges = 1;
                _this.getModulePermission();
                if (res.data.profiles) {
                    if (res.data.profiles.length > 0) {
                        _this.roleDetail.privileges = 2;
                    }
                }
            }
        });
    };
    RoleCreateComponent.prototype.getMenuList = function () {
        var _this = this;
        this.httpService.get('DropListData/menus/' + this.userTypeId).subscribe(function (res) {
            if (res) {
                _this.menuDropdownList = _this.commonService.getDropdownOptions(res.data, 'name', 'pk_id');
            }
        });
    };
    RoleCreateComponent.prototype.getProfileList = function () {
        var _this = this;
        this.httpService.get('DropListData/profileaccess/' + this.userTypeId).subscribe(function (res) {
            if (res) {
                _this.profileDropdownList = _this.commonService.getDropdownOptions(res.data, 'name', 'pk_id');
                _this.profileDropdownList2 = _this.commonService.getDropdownOptions(res.data, 'name', 'pk_id', true);
            }
        });
    };
    RoleCreateComponent.prototype.getModulePermission = function (copy_profile_id) {
        var _this = this;
        var type = 'Roles';
        var id = this.pkId;
        if (copy_profile_id) {
            type = 'profile';
            id = copy_profile_id;
        }
        this.httpService.get('/GetAccessDetailsOfRoleOrProfiles/' + id + '/' + type).subscribe(function (res) {
            if (res) {
                _this.roleDetail.Permissions = res.data.PermissionsData;
            }
        });
    };
    RoleCreateComponent.prototype.showProfileaccessTable = function (ids) {
        var stringIds;
        //Object.assign([], ids,stringIds);
        //console.log('stringIds',stringIds.toString());
        this.getModulePermission(stringIds.toString());
        this.profileaccessTable = true;
    };
    RoleCreateComponent.prototype.allChecked = function (evn, key) {
        var _this = this;
        this.roleDetail.Permissions.forEach(function (data) {
            if (evn) {
                data[key] = 1;
            }
            else {
                data[key] = 0;
            }
            _this.changeCheckBox(data, key);
            //console.log('data',data);
        });
    };
    RoleCreateComponent.prototype.changeCheckBox = function (row, key) {
        if (row[key]) {
            row[key] = 1;
        }
        else {
            row[key] = 0;
        }
        if (key != 'View') {
            row['View'] = 1;
        }
        else if (key == 'View') {
            if (!row[key]) {
                row['Edit'] = 0;
                row['Delete'] = 0;
                row['Create'] = 0;
            }
        }
    };
    RoleCreateComponent.prototype.saveUpdateRole = function () {
        var _this = this;
        if (this.roleDetail.privileges == 1) {
            delete this.roleDetail.profiles;
        }
        else {
            delete this.roleDetail.Permissions;
        }
        this.httpService.post('SaveUpdateRole', this.roleDetail).subscribe(function (res) {
            if (res) {
                _this.router.navigate(['Custom/Roles']);
            }
        });
    };
    RoleCreateComponent = __decorate([
        core_1.Component({
            selector: 'app-role-create',
            templateUrl: './role-create.component.html',
            styleUrls: ['./role-create.component.css']
        })
    ], RoleCreateComponent);
    return RoleCreateComponent;
})();
exports.RoleCreateComponent = RoleCreateComponent;
//# sourceMappingURL=role-create.component.js.map