var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var user_groups_routing_module_1 = require('./user-groups-routing.module');
var user_groups_component_1 = require("./user-groups.component");
var commonTssModules_1 = require("../shared/commonTssModules");
var user_group_create_component_1 = require('./user-group-create/user-group-create.component');
var UserGroupsModule = (function () {
    function UserGroupsModule() {
    }
    UserGroupsModule = __decorate([
        core_1.NgModule({
            imports: [
                commonTssModules_1.CommonTssModule,
                user_groups_routing_module_1.UserGroupsRoutingModule
            ],
            declarations: [user_groups_component_1.UserGroupsComponent, user_group_create_component_1.UserGroupCreateComponent]
        })
    ], UserGroupsModule);
    return UserGroupsModule;
})();
exports.UserGroupsModule = UserGroupsModule;
//# sourceMappingURL=user-groups.module.js.map