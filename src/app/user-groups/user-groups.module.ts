import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserGroupsRoutingModule } from './user-groups-routing.module';
import {UserGroupsComponent} from "./user-groups.component";
import {CommonTssModule} from "../shared/commonTssModules";
import { UserGroupCreateComponent } from './user-group-create/user-group-create.component';

@NgModule({
  imports: [
    CommonTssModule,
    UserGroupsRoutingModule
  ],
  declarations: [UserGroupsComponent, UserGroupCreateComponent]
})
export class UserGroupsModule { }
