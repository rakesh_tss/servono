var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var UserGroupsComponent = (function () {
    function UserGroupsComponent(httpService, confirmationService, breadcrumbService, commonService, router) {
        this.httpService = httpService;
        this.confirmationService = confirmationService;
        this.breadcrumbService = breadcrumbService;
        this.commonService = commonService;
        this.router = router;
        this.dataRow = {};
        this.menuFormObj = {};
        this.userTypeList = [{ label: 'Select', value: null }];
        this.refreshTable = true;
    }
    UserGroupsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.breadcrumbService.setBreadcrumb([{ label: 'User Groups' }]);
        this.httpService.get('DropListData/usertypes/0').subscribe(function (res) {
            if (res) {
                for (var key in res.data) {
                    var obj = {};
                    obj.value = res.data[key].pk_id;
                    obj.label = res.data[key].name;
                    _this.userTypeList.push(obj);
                }
            }
        });
    };
    UserGroupsComponent.prototype.loadDataLazy = function (event) {
        var _this = this;
        this.httpService.postTable('GetGridList/UserGroups', event).subscribe(function (res) {
            _this.dataRows = res.jqGridData.rows;
            _this.totalRecords = res.jqGridData.records;
        });
    };
    UserGroupsComponent.prototype.onCreate = function (data) {
        this.dialogTitle = 'Create Menu';
        if (data) {
            this.dialogTitle = 'Update Menu';
            this.dataRow = data;
        }
        else {
            this.dataRow = {};
        }
        this.displayDialog = true;
    };
    UserGroupsComponent.prototype.onEdit = function (row) {
        this.dialogTitle = 'Update Menu';
        this.dataRow = row;
        this.displayDialog = true;
    };
    UserGroupsComponent.prototype.onSave = function (row) {
        var _this = this;
        this.httpService.post('SaveUpdateUserGroup', row).subscribe(function (res) {
            if (res) {
                _this.displayDialog = false;
                _this.tableReload();
            }
        });
    };
    UserGroupsComponent.prototype.onView = function (row) {
        //this.msgs = [];
        // this.msgs.push({severity:'info', summary:'Success', detail:'Data Updated'});
        this.router.navigate(['', row.app_menu_id]);
    };
    UserGroupsComponent.prototype.onDelete = function (row) {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.httpService.post('RemoveUserGroup/' + row.pk_id, {}).subscribe(function (res) {
                    if (res) {
                        _this.tableReload();
                    }
                });
            }
        });
    };
    UserGroupsComponent.prototype.tableReload = function () {
        var _this = this;
        this.refreshTable = false;
        setTimeout(function () { return _this.refreshTable = true; }, 100);
    };
    UserGroupsComponent.prototype.dialogClose = function () {
        this.displayDialog = false;
    };
    UserGroupsComponent = __decorate([
        core_1.Component({
            selector: 'app-user-groups',
            templateUrl: './user-groups.component.html',
            styleUrls: ['./user-groups.component.css']
        })
    ], UserGroupsComponent);
    return UserGroupsComponent;
})();
exports.UserGroupsComponent = UserGroupsComponent;
//# sourceMappingURL=user-groups.component.js.map