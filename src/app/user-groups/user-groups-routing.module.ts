import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserGroupsComponent} from "./user-groups.component";
import {UserGroupCreateComponent} from "./user-group-create/user-group-create.component";

const routes: Routes = [
  {
    path:'',
    component:UserGroupsComponent
  },

  {
    path:':id',
    component:UserGroupCreateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class UserGroupsRoutingModule { }
