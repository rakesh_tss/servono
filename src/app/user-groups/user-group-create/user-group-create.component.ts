import { Component, OnInit } from '@angular/core';
import {LazyLoadEvent} from "primeng/primeng";
import {HttpService} from "../../_services/http.service";
import {Router, ActivatedRoute, Params} from '@angular/router';
import {CommonService} from "../../_services/common.service";
import {ConfirmationService} from "primeng/primeng";

@Component({
    selector: 'app-user-group-create',
    templateUrl: './user-group-create.component.html',
    styleUrls: ['./user-group-create.component.scss']
})
export class UserGroupCreateComponent implements OnInit {

    constructor(private httpService:HttpService,
                private confirmationService:ConfirmationService,
                private commonService:CommonService,
                private activatedRoute:ActivatedRoute) {
    }

    dataRows:any[];
    dataRow:any = {};
    totalRecords:number;
    menuFormObj:any = {};
    pkId:number = 0;

    ngOnInit() {
        this.activatedRoute.params.subscribe((params:Params) => {
            this.pkId = params['id'];
            this.getUserGroupDetail();
        });
    }

    userGroupDetail:any = {};

    getUserGroupDetail() {
        this.httpService.get('GetUserGroupById/' + this.pkId).subscribe(res=> {
            if (res) {
                this.userGroupDetail = res.data;
            }
        })
    }

    loadDataLazy(event:LazyLoadEvent) {
        let params:any = {};
        params = event;
        params.UserGroupId = this.pkId;
        this.httpService.postTable('GetGridList/usergroupmembers', params).subscribe(
            res=> {
                this.dataRows = res.jqGridData.rows;
                this.totalRecords = res.jqGridData.records;
            }
        );
    }

    refreshTable:boolean = true;

    tableReload() {
        this.refreshTable = false;
        setTimeout(() => this.refreshTable = true, 100);
    }

    umDialog:boolean = false;
    usergroupsList:any = [];
    ugMember:any = {};

    addUserGroupMembers() {
        this.umDialog = true;
        this.ugMember = {};
        this.ugMember.list = [];
        this.getDropdownData('group member types', '0').then(res=> {
            this.usergroupsList = this.commonService.getDropdownOptions(res, 'name', 'pk_id');
        });
    }

    groupMembersList:any = [];

    usergroupChange(groupId) {
        this.groupMembersList = [];
        this.ugMember.list = [];
        this.httpService.get('GetGroupObjectsByType/' + groupId + '/' + this.pkId).subscribe(res=> {
            if (res) {
                this.groupMembersList = this.commonService.getDropdownOptions(res.data, 'name', 'pk_id', true);
            }
        })
    }

    saveUserGroupMember() {
        this.httpService.post('MapUserGroupMembers/' + this.pkId + '/' + this.ugMember.user_group_id, this.ugMember).subscribe(res=> {
            if (res) {
                this.umDialog = false;
                this.tableReload();

            }
        })
    }
    selectedUserGroupMembers:any = [];
    removeUserGroupMembers(row) {
        let ids:any = [];
        this.selectedUserGroupMembers.forEach((item)=>{
            ids.push(item.pk_id);
        })
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
                this.httpService.post('RemoveUserGroupMembers', {list:ids}).subscribe(res=> {
                    if (res) {
                        this.tableReload();
                    }
                })

            }
        });
    }

    getDropdownData(type:string, userType:string) {
        return this.httpService.get('DropListData/' + type + '/' + userType)
            .toPromise()
            .then(res => res.data);
    }

}
