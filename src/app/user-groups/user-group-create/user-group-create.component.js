var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var UserGroupCreateComponent = (function () {
    function UserGroupCreateComponent(httpService, confirmationService, commonService, activatedRoute) {
        this.httpService = httpService;
        this.confirmationService = confirmationService;
        this.commonService = commonService;
        this.activatedRoute = activatedRoute;
        this.dataRow = {};
        this.menuFormObj = {};
        this.pkId = 0;
        this.userGroupDetail = {};
        this.refreshTable = true;
        this.umDialog = false;
        this.usergroupsList = [];
        this.ugMember = {};
        this.groupMembersList = [];
        this.selectedUserGroupMembers = [];
    }
    UserGroupCreateComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            _this.pkId = params['id'];
            _this.getUserGroupDetail();
        });
    };
    UserGroupCreateComponent.prototype.getUserGroupDetail = function () {
        var _this = this;
        this.httpService.get('GetUserGroupById/' + this.pkId).subscribe(function (res) {
            if (res) {
                _this.userGroupDetail = res.data;
            }
        });
    };
    UserGroupCreateComponent.prototype.loadDataLazy = function (event) {
        var _this = this;
        var params = {};
        params = event;
        params.UserGroupId = this.pkId;
        this.httpService.postTable('GetGridList/usergroupmembers', params).subscribe(function (res) {
            _this.dataRows = res.jqGridData.rows;
            _this.totalRecords = res.jqGridData.records;
        });
    };
    UserGroupCreateComponent.prototype.tableReload = function () {
        var _this = this;
        this.refreshTable = false;
        setTimeout(function () { return _this.refreshTable = true; }, 100);
    };
    UserGroupCreateComponent.prototype.addUserGroupMembers = function () {
        var _this = this;
        this.umDialog = true;
        this.ugMember = {};
        this.ugMember.list = [];
        this.getDropdownData('group member types', '0').then(function (res) {
            _this.usergroupsList = _this.commonService.getDropdownOptions(res, 'name', 'pk_id');
        });
    };
    UserGroupCreateComponent.prototype.usergroupChange = function (groupId) {
        var _this = this;
        this.groupMembersList = [];
        this.ugMember.list = [];
        this.httpService.get('GetGroupObjectsByType/' + groupId + '/' + this.pkId).subscribe(function (res) {
            if (res) {
                _this.groupMembersList = _this.commonService.getDropdownOptions(res.data, 'name', 'pk_id', true);
            }
        });
    };
    UserGroupCreateComponent.prototype.saveUserGroupMember = function () {
        var _this = this;
        this.httpService.post('MapUserGroupMembers/' + this.pkId + '/' + this.ugMember.user_group_id, this.ugMember).subscribe(function (res) {
            if (res) {
                _this.umDialog = false;
                _this.tableReload();
            }
        });
    };
    UserGroupCreateComponent.prototype.removeUserGroupMembers = function (row) {
        var _this = this;
        var ids = [];
        this.selectedUserGroupMembers.forEach(function (item) {
            ids.push(item.pk_id);
        });
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: function () {
                _this.httpService.post('RemoveUserGroupMembers', { list: ids }).subscribe(function (res) {
                    if (res) {
                        _this.tableReload();
                    }
                });
            }
        });
    };
    UserGroupCreateComponent.prototype.getDropdownData = function (type, userType) {
        return this.httpService.get('DropListData/' + type + '/' + userType)
            .toPromise()
            .then(function (res) { return res.data; });
    };
    UserGroupCreateComponent = __decorate([
        core_1.Component({
            selector: 'app-user-group-create',
            templateUrl: './user-group-create.component.html',
            styleUrls: ['./user-group-create.component.css']
        })
    ], UserGroupCreateComponent);
    return UserGroupCreateComponent;
})();
exports.UserGroupCreateComponent = UserGroupCreateComponent;
//# sourceMappingURL=user-group-create.component.js.map