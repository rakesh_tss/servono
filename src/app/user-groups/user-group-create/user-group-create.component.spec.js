var testing_1 = require('@angular/core/testing');
var user_group_create_component_1 = require('./user-group-create.component');
describe('UserGroupCreateComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [user_group_create_component_1.UserGroupCreateComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(user_group_create_component_1.UserGroupCreateComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=user-group-create.component.spec.js.map