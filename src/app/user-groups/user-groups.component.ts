import { Component, OnInit } from '@angular/core';
import {LazyLoadEvent} from "primeng/primeng";
import {HttpService} from "../_services/http.service";
import {CommonService} from "../_services/common.service";
import { Router } from '@angular/router';
import {BreadcrumbService} from "../_services/breadcrumb.service";
import {ConfirmationService} from "primeng/primeng";

@Component({
  selector: 'app-user-groups',
  templateUrl: './user-groups.component.html',
  styleUrls: ['./user-groups.component.css']
})
export class UserGroupsComponent implements OnInit {


  displayDialog:boolean;
  dataRows:any[];
  dataRow:any = {};
  totalRecords:number;
  menuFormObj:any = {};

  constructor(private httpService:HttpService,
              private confirmationService:ConfirmationService,
              private breadcrumbService:BreadcrumbService,
              private commonService:CommonService, private router:Router) {

  }

  userTypeList:any[] = [{label:'Select',value:null}];
  jqGridData;

  ngOnInit():void {
    this.breadcrumbService.setBreadcrumb([{label:'User Groups'}]);

    this.httpService.get('DropListData/usertypes/0').subscribe(
        res=> {
          if(res){
            for (let key in res.data) {
              let obj:any = {};
              obj.value = res.data[key].pk_id;
              obj.label = res.data[key].name;
              this.userTypeList.push(obj);
            }
          }
        }
    );
  }

  loadDataLazy(event:LazyLoadEvent) {
    this.httpService.postTable('GetGridList/UserGroups', event).subscribe(
        res=> {

          this.dataRows = res.jqGridData.rows;
          this.totalRecords = res.jqGridData.records;
        }
    );
  }

  dialogTitle:string;

  onCreate(data?:any):void {
    this.dialogTitle = 'Create Menu';
    if(data){
      this.dialogTitle = 'Update Menu';
      this.dataRow = data;
    }else{
      this.dataRow = {};
    }
    this.displayDialog = true;
  }
  onEdit(row):void{
    this.dialogTitle = 'Update Menu';
    this.dataRow = row;
    this.displayDialog = true;
  }
  onSave(row):void {
    this.httpService.post('SaveUpdateUserGroup',row).subscribe(res=>{
      if(res){
        this.displayDialog = false;
        this.tableReload();
      }
    })
  }

  onView(row):void {
    //this.msgs = [];
    // this.msgs.push({severity:'info', summary:'Success', detail:'Data Updated'});
    this.router.navigate(['',row.app_menu_id]);
  }

  onDelete(row):void {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      header: 'Delete Confirmation',
      icon: 'fa fa-trash',
      accept: () => {
        this.httpService.post('RemoveUserGroup/'+row.pk_id,{}).subscribe(res=>{
          if(res){
            this.tableReload();
          }
        })

      }
    });
  }
  refreshTable:boolean=true;
  tableReload(){
    this.refreshTable = false;
    setTimeout(() => this.refreshTable = true, 100);
  }

  dialogClose():void {
    this.displayDialog = false;
  }
}
