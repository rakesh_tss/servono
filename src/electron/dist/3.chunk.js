webpackJsonp([3,13],{

/***/ 1023:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuCreationViewComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MenuCreationViewComponent = (function () {
    function MenuCreationViewComponent() {
        this.moduleList = [];
        this.module_name = '';
        this.menu_name = '';
    }
    MenuCreationViewComponent.prototype.ngOnInit = function () {
    };
    MenuCreationViewComponent.prototype.createNewModule = function (moduleName) {
        var obj = {};
        obj.module_name = moduleName;
        obj.module_menu = [];
        this.moduleList.push(obj);
        this.module_name = '';
        console.log('obj', obj);
        console.log('this.moduleList', this.moduleList);
    };
    MenuCreationViewComponent.prototype.createModuleMenu = function (module, menuName) {
        var obj = {};
        obj.menu_name = menuName;
        obj.sub_menu = [];
        this.menu_name = '';
        module.module_menu.push(obj);
        console.log('obj', obj);
        console.log('module.module_menu', module.module_menu);
    };
    MenuCreationViewComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-menu-creation-view',
            template: __webpack_require__(1077),
            styles: [__webpack_require__(1054)]
        }), 
        __metadata('design:paramtypes', [])
    ], MenuCreationViewComponent);
    return MenuCreationViewComponent;
}());
//# sourceMappingURL=menu-creation-view.component.js.map

/***/ }),

/***/ 1024:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_http_service__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_common_service__ = __webpack_require__(1035);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(19);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuManagementComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MenuManagementComponent = (function () {
    function MenuManagementComponent(httpService, commonService, router) {
        this.httpService = httpService;
        this.commonService = commonService;
        this.router = router;
        this.dataRow = {};
        this.menuFormObj = {};
        this.msgs = [];
        this.userTypeList = [];
    }
    MenuManagementComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.commonService.getDropdown('user types').subscribe(function (res) {
            for (var key in res.data) {
                var obj = {};
                obj.value = res.data[key].app_user_type_id;
                obj.label = res.data[key].user_type;
                console.log('obj', obj);
                _this.userTypeList.push(obj);
            }
        });
    };
    MenuManagementComponent.prototype.loadDataLazy = function (event) {
        var _this = this;
        console.log('event', event);
        this.httpService.getTable('getDataListView/Default/Settings/Menu', event).subscribe(function (res) {
            _this.dataRows = res.jqGridData.rows;
            _this.totalRecords = res.jqGridData.records;
        });
    };
    MenuManagementComponent.prototype.onCreate = function () {
        this.dialogTitle = 'Create Menu';
        this.dataRow = {};
        this.displayDialog = true;
    };
    MenuManagementComponent.prototype.onUpdate = function (row) {
        this.dialogTitle = 'Update Menu';
        console.log('row', row);
        this.dataRow = row;
        this.displayDialog = true;
    };
    MenuManagementComponent.prototype.onView = function (row) {
        //this.msgs = [];
        // this.msgs.push({severity:'info', summary:'Success', detail:'Data Updated'});
        this.router.navigate(['/Settings/Menus/Edit/' + row.app_menu_id]);
    };
    MenuManagementComponent.prototype.onDelete = function (row) {
    };
    MenuManagementComponent.prototype.dialogClose = function () {
        this.displayDialog = false;
    };
    MenuManagementComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app--menu-management',
            template: __webpack_require__(1078),
            styles: [__webpack_require__(1055)],
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_common_service__["a" /* CommonService */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_http_service__["a" /* HttpService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__services_http_service__["a" /* HttpService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_common_service__["a" /* CommonService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__services_common_service__["a" /* CommonService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["Router"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_router__["Router"]) === 'function' && _c) || Object])
    ], MenuManagementComponent);
    return MenuManagementComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=menu-management.component.js.map

/***/ }),

/***/ 1025:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SettingsComponent = (function () {
    function SettingsComponent() {
    }
    SettingsComponent.prototype.ngOnInit = function () {
    };
    SettingsComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-settings',
            template: __webpack_require__(1079),
            styles: [__webpack_require__(1056)]
        }), 
        __metadata('design:paramtypes', [])
    ], SettingsComponent);
    return SettingsComponent;
}());
//# sourceMappingURL=settings.component.js.map

/***/ }),

/***/ 1035:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__http_service__ = __webpack_require__(175);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommonService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CommonService = (function () {
    function CommonService(httpService) {
        this.httpService = httpService;
    }
    /**
     *
     * @param type user
     * @returns {*}
     */
    CommonService.prototype.getDropdown = function (type) {
        return this.httpService.get('GetDropdownData/' + type);
        //return this.http.get('http://192.168.0.67:8080/DynamicServices/testSession',this.jwt()).map((response: Response) => response.json());
    };
    CommonService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__http_service__["a" /* HttpService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__http_service__["a" /* HttpService */]) === 'function' && _a) || Object])
    ], CommonService);
    return CommonService;
    var _a;
}());
//# sourceMappingURL=common.service.js.map

/***/ }),

/***/ 1043:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__menu_management_component__ = __webpack_require__(1024);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__settings_component__ = __webpack_require__(1025);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__menu_creation_view_component__ = __webpack_require__(1023);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsRoutingModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var routes = [{
        path: '',
        data: {
            title: 'Settings'
        },
        children: [
            {
                path: 'Settings',
                component: __WEBPACK_IMPORTED_MODULE_3__settings_component__["a" /* SettingsComponent */],
                data: {
                    title: 'Settings'
                }
            },
            {
                path: 'Menus',
                children: [
                    {
                        path: '',
                        component: __WEBPACK_IMPORTED_MODULE_2__menu_management_component__["a" /* MenuManagementComponent */],
                        data: {
                            title: 'Menu'
                        }
                    },
                    {
                        path: 'Edit/:menuId',
                        component: __WEBPACK_IMPORTED_MODULE_4__menu_creation_view_component__["a" /* MenuCreationViewComponent */],
                        data: {
                            title: 'Edit'
                        }
                    }
                ]
            }
        ]
    }];
var SettingsRoutingModule = (function () {
    function SettingsRoutingModule() {
    }
    SettingsRoutingModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"]],
            providers: []
        }), 
        __metadata('design:paramtypes', [])
    ], SettingsRoutingModule);
    return SettingsRoutingModule;
}());
//# sourceMappingURL=settings-routing.module.js.map

/***/ }),

/***/ 1054:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(72)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 1055:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(72)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 1056:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(72)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 1077:
/***/ (function(module, exports) {

module.exports = "<p-panel>\r\n    <p-header>\r\n        <div class=\"ui-helper-clearfix\" style=\"float:right\">\r\n            <input type=\"text\" [(ngModel)]=\"module_name\" placeholder=\"Enter Module Name\"/>\r\n            <button type=\"button\" pButton icon=\"fa-plus\" (click)=\"createNewModule(module_name)\"\r\n                    label=\"Add Module\"></button>\r\n        </div>\r\n    </p-header>\r\n    <p-tabView class=\"row\">\r\n        <p-tabPanel header=\"Module 1\" [closable]=\"true\">\r\n            sub menu here\r\n            sub menu here\r\n            sub menu here sub menu here\r\n            sub menu here sub menu here\r\n            sub menu here\r\n        </p-tabPanel>\r\n        <p-tabPanel *ngFor=\"let m of moduleList; let ndx = index;\" header=\"{{m.module_name}}\" [closable]=\"true\">\r\n\r\n            <p-panel class=\"ui-grid-col-6\">\r\n                <p-header>\r\n                    <div class=\"ui-helper-clearfix\" style=\"float:right\">\r\n                        <input type=\"text\" [(ngModel)]=\"menu_name\" placeholder=\"Add Menu\"/>\r\n                        <button type=\"button\" pButton icon=\"fa-plus\" (click)=\"createModuleMenu(m,menu_name)\"\r\n                                label=\"Add Menu\"></button>\r\n                    </div>\r\n                </p-header>\r\n                <p-orderList [value]=\"m.module_menu\" [responsive]=\"true\">\r\n                    <template let-menu pTemplate=\"item\">\r\n                        <div class=\"ui-helper-clearfix\">\r\n                            {{menu.menu_name}}\r\n                            <div style=\"font-size:14px;float:right;margin:15px 5px 0 0\"></div>\r\n                        </div>\r\n                    </template>\r\n                </p-orderList>\r\n            </p-panel>\r\n\r\n        </p-tabPanel>\r\n    </p-tabView>\r\n</p-panel>\r\n\r\n<div class=\"ui-grid-col-12 \">\r\n\r\n</div>\r\n\r\n"

/***/ }),

/***/ 1078:
/***/ (function(module, exports) {

module.exports = "<p-growl [value]=\"msgs\"></p-growl>\r\n<p-dataTable [value]=\"dataRows\"  [rows]=\"10\" [totalRecords]=\"totalRecords\" [paginator]=\"true\" [pageLinks]=\"3\"\r\n             (onLazyLoad)=\"loadDataLazy($event)\" [lazy]=\"true\" [responsive]=\"true\"\r\n             [rowsPerPageOptions]=\"[1,2,5,10,20,50,100,500,1000]\">\r\n    <p-header>\r\n        <div class=\"ui-helper-clearfix\" style=\"width:100%\">List of Menus  <button type=\"button\" pButton icon=\"fa-plus\" style=\"float:right\" (click)=\"onCreate()\" label=\"Add\"></button></div>\r\n    </p-header>\r\n    <p-column header=\"Action\">\r\n        <template let-row=\"rowData\" pTemplate=\"body\">\r\n            <button type=\"button\" class=\"ui-button-danger\" pButton (click)=\"onView(row)\" icon=\"fa-eye\"></button>\r\n            <button type=\"button\" pButton (click)=\"onUpdate(row)\" icon=\"fa-edit\"></button>\r\n            <button type=\"button\" class=\"ui-button-danger\" pButton (click)=\"onDelete(row)\" icon=\"fa-trash\"></button>\r\n        </template>\r\n    </p-column>\r\n    <p-column field=\"app_menu_id\" [sortable]=\"true\" header=\"#\"></p-column>\r\n    <p-column field=\"app_menu\" [sortable]=\"true\" header=\"Name\" [filter]=\"true\" ></p-column>\r\n    <p-column field=\"user_type\" [sortable]=\"true\" header=\"User Type\" [filter]=\"true\" [style]=\"{'overflow':'visible'}\">\r\n        <template pTemplate=\"filter\" let-col>\r\n            <p-multiSelect [options]=\"userTypeList\"  styleClass=\"ui-column-filter\"></p-multiSelect>\r\n        </template>\r\n    </p-column>\r\n    <p-column field=\"menu_group\" [sortable]=\"true\" header=\"Group\" [filter]=\"true\" >\r\n\r\n    </p-column>\r\n    <p-column field=\"created_by\" [sortable]=\"true\" header=\"Created By\"  [filter]=\"true\" >\r\n    </p-column>\r\n    <p-column field=\"created_time\" [sortable]=\"true\" header=\"Created Date\" [filter]=\"true\" [style]=\"{'overflow':'visible'}\">\r\n        <template pTemplate=\"filter\" let-col>\r\n            <p-calendar [(ngModel)]=\"date3\" [showIcon]=\"true\"></p-calendar>\r\n        </template>\r\n    </p-column>\r\n</p-dataTable>\r\n\r\n\r\n\r\n<p-dialog header=\"{{dialogTitle}}\" [(visible)]=\"displayDialog\" [responsive]=\"true\" showEffect=\"fade\" [modal]=\"true\" styleClass=\"userdialog\">\r\n    <div class=\"ui-grid ui-grid-responsive ui-fluid\" *ngIf=\"dataRow\">\r\n        <div class=\"ui-grid-col-12\">\r\n            <div class=\"ui-grid-col-4\"><label for=\"menu_group\">Menu Group</label></div>\r\n            <div class=\"ui-grid-col-8\"><input pInputText id=\"menu_group\" [(ngModel)]=\"dataRow.menu_group\" /></div>\r\n        </div>\r\n        <div class=\"ui-grid-row\">\r\n            <div class=\"ui-grid-col-4\"><label>User Type</label></div>\r\n            <div class=\"ui-grid-col-8\">\r\n                <p-dropdown  [options]=\"userTypeList\" name=\"user_type\" [(ngModel)]=\"dataRow.user_type\"></p-dropdown>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <p-footer>\r\n        <div class=\"ui-dialog-buttonpane ui-widget-content ui-helper-clearfix\">\r\n            <button type=\"button\" pButton icon=\"fa-close\" (click)=\"dialogClose()\" label=\"Cancel\"></button>\r\n            <button type=\"button\" pButton icon=\"fa-check\" (click)=\"onSave()\" label=\"Save\"></button>\r\n        </div>\r\n    </p-footer>\r\n</p-dialog>\r\n"

/***/ }),

/***/ 1079:
/***/ (function(module, exports) {

module.exports = "<p>\r\n  settings works!\r\n</p>\r\n"

/***/ }),

/***/ 72:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function() {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for(var i = 0; i < this.length; i++) {
			var item = this[i];
			if(item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};


/***/ }),

/***/ 940:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__settings_routing_module__ = __webpack_require__(1043);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__ = __webpack_require__(410);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__menu_management_component__ = __webpack_require__(1024);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__settings_component__ = __webpack_require__(1025);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__menu_creation_view_component__ = __webpack_require__(1023);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsModule", function() { return SettingsModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var SettingsModule = (function () {
    function SettingsModule() {
    }
    SettingsModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_4__settings_routing_module__["a" /* SettingsRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__["DataTableModule"],
                __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__["MultiSelectModule"],
                __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__["DialogModule"],
                __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__["ButtonModule"],
                __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__["CalendarModule"],
                __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__["DropdownModule"],
                __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__["GrowlModule"],
                __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__["TabViewModule"],
                __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__["OrderListModule"],
                __WEBPACK_IMPORTED_MODULE_5_primeng_primeng__["PanelModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__menu_management_component__["a" /* MenuManagementComponent */],
                __WEBPACK_IMPORTED_MODULE_8__menu_creation_view_component__["a" /* MenuCreationViewComponent */],
                __WEBPACK_IMPORTED_MODULE_7__settings_component__["a" /* SettingsComponent */]
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], SettingsModule);
    return SettingsModule;
}());
//# sourceMappingURL=settings.module.js.map

/***/ })

});
//# sourceMappingURL=3.chunk.js.map